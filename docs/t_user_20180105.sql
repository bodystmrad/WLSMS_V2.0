/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.6.22 : Database - wlsms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wlsms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `wlsms`;

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` varchar(255) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `lastUpdateTime` datetime DEFAULT NULL,
  `lastUpdateUserId` varchar(32) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `avatar` longtext,
  `bingdingFarmToolId` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `departmentId` varchar(32) DEFAULT NULL,
  `email` longtext,
  `idcard` varchar(32) DEFAULT NULL,
  `interest` longtext,
  `mobile` varchar(16) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `realName` varchar(64) DEFAULT NULL,
  `remark` longtext,
  `secretLevel` int(11) NOT NULL,
  `sex` varchar(16) DEFAULT NULL,
  `state` varchar(8) DEFAULT NULL,
  `usable` int(11) NOT NULL,
  `userCode` varchar(32) DEFAULT NULL,
  `userName` varchar(64) DEFAULT NULL,
  `userPass` varchar(64) DEFAULT NULL,
  `userType` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`createTime`,`lastUpdateTime`,`lastUpdateUserId`,`age`,`avatar`,`bingdingFarmToolId`,`birthday`,`departmentId`,`email`,`idcard`,`interest`,`mobile`,`phone`,`position`,`realName`,`remark`,`secretLevel`,`sex`,`state`,`usable`,`userCode`,`userName`,`userPass`,`userType`) values ('1','2015-05-04 22:57:21','2015-05-04 22:57:25','1',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin',NULL,0,'M','Y',1,NULL,'admin','21232f297a57a5a743894a0e4a801fc3','admin'),('297e5fde606f1c0b01606f27de890001','2017-12-19 22:22:52','2017-12-19 22:22:52','1',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test',NULL,0,'M','Y',1,NULL,'test','098f6bcd4621d373cade4e832627b4f6',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
