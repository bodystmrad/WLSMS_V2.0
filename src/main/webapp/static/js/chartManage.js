/**
 * 温度折线图相关
 */
//初始化
$(function() {
			chartObj = new Highcharts.Chart(
					{
						chart : {
							spacingTop : 0,
							renderTo : 'container',
							//plotShadow:true,
							//backgroundColor:'green'
						defaultSeriesType: 'area' //图表类别，可取值有：line、spline、area、areaspline、bar、column等
						},
						title : {
							text : '',
						},
						//subtitle: null,
						scrollbar: {
				            enabled: false   
				        },
				        
						xAxis : {
							tickmarkPlacement : 'on',//'on',
							tickInterval : 1,
							minorTickInterval : 0.1,
							gridLineWidth : 2,
							max:20,
							min:0,
							 labels:{ 
							        step:1
							    }
							//tickPixelInterval: 100,
					        //tickWidth:20
						/* labels:{
							formatter:function(){
								if((this.value*2+startHour)>24){
									return (this.value*2+startHour)-24 +':45';
								}else{
									return this.value*2+startHour+':45' ;
								}

							}
						} */
						},
						yAxis : {
							title : {
								text : ''
							},
							minorTickInterval : 1,
							tickInterval : 1,
							minorGridLineWidth : 2,
							gridLineWidth : 2,//副格线的宽度
							labels : {},
							min : 15,
							max : 45, //显示的最大值
							maxPadding : 2,
						},
						tooltip : {
							valueSuffix : '°C'
						},
						legend : {
							enabled : false
						},
						exporting : {
							enabled : false
						},
						series : [ {
							name : '体温',
							//data : [ 35, 36, 37, 35, 36, 37, 39, 38, 36, 37,
									//39, 37, 37 ]
						} ],
						credits : {
							enabled : false
						}
					});
		});

function ChangeTime(){
	$.ajax({
		type : "post",
		url : "getDegreeByMac.action",//��Ӳ�����Ϣ
		dataType : "json",
		data : {
			"strMac" : curMac4Chart,
			"type4Time":$(".select4Time").val()
		},
		success : function(data) {
			curChartData=data;
			//console.log(curChartData);
			if(data.times4chart){
				chartObj.xAxis[0].setCategories(data.times4chart[0]);
				chartObj.series[0].setData(data.degrees4chart[0],true,true,false);
				chartObj.xAxis[0].update({
                    max: data.times4chart[0].length-1,
                    //min:maxNo
                    labels:{step:data.times4chart[0].length>24?Math.ceil(data.times4chart[0].length/16):2}
                });
				//chartObj.setTitle('444'+data.dates4Chart[0]);
				var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					};
				chartObj.setTitle(title);
			}else{
				layer.alert('没有数据！');
				return;
			}
			maxDateIndex=data.dates4Chart.length;
			curDateIndex=1;
			$(".PreviousDay").css("display","none");
			if(data.dates4Chart.length>1){
				$(".NextDay").css("display","block");
			}else{
				$(".NextDay").css("display","none");
			}
				/* if(data.times4chart.length<21&&data.times4chart.length>1){
	        		//chartObj.xAxis[0].max(data.PI4chart.length);
	        		//chartObj.xAxis[0].max=data.PI4chart.length;
	        		//chartObj.xAxis[0].setExtremes(1,10);
	        		var maxNo=data.times4chart.length-1;
	        		chartObj.xAxis[0].update({
	                    max: maxNo,
	                    //min:maxNo
	                });//alert(maxNo);
	        	}else if(data.times4chart.length<=1){
	        		chartObj.xAxis[0].update({
	                    max: 0,
	                    //min:0
	                });
	        	}else{
	        		chartObj.xAxis[0].update({
	                    max: 20,
	                    //min:20
	                });
	        	} */
			var strDHS=data.dhs4chart;
			var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
			var classname4td="";
			var degreesResult;
			for(var i = 0; i < data.degrees4chart.length; i++){
				if(i==0){
					degreesResult=data.degrees4chart[i];
				}else{
					degreesResult=degreesResult.concat(data.degrees4chart[i]); 
				}
			}
			if(strDHS!=null)
			for(var i = 0; i < strDHS.length; i++){
				if(i%2==0){
					classname4td="tdOdd";
				}else{
					classname4td="tdEven";
				}
				strHtml4Table=strHtml4Table+'<tr class="'+classname4td+'"><td align="right">'+strDHS[i]+'</td><td>'+getFloatStr(degreesResult[i])+'</td></tr>';
			}
			$(".ttTable").html(strHtml4Table);
			
		}

	})
}
function PreviousDay(){
	curDateIndex--;
	if(curDateIndex==1){
		$(".PreviousDay").css("display","none");
	}
	if(curDateIndex<maxDateIndex){
		$(".NextDay").css("display","block");
	}
	chartObj.xAxis[0].update({
        max: curChartData.times4chart[curDateIndex-1].length-1,
        labels:{step:curChartData.times4chart[curDateIndex-1].length>24?Math.ceil(curChartData.times4chart[curDateIndex-1].length/16):1}
    });
	var title = {
		    text:curChartData.dates4Chart[curDateIndex-1],
		    style:{
		        color:"#333333"
		    }
		};
	chartObj.setTitle(title,null,false);
	console.log(curDateIndex);
	chartObj.xAxis[0].setCategories(curChartData.times4chart[curDateIndex-1]);
	chartObj.series[0].setData(curChartData.degrees4chart[curDateIndex-1],true,true,false);
}
function NextDay(){
	
	curDateIndex++;
	if(curDateIndex==maxDateIndex){
		$(".NextDay").css("display","none");
	}
	if(curDateIndex>1){
		$(".PreviousDay").css("display","block");
	}
	chartObj.xAxis[0].update({
        max: curChartData.times4chart[curDateIndex-1].length-1,
        labels:{step:curChartData.times4chart[curDateIndex-1].length>24?Math.ceil(curChartData.times4chart[curDateIndex-1].length/16):1}
    });
	var title = {
		    text:curChartData.dates4Chart[curDateIndex-1],
		    style:{
		        color:"#333333"
		    }
		};
	chartObj.setTitle(title,null,false);
	console.log(curDateIndex);
	chartObj.xAxis[0].setCategories(curChartData.times4chart[curDateIndex-1]);
	chartObj.series[0].setData();
	chartObj.series[0].setData(curChartData.degrees4chart[curDateIndex-1],true,true,false);//chartObj.redraw();//console.log(curChartData);return;
}
var curMac4Chart;//打开的折线图对应的设备的mac地址
var curChartData;//当前病人的折线图相关数据，从getDegreeByMac请求获得
var curDateIndex;//折线图当前的日期序号，即第几天
var maxDateIndex;//当前折线图总共的天数
//点击记录显示折线图
function showChart(ele) {
	var tempTimer;
	var curbed = $(ele).parents(".bedLi");
	currentBedName = $(curbed).attr("bedid");
	var nowaboutinfo = bedsOtherinfo[currentBedName];
	var nowaboutinfoBed = bedsBasicinfo[currentBedName];
	//console.dir(bedsBasicinfo);
	currentBedId = nowaboutinfoBed.idofbed;//将当前床位id存到全局变量中
	curMac4Chart=nowaboutinfoBed.num;
	//alert(nowaboutinfo.personid);
	//给chat赋值
	var contentInfo = "<table style='width:100%;font-size:14px;color:#666666;margin-top:14px;border-collapse:separate;border-spacing:10px;'>";
	contentInfo += "<tr><td>病人信息：" + nowaboutinfoBed.personinfo
			+ "</td><td>贴片号码：" + nowaboutinfoBed.num + "</td><td>病案号码："
			+ nowaboutinfoBed.sicknum + "</td>";
	contentInfo += "<tr><td>入院时间：" + nowaboutinfoBed.roomtime
			+ "</td><td>监控时间：" + nowaboutinfoBed.listentime
			+ "</td><td>剩余时间：约" + nowaboutinfoBed.surplustime
			+ "小时</td>";
	contentInfo += "<tr><td>监控范围：" + nowaboutinfoBed.range
			+ "℃</td><td>主治医师：" + nowaboutinfoBed.doctor
			+ "</td><td>医师诊断：" + nowaboutinfoBed.diagnosis + "</td>";
	contentInfo = contentInfo + "</table>";
	$(".bedmoreinfo .personid").html(contentInfo);
	outpatientid = $(curbed).attr("patientid");
	var str = $(curbed).attr("roomtime");
	// 转换日期格式
	str = str.replace(/-/g, '/'); // "2010/08/01";
	$("#day1").text($(curbed).attr("roomtime"));
	// 创建日期对象
	var date = new Date(str);
	// 加一天
	date.setDate(date.getDate() + 1);//获取AddDayCount天后的日期
	var y = date.getFullYear();
	var m = date.getMonth() + 1;//获取当前月份的日期
	var d = date.getDate();
	$("#day2").text(y + "-" + m + "-" + d);
	$("#day1").css("display", "none");
	$("#day2").css("display", "none");
	//*****************************************************************通过mac地址获取温度数据
	$.ajax({
		type : "post",
		url : "getDegreeByMac.action",//��Ӳ�����Ϣ
		dataType : "json",
		data : {
			"strMac" : nowaboutinfoBed.num,
			"type4Time":$(".select4Time").val()
		},
		success : function(data) {
			$('.modelbg').show();
			$(".bedmoreinfo").css({
				"left" : getClientWidth() / 2 - 470 + "px",
				"top" : getClientHeight() / 2 - 335 + "px"
			}).show();
			if(data.times4chart){
				var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					};
				chartObj.setTitle(title);
			}
			
			curChartData=data;
			if(data.times4chart&&data.times4chart.length>0){
				chartObj.xAxis[0].setCategories(data.times4chart[0]);
				chartObj.series[0].setData(data.degrees4chart[0],true,true,false);
				chartObj.xAxis[0].update({
                    max: data.times4chart[0].length-1,
                    //min:maxNo
                    labels:{step:data.times4chart[0].length>24?Math.ceil(data.times4chart[0].length/16):2}
                });
				//chartObj.setTitle('444'+data.dates4Chart[0]);
				/* var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					}; */
				//chartObj.setTitle(title);
			}else{
				chartObj.series[0].setData([],true,true,false);
				var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
				$(".ttTable").html(strHtml4Table);
			/* tempTimer = setTimeout(function() {
				chartObj.series[0].setData([],true,true,false);
				var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
				$(".ttTable").html(strHtml4Table);
				$('.modelbg').show();
				$(".bedmoreinfo").css({
					"left" : getClientWidth() / 2 - 470 + "px",
					"top" : getClientHeight() / 2 - 335 + "px"
				}).show();//.show()
				if(data.times4chart){
					var title = {
						    text:data.dates4Chart[0],
						    style:{
						        color:"#333333"
						    }
						};
					chartObj.setTitle(title);
				}
				
			}, 500) */
				return;
			}
			maxDateIndex=data.dates4Chart.length;
			curDateIndex=1;
			$(".PreviousDay").css("display","none");
			if(data.dates4Chart.length>1){
				$(".NextDay").css("display","block");
			}else{
				$(".NextDay").css("display","none");
			}
		
				/* if(data.times4chart.length<21&&data.times4chart.length>1){
	        		//chartObj.xAxis[0].max(data.PI4chart.length);
	        		//chartObj.xAxis[0].max=data.PI4chart.length;
	        		//chartObj.xAxis[0].setExtremes(1,10);
	        		var maxNo=data.times4chart.length-1;
	        		chartObj.xAxis[0].update({
	                    max: maxNo,
	                    //min:maxNo
	                });//alert(maxNo);
	        	}else if(data.times4chart.length<=1){
	        		chartObj.xAxis[0].update({
	                    max: 0,
	                    //min:1
	                });
	        	}else{
	        		chartObj.xAxis[0].update({
	                    max: 20,
	                    //min:20
	                });
	        	} */
			var strDHS=data.dhs4chart;
			var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
			var classname4td="";
			var degreesResult;
			for(var i = 0; i < data.degrees4chart.length; i++){
				if(i==0){
					degreesResult=data.degrees4chart[i];
				}else{
					degreesResult=degreesResult.concat(data.degrees4chart[i]); 
				}
			}
			if(strDHS!=null)
			for(var i = 0; i < strDHS.length; i++){
				if(i%2==0){
					classname4td="tdOdd";
				}else{
					classname4td="tdEven";
				}
				strHtml4Table=strHtml4Table+'<tr class="'+classname4td+'"><td align="right">'+strDHS[i]+'</td><td>'+getFloatStr(degreesResult[i])+'</td></tr>';
			}
			$(".ttTable").html(strHtml4Table);
			//chartObj.series[0].setData([35.1,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35  ,35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35,37,37,37,35,37,37,37,35]);
			//chartObj.xAxis[0].setCategories(["15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","15:10",""]);
			
			if (tempTimer) {
				clearTimeout(tempTimer);
			}
			var prototype = curbed;
			/* tempTimer = setTimeout(function() {
				var left = prototype.offsetLeft;
				var right = getClientWidth() - prototype.offsetLeft
						- prototype.offsetWidth;
				var top = prototype.offsetTop;
				var bottom = getClientHeight() - prototype.offsetTop;
				var infoLeft = 0;
				if (left > right) {
					infoLeft = left - $(".bedmoreinfo").width();
				} else {
					infoLeft = left + prototype.offsetWidth;
				}
				$('.modelbg').show();
				$(".bedmoreinfo").css({
					"left" : getClientWidth() / 2 - 470 + "px",
					"top" : getClientHeight() / 2 - 335 + "px"
				}).show();
				if(data.times4chart){
					var title = {
						    text:data.dates4Chart[0],
						    style:{
						        color:"#333333"
						    }
						};
					chartObj.setTitle(title);
				}
				
			}, 500) */
		}

	})
	return;
	//*****************************************************************
	//chartObj.xAxis[0].setCategories(nowaboutinfo.times);
	chartObj.xAxis[0].setCategories(nowaboutinfo.times4chart);
	//chartObj.series[0].setData(nowaboutinfo.degrees);
	chartObj.series[0].setData(nowaboutinfo.degrees4chart);
	//chartObj.series[0].setData([35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,37,37,37,37,37]);
	//chartObj.series[0].setData([35.1,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35  ,35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35,37,37,37,35,37,37,37,35]);
	startHour = nowaboutinfo.times;
	startHour = 2;
	if (startHour == null || startHour == undefined)
		startHour = 2;
	//****************************************************如果温度数据数量不够一天则显示一天的日期
	if (nowaboutinfo.degrees)
		if (nowaboutinfo.degrees.length > 0
				&& (startHour + (nowaboutinfo.degrees.length - 1) * 2) <= 24) {
			$("#day2").css("display", "none");
			$("#day1").css("width", "600px");

		} else {
			$("#day2").css("display", "inline-block");
			$("#day1").css("width", "300px");
		}
	$("#day1").css("display", "none");
	$("#day2").css("display", "none");
	//****************************************************

	if (tempTimer) {
		clearTimeout(tempTimer);
	}
	var prototype = curbed;
	tempTimer = setTimeout(function() {
		var left = prototype.offsetLeft;
		var right = getClientWidth() - prototype.offsetLeft
				- prototype.offsetWidth;
		var top = prototype.offsetTop;
		var bottom = getClientHeight() - prototype.offsetTop;
		var infoLeft = 0;
		if (left > right) {
			infoLeft = left - $(".bedmoreinfo").width();
		} else {
			infoLeft = left + prototype.offsetWidth;
		}
		$('.modelbg').show();
		$(".bedmoreinfo").css({
			"left" : getClientWidth() / 2 - 370 + "px",
			"top" : getClientHeight() / 2 - 335 + "px"
		}).show();//.show()
	}, 500)
}

function showChart4Exp(mac) {
	var tempTimer;
	curMac4Chart=mac;
	//给chat赋值
	var contentInfo = "<table style='width:100%;font-size:14px;color:#666666;margin-top:14px;border-collapse:separate;border-spacing:10px;'>";
	contentInfo += "<tr><td>病人信息：" + ''
			+ "</td><td>贴片号码：" + mac + "</td><td>病案号码："
			+ '' + "</td>";
	contentInfo += "<tr><td>入院时间：" + ''
			+ "</td><td>监控时间：" + ''
			+ "</td><td>剩余时间：" + ''
			+ "</td>";
	contentInfo += "<tr><td>监控范围：" + ''
			+ "</td><td>主治医师：" + ''
			+ "</td><td>医师诊断：" + '' + "</td>";
	contentInfo = contentInfo + "</table>";
	$(".bedmoreinfo .personid").html(contentInfo);

	$("#day1").css("display", "none");
	$("#day2").css("display", "none");
	//*****************************************************************通过mac地址获取温度数据
	$.ajax({
		type : "post",
		url : "getDegreeByMac.action",//��Ӳ�����Ϣ
		dataType : "json",
		data : {
			"strMac" : mac,
			"type4Time":$(".select4Time").val()
		},
		success : function(data) {
			$('.modelbg').show();
			$(".bedmoreinfo").css({
				"left" : getClientWidth() / 2 - 470 + "px",
				"top" : getClientHeight() / 2 - 335 + "px"
			}).show();
			if(data.times4chart){
				var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					};
				chartObj.setTitle(title);
			}
			
			curChartData=data;
			if(data.times4chart&&data.times4chart.length>0){
				chartObj.xAxis[0].setCategories(data.times4chart[0]);
				chartObj.series[0].setData(data.degrees4chart[0],true,true,false);
				chartObj.xAxis[0].update({
                    max: data.times4chart[0].length-1,
                    //min:maxNo
                    labels:{step:data.times4chart[0].length>24?Math.ceil(data.times4chart[0].length/16):2}
                });
				//chartObj.setTitle('444'+data.dates4Chart[0]);
				/* var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					}; */
				//chartObj.setTitle(title);
			}else{
				chartObj.series[0].setData([],true,true,false);
				var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
				$(".ttTable").html(strHtml4Table);
				return;
			}
			maxDateIndex=data.dates4Chart.length;
			curDateIndex=1;
			$(".PreviousDay").css("display","none");
			if(data.dates4Chart.length>1){
				$(".NextDay").css("display","block");
			}else{
				$(".NextDay").css("display","none");
			}
		
			var strDHS=data.dhs4chart;
			var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
			var classname4td="";
			var degreesResult;
			for(var i = 0; i < data.degrees4chart.length; i++){
				if(i==0){
					degreesResult=data.degrees4chart[i];
				}else{
					degreesResult=degreesResult.concat(data.degrees4chart[i]); 
				}
			}
			if(strDHS!=null)
			for(var i = 0; i < strDHS.length; i++){
				if(i%2==0){
					classname4td="tdOdd";
				}else{
					classname4td="tdEven";
				}
				strHtml4Table=strHtml4Table+'<tr class="'+classname4td+'"><td align="right">'+strDHS[i]+'</td><td>'+getFloatStr(degreesResult[i])+'</td></tr>';
			}
			$(".ttTable").html(strHtml4Table);
			//chartObj.series[0].setData([35.1,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35  ,35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35,37,37,37,35,37,37,37,35]);
			//chartObj.xAxis[0].setCategories(["15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","15:10",""]);
			
			if (tempTimer) {
				clearTimeout(tempTimer);
			}
			var prototype = curbed;

		}

	})
}

//点击病人名称显示折线图界面
function showDegree(ele){
	var tempTimer;
	//var curbed = $(ele).parents(".bedLi");
	currentBedName = $(ele).attr("patientid");
	var nowaboutinfo = bedsOtherinfo[$(ele).attr("bedid")];
	var nowaboutinfoBed = bedsBasicinfo[currentBedName];
	console.dir(nowaboutinfoBed);
	currentBedId = nowaboutinfoBed.idofbed;//将当前床位id存到全局变量中
	curMac4Chart=nowaboutinfoBed.num;
	//alert(nowaboutinfo.personid);
	//给chat赋值
	var contentInfo = "<table style='width:100%;font-size:14px;color:#666666;margin-top:14px;border-collapse:separate;border-spacing:10px;'>";
	contentInfo += "<tr><td>病人信息：" + nowaboutinfoBed.personinfo
			+ "</td><td>贴片号码：" + nowaboutinfoBed.num + "</td><td>病案号码："
			+ nowaboutinfoBed.sicknum + "</td>";
	contentInfo += "<tr><td>入院时间：" + nowaboutinfoBed.roomtime
			+ "</td><td>监控时间：" + nowaboutinfoBed.listentime
			+ "</td><td>剩余时间：约" + nowaboutinfoBed.surplustime
			+ "小时</td>";
	contentInfo += "<tr><td>监控范围：" + nowaboutinfoBed.range
			+ "℃</td><td>主治医师：" + nowaboutinfoBed.doctor
			+ "</td><td>医师诊断：" + nowaboutinfoBed.diagnosis + "</td>";
	contentInfo = contentInfo + "</table>";
	$(".bedmoreinfo .personid").html(contentInfo);
	outpatientid = bedsBasicinfo[currentBedName].patientid;//$(curbed).attr("patientid");
	var str = bedsBasicinfo[currentBedName].roomtime;//$(curbed).attr("roomtime");
	// 转换日期格式
	str = str.replace(/-/g, '/'); // "2010/08/01";
	//$("#day1").text($(curbed).attr("roomtime"));
	// 创建日期对象
	var date = new Date(str);
	// 加一天
	date.setDate(date.getDate() + 1);//获取AddDayCount天后的日期
	var y = date.getFullYear();
	var m = date.getMonth() + 1;//获取当前月份的日期
	var d = date.getDate();
	$("#day2").text(y + "-" + m + "-" + d);
	//*****************************************************************通过mac地址获取温度数据
	$.ajax({
		type : "post",
		url : "getDegreeByMac.action",//��Ӳ�����Ϣ
		dataType : "json",
		data : {
			"strMac" : nowaboutinfoBed.num,
			"type4Time":$(".select4Time").val()
		},
		success : function(data) {
			$('.modelbg').show();
			$(".bedmoreinfo").css({
				"left" : getClientWidth() / 2 - 470 + "px",
				"top" : getClientHeight() / 2 - 335 + "px"
			}).show();//.show()
			if(data.times4chart){
				var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					};
				chartObj.setTitle(title);
			}
			curChartData=data;
			//data.degrees4chart=[35.1,36,37,35,36,35.1,36,37,35,36,35.1,36,37,35,36,35.1,36,37,35,36,35.1,36,37,35,36,35.1,36,37,35,36,35.1,36,37,35,36,35.1,36,37,35,36];
			//data.dhs4chart=["06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10","06-01 02","06-01 04","06-01 06","06-01 08","06-01 10"];
			//data.times4chart=["02","04","06","08","10","02","04","06","08","10","02","04","06","08","10","02","04","06","08","10","02","04","06","08","10","02","04","06","08","10","02","04","06","08","10","02","04","06","08","10"];
			if(data.times4chart&&data.times4chart.length>0){
				chartObj.xAxis[0].setCategories(data.times4chart[0]);
				chartObj.series[0].setData(data.degrees4chart[0],true,true,false);
				chartObj.xAxis[0].update({
                    max: data.times4chart[0].length-1,
                    //min:maxNo
                    labels:{step:data.times4chart[0].length>24?Math.ceil(data.times4chart[0].length/16):2}
                });
				//chartObj.setTitle('444'+data.dates4Chart[0]);
				/* var title = {
					    text:data.dates4Chart[0],
					    style:{
					        color:"#333333"
					    }
					};
				chartObj.setTitle(title); */
			}else{
				chartObj.series[0].setData([],true,true,false);
				var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
				$(".ttTable").html(strHtml4Table);
				/* tempTimer = setTimeout(function() {
					chartObj.series[0].setData([],true,true,false);
					var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
					$(".ttTable").html(strHtml4Table);
					$('.modelbg').show();
					$(".bedmoreinfo").css({
						"left" : getClientWidth() / 2 - 470 + "px",
						"top" : getClientHeight() / 2 - 335 + "px"
					}).show();//.show()
					if(data.times4chart){
						var title = {
							    text:data.dates4Chart[0],
							    style:{
							        color:"#333333"
							    }
							};
						chartObj.setTitle(title);
					}
					
				}, 500) */
				return;
			}
			maxDateIndex=data.dates4Chart.length;
			curDateIndex=1;
			$(".PreviousDay").css("display","none");
			if(data.dates4Chart.length>1){
				$(".NextDay").css("display","block");
			}else{
				$(".NextDay").css("display","none");
			}
				/* if(data.times4chart.length<21&&data.times4chart.length>1){
	        		//chartObj.xAxis[0].max(data.PI4chart.length);
	        		//chartObj.xAxis[0].max=data.PI4chart.length;
	        		//chartObj.xAxis[0].setExtremes(1,10);
	        		chartObj.xAxis[0].update({
	                    max: data.times4chart.length-1
	                });
	        	}else if(data.times4chart.length<=1){
	        		chartObj.xAxis[0].update({
	                    max: 1
	                });
	        	}else{
	        		chartObj.xAxis[0].update({
	                    max: 20
	                });
	        	} */
			var strDHS=data.dhs4chart;
			var strHtml4Table='<thead style="position:absolute;top:58px"><td style="border:0px;font-size:16px;width:99px">时间</td><td style="border:0px;font-size:16px;width:101px">温度</td></thead>';
			var classname4td="";
			var degreesResult;
			for(var i = 0; i < data.degrees4chart.length; i++){
				if(i==0){
					degreesResult=data.degrees4chart[i];
				}else{
					degreesResult=degreesResult.concat(data.degrees4chart[i]); 
				}
			}
			if(strDHS!=null)
			for(var i = 0; i < strDHS.length; i++){
				if(i%2==0){
					classname4td="tdOdd";
				}else{
					classname4td="tdEven";
				}
				strHtml4Table=strHtml4Table+'<tr class="'+classname4td+'"><td align="right">'+strDHS[i]+'</td><td>'+getFloatStr(degreesResult[i])+'</td></tr>';
			}
			$(".ttTable").html(strHtml4Table);
			//chartObj.series[0].setData([35.1,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35  ,35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,35,37,37,37,35,37,37,37,35]);
			//chartObj.xAxis[0].setCategories(["15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","","","15:10","","15:10",""]);
			$("#day1").css("display", "none");
			$("#day2").css("display", "none");
			if (tempTimer) {
				clearTimeout(tempTimer);
			}
			/* tempTimer = setTimeout(function() {
				$('.modelbg').show();
				$(".bedmoreinfo").css({
					"left" : getClientWidth() / 2 - 470 + "px",
					"top" : getClientHeight() / 2 - 335 + "px"
				}).show();//.show()
				if(data.times4chart){
					var title = {
						    text:data.dates4Chart[0],
						    style:{
						        color:"#333333"
						    }
						};
					chartObj.setTitle(title);
				}
			}, 500) */
		}

	})
}