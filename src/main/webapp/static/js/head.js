var  $c=function(array){var nArray = [];for (var i=0;i<array.length;i++) nArray.push(array[i]);return nArray;};
Array.prototype.each=function(func){
for(var i=0,l=this.length;i<l;i++) {func(this[i],i);};
};
document .getElementsByClassName=function(cn){
var hasClass=function(w,Name){
var hasClass = false;
w.className.split(' ').each(function(s){
if (s == Name) hasClass = true;
});
return hasClass;
}; 
var elems =document.getElementsByTagName("*")||document.all;
            var elemList = [];
           $c(elems).each(function(e){
if(hasClass(e,cn)){elemList.push(e);}
		   })
        return $c(elemList);
};		
function change_bg(obj){
var a=document.getElementsByClassName("nav")[0].getElementsByTagName("a");
for(var i=0;i<a.length;i++){a[i].className="";}
obj.className="nav_current";
}
// 页面上的全选函数
function selectAll(node) {
	var status = node.checked;
	if (status) {
		$("input[name='id']").each(function() {
			this.checked = true;
		});
	} else {
		$("input[name='id']").each(function() {
			this.checked = false;
		});
	}
}
/**
 * 检测已选中的记录数
 */
function checkNum() {
	var resultArr = new Array();
	var countNum=0;
	$("input[name='id']").each(function() {
		if(this.checked){
			countNum++;
			resultArr[countNum]=this.value;
		};
	});
	resultArr[0]=countNum;
}
function editRecord(itemName, newRecord, queryParam) {
	var params = {};
	var urlInfo = itemName + "/edit.action";
	if (!newRecord) {
		// 如果不是添加的话，则为修改的操作
		var rows = $("#" + itemName + "DataGrid").datagrid("getSelections");
		if (rows == null || rows == "") {
			$.messager.alert('提示', '请选择要编辑的行');
			return;
		} else if (rows.length > 1) {
			$.messager.alert('提示', '不可以同时编辑多行');
			return;
		} else {
			params["id"] = rows[0].id;
		}
	}
	var dialogName = "#" + itemName + "Dialog";
	if (queryParam != null) {
		for ( var key in queryParam) {
			params[key] = queryParam[key];
		}
	}
	$.ajax({
		url : urlInfo,
		type : "POST",
		data : params,
		success : function(data, textStatus, jqXHR) {
			$(dialogName).html(data);
			$.parser.parse(dialogName);
			$(dialogName).window("open");
		}
	});
}
function deleteRecord(itemName, callback) {
	// 遍历ID，寻找要获取到的记录ID
	var resultArr=checkNum();
	// 如果不是添加的话，则为修改的操作
	if (resultArr[0]==0) {
		$.messager.alert('提示', '请选择要编辑的行');
		return;
	}
	$.messager.confirm('警告', '确定删除选定记录?', function(r) {
		if (r) {
			var itemId = "";
			for (var i=1;i<resultArr.length;i++) {
				itemId += resultArr[i]+ ",";
			}
			itemId = itemId.substr(0, itemId.length - 1);
			var urlInfo = itemName + "/delete.action";
			// 提交删除的请求
			$.ajax({
				"type" : "post",
				"url" : urlInfo,
				"data" : {
					"id" : itemId
				},
				"success" : function(data, textStatus, jqXHR) {
					// $("#"+divid).html(data);
					if (data == 'ERROR') {
						$.messager.alert('提示', '删除失败!', 'error');
					} else {
						if (callback) {
							callback();
						} else {
							$("#" + itemName + "DataGrid").datagrid("reload");
						}
						$("#" + itemName + "DataGrid").datagrid(
								"clearSelections");
					}
				},
				"error" : function(XMLHttpRequest, status, error) {
					$.messager.alert('错误', '保存数据出错');
				}
			});
		}
	});
};
function saveRecord(itemName, itemId) {
	var formObj = $("#" + itemName + "Form");
	var isValid = formObj.valid();
	if (!isValid) {
		return;
	}
	var urlInfo = formObj.attr("action");
	if (!urlInfo) {
		urlInfo = "save.action";
	}
	var dataStr = formObj.serialize();
	if (itemId != null && itemId != "") {
		dataStr += "&id=" + itemId;
		urlInfo = formObj.attr("updateAction");
		if (!urlInfo) {
			urlInfo = "update.action";
		}
	}
	formObj.attr("action",urlInfo);
	formObj.submit();// alert('dataStr:'+dataStr);
}
function showRecord(itemName) {
	var params = {};
	var urlInfo = itemName + "/show.action";
	// 如果不是添加的话，则为修改的操作
	var rows = $("#" + itemName + "DataGrid").datagrid("getChecked");
	if (rows == null || rows == "") {
		$.messager.alert('提示', '请选择要查看的行');
		return;
	} else if (rows.length > 1) {
		$.messager.alert('提示', '不可以同时查看多行');
		return;
	} else {
		params["id"] = rows[0].id;
	}
	var dialogName = "#" + itemName + "Dialog";
	$.ajax({
		url : urlInfo,
		type : "POST",
		data : params,
		success : function(data, textStatus, jqXHR) {
			$(dialogName).html(data);
			$.parser.parse(dialogName);
			$(dialogName).window("open");
		}
	});
}