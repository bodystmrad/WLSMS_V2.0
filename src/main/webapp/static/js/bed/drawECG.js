var sentTargetSampleRateOrNot4ECG=false;
onmessage = function(event){
    var ecgChartJsonObj = event.data;
    ecgChartJsonObj.timer=setTimeout(function(){
		drawEcg(ecgChartJsonObj);
	},ecgChartJsonObj.intervalSeconds);
    // 向线程创建源送回消息
//    postMessage(result);
}

function drawEcg(ecgChartJsonObj){
	var chart=ecgChartJsonObj.chart;
	var dataTemp=ecgChartJsonObj.dataTemp;
	var ecgData=ecgChartJsonObj.data;
	var targetSampleRate=ecgChartJsonObj.targetSampleRate;
	var pointNumPer200ms=ecgChartJsonObj.pointNumPer200ms;
	var width_px=ecgChartJsonObj.width_px;
	//将计算的目标采样率发送到服务器
	if(!sentTargetSampleRateOrNot4ECG){
		var result=sendMsgByWs(ecgChartJsonObj.ws,"/TargetSampleRate/"+Bodystm.DataType.ECG+"/"+targetSampleRate);
		if(result)
			sentTargetSampleRateOrNot4ECG=true;
	}
	var curIndex=ecgChartJsonObj.curIndex;
	if(ecgChartJsonObj.ifBuffered==false){
		if(ecgChartJsonObj.dataTemp.length>=ecgChartJsonObj.bufferSize){//缓存2s数据
			ecgChartJsonObj.ifBuffered=true;
		}
		ecgChartJsonObj.timer=setTimeout(function(){
			drawEcg(ecgChartJsonObj);
		},ecgChartJsonObj.intervalSeconds);
		return;
	}
	//调整绘图间隔
	var intervalTemp=ecgChartJsonObj.intervalSeconds;
	ecgChartJsonObj.counter4BufferManage++;
	if(ecgChartJsonObj.counter4BufferManage==10){
		ecgChartJsonObj.counter4BufferManage=0;
		if(ecgChartJsonObj.dataTemp.length>=ecgChartJsonObj.bufferSize-ecgChartJsonObj.buffer_config_bufferNum
				&&ecgChartJsonObj.dataTemp.length<=ecgChartJsonObj.bufferSize+ecgChartJsonObj.buffer_config_bufferNum){
			intervalTemp=ecgChartJsonObj.defaultIntervalSeconds;
			ecgChartJsonObj.intervalSeconds=ecgChartJsonObj.defaultIntervalSeconds;
		}else if(ecgChartJsonObj.dataTemp.length-ecgChartJsonObj.buffer_config_bufferNum>ecgChartJsonObj.bufferSize){//缓存2s数据
			//ecgChartJsonObj.intervalSeconds-=1;
//			intervalTemp=ecgChartJsonObj.intervalSeconds-2;
			if(ecgChartJsonObj.dataTemp.length<1.5*ecgChartJsonObj.bufferSize){//如果小于150%，则缩小间隔
				if(ecgChartJsonObj.intervalSeconds>=ecgChartJsonObj.defaultIntervalSeconds-5&&
						ecgChartJsonObj.dataTemp.length>ecgChartJsonObj.dataTempLenth4Last){
					ecgChartJsonObj.intervalSeconds-=1;
				}
				intervalTemp=ecgChartJsonObj.intervalSeconds;
			}else{
				//直接整段丢数据，不调整间隔
				ecgChartJsonObj.ifTooMuchData=true;
			}
		}else if(ecgChartJsonObj.dataTemp.length+ecgChartJsonObj.buffer_config_bufferNum<ecgChartJsonObj.bufferSize){
			//ecgChartJsonObj.intervalSeconds+=1;
//			intervalTemp=ecgChartJsonObj.intervalSeconds+15;
			if(ecgChartJsonObj.dataTemp.length>0.5*ecgChartJsonObj.bufferSize){//如果小于150%，则缩小间隔
				if(ecgChartJsonObj.intervalSeconds<=ecgChartJsonObj.defaultIntervalSeconds+5&&
						ecgChartJsonObj.dataTemp.length<ecgChartJsonObj.dataTempLenth4Last){
					ecgChartJsonObj.intervalSeconds+=1;
				}
				intervalTemp=ecgChartJsonObj.intervalSeconds;
			}else{
				//<50%,间隔恢复默认，不调整间隔
				ecgChartJsonObj.intervalSeconds=ecgChartJsonObj.defaultIntervalSeconds;
				intervalTemp=ecgChartJsonObj.defaultIntervalSeconds;
			}
		}
		ecgChartJsonObj.dataTempLenth4Last=ecgChartJsonObj.dataTemp.length;
	}
//	ecgChartJsonObj.intervalSeconds=intervalTemp;
	ecgChartJsonObj.timer=setTimeout(function(){
		drawEcg(ecgChartJsonObj);
	},intervalTemp);
	var tmp;
	//断开集中器再次重连后需要再次缓存5s的数据
	if(ecgChartJsonObj.dataTemp.length==0){
		ecgChartJsonObj.ifBuffered=false;
	}
	ecgChartJsonObj.start=ecgChartJsonObj.start==0?1:0;
	for (var i = ecgChartJsonObj.start; i < pointNumPer200ms; i+=2) {//1-2
		tmp=dataTemp.shift();
		tmp=dataTemp.shift();//1-2
		tmp=tmp==undefined?0:tmp;
		if(tmp>3){
			tmp=3;
		}else if(tmp<-1){
			tmp=-1;
		}
//		ecgData[i]=[i,tmp];
		if(curIndex>=width_px){
			curIndex=0;
			//丢弃整段数据
			if(ecgChartJsonObj.ifTooMuchData){
				ecgChartJsonObj.ifTooMuchData=false;
				while(dataTemp.length>=ecgChartJsonObj.bufferSize){
					dataTemp.splice(0,width_px);//清空数组 
				}
				
			}
		}
//		ecgData[curIndex]=[curIndex,tmp];
		ecgData[curIndex/2]=[curIndex,tmp];//1-2
		curIndex+=2;//1-2
    }
	/*for (var i = 0; i < pointNumPer200ms; i++) {
		tmp=dataTemp.shift();
		tmp=tmp==undefined?0:tmp;
		if(tmp>3){
			tmp=3;
		}else if(tmp<-1){
			tmp=-1;
		}
		if(curIndex>=width_px){
			curIndex=0;
			//丢弃整段数据
			if(ecgChartJsonObj.ifTooMuchData){
				ecgChartJsonObj.ifTooMuchData=false;
				while(dataTemp.length>=ecgChartJsonObj.bufferSize){
					dataTemp.splice(0,width_px);//清空数组 
				}
				
			}
		}
		ecgData[curIndex]=[curIndex,tmp];
		curIndex++;
    }*/
	//为了防止前端后端数据速度不一致，每次读取缓存里所有
	/*if(dataTemp.length==0){
		for (var i = curIndex; i < curIndex+pointNumPer200ms; i++) {
			ecgData[i]=[i,0];
	    }
		curIndex+=pointNumPer200ms;
		if(curIndex>=width_px){
			curIndex=0;
		}
	}else{
		while(true){
			tmp=dataTemp.shift();
			if(tmp==undefined){
				break;
			}
			ecgData[curIndex]=[curIndex,tmp];
			curIndex++;
			if(curIndex>=width_px){
				curIndex=0;
			}
		}
		
	}*/
	/*curIndex+=pointNumPer200ms;
	ecgData[curIndex]=undefined;
	ecgData[curIndex+1]=undefined;
	if(curIndex>=width_px){
		curIndex=0;
	}*/
	for(var i=0;i<20;i+=2){//1-2
		if(curIndex+i>=width_px){
			ecgData[(curIndex+i-width_px)/2]=undefined;//1-2
		}else{
			ecgData[(curIndex+i)/2]=undefined;//1-2
		}
	}
	/*for(var i=0;i<10;i++){
		if(curIndex+i>=width_px){
			ecgData[curIndex+i-width_px]=undefined;
		}else{
			ecgData[curIndex+i]=undefined;
		}
	}*/
	/*chart.setOption({
        series: [{
            data: ecgData
        }]
    });*/
	var option=chart.getOption();
	option.series[0].data=ecgData;
	chart.setOption(option);
	ecgChartJsonObj.curIndex=curIndex;
	option=null;
}