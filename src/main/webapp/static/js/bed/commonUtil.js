/**
 * 公用方法
 */
var contextPath=getRootPath_web() ;
function getRootPath_web() {
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
} 
Date.prototype.format = function(format)
{
 var o = {
 "M+" : this.getMonth()+1, //month
 "d+" : this.getDate(),    //day
 "h+" : this.getHours(),   //hour
 "m+" : this.getMinutes(), //minute
 "s+" : this.getSeconds(), //second
 "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
 "S" : this.getMilliseconds() //millisecond
 }
 if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
 (this.getFullYear()+"").substr(4 - RegExp.$1.length));
 for(var k in o)if(new RegExp("("+ k +")").test(format))
 format = format.replace(RegExp.$1,
 RegExp.$1.length==1 ? o[k] :
 ("00"+ o[k]).substr((""+ o[k]).length));
 return format;
}
function addDate(date, days) {
    if (days == undefined || days == '') {
        days = 1;
    }
    var date = date;//new Date(date);
    date.setDate(date.getDate() + days);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    return date.getFullYear() + '-' + getFormatDate(month) + '-' + getFormatDate(day);
}
//日期月份/天的显示，如果是1位数，则在前面加上'0'
function getFormatDate(arg) {
    if (arg == undefined || arg == '') {
        return '';
    }

    var re = arg + '';
    if (re.length < 2) {
        re = '0' + re;
    }

    return re;
}
function ifEndDateBeforeStartDate(startDate,endDate){
	msStart=Date.parse(startDate.replace(/-/g,"/"));
	msEnd=Date.parse(endDate.replace(/-/g,"/"));
	var now=new Date().getTime();
	if(msEnd<msStart){
		layer.msg("结束时间不得早于开始时间！",{time:2000});
	}
	if(msStart>now||msEnd>now){
		layer.msg("选择的时间不得晚于当前时间！",{time:2000});
		return true;
	}
	return msEnd<msStart;
}
function GetUrlParms()    
{
    var args=new Object();   

    var query=location.search.substring(1);//获取查询串   

    var pairs=query.split("&");//在逗号处断开   

    for(var   i=0;i<pairs.length;i++)   

    {   

        var pos=pairs[i].indexOf('=');//查找name=value   

            if(pos==-1)   continue;//如果没有找到就跳过   

            var argname=pairs[i].substring(0,pos);//提取name   

            var value=pairs[i].substring(pos+1);//提取value   

            args[argname]=unescape(value);//存为属性   

    }

    return args;

}