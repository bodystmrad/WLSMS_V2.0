function addBed(){
	layer.msg('添加床位<br>床位名称：<input style="margin-left:10px;float:right;color:black" id="bedName4Add">'+
			'<br>集中器MAC：<input style="margin-top:10px;color:black" id="mac4Add">', {
        time: 60000, //20s后自动关闭
        btn: ['确定', '取消'],
        btn1:function(index, layero){
      	  if($('#roomName').val()!=""){
      		if($('#mac4Add').val().length!=12){
      			layer.msg('mac地址长度不对（必须为12位）！', {icon: 1,time: 2000});
        		return;
      		}
      		$.ajax({
        		url:"add.action",
        		type:"post",
        		dataType:"json",
        		data:{
        			bedid:$('#bedName4Add').val(),
        			concentratorNo:$('#mac4Add').val()
        		},
        		success:function(data){
        			if(data.success){
        				layer.msg('添加成功！', {icon: 1,time: 2000});
            			location.href="addBed.action";
        			}else{
        				layer.msg('添加失败！'+data.msg, {icon: 1,time: 2000});
        			}
        			
        		}
        	});
      	  }else{
      		  //layer.close(index);
      	  }
      	  
	   }
	});
}

function editBed(ele){
	var id=$(ele).attr("bedId");
	var name=$(ele).closest('li').find(".spanBedName").text(); 
	var mac=$(ele).closest('li').find(".spanMac").text(); 
	layer.msg('修改床位<br>床位名称：<input style="margin-left:10px;float:right;color:black" id="bedName4Add" value="'+name+'">'+
			'<br>集中器MAC：<input style="margin-top:10px;color:black" id="mac4Add" value="'+mac+'">', {
        time: 60000, //20s后自动关闭
        btn: ['确定', '取消'],
        btn1:function(index, layero){
        	if($('#mac4Add').val().length!=12){
        		layer.msg('mac地址长度不对！', {icon: 1,time: 2000});
        		return;
        	}
        	$.ajax({
        		url:"editBed.action",
        		type:"post",
        		dataType:"json",
        		data:{
        			id:id,
        			name:$('#bedName4Add').val(),
        			mac:$('#mac4Add').val()
        		},
        		success:function(data){
        			if(data.success){
        				layer.msg('修改成功！', {icon: 1,time: 1000});
            			location.href="addBed.action";
            			layer.close(index);
        			}else{
        				layer.msg(data.msg, {icon: 1,time: 2000});
        			}
        		}
        	});
      	  
	   }
	});
}

//删除病床
function delBed(ele){
	//询问框
	layer.confirm('您确定要删除该床位？', {
	  btn: ['确定','取消'] //按钮
	}, function(){
		var bedid=$(ele).attr("bedId");
		$.ajax({
			type:"post",
			url:"deleteByName.action",
			dataType:"json",
			data:{bedid:bedid},
			success:function(data){
				layer.msg('删除成功！', {icon: 1,time: 2000});
				location.href="addBed.action";
			}
		})
	}, function(){
	  /*layer.msg('也可以这样', {
	    time: 20000, //20s后自动关闭
	    btn: ['明白了', '知道了']
	  });*/
	});
}