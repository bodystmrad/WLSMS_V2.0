$(function () {
	var form = layui.form
	,layer = layui.layer
	,layedit = layui.layedit
	,laydate =layui.laydate;
	var patientId,bedId,mac;
	initParas();
	initPage();
	initPatientInfo();
	//菜单点击事件
	 $("ul.menu-list-box-ul>li").click(function(){
//	    $("ul.menu-list-box-ul>li").css("background-color","inherit");
    	$("ul.menu-list-box-ul>li").css("color","#fff");
    	$(this).css({
//	    		"background-color": "rgba(0,51,255,0.8)",
    		"color":"rgba(0,51,255,0.8)"
    	})
    	$(".page-content").children("div").css("display","none");
    	var index=parseInt($(this).attr("index"));
    	switch(index){
	    	case 0://病人信息
	    		initPatientInfo();
	    		break;
	    	case 1://监护详情
	    		initCustodyDetail();
	    		break;
	    	case 2://数据回顾
	    		initViewHistoryData();
	    		break;
	    	case 3://报警设置
	    		initAlarmSetting();
	    		break;
	    	case 4://报警记录
	    		initAlarmRecord();
	    		break;
	    	case 5://设备信息
	    		initDeviceInfo();
	    		break;
	    	case 6://其它
	    		initOther();
	    		break;
    	}
    })
    //从url中获取所点击的床位以及病人相关信息
    function initParas(){
		 var curWwwPath = window.document.location.href;
		 var paras=GetUrlParms();
		 patientId=paras['patientId'];
		 bedId=paras['bedId'];
		 mac=paras['mac'];
	}
	 //初始化页面控件
	 function initPage(){
		//日期
	  laydate.render({
	    elem: '#startTime',
	    value:(new Date()).format("yyyy-MM-dd 00:00:00"),
	    type:"datetime",
	  });
	  laydate.render({
	    elem: '#endTime',
	    value:new Date(),
	    type:"datetime",
	  });
	  $("#saveAlarmSetting").click(function(){
//		  var 
	  });
	 }
    //初始化病人信息表单
	function initPatientInfo(){
		 $("#patientInfoDiv").show();
		 var curForm=$("#patientInfoDiv");// form
			$(curForm).find("input[name=id]").val(patientId);
			$(curForm).find("input[name=mac]").val(mac);
		 $.ajax({
				url:contextPath+"/patient/getPatientById.action",
				data:{
					id:patientId,
				},
				type:"post",
				dataType:"json",
				success:function(data){
					var curForm=$("#patientInfoDiv");// form
					/*$(curForm).find("input[name=id]").val(data.id);
					$(curForm).find("input[name=mac]").val(mac);*/
					$(curForm).find("input[name=name]").val(data.name);
					$(curForm).find("input[name=age]").val(data.age);
					$(curForm).find("input:radio[name=sex][value='"+(data.sex=='man'?0:1)+"']").attr("checked","true");
//					$(curForm).find("input[name=sex][value=1]").attr("checked","true");	
//					$("#wrwer").attr("checked","");	
					$(curForm).find("input[name=idnumber]").val(data.idnumber=="null"?"":data.idnumber);
					$(curForm).find("input[name=admissionNumber]").val(data.admissionNumber);
					$(curForm).find("select[name=patientType]").val(data.patientType=="null"?"1":"2");
					$(curForm).find("input[type=checkbox]").prop("checked",data.pacing=="on");
					form.render();
				}
		 })
	} 
	//监听提交
	//确认修改病人信息按钮点击事件
	  form.on('submit(savePatient)', function(data){
	    /*layer.alert(JSON.stringify(data.field), {
	      title: '最终的提交信息'
	    })*/
	    $.ajax({
				url:contextPath+"/patient/update.action",
				data:data.field,
				type:"post",
				dataType:"json",
				success:function(data){
					layer.msg("保存成功",{time:2000})
				}
		 })
	    return false;
	  });
	
	//监护详情
	function initCustodyDetail(){
		$("#custodyDetailDiv").show();
//		$("#byTable table").width(570);
	}
	$("#btnQueryHisData").click(function(){
		initViewHistoryData();
	});
	//数据回顾
	function initViewHistoryData(){
		$("#viewHistoryDataDiv").show();
		var startTime=$("#startTime").val();
		var endTime=$("#endTime").val();
		var resolution=$("#viewHistoryDataDiv").find("select[name=resolution] option:selected").val();
		$("#byTable tbody").empty();
		var ifByTable=$("#viewHistoryDataDiv").find("select[name=dataShowType] option:selected").val()=="0";
		$.ajax({
			url:contextPath+"/HistoricalData/queryData.action",
			data:{
				Patient_Id:patientId,
				startTime:startTime,
				endTime:endTime,
				Resolution:resolution
			},
			type:"post",
			dataType:"json",
			success:function(data){
				if(ifByTable){
					for(var i=0;i<data.Hist.length;i++){
						if(data.Hist[i]!=null){
							var curHisData=data.Hist[i];
							var tr='<tr>'
						        +'<td>'+curHisData.createTime+'</td>'
						        +'<td>'+curHisData.hr+'</td>'
						        +'<td>'+curHisData.rr+'</td>'
						        +'<td>'+curHisData.spO2+'</td>'
						        +'<td>'+curHisData.temperature1+'</td>'
						        +'<td>'+curHisData.sbp+'</td>'
						        +'<td>'+curHisData.dbp+'</td>'
						        +'<td>'+curHisData.BP4PR+'</td>'
						      +'</tr>';
							$("#byTable tbody").append(tr);
						}
					}
					$("#byChart").hide();
					$("#byTable").show();
				}else{//以图表形式展示
					var data4HR=[],label4HR=[];
					var data4SpO2=[];
					var data4RR=[];
					var data4NIBP=[];
					var data4Temp=[];
					for(var i=0;i<data.Hist.length;i++){
						if(data.Hist[i]!=null){
							var curHisData=data.Hist[i];
							data4HR.push(curHisData.hr);
							label4HR.push(curHisData.createTime);
							data4SpO2.push(curHisData.spO2);
							data4RR.push(curHisData.rr);
							data4NIBP.push(curHisData.sbp);
							data4Temp.push(curHisData.temperature1);
						}
					}
					//HR
					if(echartsManage.lineChart4HR==null){
						echartsManage.lineChart4HR=echarts.init(document.getElementById('chartDiv4HR'));
					}
					echartsManage.lineOption4HR.xAxis.data=label4HR;
					echartsManage.lineOption4HR.series[0].data=data4HR;
					echartsManage.lineChart4HR.setOption(echartsManage.lineOption4HR);
					//SpO2
					if(echartsManage.lineChart4SpO2==null){
						echartsManage.lineChart4SpO2=echarts.init(document.getElementById('chartDiv4SpO2'));
					}
					echartsManage.lineOption4SpO2.xAxis.data=label4HR;
					echartsManage.lineOption4SpO2.series[0].data=data4SpO2;
					echartsManage.lineChart4SpO2.setOption(echartsManage.lineOption4SpO2);
					//RR
					if(echartsManage.lineChart4RR==null){
						echartsManage.lineChart4RR=echarts.init(document.getElementById('chartDiv4RR'));
					}
					echartsManage.lineOption4RR.xAxis.data=label4HR;
					echartsManage.lineOption4RR.series[0].data=data4RR;
					echartsManage.lineChart4RR.setOption(echartsManage.lineOption4RR);
					//NIBP
					if(echartsManage.lineChart4NIBP==null){
						echartsManage.lineChart4NIBP=echarts.init(document.getElementById('chartDiv4NIBP'));
					}
					echartsManage.lineOption4NIBP.xAxis.data=label4HR;
					echartsManage.lineOption4NIBP.series[0].data=data4NIBP;
					echartsManage.lineChart4NIBP.setOption(echartsManage.lineOption4NIBP);
					//Temp
					if(echartsManage.lineChart4Temp==null){
						echartsManage.lineChart4Temp=echarts.init(document.getElementById('chartDiv4Temp'));
					}
					echartsManage.lineOption4Temp.xAxis.data=label4HR;
					echartsManage.lineOption4Temp.series[0].data=data4Temp;
					echartsManage.lineChart4Temp.setOption(echartsManage.lineOption4Temp);
					$("#byChart").show();
					$("#byTable").hide();
				}
				
			}
		})
	}
	//报警设置
	function initAlarmSetting(){
		$("#alarmSettingDiv").show();
		$("#alarmSettings tbody").empty();
		$.ajax({
			url:contextPath+"/Alarm/alarmSetup.action",
			data:{
				PatientId:patientId,
				mac:mac
			},
			type:"post",
			dataType:"json",
			success:function(result){
				var data=result.Alarm;
				for(var i=0;i<data.length;i++){
					var tr='<tr>'
						+'<td>'+data[i].alarm_type+'</td>'
				        +'<td>'+data[i].upper_limit+'</td>'
				        +'<td>'+data[i].lower_limit+'</td>'
				        +'<td>'+data[i].priority+'</td>'
				        +'<td><input type="checkbox" name="lock" value="{{d.id}}" title="使能" lay-filter="lockDemo" '+
				        data[i].whether=="1"?"checked":""+'></td>'
				        +'<td><input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF" '+
				        data[i].bolt_lock=="1"?"checked":""+'></td>'
				      +'</tr>';
					$("#alarmSettings tbody").append(tr);
				}
				var Asphyxia=data.Asphyxia;
				var volume=data.volume;
				$("#zxsj").val(Asphyxia);
				$("#alarmVolume").val(volume);
			}
		})
		
	}
	$("#saveAlarmSetting").click(function(){
		var trArr=$("#alarmSettings tbody").find("tr");
		var paraList=[];
		for(var i=0;i<trArr.length;i++){
			var tds=$(trArr[i]).children();
			var alarm_type=$(tds[0]).attr("alarm_type");
			var bolt_lock=$(tds[5]).find("input").prop("checked");
			var lower_limit=$(tds[1].text());
			var priority=$(tds[3].text());
			var upper_limit=$(tds[2].text());
			var whether=$(tds[4]).find("input").prop("checked");
			paraList.push({
				bed_id:bedId,
				patient_id:patientId,
				alarm_type:alarm_type,
				bolt_lock:bolt_lock,
				lower_limit:lower_limit,
				priority:priority,
				upper_limit:upper_limit,
				whether:whether
			});
		}
//		var state=
		var data={
			list:paraList,
			mac:mac,
//			state:state,
			Asphyxia:$("#zxsj").val(),
			volume:$("#alarmVolume").val(),
		}
		$.ajax({
			url:contextPath+"/Alarm/save.action",
			data:data,
			type:"post",
			dataType:"json",
			success:function(data){
				layer.msg("保存成功",{time:2000})
			}
		})
	});
	//报警记录
	function initAlarmRecord(){
		$("#alarmRecordDiv").show();
		$("#alarmRecordDiv tbody").empty();
		$.ajax({
			url:contextPath+"/Alarm/alarmRecord.action",
			data:{
				PatientId:patientId
			},
			type:"post",
			dataType:"json",
			success:function(data){
				for(var i=0;i<data.length;i++){
					var tr='<tr>'
						+'<td>'+data[i].createTime+'</td>'
				        +'<td>'+data[i].alarm_Type+'</td>'
				        +'<td>'+data[i].alarm_Details+'</td>'
				      +'</tr>';
					$("#alarmRecordDiv tbody").append(tr);
				}
			}
		})
	}
	//设备信息
	function initDeviceInfo(){
		$("#deviceInfoDiv").show();
		$("#deviceInfoDiv tbody").empty();
		$.ajax({
			url:contextPath+"/EquipInfo/EquipInfo.action",
			data:{
				PatientId:patientId
			},
			type:"post",
			dataType:"json",
			success:function(data){
				for(var i=0;i<data.length;i++){
					var tr='<tr>'
						+'<td>'+data[i].createTime+'</td>'
				        +'<td>'+data[i].equip_Type+'</td>'
				        +'<td>'+data[i].mac+'</td>'
				        +'<td>'+data[i].equip_Desc+'</td>'
				        +'<td>'+data[i].equip_State+'</td>'
				      +'</tr>';
					$("#deviceInfoDiv tbody").append(tr);
				}
			}
		})
	}
	//其它
	function initOther(){
		$("#otherDiv").show();
	}
})


/*function savePatientInfo(){
	// jquery 表单提交 
	$("#patientInfoDiv form").ajaxSubmit(function(message) { 
	// 对于表单提交成功后处理，message为提交页面saveReport.htm的返回内容 
	}); 

	return false; // 必须返回false，否则表单会自己再做一次提交操作，并且页面跳转 
}*/