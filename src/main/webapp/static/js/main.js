/**
 * 
 */
//温度监控页面相关
	var bedsinfo = {};//当前所有病床的信息
	var bedsBasicinfo = {};
	var bedsOtherinfo = {};//当前所有病床的其他信息
	var chartObj = {};//当前chart表对象
	var nowDelBed = {};//当前要删除的病房
	var nowAddPersonBed = {};//当前要添加病人的病床
	var nowStopBed = {};//当前要停止监控的病床
	var nowaddBed = {};//当前要添加的病床
	var startHour=2;//折线图从几时开始
	var outpatientid="";//当前要出院的病人
	var currentBedId="";//鼠标悬停后存储当前床位id
	var currentBedName="";//鼠标悬停后存储当前床位名称
	var autorefresh;
	var autorefresh4Unnormal;
	var autorefresh4Patient;
	var autorefresh4Concentrator;
	var nowDelPatientId=0;//当前要删除的病人的id
    var nowDelBedNameOfPatient="";//当前要删除的病人的床位号
    var sureDelPatient;//询问用户是否删除病人的确认结果
    var nowPageType=0;//现在页面类型 0：床位列表 1：档案管理
    var setting4ifwarn=true;//是否进行报警的设置，true为报警，false为不报警
    
    var ifwarning=false;//是否有其他设备正在报警
	var soundpath=null;
	var indexalert;
	//档案管理分页相关
	var pageSize = 15; //每页显示个数
    var nowpage = 1; //当前第几页
    var totalPages = 1;
    var pageSize4C = 15; //每页显示个数
    var nowpage4C = 1; //当前第几页
    var totalPages4C = 1;
		$(document).ready(function(){
/* 			$('body').width(window.innerWidth);
			$('body').height(window.innerHeight); */
			//alert(document.body.scrollHeight);
			$(".menulist").css("height",document.body.scrollHeight-60);
			$(".mainArea").css("height",document.body.scrollHeight-60).css("width",document.body.scrollWidth-120);
			/*  $(".mainArea").empty(); */ 
			addBasicInfo();
			getBedsData();
			startAutoRefresh();//自动刷新页面 
			//每隔一段时间获取一次集中器异常状态
			setInterval("getNosignedConcentrator()", 600000);

			//监听按钮回车事件
			$("body").bind("keydown",function(e){
				
		        // 兼容FF和IE和Opera    
			var theEvent = e || window.event;    
			var code = theEvent.keyCode || theEvent.which || theEvent.charCode;    
			if (code == 13) {  
				var visible1=$(".addpersonbox").is(":visible");//是否隐藏
				var visible2=$(".putbedidbox").is(":visible");//是否可见
				if(visible1){
					$("#yes33").click();
				}else if(visible2){
					$("#addbed").click();
				}
		      }    
			});
		});
		window.onresize = function(){
			location.reload(true);
			if(nowPageType==0){
            	getBedsData();
            }else if(nowPageType==1){
            	getPatientListData();
            	//$("#showunnormal").trigger("click");
            }else{
            	getBedsDataOfUnnormal();
            	//location.reload(true);
            }
			
		}
		function getNosignedConcentrator(){
			$.ajax({
				 type:"post",
	              url:"../concentrator/getNosignedConcentrator.action",//此处返回true或false
	              dataType:"text",
	              data:{"userId":$("#user_attr_id").val()},
	              success:function(data){
	              var objResult=JSON.parse(data);
	            	  if(objResult.success&&!ifwarning&&setting4ifwarn){
							document.getElementById("snd").volume=0.5;
							ifwarning=true;
							$("#snd").attr('src','../static/sound/super.wav');
							setTimeout("indexalert=layer.alert('"+objResult.bedName+"床的集中器连接异常，请检查设备!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4Concentrator('"+objResult.macs+"');layer.close(indexalert);})",500);
	            	  }
	              }
			});
		}
		//开始自动刷新
		function startAutoRefresh(){
			clearTimeout(autorefresh);
			autorefresh = setInterval("getBedsData()", 5000);//自动刷新页面
		}
		//停止页面自动刷新
		function stopAutoRefresh() {
			clearTimeout(autorefresh);
		}
		//开始自动刷新
		function startAutoRefresh4Unnormal(){
			clearTimeout(autorefresh4Unnormal);
			autorefresh4Unnormal = setInterval("getBedsDataOfUnnormal()", 5000);//自动刷新页面
		}
		//停止页面自动刷新
		function stopAutoRefresh4Unnormal() {
			clearTimeout(autorefresh4Unnormal);
		}
		//开始自动刷新
		function startAutoRefresh4Patient(){
			clearTimeout(autorefresh4Patient);
			autorefresh4Patient = setInterval("getPatientListData()", 600000);//自动刷新页面
		}
		//停止页面自动刷新
		function stopAutoRefresh4Patient() {
			clearTimeout(autorefresh4Patient);
		}
		function startAutoRefresh4Concentrator(){
			clearTimeout(autorefresh4Concentrator);
			autorefresh4Concentrator= setInterval("getConcentratorListData()", 600000);//自动刷新页面
		}
		//停止页面自动刷新
		function stopAutoRefresh4Concentrator() {
			clearTimeout(autorefresh4Concentrator);
		}
		//集中器停止报警
		function stopWarning4Concentrator(strmacs){//stopwarning
			$.ajax({
				type:"post",
				url:"../concentrator/stopwarning.action",
				dataType:"json",
				data:{"strmacs":strmacs},
				success:function(data){

				},
				error:function(){

				}
			})
		}
		
		function getPatientListData(){
			bedsBasicinfo = [];
			//用于点击病人名称显示折线图
			$.ajax({
				type:"post",
				url:"listAll4Patients.action",
				dataType:"json",
				data:{"pageNo":nowpage,"pageSize":pageSize},
				success:function(data){
					//$(".totalcount").html(data.total);
					bedsinfo = data.data;
					bedsOtherinfo = data.otherdata;
					$(bedsinfo).each(function(index,dom) {
						bedsBasicinfo[dom.patientid]=dom;
					})
				},
				error:function(){
					//$(".totalcount").html(0);
					bedsinfo = [];
					bedsOtherinfo = [];
				}
			})
			$(".patientListbox").empty();
             $.ajax({
              type:"post",
              url:"../patient/list.action",//此处返回true或false
              dataType:"json",
              data:{"pageNo":nowpage,"pageSize":pageSize,"userId":$("#user_attr_id").val(),"patientName":$(".name4SearchPatient").val()},
              success:function(data){
            	  $("#patientNum4Doc").text('当前'+(data.total==undefined?0:data.total)+'位患者');
                   if(data.total%pageSize != 0){
                       totalPages = data.total==undefined?1:(parseInt(data.total/pageSize) +1);
                   }else{
                       totalPages =  data.total==undefined?1:(parseInt(data.total/pageSize));
                   }
                   if(totalPages==0)totalPages=1;
                   $(".pageNum4Doc").html('<input type="text" id="newPageNo" onchange="changePatientPage(this.value);"  onkeyup="fillNumOnly(this)" style="font-size:14px;width:24px; height:20px;" value="'+nowpage+'">'+''+'/'+totalPages);
                   //putPatientsData(data.rows);//onkeyup="this.value=this.value.replace(/[^0-9]/g,\"\");"
                   //$("#header4PatientTable").append('');patientListbox
                   $(".patientListbox").empty();
                   $(".patientListbox").append('<tr id="header4PatientTable" style="background: #D5F5DF;">'
				+'<td align="center"><span class="tab_b">病案号码</span></td>'
				+'<td align="center"><span class="tab_b">患者姓名</span></td>'
				+'<td align="center"><span class="tab_b">性别/年龄</span></td>'
				+'<td align="center"><span class="tab_b">医师诊断</span></td>'
				+'<td align="center"><span class="tab_b">病床号码</span></td>'
				+'<td align="center"><span class="tab_b">温度监控起始时间</span></td>'
				+'<td align="center"><span class="tab_b">贴片号码</span></td>'
				+'<td align="center"><span class="tab_b">实时温度</span></td>'
				+'<td align="center"><span class="tab_b">功能操作</span></td></tr>');
					var className4PatientListTr="";
                    $(data.rows).each(function(index,dom){
                    	if(dom.status==0){
                    		className4PatientListTr="historyPatientTr";
                    	}else{
                    		className4PatientListTr="currentPatientTr";
                    	}
                     $(".patientListbox").append('  <tr class="'+className4PatientListTr+'">'+//style=" border-bottom:1px solid #e4ebf1"
                     '<td align="center"><span class="tab_b">'+dom.sickid+'</span></td>'+
                     '<td align="center"><span class="tab_b" style="cursor:pointer;" onclick="showDegree(this);" patientid="'+dom.id+'" bedid="'+dom.bedid+'">'+dom.name+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+(dom.sex=="null"?"":dom.sex)+'/'+(dom.age>0?dom.age:"")+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+dom.diagnosis+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+dom.bedid+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+dom.startTime+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+dom.cardnum+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+((dom.degree==undefined||dom.degree<15)?'15.00':dom.degree)+'</span></td>'+
                     '<td align="center" style="color:#1295FE"><span class="btnUpdatePatient" onclick="updatePatient(this);" personnelId="'+dom.id+'">修改</span>&nbsp;|&nbsp;<span class="btnUpdatePatient" bedName="'+dom.bedid+'"  personnelId="'+dom.id+'" onclick="delPatient(this);">删除</span></td>'+
                     '</tr>');
                    })
              }
             })
         }
		function changePatientPage(newPageNo){
			if(newPageNo> totalPages||newPageNo==0||newPageNo==nowpage){
				$("#newPageNo").val(nowpage);
				return;
			}else{
				nowpage=newPageNo;
				getPatientListData();
			}
		}
		
		//档案管理上一页
		function pagePrev(){
			if(nowpage == 1){
                return;
            }else{
               nowpage--;
            }
			getPatientListData();
		}
		//档案管理下一页
		function pageNext(){
			if(nowpage == totalPages){
                return;
            }else{
                nowpage++;
            }
			getPatientListData();
		}
		//档案管理上一页
		function pagePrev4Concentrator(){
			if(nowpage4C == 1){
                return;
            }else{
               nowpage4C--;
            }
			getConcentratorListData();
		}
		//档案管理下一页
		function pageNext4Concentrator(){
			if(nowpage4C == totalPages4C){
                return;
            }else{
                nowpage4C++;
            }
			getConcentratorListData();
		}
		$(".menulv2").click(function() {
			nowPageType=0;
			$("#concentratorManage").removeClass("menuclicked");
			$(".menulv2").removeClass("menuclicked");
			$("#docManage").removeClass("menuclicked");
			$(this).addClass("menuclicked");
		});
		/* $("#docManage").click(function() {
			
			
		}); */
		$("#showunnormal").click(function(){//显示温度异常页面
			bedsBasicinfo = [];
			stopAutoRefresh();
			stopAutoRefresh4Patient();
			stopAutoRefresh4Concentrator();
			$(".mainArea").empty();
			$(".mainArea").html('<div  style="height:92px;width:100%;margin-left:30px;"><ul style="list-style:none;padding:0px;margin:0px;">'+
			'<li class="" style="height:48px;width:160px;margin:24 0 20 0;display:inline;float:left;" ><span style=" width: 140px; text-align: center; font-size: 48px;'+
			' color: orange;"><span class="highcount">1</span><span style="font-size: 14px; color: #FFFFFF">位患者体温过高</span></span></li></ul></div>'+
			'<div style="height:auto;width:100%;margin-left:20px"><ul id="bedList4high" style="list-style:none;padding:0px;margin:0px;"></ul></div>'+
			'<div  style="height:48px;width:100%;margin-left:30px;margin-top:54px;margin-bottom:20px;position:relative;float:left"><ul style="list-style:none;padding:0px;'+
			'margin:0px;"><li class="" style="height:48px;width:160px;margin:0;display:inline;float:left;backround:red"><span style=" width: 140px; text-align: center;'+
			' font-size: 48px; color: #1295FE;"><span class="lowcount">2</span><span style="font-size: 14px; color: #FFFFFF">位患者体温过低</span></span></li></ul></div>'+
			'<div style="height:auto;width:100%;margin-left:20px;margin-top:20px"><ul id="bedList4low" style="list-style:none;padding:0px;margin:0px;"></ul></div>'+//);
			'<div  style="height:48px;width:100%;margin-left:30px;margin-top:54px;margin-bottom:20px;position:relative;float:left"><ul style="list-style:none;padding:0px;'+
			'margin:0px;"><li class="" style="height:48px;width:160px;margin:0;display:inline;float:left;backround:red"><span style=" width: 140px; text-align: center;'+
			' font-size: 48px; color: #1295FE;"><span class="expcount">2</span><span style="font-size: 14px; color: #FFFFFF">个未识别设备</span></span></li></ul></div>'+
			'<div style="height:auto;width:100%;margin-left:20px;margin-top:20px"><ul id="bedList4exp" style="list-style:none;padding:0px;margin:0px;"></ul></div>');
			startAutoRefresh4Unnormal();
			getBedsDataOfUnnormal();
		});
		$("#shownormal").click(function(){//显示温度监控页面（床位列表）
			bedsBasicinfo = [];
			stopAutoRefresh4Unnormal();
			stopAutoRefresh4Patient();
			stopAutoRefresh4Concentrator();
			startAutoRefresh();
			$(".mainArea").empty();
			addBasicInfo();
			getBedsData();
		});
		function addBasicInfo(){
			$(".mainArea").html('<div id="bedstatistic" style="height:130px;width:100%;margin-left:10px;"><ul style="list-style:none;padding:0px;'+
					'margin:0px;"><li class="statisticLi" ><span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: orange;">'+
					'<span class="highcount"></span><span style="font-size: 14px; color: #FFFFFF">位</span></span><span style="display: block; width: 140px; '+
					'text-align: center; font-size: 14px; color: #FFFFFF">患者体温过高</span></li><li class="statisticLi"><span style="margin-left:46px; width: 140px;'+
					' text-align: center; font-size: 48px; color: #1295FE;"><span class="lowcount"></span><span style="font-size: 14px; color: #FFFFFF">位</span></span>'+
					'<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者体温过低</span></li><li class="statisticLi">'+
					'<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;"><span class="notemptycount"></span><span style="font-size: 14px;'+
					' color: #FFFFFF">位</span></span><span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者入住</span></li>'+
					'<li class="statisticLi"><span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;">'+
					'<span class="emptycount"></span><span style="font-size: 14px; color: #FFFFFF">个</span></span><span style="display: block; width: 140px; text-align: center;'+
					' font-size: 14px; color: #FFFFFF">床位空置</span></li><li class="statisticLi"><span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px;'+
					' color: #FFFFFF;"><span class="totalcount"></span><span style="font-size: 14px; color: #FFFFFF">个</span></span><span style="display: block; width: 140px; '+
					'text-align: center; font-size: 14px; color: #FFFFFF">当前总计床位</span></li>'+
					//添加去噪设置（是否mean）
					'<li style="height: 84px;width: 250px;margin: 46 40 0 10;display: inline;float: right;color:white">'
					+'<p style="display:inline;cursor:pointer;"onclick="location.reload(true)">刷新界面</p>&nbsp;|&nbsp;<p style="display:inline">报警设置</p>'
					+'<input type="radio" name="arg4ifwarn" value="1" style="margin:10px;cursor:pointer;" id="radioyes" onchange="setting4ifwarn=true;">是'
					+'<input type="radio" name="arg4ifwarn" value="0" style="margin:10px;cursor:pointer;" id="radiono" onchange="setting4ifwarn=false;">否'
					//+'<a href="javascript:void(0);" onclick="updateArg()" style="color: #1295FE;margin-left:20px;">修改</a>'
					+'</li>'+
					//'<li class="btnAddBedLi4Deg"><div class="btnDiv4Deg" onclick="addBed()"><span class="btnSpan4Deg" '+
					//'style="background: url(../images/icon_add_sickbed_small.png) no-repeat;">添加床位</span></div></li><li class="btnAddBedLi4Deg">'+
					//'<div class="btnDiv4Deg" onclick="addPatient4btn()"><span class="btnSpan4Deg" style="background: url(../images/icon_add_patient_small.png) no-repeat;">患者入住</span></div></li>'+
					'</ul></div>'+
					'<div class="bedsArea"><ul id="bedList" style="list-style:none;padding:0px;margin:0px;"></ul></div>');
			if(setting4ifwarn){//ifwarning//setting4ifwarn
				$("#radioyes").attr("checked","true");		
			}else{
				$("#radiono").attr("checked","true");
			}
		}
		$("#docManage").click(function(){
			nowPageType = 1;
			stopAutoRefresh();
			stopAutoRefresh4Unnormal();
			stopAutoRefresh4Concentrator();
			$(this).addClass("menuclicked");
			$("#concentratorManage").removeClass("menuclicked");
			$(".menulv2").removeClass("menuclicked");
			$(".mainArea").empty();
			$(".mainArea").html('<div class="pageDocManage"><div class="divheader4DocManage"><div style="font-size:24px;color:#33A684;margin-left:30px;display:block;position:relative;top:26px">档案管理&nbsp;<font id="patientNum4Doc" style="font-size:14px;" color="#666666">当前位患者</font>'
				+'</div><div style="margin-right:30px;position:relative;right:30px;float:right;color:#3CB397;cursor:pointer;" class="btnDiv4Doc" onclick="addPatient4btn()">'
				+'<span class="btnSpan4Deg" style="background: url(../images/icon_add_patient_small_green.png) no-repeat;">患者入住</span></div>'
				+'<hr style="width: auto; height: 1px; border: none;margin: 46 30 0 30;position:relative; border-top: 1px solid #33A684;" />'
				+'</div><table class="patientListbox" border="1" cellspacing="5" style="font-size:14px;border:1px solid #666666;margin:18 0 18 30;width:90%;positon:relative;" cellpadding="5">'
				+'<tr id="header4PatientTable" style="background: #D5F5DF;">'
				+'<td align="center"><span class="tab_b">病案号码</span></td>'
				+'<td align="center"><span class="tab_b">患者姓名</span></td>'
				+'<td align="center"><span class="tab_b">性别/年龄</span></td>'
				+'<td align="center"><span class="tab_b">医师诊断</span></td>'
				+'<td align="center"><span class="tab_b">病床号码</span></td>'
				+'<td align="center"><span class="tab_b">温度监控起始时间</span></td>'
				+'<td align="center"><span class="tab_b">贴片号码</span></td>'
				+'<td align="center"><span class="tab_b">实时温度</span></td>'
				+'<td align="center"><span class="tab_b">功能操作</span></td></tr></table>'
				+'<div class="pageManage" style="width:auto;height:24px;float:right;margin-right:30px;"><div class="pagePrev4Doc" onclick="pagePrev()"></div>'
				+'<div class="pageNum4Doc" ><input type="text" style="font-size:14px;width:24px; height:20px;" readonly="true" value="1">/1</div><div class="pageNext4Doc" onclick="pageNext()"></div></div>'
				+'<div style="font-size:14px;float:right;margin-right:20px;"><span>姓名：</span><input class="name4SearchPatient" onchange="refreshPatientList();" style="border-radius: 10px;width:100px;height:24px;padding-left:10px;padding-right:10px;" type="text"><a href="javascript:void(0);" style="color: #1295FE;margin-left:10px;">搜索</a></div>'
				+'</div>');
			//去后台请求所有病人信息数据
			$(".pageDocManage").width($(".mainArea").width()-60).height($(".mainArea").height()-60);
			$(".patientListbox").css("width",$(".pageDocManage").width()-60);
			getPatientListData();
			startAutoRefresh4Patient();
	
		});
		
		$("#concentratorManage").click(function(){
			nowPageType=1;
			stopAutoRefresh();
			stopAutoRefresh4Unnormal();
			stopAutoRefresh4Patient();
			$(this).addClass("menuclicked");
			$("#docManage").removeClass("menuclicked");
			$(".menulv2").removeClass("menuclicked");
			$(".mainArea").empty();
			$(".mainArea").html('<div class="pageConcentratorManage">'
					+'<div class="divheader4Concentrator">'
					+'<div style="font-size: 24px; color: #33A684; margin-left: 30px; display: block; position: relative; top: 26px">'
					+'设备管理<font id="concentratorNum4Doc" style="font-size: 14px;"color="#666666">当前个集中器</font>'
					+'</div>'
					+'<div style="margin-right: 30px; position: relative; right: 30px; float: right; color: #3CB397;cursor:pointer;"'
					+'class="btnDiv4AddConcentrator"  onclick="addConcentrator()">'
					+'<span class="btnSpan4AddConcentrator" style="background: url(../images/icon_add_concentrator.png) no-repeat;">添加集中器</span>'
					+'</div>'
					+'<hr style="width: auto; height: 1px; border: none; margin: 46 30 0 30; position: relative; border-top: 1px solid #33A684;" />'
					+'</div>'
					+'<center>'
					//添加去噪设置（是否mean）
					+'<div style="margin-top:20px;">'
					+'<p style="display:inline">去噪设置</p>'
					+'<input type="radio" name="arg" value="1" style="margin-left:20px;cursor:pointer;">是'
					+'<input type="radio" name="arg" value="0" style="margin-left:20px;cursor:pointer;">否'
					//+'<button onClick="updateArg()" style="margin-left:20px;">提交</button>'
					//+'<span class="btnUpdateUser" onClick="updateArg()" style="color: #1295FE;">修改</span>'
					+'<a href="javascript:void(0);" onclick="updateArg()" style="color: #1295FE;margin-left:20px;">修改</a>'
					+'</div>'
					+'<table class="concentratorlistbox" border="1" cellspacing="0" style="margin: 18 0 18 30;  position: relative;" cellpadding="0">'
					+'<tr style="background: #D5F5DF;">'
					+'<td align="center"><span class="tab_b">病床号码</span></td>'
					+'<td align="center"><span class="tab_b">集中器号码</span></td>'
					+'<td align="center"><span class="tab_b">集中器状态</span></td>'
					+'<td align="center"><span class="tab_b">功能操作</span></td>'
					+'</tr>'
					+'</table>'
					+'<div class="pageManage" style="width:auto;height:24px;float: left;margin-left:490px;"><div class="pagePrev4Doc" onclick="pagePrev4Concentrator()"></div>'
					+'<div class="pageNum4Doc" ><input type="text" style="font-size:14px;width:24px; height:20px;" readonly="true" value="1">/1</div><div class="pageNext4Doc" onclick="pageNext4Concentrator()"></div></div>'
					+'</center>'
					+'</div>');
			//去后台请求所有病人信息数据
			$(".pageConcentratorManage").width($(".mainArea").width()-60).height($(".mainArea").height()-60);
			$(".pageManage").css('margin-left',$(".pageConcentratorManage").width()/2+180);
			//获取去噪设置当前参数
			$.ajax({
				type:"post",
				dataType:"json",
				url:"../system/getSystemSetting.action",
				data:{},
				success:function(data){
					if(data.success==true){
						//alert(data.ifAvg);
						if(data.ifAvg==1){
							$("input[name='arg'][value=1]").attr("checked",true); 
						}else{
							$("input[name='arg'][value=0]").attr("checked",true); 
						}
					}else{
						alert("操作失败！");
					}
				}
			})
			getConcentratorListData();
			startAutoRefresh4Concentrator();
	
		});
		//修改去噪设置参数
		function updateArg()
		{
			var arg=$("input[name='arg']:checked").val();
			if(arg==undefined){
				layer.alert("请选择参数");return;
			}
			$.ajax({
				type:"post",
				dataType:"json",
				url:"../system/updateArg4Avg.action",
				data:{"ifGetAvg":arg},
				success:function(data){
					if(data.success==true){
						layer.alert("操作成功！");
					}else{
						layer.alert("操作失败！(04)");
					}
				}
				
			})
		}
		function updateConcentrator(id,bedName,concentratorNo){
			$(".title4Concentrator").text("修改集中器");
			$('.modelbg').show();
			$('#id4UpConcentrator').val(id);
			$(".concentratorNo").val(concentratorNo);
			$(".bedid4Concentrator").val(bedName);	
			$(".addconcentratorbox").show();
			
		}
		//确认添加或修改集中器信息，并保存
		function sureAddConcentrator(){
			var id=$('#id4UpConcentrator').val();
			var bedName=$(".bedid4Concentrator").val();
			var concentratorNo=$(".concentratorNo").val();
			if(bedName.trim().length==0||concentratorNo.trim().length==0){
				layer.alert("请正确填写参数！");
				return;
			}
			var paras={"bedName":bedName,"concentratorNo":concentratorNo};
			if(id.length>0){
				paras={"id":id,"bedName":bedName,"concentratorNo":concentratorNo}
			}
			$.ajax({
		        type:"post",
		        url:"../concentrator/add.action",//��Ӳ�����Ϣ
		        dataType:"json",
		        data:paras,//{"medicalTreatmentType":sicktype,"personnelId":personid,"address":address,"id":patientid,"name":name,"sex":sex,"age":age,"bedName":bedid,//"bed.id":idofbed,
		        	//"disease":sickname,"doctor":doctor,"equipNo":cardname,"rangeEnd":highc,"rangeStart":lowc,"startTime":startTime},
		        //data:{"medicalTreatmentType":sicktype,"personnelId":personid,"address":address,"id":patientid,"name":name,"sex":sex,"age":age,"startTime":roomtime,
		        	//"disease":sickname,"doctor":doctor,"equipNo":cardname,"rangeEnd":highc,"rangeStart":lowc},//,"bed_id":idofbed//2015-11-28 10:01:43
		        success:function(data){
		        	if(data.success == false){ 
		        		//alert(data.bedstatus);
		        		layer.alert("保存失败，请重试！(05)");
						return;
					}
		            $('.addconcentratorbox').hide();
		            $('.modelbg').hide();
		            getConcentratorListData();
		        }

		    })
		}
		var toDeleteConcentratorId;//将要删除的集中器的id
		function deleteConcentrator(id){
			toDeleteConcentratorId=id;
			var index = layer.open({
			    type: 2,
			    area: ['360px', '240px'],
			    fix: true, //不固定
			    maxmin: false,
			    content: ['../static/layerHtml/delConConfirm.html', 'no'],
			    scrollbar: false,
			    closeBtn: 0,
			    title:false,
			    shade: [0.5,'#000000'] //0.1透明度的白色背景
			}); 
			layer.style(index, {
				'border-radius': '4px'
			});
		}
		//添加集中器
		function addConcentrator(){
			$(".title4Concentrator").text("添加集中器");
			$('.modelbg').show();
			$('#id4UpConcentrator').val('');
			$(".concentratorNo").val("");
			$(".bedid4Concentrator").val("");	
			$(".addconcentratorbox").show();
		}
		function getConcentratorListData(){
			$.ajax({
	              type:"post",
	              url:"../concentrator/list.action",//此处返回true或false
	              dataType:"json",
	              data:{"pageNo":nowpage4C,"pageSize":pageSize4C,"userId":$("#user_attr_id").val()},
	              success:function(data){
	            	  $("#concentratorNum4Doc").text('当前'+data.total+'个集中器');
	                   if(data.total%pageSize4C != 0){
	                       totalPages4C = parseInt(data.total/pageSize4C) +1;
	                   }else{
	                       totalPages4C =  parseInt(data.total/pageSize4C);
	                   }
	                   //$(".pageNum4Doc").text(nowpage+'/'+(totalPages==0?1:totalPages));
	                   $(".pageNum4Doc").html('<input type="text" id="newPageNo" onchange="changeConcentratorPage(this.value);"  onkeyup="fillNumOnly(this)" style="font-size:14px;width:24px; height:20px;" value="'+nowpage4C+'">'+''+'/'+(totalPages4C==0?1:totalPages4C));
	                   //putPatientsData(data.rows);
	                   //$("#header4PatientTable").append('');patientListbox
	                   $(".concentratorlistbox").empty();
	                   $(".concentratorlistbox").append('<tr style="background: #D5F5DF;">'
	               			+'<td align="center"><span class="tab_b">病床号码</span></td>'
	            			+'<td align="center"><span class="tab_b">集中器号码</span></td>'
	            			+'<td align="center"><span class="tab_b">集中器状态</span></td>'
	            			+'<td align="center"><span class="tab_b">功能操作</span></td>'
	            			+'</tr>');
	                   console.dir(data);

	                    $(data.rows).each(function(index,dom){
	                     $(".concentratorlistbox").append('  <tr >'+//style=" border-bottom:1px solid #e4ebf1"
	                     '<td align="center"><span class="tab_b">'+dom.bedName+'</span></td>'+
	                     '<td align="center"><span class="tab_b">'+dom.concentratorNo+'</span></td>'+
	                     '<td align="center"><span class="tab_b">'+(dom.signaled==1?"正常":"异常")+'</span></td>'+
	                     '<td align="center" style="color:#1295FE"><span class="btnUpdateUser" '
	                     +'onclick="updateConcentrator(\''+dom.id+'\',\''+dom.bedName+'\',\''+dom.concentratorNo+'\');">修改</span>'
	                     +'&nbsp;|&nbsp;<span class="btnUpdateUser" onclick="deleteConcentrator(\''+dom.id+'\');">删除</span></td>'+
	                     '</tr>');
	                    })
	              }
	             })
		}
		function changeConcentratorPage(newPageNo){
			if(newPageNo> totalPages4C||newPageNo==0||newPageNo==nowpage4C){
				$("#newPageNo").val(nowpage4C);
				return;
			}else{
				nowpage4C=newPageNo;
				getConcentratorListData();
			}
		}
		//搜索姓名文本框内容改变后刷新病人列表
		function refreshPatientList(){
			getPatientListData();
		}
		
		function getClientHeight(){
			return  document.body.scrollHeight > document.documentElement.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight;
		}
		function getClientWidth(){
			return  document.body.scrollWidth > document.documentElement.scrollWidth?document.body.scrollWidth:document.documentElement.scrollWidth;
		}
		
		//去后台请求所有病床信息数据
		function getBedsData(){
			$.ajax({
				type:"post",
				url:"listAll.action",
				dataType:"json",
				data:{},
				success:function(data){
					$(".totalcount").html(data.total);
					bedsinfo = data.data;
					bedsOtherinfo = data.otherdata;
					putBedsData(bedsinfo);
				},
				error:function(){
					$(".totalcount").html(0);
					bedsinfo = [];
					bedsOtherinfo = [];
					putBedsData(bedsinfo);
				}
			})
			}
		
		//获取温度异常的床位信息
		function getBedsDataOfUnnormal(){
			$.ajax({
				type:"post",
				url:"listAll.action",//"bed/listAll.action",//"tiwen/bedinfo.json",
				dataType:"json",
				data:{},
				success:function(data){
					$(".totalcount").html(data.total);
					bedsinfo = data.data;
					bedsOtherinfo = data.otherdata;
					putBedsDataOfUnnormal(bedsinfo);
					putBedsDataOfExp(data.expData);
				},
				error:function(){
					$(".totalcount").html(0);
					bedsinfo = [];
					bedsOtherinfo = [];
					putBedsDataOfUnnormal(bedsinfo);
					putBedsDataOfExp(data.expData);
				}
			})}
		//删除病床
		function delBed(ele){
			nowDelBed = $(ele).parents(".bedLi");
			var bedid = $(nowDelBed).attr("id");
			var index = layer.open({
			    type: 2,
			    area: ['360px', '240px'],
			    fix: true, //不固定
			    maxmin: false,
			    content: ['../static/layerHtml/delBedConfirm.html', 'no'],
			    scrollbar: false,
			    closeBtn: 0,
			    title:false,
			    shade: [0.5,'#000000'] //0.1透明度的白色背景
			}); 
			layer.style(index, {
				'border-radius': '4px'
			});
		}
		function stopWarning(strmac){
			$.ajax({
				type:"post",
				url:"stopwarning.action",
				dataType:"json",
				data:{"strmac":strmac},
				success:function(data){

				},
				error:function(){

				}
			})
		}
		function stopWarning4nosign(strmac){
			$.ajax({
				type:"post",
				url:"stopwarning4nosign.action",
				dataType:"json",
				data:{"strmac":strmac},
				success:function(data){

				},
				error:function(){

				}
			})
		}
		function stopWarning4nopower(strmac){
			$.ajax({
				type:"post",
				url:"stopwarning4nopower.action",
				dataType:"json",
				data:{"strmac":strmac},
				success:function(data){

				},
				error:function(){

				}
			})
		}
		function stopWarning4openarms(strmac){
			$.ajax({
				type:"post",
				url:"stopwarning4openarms.action",
				dataType:"json",
				data:{"strmac":strmac},
				success:function(data){

				},
				error:function(){

				}
			})
		}
		
		//放入病床信息数据
		function putBedsData(data){
			var highcount = 0;//高温病人数量
			var lowcount=0;//低温病人数量
			var emptycount=0;//空床数量
			var notemptycount=0;//非空床数量
			$("#bedList").empty();
			$(data).each(function(index,dom) {
				//alert(dom.bedstate);
				if(dom.bedstate == "empty"){
					emptycount++;
					$("#bedList").append('<li class="bedLi" bedName="'+dom.bedid+'" id="'+dom.idofbed+'" patientid=""><div class="addPatient4bed" onclick="addPatient(this)" bedName="'+dom.bedid+'" id="'+dom.bedid+'"><span class="addPatientSpan">'+
							'患者入住</span></div><div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">'+
							'床位号：</span><span class="span4BedNum">'+dom.bedid+'</span></div><div><div class="smallbtnDelInEmptyBed4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
				}
				else{
					notemptycount++;
					//alert(dom.state);
					var tempTypeText = "";
					var strbedclassname="";					
					
					if(dom.state == "superhigh"){
						tempTypeText = "高温";
						highcount++;
						strbedclassname="superhigh4bed";
						strbedlistname="#bedList4high";
						soundpath="../static/sound/super.wav";
						document.getElementById("snd").volume=0.5;
					}else if(dom.state == "high"){
						tempTypeText = "高温";
						highcount++;
						strbedclassname="high4bed";
						soundpath="../static/sound/normal.wav";
						document.getElementById("snd").volume=1.0;
					}else if(dom.state == "low"){
						tempTypeText = "低温";
						lowcount++;
						strbedclassname="low4bed";
						soundpath="../static/sound/normal.wav";
						document.getElementById("snd").volume=1.0;
					}else{
						tempTypeText = "正常";
						dom.state="normal";
						strbedclassname="normal4bed";
						soundpath="";
					}
					//获取贴片的电量
					var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10&&powerNum>0||dom.nopower=="1"||dom.nopower=="0")
					{
						strbedclassname="nopower4bed";
					}
					//
					//setTimeout("$(\"#snd\").attr('src',soundpath);layer.alert('"+dom.bedid+"床患者体温异常!',function(){})",5000);
					if(dom.ifWarn&&dom.warningType!="0"&&!ifwarning&&setting4ifwarn){
						ifwarning=true;
						$("#snd").attr('src',soundpath);
						setTimeout("indexalert=layer.alert('"+dom.bedid+"床患者体温异常!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning('"+dom.num+"');layer.close(indexalert);})",500);//stopWarning('"+dom.bedid+"');layer.close(indexalert);
						
					}
					if(dom.warningOrNot4OpenArms&&!ifwarning&&setting4ifwarn){
						ifwarning=true;
						$("#snd").attr('src',soundpath);
						setTimeout("indexalert=layer.alert('"+dom.bedid+"床患者胳膊处于打开状态超过半小时，请检查!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4openarms('"+dom.num+"');layer.close(indexalert);})",500);
						
					}
					bedsBasicinfo[dom.bedid]=dom;
					var nowdegree;
					if(dom.degree==undefined){
						nowdegree="--";
					}else if(dom.degree<=15){
						nowdegree='Lo';
					}else if(dom.degree>=45){
						nowdegree='Hi';
					}else{
						nowdegree=dom.degree;
					}
					//获取贴片的工作状态，如出现故障则在病床号后边加叹号作为标志
					var statusOfDevice="";
					if(dom.deviceStatus==0){
						statusOfDevice="(!)";
						nowdegree="--";
						if(dom.warningOrNot4NoSign&&!ifwarning&&setting4ifwarn){
							ifwarning=true;
							document.getElementById("snd").volume=1;
							$("#snd").attr('src','../static/sound/normal.wav');
							setTimeout("indexalert=layer.alert('"+dom.bedid+"床设备断开连接或产生故障，请检查设备!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4nosign('"+dom.num+"');layer.close(indexalert);})",500);
						}
					}
					
					/* bedsBasicinfo[dom.bedid]=dom;
					var nowdegree=(dom.degree==undefined||dom.degree<25)?'25.00':dom.degree;
					nowdegree=(nowdegree>42)?'42.00':nowdegree; */
					//判断超低温或者超高温
					var flag4Unnormal='';
					/* if(dom.degree!=undefined){
						if(dom.degree<34){
							flag4Unnormal='LL';
						}else if(dom.degree>42){
							flag4Unnormal='HH';
						}
					} */
					var strpersonalinfo=dom.personinfo;
					//var seximgurl=(strpersonalinfo.indexOf("-女-")>0)?"../images/woman.ico":"../images/man.ico";
					/* var sexstr;
					if(strpersonalinfo.indexOf("-null-")>0){
						sexstr="";
					}else{
						sexstr=(strpersonalinfo.indexOf("-女-")>0)?"女":"男";
					} */
					var sexstr=dom.sex==undefined?"":dom.sex;
					var strHTML='<li class="bedLi" id="'+dom.idofbed+'" bedid="'+dom.bedid+'" roomtime="'+dom.roomtime+'" patientid="'+dom.patientid+'">'
					
					if(dom.status4OpenArms!=undefined&&dom.status4OpenArms){//打开了胳膊，右上角显示图标sign_warning_32px.png
						strHTML+='<img width="16px" style="float:right;margin-right:10px;margin-top:6px;" height="16px" src="../images/sign_warning_32px.png">';
					}
					//获取贴片的电量
					//var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10&&powerNum>0||dom.nopower=="1"||dom.nopower=="0"){//&&!ifwarning
						//setTimeout("alert('"+dom.bedid+"床电量不足，剩余电量为："+powerNum+"%')",500);
						strHTML+='<img width="32px" style="float:right;margin-right:5px;margin-top:0px;" height="32px" src="../images/battery_low_32px.png">';
					}
					
					if((powerNum!=undefined&&powerNum<=10&&powerNum>0||dom.nopower=="1")&&!ifwarning&&setting4ifwarn){
						ifwarning=true;
						$("#snd").attr('src','../static/sound/normal.wav');
						setTimeout("indexalert=layer.alert('"+dom.bedid+"床设备电量不足，请及时更换!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4nopower('"+dom.num+"');layer.close(indexalert);})",500);
					}
					strHTML+='<div class="'+strbedclassname+'"><span class="curDegreeSpan" >'+nowdegree+'℃</span>'+
					'<span class="patientNameSpan">'+dom.name+'</span><br><span class="patientAgeSpan">'+sexstr+' '+(dom.age>0?dom.age:"")+'</span><span style="font-size:10px;color:#FFFFFF;display:block;margin-top:0px;">'+flag4Unnormal+'</span></div>'+
					'<div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">床位号：</span><span class="span4BedNum">'+dom.bedid+statusOfDevice+'</span>'+
					'</div><div><div class="smallbtnRecInBedList4div" onclick="showChart(this)"><span class="span4Rec">记录</span></div><div class="smallbtnDelInBedList4div">'+
					'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>'
					$("#bedList").append(strHTML);
					
				}
				
			})
			$("#bedList").append('<li class="bedLi addbedbtn" onclick="addBed()"><div class="addBed4bed"></div><div class="divbed_bedNum" style="position:relative;">'+
			'<div id="addBedInList4div" class="addBedInBedsList"><center><span id="addBedInLists4pan">添加床位</span></center>'+
			'</div></div></li>');
			$(".highcount").html(highcount);
			$(".lowcount").html(lowcount);
			$(".emptycount").html(emptycount);
			$(".notemptycount").html(notemptycount);
			$(".highandlowcount").html(highcount+lowcount);
		}
		//获取异常体温床位数据并显示
		function putBedsDataOfUnnormal(data){
			var highcount = 0;//高温病人数量
			var lowcount=0;//低温病人数量
			var emptycount=0;//空床数量
			var notemptycount=0;//非空床数量
			$("#bedList4high").empty();
			$("#bedList4low").empty();
			$(data).each(function(index,dom) {
				//alert(dom.bedstate);
				if(dom.bedstate == "empty"){
					emptycount++;
				}
				else{
					notemptycount++;
					//alert(dom.state);
					var tempTypeText = "";
					var strbedclassname="";
					var strbedlistname="";
					
					
					if(dom.state == "superhigh"){
						tempTypeText = "高温";
						highcount++;
						strbedclassname="superhigh4bed";
						strbedlistname="#bedList4high";
						soundpath="../static/sound/super.wav";
						document.getElementById("snd").volume=0.5;
					}else if(dom.state == "high"){
						tempTypeText = "高温";
						highcount++;
						strbedclassname="high4bed";
						strbedlistname="#bedList4high";
						soundpath="../static/sound/normal.wav";
						document.getElementById("snd").volume=1.0;
					}else if(dom.state == "low"){
						tempTypeText = "低温";
						lowcount++;
						strbedclassname="low4bed";
						strbedlistname="#bedList4low";
						soundpath="../static/sound/normal.wav";
						document.getElementById("snd").volume=1.0;
					}else{
						tempTypeText = "正常";
						dom.state="normal";
						strbedclassname="normal4bed";
						strbedlistname="#bedList";
						soundpath="";
					}
					//获取贴片的电量
					var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10&&powerNum>0||dom.nopower=="1"||dom.nopower=="0")
					{
						strbedclassname="nopower4bed";
					}
					if(dom.ifWarn&&dom.warningType!="0"&&!ifwarning&&setting4ifwarn){
						ifwarning=true;
						$("#snd").attr('src',soundpath);
						setTimeout("indexalert=layer.alert('"+dom.bedid+"床患者体温异常!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning('"+dom.num+"');layer.close(indexalert);})",500);//stopWarning('"+dom.bedid+"');layer.close(indexalert);
						
					}
					if(dom.warningOrNot4OpenArms&&!ifwarning&&setting4ifwarn){
						ifwarning=true;
						$("#snd").attr('src',soundpath);
						setTimeout("indexalert=layer.alert('"+dom.bedid+"床患者胳膊处于打开状态超过半小时，请检查!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4openarms('"+dom.num+"');layer.close(indexalert);})",500);
						
					}
					bedsBasicinfo[dom.bedid]=dom;
					var nowdegree;
					if(dom.degree==undefined){
						nowdegree="--";
					}else if(dom.degree<=15){
						nowdegree='Lo';
					}else if(dom.degree>=45){
						nowdegree='Hi';
					}else{
						nowdegree=dom.degree;
					}
					
					//获取贴片的工作状态，如出现故障则在病床号后边加叹号作为标志
					var statusOfDevice="";
					if(dom.deviceStatus==0){
						statusOfDevice="(!)";
						nowdegree="--";
						if(dom.warningOrNot4NoSign&&!ifwarning&&setting4ifwarn){
							ifwarning=true;
							document.getElementById("snd").volume=1;
							$("#snd").attr('src','../static/sound/normal.wav');
							setTimeout("indexalert=layer.alert('"+dom.bedid+"床设备断开连接或产生故障，请检查设备!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4nosign('"+dom.num+"');layer.close(indexalert);})",500);
						}
					}

					/* bedsBasicinfo[dom.bedid]=dom;
					var nowdegree=(dom.degree==undefined||dom.degree<25)?'25.00':dom.degree;
					nowdegree=(nowdegree>42)?'42.00':nowdegree; */
					//判断超低温或者超高温
					var flag4Unnormal='';
					/* if(dom.degree!=undefined){
						if(dom.degree<=34){
							flag4Unnormal='LL';
						}else if(dom.degree>42){
							flag4Unnormal='HH';
						}
					} */
					var strpersonalinfo=dom.personinfo;
					//var seximgurl=(strpersonalinfo.indexOf("-女-")>0)?"../images/woman.ico":"../images/man.ico";
					/* var sexstr;
					if(strpersonalinfo.indexOf("-null-")>0){
						sexstr="";
					}else{
						sexstr=(strpersonalinfo.indexOf("-女-")>0)?"女":"男";
					} */
					var sexstr=dom.sex==undefined?"":dom.sex;
					var strHTML='<li class="bedLi" id="'+dom.idofbed+'" bedid="'+dom.bedid+'" roomtime="'+dom.roomtime+'" patientid="'+dom.patientid+'">';
					//获取贴片的电量
					var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10&&powerNum>0||dom.nopower=="1"||dom.nopower=="0"){//&&!ifwarning
						//setTimeout("alert('"+dom.bedid+"床电量不足，剩余电量为："+powerNum+"%')",500);
						strHTML+='<img width="32px" style="float:right;margin-right:10px;margin-top:0px;" height="32px" src="../images/battery_low_32px.png">';
					}
					if((powerNum!=undefined&&powerNum<=10&&powerNum>0||dom.nopower=="1")&&!ifwarning&&setting4ifwarn){
						ifwarning=true;
						$("#snd").attr('src','../static/sound/normal.wav');
						setTimeout("indexalert=layer.alert('"+dom.bedid+"床设备电量不足，请及时更换!',{closeBtn: 0},function(){$(\"#snd\").attr('src','');ifwarning=false;stopWarning4nopower('"+dom.num+"');layer.close(indexalert);})",500);
					}
					strHTML+='<div class="'+strbedclassname+'"><span class="curDegreeSpan" >'+nowdegree+'℃</span>'+
					'<span class="patientNameSpan">'+dom.name+'</span><br><span class="patientAgeSpan">'+sexstr+' '+(dom.age>0?dom.age:"")+'</span><span style="font-size:10px;color:#FFFFFF;display:block;margin-top:0px;">'+flag4Unnormal+'</span></div>'+
					'<div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">床位号：</span><span class="span4BedNum">'+dom.bedid+statusOfDevice+'</span>'+
					'</div><div><div class="smallbtnRecInBedList4div" onclick="showChart(this)"><span class="span4Rec">记录</span></div><div class="smallbtnDelInBedList4div">'+
					'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>'
					if(strbedlistname!="#bedList")
					$(strbedlistname).append(strHTML);
					/* //获取贴片的电量
					var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10){
						//setTimeout("alert('"+dom.bedid+"床电量不足，剩余电量为："+powerNum+"%')",500);
					} */
				}
				
			})
			$(".highcount").html(highcount);
			$(".lowcount").html(lowcount);
			$(".emptycount").html(emptycount);
			$(".notemptycount").html(notemptycount);
			$(".highandlowcount").html(highcount+lowcount);
		}
		//获取异常体温床位数据并显示
		function putBedsDataOfExp(expData){console.log(expData);
			var expcount = 0;//异常设备数量
			$("#bedList4exp").empty();
			$(expData.expmacs).each(function(index,dom) {
				var strbedclassname="";
				var nowdegree=expData.exptemps[index];
				if(nowdegree==undefined||nowdegree<0){
					nowdegree="--";
					strbedclassname="normal4bed";
				}else if(nowdegree<=25){
					nowdegree='Lo';
					strbedclassname="low4bed";
				}else if(nowdegree>=42){
					nowdegree='Hi';
					strbedlistname="#bedList4high";
				}else{
					nowdegree=dom.degree;
					strbedclassname="normal4bed";
				}

				//判断超低温或者超高温
				var flag4Unnormal='';
				var strpersonalinfo="";
				var sexstr="";
				var strHTML='<li class="bedLi" id="" bedid="'+dom.bedid+'" roomtime="'+dom.roomtime+'" patientid="'+dom.patientid+'">';
				strHTML+='<div class="'+strbedclassname+'"><span class="curDegreeSpan" >'+nowdegree+'℃</span>'+
				'<span class="patientNameSpan">--</span><br><span class="patientAgeSpan">'+' '+'</span><span style="font-size:10px;color:#FFFFFF;display:block;margin-top:0px;">'+flag4Unnormal+'</span></div>'+
				'<div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">床位号：</span><span class="span4BedNum">--</span>'+
				'</div><div><div class="smallbtnRecInBedList4div" style="margin-left:48px;" onclick="showChart4Exp(\''+dom+'\')"><span class="span4Rec">记录</span></div>'+
				'</div></div></li>'
				$("#bedList4exp").append(strHTML);
				
			})
			$(".expcount").html(expData.expmacs.length);
		}
		//添加床位
		function addBed(ele){
			nowaddBed = ele;
			$('.modelbg').show();
			if($('.addbedid').val().length>0)
			if(!isNaN($('.addbedid').val())){
				$('.addbedid').val(parseInt($('.addbedid').val())+1);
			}else{
				$('.addbedid').val('');
			}
			$(".concentratorNo").val("");
			$(".putbedidbox").show();
		}
		//添加病床
		function addbed(){
/* 			alert(nowaddBed);
			alert($(obj).prop("className"));return; */
			if($(".addbedid").val().trim().length==0){
				layer.alert("请检查病床ID是否填写正确");
				return;
			}
			if($(".concentratorNo").val().length!=0&&$(".concentratorNo").val().length!=12){
				layer.alert("请检查集中器ID是否填写正确");
				return;
			}
			$.ajax({
				type:"post",
				url:"add.action",//"tiwen/addbed.json",
				dataType:"json",
				data:{"bedid":$(".addbedid").val().trim(),"concentratorNo":$(".concentratorNo").val()},
				success:function(data){
					if(data.success == false){ 
						layer.alert("添加失败，请确认要添加的病床号没有重复！(06)");
						return;
					}
					getBedsData();
					$('.putbedidbox').hide();
					return;
					$(".addbedbtn").before('<li class="bedLi" bedName="'+data.bedid+'" id="'+data.idofbed+'" patientid=""><div class="addPatient4bed" onclick="addPatient(this)" bedName="'+data.bedid+'" id="'+data.bedid+'"><span class="addPatientSpan">'+
							'患者入住</span></div><div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">'+
							'床位号：</span><span class="span4BedNum">'+data.bedid+'</span></div><div><div class="smallbtnDelInEmptyBed4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
/* 					listenAddBed();
					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */
				},
				error:function(data){layer.alert("添加失败(07)");}
			})
		}
		//添加病人（温度监控界面）
		function addPatient(ele){
			$("#titie4AddOrUpdatePatient").text("添加患者");
			var IsChrome = navigator.userAgent.toLowerCase().match(/chrome/);
			if (IsChrome == "chrome"&& navigator.userAgent.toLowerCase().match(/edge/) != "edge") {
				$(".addpersonbox table tr").css("height", "24px");
			} else {
				$(".addpersonbox table tr").css("height", "32px");
			}
			openAddPerson();
			nowAddPersonBed = $(ele).parents(".bedLi");
			//console.dir(nowAddPersonBed);
			$("#bedid").val(ele.getAttribute("bedName"));

		}
		//添加病人（档案管理页面）
		function addPatient4btn() {
			$("#titie4AddOrUpdatePatient").text("添加患者");
			var IsChrome = navigator.userAgent.toLowerCase().match(/chrome/);
			if (IsChrome == "chrome"&& navigator.userAgent.toLowerCase().match(/edge/) != "edge") {
				$(".addpersonbox table tr").css("height", "24px");
			} else {
				$(".addpersonbox table tr").css("height", "32px");
			}
			openAddPerson();
		}
		//添加病人窗口点击取消
		function cancelAddPatient() {
			$('.addpersonbox').hide();
			$('.modelbg').hide();
		}
		function updatePatient(ele) {
			var IsChrome = navigator.userAgent.toLowerCase().match(/chrome/);
			if (IsChrome == "chrome"&& navigator.userAgent.toLowerCase().match(/edge/) != "edge") {
				$(".addpersonbox table tr").css("height", "24px");
			} else {
				$(".addpersonbox table tr").css("height", "32px");
			}
			$('.modelbg').show();
			nowDelPatientId = $(ele).attr("personnelId");
			$("#titie4AddOrUpdatePatient").text("修改患者信息");
			$
					.ajax({
						type : "post",
						url : "../patient/getPatientById.action",//添加病人信息
						dataType : "json",
						data : {
							id : nowDelPatientId
						},
						success : function(data) {
							$("#patientid").val(data.id);
							$(".addpersonbox").find("input[name=name]").val(
									data.name);//病人姓名
							$(".addpersonbox").find(
									"input[name=sex][value=" + data.sex + "]")
									.click();//性别
							$(".addpersonbox").find("input[name=age]").val(
									data.age);//年龄
							$(".addpersonbox").find("input[name=roomtime]")
									.val(data.roomtime);//入院时间
							$(".addpersonbox").find("input[name=sicknum]").val(
									data.sickid);//病案号码
							$(".addpersonbox").find("input[name=sickname]")
									.val(data.sickname);//所患疾病
							$(".addpersonbox").find("input[name=doctor]").val(
									data.doctor);//主管医生
							$(".addpersonbox").find("input[name=roomid]").val(
									data.roomid);//病房号
							$(".addpersonbox").find("input[name=bedid]").val(
									data.bedid);//病床号
							$("#idofbed").val(data.idofbed);//病床id
							$(".addpersonbox").find("input[name=cardname]")
									.val(data.cardname);//贴片号码
							$(".addpersonbox").attr("mac",data.cardname);
							$(".addpersonbox").find("input[name=highc]").val(
									data.highc);//温度监控范围 最低温度
							$(".addpersonbox").find("input[name=lowc]").val(
									data.lowc);//温度监控范围 最高温度
							$(".addpersonbox").find("input[name=superHighDegree]").val(
											data.superHighDegree);//温度监控范围 最高温度
							$(".addpersonbox").find("input[name=count]").val(
									data.count);//温度采集间隔数字
							$(".addpersonbox").find("input[name=sicktype]")
									.val(data.sicktype);//就医类型
							$(".addpersonbox").find("input[name=personid]")
									.val(data.personid);//身份证
							$(".addpersonbox").find("input[name=address]").val(
									data.address);//地址
							$(".addpersonbox").show();
						}
					})
		}
		function delPatient(ele) {
			nowDelPatientId = $(ele).attr("personnelId");
			nowDelBedNameOfPatient = $(ele).attr("bedName");
			var index = layer
					.open({
						type : 2,
						area : [ '360px', '240px' ],
						fix : true, //不固定
						maxmin : false,
						content : [
								'../static/layerHtml/delPatientConfirm.html',
								'no' ],
						scrollbar : false,
						closeBtn : 0,
						title : false,
						shade : [ 0.5, '#000000' ]
					//0.1透明度的白色背景
					});
			layer.style(index, {
				'border-radius' : '4px'
			});

		}
		function sureDelPatient() {
			$.ajax({
				type : "post",
				url : "../patient/delete.action",//此处返回true或false
				dataType : "json",
				data : {
					id : nowDelPatientId,
					bedname : nowDelBedNameOfPatient
				},
				success : function(data) {
					$(".modelbg").hide();
					$(".confirmbox").hide();
					//location.reload(true);
					getPatientListData();
				}
			})
		}

		//rxz 病人出院
		function patientOut() {
			$(".bedmoreinfo").hide();
			var index = layer
					.open({
						type : 2,
						area : [ '360px', '240px' ],
						fix : true, //不固定
						maxmin : false,
						content : [
								'../static/layerHtml/patientOutConfirm.html',
								'no' ],
						scrollbar : false,
						closeBtn : 0,
						title : false,
						shade : [ 0.5, '#000000' ]
					//0.1透明度的白色背景
					});
			layer.style(index, {
				'border-radius' : '4px'
			});
		}

		function surePatientOut() {
			$.ajax({
				type : "post",
				url : "../patient/patientout.action",//��Ӳ�����Ϣ
				dataType : "json",
				data : {
					"id" : outpatientid
				},
				success : function(data) {
					if (data.success == false) {
						//alert(data.bedstatus);
						layer.alert("操作失败！(02)");
						return;
					}
					getBedsData();
				}

			})
		}
		//转科室
		function changeDepartment() {
			$(".bedmoreinfo").hide();
			//初始化部门列表
			$.ajax({
				type : "post",
				url : "listDepartments.action",//��Ӳ�����Ϣ
				dataType : "json",
				data : {},
				success : function(data) {
					if (data.success == false) {
						//alert(data.bedstatus);
						alert("操作失败！");
						return;
					}
					$("#selDepartment").empty();
					for (var i = 0; i < data.length; i++) {
						$("#selDepartment").append(
								"<option value='"+data[i].id+"'>"
										+ data[i].name + "</option>"); //为Select追加一个Option(下拉项) 
					}
					if (data.length > 0) {
						InitBedsByDepartment();
					}
				}

			})
			var h = $(window).height();
			var w = $(window).width();
			var st = $(window).scrollTop();
			var sl = $(window).scrollLeft();
			$(".changeBedDialog").css({
				"left" : ((w - 250) / 2) + sl + "px",
				"top" : ((h - 125) / 2) + st + "px"
			}).show();
		}
		function InitBedsByDepartment() {
			var departmentId = $("#selDepartment").val();
			//jQuery("#select1  option:selected").text();
			$.ajax({
				type : "post",
				url : "getBedsByDepartmentId.action",//��Ӳ�����Ϣ
				dataType : "json",
				data : {
					"departmentId" : departmentId
				},
				success : function(data) {
					if (data.success == false) {
						//alert(data.bedstatus);
						alert("操作失败！");
						return;
					}
					$("#selBed").empty();
					for (var i = 0; i < data.length; i++) {
						$("#selBed").append(
								"<option value='"+data[i].id+"'>"
										+ data[i].name + "</option>"); //为Select追加一个Option(下拉项) 
					}
				}

			})
		}
		//确认转科室
		function sureChangeDepartment() {
			/* alert(outpatientid);
			alert(currentBedName);
			alert(currentBedId); */
			$.ajax({
				type : "post",
				url : "../patient/changeBed.action",//��Ӳ�����Ϣ
				dataType : "json",
				data : {
					"patientid" : outpatientid,
					"oldbedid" : currentBedId,
					"newbedid" : $("#selBed").val()
				},
				success : function(data) {
					if (data.success == false) {
						//alert(data.bedstatus);
						alert("操作失败！");
						return;
					}
					location.reload(true);
				}

			})
		}
		//显示体温表打印页面
		function showPrint() {
			var id = outpatientid;//$(this).attr('patientid');20160122
			window.open('../show/export.action?id=' + id);
		}
		
		
		//将传入数据转换为字符串,并清除字符串中非数字与.的字符  
	    //按数字格式补全字符串  
	    var getFloatStr = function(num){  
	        num += '';  
	        num = num.replace(/[^0-9|\.]/g, ''); //清除字符串中的非数字非.字符  
	          
	        if(/^0+/) //清除字符串开头的0  
	            num = num.replace(/^0+/, '');  
	        if(!/\./.test(num)) //为整数字符串在末尾添加.00  
	            num += '.00';  
	        if(/^\./.test(num)) //字符以.开头时,在开头添加0  
	            num = '0' + num;  
	        num += '00';        //在字符串末尾补零  
	        num = num.match(/\d+\.\d{2}/)[0];  
	        return num;
	    };  
		
		

		function checkNum(obj) {
		     //检查是否是非数字值
		     if (isNaN(obj.value)) {
		         obj.value = "";
		     }
		     if (obj != null) {
		         //检查小数点后是否对于两位
		         if (obj.value.toString().split(".").length > 1 && obj.value.toString().split(".")[1].length > 2) {
		             alert("小数点后多于两位！");
		             obj.value = "";
		         }
		     }
		 };
		 function fillNumOnly(obj){
			 var str = obj.value;  
			    if (str.trim() == "")  
			        return;  
			    if(str.length==1&&str=="0"){
			    	obj.value=nowpage;
			    	return; 
			    }
			    if (/[^0-9]/g.test(str)) {  
			        obj.value = str.substr(0, str.length - 1);  
			    }  
		 }