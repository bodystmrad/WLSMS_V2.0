<%@page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/common/script.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>智慧农业系统</title>
<%-- <link rel="stylesheet" type="text/css" href="${ctx }/static/css/styleFront.css" />
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/qita.css" /> --%>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<%-- <script type="text/javascript" src="${ctx }/static/js/jquery.form.js"></script>
<script type="text/javascript" src="${ctx }/static/js/jquery.mockjax.js"></script>
<script type="text/javascript" src="${ctx }/static/js/jquery.simulate.js"></script>
<script type="text/javascript" src="${ctx }/static/js/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx }/static/js/additional-methods.js"></script>
<script type="text/javascript" src="${ctx }/static/js/head.js"></script>
<script type="text/javascript" src="${ctx }/pcJs/layer/layer.js"></script> --%>
</head>
<body>
<header>
	<div class="top">&nbsp;</div>
    <div class="menu">
    	<div class="menucont">
        	<div class="logo">农业管理系统</div>
            <div class="menulist">
            	<ul class="nav">
            		<li><a href="${ctx }/todayTask/index.action" onmouseover="change_bg(this)">今日任务</a></li>
            		<li><a href="${ctx }/mytask/index.action" onmouseover="change_bg(this)">我的任务</a></li>
            		<li><a href="${ctx }/taskcenter/index.action?pageSize=6" onmouseover="change_bg(this)">任务中心</a></li>
					<li><a href="${ctx }/taskissuance/index.action" onmouseover="change_bg(this)">发布任务</a></li>
                    <li><a href="${ctx }/todayTask/draft.action" onmouseover="change_bg(this)">草稿箱</a></li>
                    <li><a href="${ctx }/user/index.action" onmouseover="change_bg(this)">管理后台</a></li>
                </ul>
            </div>
            <div class="top_user_con">
            	<div class="top_right_newpager">
            		<div class="top_news_fenge">你好,${user_attr.realName }&nbsp;|&nbsp;<a href="${ctx }/person/index.action">用户中心</a>&nbsp;|&nbsp;</div>
                    <div class="top_news_font"><a href="${ctx }/message/index.action">新消息</a></div>
                    <div class="top_news_shuzi"><a href="${ctx }/message/index.action">${msgNum }</a></div>
                    <div class="top_news_fenge">|&nbsp;<a href="${ctx}/logOut.action">注销</a></div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="path">
   <div class="path_con">当前位置：<span class="path_con_hover">今日任务</span></div>
</div>

<div class="wrapbg">
    <sitemesh:write property='body'/>
<div class="clear"></div>
</div>
<br/>
</body>
</html>
