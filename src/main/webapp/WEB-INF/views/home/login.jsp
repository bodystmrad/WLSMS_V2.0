<%@page contentType="text/html;charset=UTF-8"%>
<%@include file="../common/taglib.jsp" %>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
label.error{
	color:red;
}
</style>
<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/jquery.validate.js"></script>
<script type="text/javascript" src="../static/js/additional-methods.js"></script>
<title>系统登录</title>
</head>
  <!-- <body style="background-image: url('../images/bg.png');color:#000000"> -->
  <body id="body" style="background:#255976;color:#FFFFFF" >
  <div style="visibility:hidden">aaa</div>
  <center>
  <div style="font-size: 40px;color: #FFFFFF;margin-top: 234px;">无线生命体征监测客户端</div>
  <form id="loginForm" method="post" action="logIn!Check.action">
  <table cellpadding="10px" width="auto">
  			<tr>
  			<!-- easyui-validatebox -->
  			<td><input  style="font-size:18px;height:40px;width:280px" id="userName" name="userName" type="text" value="" placeholder="用户名" ></td>
  			</tr>
  			<tr>
  			
  			<td><input  style="font-size:18px;height:40px;width:280px"  name="userPass" type="password" placeholder="密码" ></td>
  			</tr>
  			<c:if test="${!empty loginErr }"><td>${loginErr }</td></c:if>
  			<tr>
  			<td><input id="login" type="submit" value="登录" style="font-size:18px;color:#FFFFFF;background:#33A684;height:40px;width:280px">
				<!-- <input type="reset" value="重置"> --></td>
  			</tr>
  			<tr><td colspan="2">&nbsp;</td></tr>
  			<!-- <tr>
  			<td colspan="2" align="center">Copyright © 2016</td>
  			</tr> -->
  		</table>
  </form>

 
  </center>
  <center id="cpy" >
  <span style="font-size:14px;">Copyright © 2016</span>
</center>
<script type="text/javascript">
 function getClientHeight(){
	return  document.body.scrollHeight > document.documentElement.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight;
} 
$(function(){
	//$("#body").height(getClientHeight());//$(window).height());
	//$(document.body).height(getClientHeight());
	$("#cpy").css("margin-top",$(window).height()-580);
	$('body').bind('keyup', function(event) {
		if (event.keyCode == "13") {
			//回车执行查询
			$('#login').click();
		}
	});
});

/* $(function(){
	$("#loginForm").validate({
		rules: {
			userPass:{
				required:true
			},			
			userName:{
				required:true,
				minlength:4
			}
		}
	});
}); */
</script>
</body>
</html>