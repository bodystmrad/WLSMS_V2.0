<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>设置采集参数</title>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
</head>
<body>

<p>是否在采集数据时取两分钟内平均值
</p>
<input type="radio" name="arg" value="1">是
<input type="radio" name="arg" value="0">否
<button onClick="updateArg()">提交</button>

<script typr="text/javascript">
(function(){
	$.ajax({
		type:"post",
		dataType:"json",
		url:"getSystemSetting.action",
		data:{},
		success:function(data){
			if(data.success==true){
				//alert(data.ifAvg);
				if(data.ifAvg==1){
					$("input[name='arg'][value=1]").attr("checked",true); 
				}else{
					$("input[name='arg'][value=0]").attr("checked",true); 
				}
			}else{
				alert("操作失败！");
			}
		}
	})
})();
function updateArg()
{
	var arg=$("input[name='arg']:checked").val();
	if(arg==undefined){
		alert("请选择参数");return;
	}
	$.ajax({
		type:"post",
		dataType:"json",
		url:"updateArg4Avg.action",
		data:{"ifGetAvg":arg},
		success:function(data){
			if(data.success==true){
				alert("操作成功！");
			}else{
				alert("操作失败！");
			}
		}
		
	})
}
</script>
</body>
</html>