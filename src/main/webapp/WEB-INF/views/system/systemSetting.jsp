<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>WLSMS</title>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/bed/index.css">
<link rel="stylesheet" href="${ctx}/static/js/layui-v2.1.7/css/layui.css" media="all">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<link rel="shortcut icon" href="../images/c_temperature.png"/>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<script src="${ctx }/static/js/bed/bed.js"></script>
<script src="${ctx }/static/js/chartManage.js"></script>
<style>
</style>
</head>
<body class="layui-layout-body">
<audio id="snd" src="" autoplay="true" width=0  height=0 loop="true"></audio>
<div class="layui-layout layui-layout-admin">
  <div class="layui-header layui-bg-cyan">
    <div class="layui-logo" style="margin-left:10px;font-size:24px;width:300px">无线生命体征监测后台管理</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left" style="left: 300px;">
      <li class="layui-nav-item layui-this"><a href="../system/systemSetting.action" style="font-size:16px"><img src="${ctx }/images/icon_setting.png" alt="系统设置">&nbsp;系统设置</a></li>
      <li class="layui-nav-item"><a href="../bed/addBed.action" style="font-size:16px"><img src="${ctx }/images/icon_files_manage.png" alt="添加床位">&nbsp;添加床位</a></li>
      
    </ul>

  </div>
  
  
  <div class="layui-body">
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
      内容主体区域



    </div>
  </div>
  
  <div class="layui-footer" style="left:0px;">
    <!-- 底部固定区域 -->
    © www.ptorigin.com.cn - 铂元智能科技
  </div>
</div>
<script type="text/javascript" src="${ctx}/static/js/layui-v2.1.7/layui.all.js"></script>
<script>
//由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
;!function(){
  var layer = layui.layer
  ,form = layui.form;
  var element = layui.element;
  form.render();
}();
</script> 
</body>
</html>