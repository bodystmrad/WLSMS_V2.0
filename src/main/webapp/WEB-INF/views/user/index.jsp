<%@page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/common/taglib.jsp"%>
<%@ page isELIgnored="false" %>
<html>
<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/layer/layer.js"></script>
<head>
     <title>体温自动监控系统后台管理</title>
</head>
<body style="background-image: url('../images/bg.png');">
<center>
  <div style="font-size: 48px;color: #FFFFFF;margin: 50px;">无线生命体征监测系统后台管理</div>
  <a style="cursor:pointer" onclick="newUser(1);"><bold>添加用户</bold></a><br><br>
<table>
	<tr>
		<th width="5%"><input id="checkAll" type="checkbox"
			onclick="selectAll(this)" /></th>
		<th width="35%">科室名称</th>
		<th width="35%">用户名</th>
		<th width="30%">操作</th>
	</tr>
	 <c:forEach items="${resultPage.result }" var="item" varStatus="ind">
                    <tr class="bg${ind.index%2+1 }">
                        <td style="text-align:center;padding:0px"><input name="id" type="checkbox" value="${item.id }" /></td>
                        <td style="text-align:center;padding:0px">${item.realName } </td>
                        <td style="text-align:center;padding:0px">${item.userName } </td>

                        <td style="text-align:center;padding:0px">
                        	<div class="exit"><a href="JavaScript:void(0)" onclick="editUser(2,'${item.id}','${item.realName}','${item.userName}','${item.userPass}');"><img src="${ctx }/static/images/exit.png" /></a></div>
                            <div class="delete"><a href="javascript:void(0)" onclick="return deleteRec('${item.id}');"><img src="${ctx }/static/images/delete.png" /></a></div>
                        </td>
                    </tr>
                    </c:forEach> 
</table>

</center>
</body>
<script type="text/javascript">
function deleteRec(id){
	if(id==1){
		layer.alert('不能删除该默认管理员账户！');
		return;
	}
	layer.confirm('您确定要删除吗？', function(){ 
		 location.href='${ctx }/user/delete.action?id='+id;
		 });  
	return false;
}
function newUser(wintype){
	var content='';
	content="../static/layerHtml/edit.html?winType=1";
	//根据从服务器获取的数据来初始化工人列表
var pageii=layer.open({  
    type: 2,  
    ss:1,
    title: "新建用户",  
    area: ['220px', '245px'],
    border: [0], //去掉默认边框  
    shade: [0], //去掉遮罩  
    closeBtn: [1], //去掉默认关闭按钮  
    shift: 'left', //从左动画弹出
    content:content
});  
}
function editUser(wintype,id,realName,userName,userPass){
	var content='';
	content="../static/layerHtml/edit.html?winType="+wintype+"&id="+id+"&realName="+realName+"&userName="+userName+"&userPass="+userPass+"";
	

	//根据从服务器获取的数据来初始化工人列表
var pageii=layer.open({  
    type: 2,  
    ss:1,
    title: "修改用户",  
    area: ['220px', '245px'],
    border: [0], //去掉默认边框  
    shade: [0], //去掉遮罩  
    closeBtn: [1], //去掉默认关闭按钮  
    shift: 'left', //从左动画弹出
    content:content
});  
}
</script>
</html>