<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>温度持续监测传感器客户端</title>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/user/degreelisten.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/user/normalize.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/user/common.css"> 
<link rel="shortcut icon" href="../images/c_temperature.png"/>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<script src="${ctx }/highcharts/highcharts.js"></script>
<script src="${ctx }/modules/exporting.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/static/js/layer/layer.js"></script>
<script type="text/javascript" src="${ctx }/static/js/uploadPreview.min.js"></script>
<script type="text/javascript" src="${ctx }/static/js/ajaxfileupload.js"></script>
<style>
</style>
</head>
<body>
	<div id="page">
		<div class="header">
			<div class="headermain clearfix"  style="width:450px">
				<div style="position:absolute;top:10px;left:20px;background: url('..${logoPath }') #52b5ff no-repeat;width: 200px; height: 40px;"></div>
				<center style="height: 60px">
					<span
						style="font-size: 24px; color: #FFFFFF; top: 25%; position: relative;">温度持续监测传感器客户端后台管理</span>
						<span style="font-size:12px;color:#FFFFFF;float:right;margin-top:30px;margin-right:120px;">v1.1.1</span>
				</center>
				<%-- div class="logininfo"><span>${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }"></div> --%>
			</div>
		</div>
		<div class="main">
			<div class="menulist">
				<ul style="padding-top: 30px; margin:0px">
					<li>
						<div id="userManage" class="mainmenu menuclicked">
							<span class="mainmenucontent"
								style="background: url(../images/icon_files_manage.png) no-repeat 0px 3px;margin-top:30px; padding-left: 25px; line-height: 60px;">
								账号管理 </span>
						</div>
					</li>

					<li>
						<div id="systemSetting" class="mainmenu">
							
							<span class="mainmenucontent"
								style="background: url(../images/icon_setting.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
								系统设置 </span>
						</div>
					</li>
				</ul>
				<div style="width:120px;height:50px;position:absolute;bottom:20px;text-align:center;">
					<span style="font-size:14px;">账号：${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }">
					<hr style="margin-top: 5px;  width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
					<a href="../home/logOut.action" style="text-decoration:none;font-size:14px;color:#FFFFFF;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</a><!-- <span onclick="logOut();" style="cursor:pointer;font-size:14px;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</span> -->
				</div>
			</div>
			<!-- 主要显示区域 -->
			<div class="mainArea" style="position:fixed;left:120px;top:60px">
			<div class="pageSystemSetting">
					<div class="divheader4DocManage">
						<div
							style="font-size: 24px; color: #33A684; margin-left: 30px; display: block; position: relative; top: 26px">
							系统设置
						</div>
						<hr
							style="width: auto; height: 1px; border: none; margin: 46 30 0 30; position: relative; border-top: 1px solid #33A684;" />
					</div>
					<div style="margin:0px;width:100%;height:100%;">
						<span style="display:block;margin-top:26px;margin-left:60px;font-size:16px;'">设置医院banner图片</span>
						<table style="text-align:right;color:#666666;margin-top:16px;margin-left:70px;font-size:14px;border-collapse:separate;border-spacing:10px;" cellspacing="5">
							<tr>
								<td>选择图片</td>
								<td>
									<input id="filePath" type="text" style="border:1px solid #999999;border-radius:12px;padding-left:5px;display:block;width:310px;height:24px;">
								</td>
								<td>
									<div onclick="selectFile();" style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #666666;width:78px;height:22px;float:left;color:#666666">
									选择
									</div><input type="file" id="file_s" name="file" size="8" style="width:75px;opacity:0;display:none;">
								</td>
								<td>
									<div style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #33A684;width:78px;height:22px;float:left;color:#33A684">上传</div>
								</td>
							</tr>
							<tr>
								<td>要求</td>
								<td colspan="3" style="text-align:left;color:#999999;">图片需要为白色内容、含透明通道的.png格式图片，尺寸要求控制在宽400像</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="3" style="text-align:left;color:#999999;">素×高80像素。</td>
							</tr>
							<tr>
								<td valign="top">预览</td>
								<td colspan="3" style="width:500px;height:120px;background:#35597E;">
									<div id="div4Img">
									<img id="img_preview" data-src="400x80" alt="banner/logo" src="" class="img-rounded" style="line-height:80px;font-size:30px;color:#FFFFFF;text-align:center;width: 400px; height: 80px;margin-left:50px;float:left;">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div onclick="$('#systemSetting').trigger('click');" style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #666666;width:78px;height:22px;float:right;color:#666666">取消</div>
								</td>
								<td>
									<div onclick="uploadImg()" style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #33A684;width:78px;height:22px;float:left;color:#33A684">保存</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<!-- <div class="pageDocManage">
					<div class="divheader4DocManage">
						<div
							style="font-size: 24px; color: #33A684; margin-left: 30px; display: block; position: relative; top: 26px">
							档案管理<font id="patientNum4Doc" style="font-size: 14px;"
								color="#666666">当前个账号</font>
						</div>
						<div
							style="margin-right: 30px; position: relative; right: 30px; float: right; color: #3CB397;"
							class="btnDiv4Doc" onclick="addPatient4btn()">
							<span class="btnSpan4Deg"
								style="background: url(../images/icon_add_patient_small.png) no-repeat;">添加账号</span>
						</div>
						<hr
							style="width: auto; height: 1px; border: none; margin: 46 30 0 30; position: relative; border-top: 1px solid #33A684;" />
					</div>
					<table class="listbox" border="1" cellspacing="0"
						style="margin: 18 0 18 30; width: 90%; positon: relative;"
						cellpadding="0">
						<tr style="background: #D5F5DF;">
							<td align="center"><span class="tab_b">科室名称</span></td>
							<td align="center"><span class="tab_b">用户名</span></td>
							<td align="center"><span class="tab_b">功能操作</span></td>
						</tr>
					</table>
					<div class="pageManage"
						style="width: auto; height: 24px; float: right;margin-right:30px;">
						<div class="pagePrev4Doc"></div>
						<div class="pageNum4Doc">1/1</div>
						<div class="pageNext4Doc"></div>
					</div>
				</div> -->
			</div>
	</div>
	<script>
	/* $(function(){
		new uploadPreview({UpBtn:"file_s",ImgShow: "img_preview",DivShow:"div4Img", Width: 200, Height: 80, ImgType: ["png"], callback: function () {$("#filePath").val($("#file_s").val());}});
	}); */
	function selectFile(){
		$("#file_s").trigger('click');
	}
	function uploadImg(){
		//$("#filePath").val($("#file_s").val());
		//$("#img_preview").attr('src',$("#file_s").val());v
		var hospitalName=$("#hospitalName").val();
		$.ajaxFileUpload({
			url: '../system/uploadLogo.action', //用于文件上传的服务器端请求地址
            secureuri: false, //是否需要安全协议，一般设置为false
            fileElementId: 'file_s', //文件上传域的ID
            dataType: 'json', //返回值类型 一般设置为json
            data:{'hospitalName':hospitalName},
            success: function (data, status)  //服务器成功响应处理函数
            {
            	if(data.success)layer.msg('保存成功！',{time:2000});
            	//$("#img_preview").attr('src','..'+data.imgPath);
            },
            error: function (data, status, e)//服务器响应失败处理函数
            {
                alert(e);
            }
		})
	}
	var autorefresh;
	var nowDelUserId=0;//当前要删除的账号的id
    var nowPageType=0;//现在页面类型 0：账号管理 1：系统设置
		$(document).ready(function(){
			//alert(document.body.scrollHeight);
			$(".menulist").css("height",document.body.scrollHeight-60);
			$(".mainArea").css("height",document.body.scrollHeight-60).css("width",document.body.scrollWidth-120);
			initUserManage();
			getUserListData();
		});
		window.onresize = function(){
			if(nowPageType==0){
            	
            }else if(nowPageType==1){
            	//location.reload(true);
            }
		}

		//档案管理分页相关
		var pageSize = 15; //每页显示个数
        var nowpage = 1; //当前第几页
        var totalPages = 1;
		function getUserListData(){
             $.ajax({
              type:"post",
              url:"../user/list.action",//此处返回true或false
              dataType:"json",
              data:{"pageNo":nowpage,"pageSize":pageSize},
              success:function(data){
            	  $("#userNum4Doc").text('当前'+data.total+'个账号');
                   if(data.total%pageSize != 0){
                       totalPages = parseInt(data.total/pageSize) +1;
                   }else{
                       totalPages =  parseInt(data.total/pageSize);
                   }
                   $(".pageNum4Doc").text(nowpage+'/'+totalPages);
                   //putPatientsData(data.rows);
                   //$("#header4PatientTable").append('');patientListbox
                   $(".userlistbox").empty();
                   $(".userlistbox").append('<tr style="background: #D5F5DF;">'
               			+'<td align="center"><span class="tab_b">科室名称</span></td>'
            			+'<td align="center"><span class="tab_b">用户名</span></td>'
            			+'<td align="center"><span class="tab_b">功能操作</span></td>'
            			+'</tr>');
                   console.dir(data);

                    $(data.rows).each(function(index,dom){
                     $(".userlistbox").append('  <tr >'+//style=" border-bottom:1px solid #e4ebf1"
                     '<td align="center"><span class="tab_b">'+dom.realName+'</span></td>'+
                     '<td align="center"><span class="tab_b">'+dom.userName+'</span></td>'+
                     '<td align="center" style="color:#1295FE"><span class="btnUpdateUser" '
                     +'onclick="updateUser(\''+dom.id+'\',\''+dom.realName+'\',\''+dom.userName+'\',\''+dom.userPass+'\');">修改</span>'
                     +'&nbsp;|&nbsp;<span class="btnUpdateUser" onclick="deleteUser(\''+dom.id+'\');">删除</span></td>'+
                     '</tr>');
                    })
              }
             })
         }
		
		//档案管理上一页
		function pagePrev(){
			if(nowpage == 1){
                return;
            }else{
               nowpage--;
            }
			getUserListData();
		}
		//档案管理下一页
		function pageNext(){
			if(nowpage == totalPages){
                return;
            }else{
                nowpage++;
            }
			getUserListData();
		}
		
		$("#userManage").click(function(){
			nowPageType=0;
			$("#systemSetting").removeClass("menuclicked");
			$("#userManage").addClass("menuclicked");
			$(".mainArea").empty();
			initUserManage();
			getUserListData();
		});
		function initUserManage(){
			$(".mainArea").html('<div class="pageUserManage"><div class="divheader4DocManage"><div style="font-size: 24px; color: #33A684; margin-left: 30px;'
					+' display: block; position: relative; top: 26px">账号管理&nbsp;<font id="userNum4Doc" style="font-size: 14px;" color="#666666">当前个账号</font></div>'
					+'<div style="cursor:pointer;margin-right: 30px; position: relative; right: 30px; float: right; color: #3CB397;" class="btnDiv4Doc" onclick="addUser()">'
					+'<span class="btnSpan4Deg" style="background: url(../images/icon_add_patient_small_green.png) no-repeat;">添加账号</span></div>'
					+'<hr style="width: auto; height: 1px; border: none; margin: 46 30 0 30; position: relative; border-top: 1px solid #33A684;" /></div>'
					+'<center><table class="userlistbox" border="1" cellspacing="0" style="margin: 18 0 18 30;  positon: relative;" cellpadding="0">'
					/* +'<tr style="background: #D5F5DF;">'
					+'<td align="center"><span class="tab_b">科室名称</span></td>'
					+'<td align="center"><span class="tab_b">用户名</span></td>'
					+'<td align="center"><span class="tab_b">功能操作</span></td>'
					+'</tr>' */
					+'</table>'
					+'<div class="pageManage" style="width: auto; height: 24px; float: left;margin-left:490px;">'
					+'<div class="pagePrev4Doc" onclick="pagePrev()"></div><div class="pageNum4Doc">1/1</div><div class="pageNext4Doc" onclick="pageNext()"></div></div></center>'
					+'</div>');
					$(".pageUserManage").width($(".mainArea").width()-60).height($(".mainArea").height()-60);
					//$(".userlistbox").css("width",$(".pageUserManage").width()-60);
					$(".pageManage").css('margin-left',$(".pageUserManage").width()/2+180);
		}
		
		$("#systemSetting").click(function(){
			nowPageType=1;
			$("#userManage").removeClass("menuclicked");
			$("#systemSetting").addClass("menuclicked");
			$(".mainArea").empty();
			initSystemSetting();
		});
		function initSystemSetting(){
			$(".mainArea").html('<div class="pageSystemSetting">'
					+'<div class="divheader4DocManage">'
					+'<div style="font-size: 24px; color: #33A684; margin-left: 30px; display: block; position: relative; top: 26px">系统设置</div>'
					+'<hr style="width: auto; height: 1px; border: none; margin: 46 30 0 30; position: relative; border-top: 1px solid #33A684;" /></div>'
					+'<div style="margin:0px;width:100%;height:100%;">'
					+'<span style="display:block;margin-top:26px;margin-left:60px;font-size:16px;">设置医院banner图片</span>'
					+'<table style="text-align:right;color:#666666;margin-top:16px;margin-left:70px;font-size:14px;border-collapse:separate;border-spacing:10px;" cellspacing="5">'
					+'<tr><td>医院名称</td>'
					+'<td><input id="hospitalName" type="text" style="border:1px solid #999999;border-radius:12px;padding-left:5px;display:block;width:310px;height:24px;"></td></tr>'
					+'<tr><td>选择图片</td>'
					+'<td><input id="filePath" readonly="true" type="text" style="border:1px solid #999999;border-radius:12px;padding-left:5px;display:block;width:310px;height:24px;"></td>'
					+'<td><div onclick="selectFile();" style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #666666;width:78px;height:22px;float:left;color:#666666">选择</div><input type="file" id="file_s" name="file" size="8" style="width:75px;opacity:0;display:none;"></td>'
					+'<td></td></tr>'//<div style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #33A684;width:78px;height:22px;float:left;color:#33A684">上传</div>
					+'<tr><td>要求</td><td colspan="3" style="text-align:left;color:#999999;">图片需要为白色内容、含透明通道的.png格式图片，尺寸要求控制在宽400像</td></tr>'
					+'<tr><td></td><td colspan="3" style="text-align:left;color:#999999;">素×高80像素。</td></tr>'
					+'<tr><td valign="top">预览</td><td colspan="3" style="width:500px;height:120px;background:#35597E;"><div id="div4Img"><img id="img_preview" data-src="400x80" alt="banner/logo" src="" class="img-rounded" style="line-height:80px;font-size:30px;color:#FFFFFF;text-align:center;width: 400px; height: 80px;margin-left:50px;float:left;"></div></td></tr>'
					+'<tr><td colspan="3"><div onclick="$(\'#systemSetting\').trigger(\'click\');" style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #666666;width:78px;height:22px;float:right;color:#666666">取消</div></td>'
					+'<td><div onclick="uploadImg()" style="line-height:22px;text-align:center;border-radius:12px;border:1px solid #33A684;width:78px;height:22px;float:left;color:#33A684">保存</div></td>'
					+'</tr></table></div></div>');
			$(".pageSystemSetting").width($(".mainArea").width()-60).height($(".mainArea").height()-60);
			new uploadPreview({UpBtn:"file_s",ImgShow: "img_preview",DivShow:"div4Img", Width: 200, Height: 80, ImgType: ["png"], callback: function () {$("#filePath").val($("#file_s").val());}});
		}
		
		function getClientHeight(){
			return  document.body.scrollHeight > document.documentElement.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight;
		}
		function getClientWidth(){
			return  document.body.scrollWidth > document.documentElement.scrollWidth?document.body.scrollWidth:document.documentElement.scrollWidth;
		}
		
		var oldPWD='';
		var winType=0;//0表示添加账号，1表示修改账号
		var Id4updateUser='';
		function addUser(){
			winType=0;
			$("#titie4AddOrUpdateUser").text("添加账号");
			$("#realName4AddUser").val('');
			$("#userName4AddUser").val('');
			$("#pwd4AddUser").val('');
			$("#surepwd4AddUser").val('');
			$(".modelbg").show();
			$(".addUserBox").show();	
		}
		function sureAddUser(){
			var realName=$("#realName4AddUser").val().trim();
			var userName=$("#userName4AddUser").val().trim();
			var userPass=$("#pwd4AddUser").val().trim();
			var surePWD=$("#surepwd4AddUser").val().trim();
			if(realName==""||userName==""||userPass==""){
				layer.alert("请将参数填写完整！");
				return;
			}
			if(userPass!=surePWD){
				layer.alert("两次输入密码不一致！");
				return;
			}
			if(winType==0){
				$.post('save.action',{realName:realName,userName:userName,userPass:userPass},function(data,status){
					if(!JSON.parse(data).success)layer.alert('该用户名已经存在！');
					if(status=="success"){
						getUserListData();
					}
				});
			}else{
				var paras={id:Id4updateUser,realName:realName,userName:userName,userPass:userPass};
				$.post('save.action',paras,function(data,status){
					if(!JSON.parse(data).success)layer.alert('修改账号信息失败！');
					if(status=="success"){
						getUserListData();
					}
				});
			}
			
		}
		function updateUser(userId,realName,userName,userPass){
			winType=1;
			Id4updateUser=userId;
			$("#titie4AddOrUpdateUser").text("修改账号");
			$("#realName4AddUser").val(realName);
			$("#userName4AddUser").val(userName);
			$("#pwd4AddUser").val(userPass);
			$("#surepwd4AddUser").val(userPass);
			oldPWD=userPass;
			$(".modelbg").show();
			$(".addUserBox").show();	
		}
		function deleteUser(userId){
			if(userId==1){
				layer.alert('不能删除该默认管理员账户！');
				return;
			}
			nowDelUserId=userId;
			var index = layer.open({
			    type: 2,
			    area: ['360px', '240px'],
			    fix: true, //不固定
			    maxmin: false,
			    content: ['../static/layerHtml/delUserConfirm.html', 'no'],
			    scrollbar: false,
			    closeBtn: 0,
			    title:false,
			    shade: [0.5,'#000000'] //0.1透明度的白色背景
			}); 
			layer.style(index, {
				'border-radius': '4px'
			});
		}
		function sureDelUser(){
			$.ajax({
				type:"post",
				url:"deleteUserById.action",//deleteUserById.action
				dataType:"json",
	            data:{id:nowDelUserId},
	            success:function(data){
	               getUserListData();
	            }
			});
		}

		//添加病床
		function addbed(){
/* 			alert(nowaddBed);
			alert($(obj).prop("className"));return; */
			$.ajax({
				type:"post",
				url:"add.action",//"tiwen/addbed.json",
				dataType:"json",
				data:{"bedid":$(".addbedid").val()},
				success:function(data){
					if(data.success == false){ 
						alert("添加失败，请确认要添加的病床号没有重复！");
						return;
					}
					getBedsData();return;
					$(".addbedbtn").before('<li class="bedLi" bedName="'+data.bedid+'" id="'+data.idofbed+'" patientid=""><div class="addPatient4bed" onclick="addPatient(this)" bedName="'+data.bedid+'" id="'+data.bedid+'"><span class="addPatientSpan">'+
							'患者入住</span></div><div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">'+
							'床位号：</span><span class="span4BedNum">'+data.bedid+'</span></div><div><div class="smallbtnDelInEmptyBed4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
				},
				error:function(data){alert("添加失败");}
			})
		}
	</script>
	<div class="modelbg"></div>
	<div class="addUserBox">
	    <div style="width: 320px; height: 34px;margin:26 20 0 20;">
		<span id="titie4AddOrUpdateUser" style="margin-left:0px;font-weight: bold; font-size: 18px; color: #3CB397">添加账号</span>
		<hr style="width: 320px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
	    </div>
		<div class="text" >
			<table class="persontab" style="margin-top:10px;margin-left:30px;text-align:right;font-size: 14px;color:#666666;">
				<tr><td style="margin-left:30px;font-size: 14px;color:#666666;">科室名称&nbsp;&nbsp;</td><td><input placeholder="科室名称" class="realName" id="realName4AddUser" type="text"></td></tr>
				<tr><td style="margin-left:30px;font-size: 14px;color:#666666;">用户名&nbsp;&nbsp;</td><td><input placeholder="用户名" class="userName" id="userName4AddUser" type="text"></td></tr>
				<tr><td style="margin-left:30px;font-size: 14px;color:#666666;">密码&nbsp;&nbsp;</td><td><input placeholder="密码" class="userPass" id="pwd4AddUser" type="password"></td></tr>
				<tr><td style="margin-left:30px;font-size: 14px;color:#666666;">确认密码&nbsp;&nbsp;</td><td><input placeholder="确认密码" class="userPass" id="surepwd4AddUser" type="password"></td></tr>
			</table>
		</div>
		<div onclick="$('.modelbg').hide();$('.addUserBox').hide();" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;
				position:relative;margin-left:162px;margin-top:15px;padding-top:4px;">取消</div>
			<div id="yes33" onclick="$('.modelbg').hide();$('.addUserBox').hide();sureAddUser();" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;
				position:relative;margin-left:252px;margin-top:-26px;padding-top:4px;float:left">保存</div>
	</div>
</body>
</html>