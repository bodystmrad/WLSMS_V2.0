<%@page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/common/taglib.jsp"%>
	<div class="page">
		<c:if test="${resultPage.pageNo>1 }">
			<a href="javascript:void(0)" onclick="changePage(1)">首页</a>
		</c:if>
		<c:if test="${resultPage.hasPre }">
			<a href="javascript:void(0)"
				onclick="changePage(${resultPage.pageNo-1})">上一页</a>
		</c:if>
		<c:set var="startPage" value="1" scope="request"></c:set>
		<c:set var="endPage" value="${resultPage.totalPages}" scope="request"></c:set>
		<c:set var="distinctPage" value="4" scope="request"></c:set>
		<c:if test="${resultPage.totalPages>2*requestScope.distinctPage }">
			<c:choose>
				<c:when test="${resultPage.pageNo<requestScope.distinctPage }">
					<c:set var="endPage" value="${2*requestScope.distinctPage }"
						scope="request"></c:set>
				</c:when>
				<c:when
					test="${resultPage.totalPages-page.pageNo<requestScope.distinctPage }">
					<c:set var="startPage"
						value="${resultPage.totalPages-2*requestScope.distinctPage }"
						scope="request"></c:set>
				</c:when>
				<c:otherwise>
					<c:set var="startPage" value="${resultPage.pageNo-4 }"
						scope="request"></c:set>
					<c:set var="endPage" value="${resultPage.pageNo+3 }"
						scope="request"></c:set>
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${endPage==0 }">
			<c:set var="endPage" value="1" scope="request"></c:set>
		</c:if>
		<c:forEach var="step" begin="${requestScope.startPage }"
			end="${requestScope.endPage }">
			<c:if test="${step==resultPage.pageNo }">
				<a href="#"><b>${step}</b></a>
			</c:if>
			<c:if test="${step!=resultPage.pageNo }">
				<a href="javascript:void(0)" onclick="changePage('${step}')">${step }</a>
			</c:if>
		</c:forEach>
		<c:if test="${resultPage.hasNext }">
			<a href="javascript:void(0)"
				onclick="changePage('${resultPage.pageNo+1}')">下一页</a>
		</c:if>
		<c:if
			test="${resultPage.pageNo!=resultPage.totalPages&&resultPage.totalPages>1}">
			<a href="javascript:void(0)"
				onclick="changePage('${resultPage.totalPages}')">尾页</a>
		</c:if>
		<!-- 
		到第<INPUT title="输入数字，回车跳转" type="text" value="1">页 <span class="qued"><a href="#"><img src="${ctx }/static/images/qued.png" /></a></span>
		 -->
	</Div>
