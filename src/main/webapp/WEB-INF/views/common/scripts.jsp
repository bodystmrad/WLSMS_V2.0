<%@page contentType="text/html;charset=UTF-8"%>
<!-- easyUI相关 -->
<link rel="stylesheet" type="text/css" href="pcJs/themes/default/easyui.css" rel="stylesheet" title="blue">
<link rel="stylesheet" type="text/css" href="pcJs/themes/icon.css">
<link rel="stylesheet" type="text/css" href="pcJs/uploadify/uploadify.css">
<link rel="stylesheet" type="text/css" href="pcCss/commons.css">
<script type="text/javascript" src="pcJs/jquery.min.js"></script>
<script type="text/javascript" src="pcJs/jquery.easyui.min.js"></script>
<script type="text/javascript" src="pcJs/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="pcJs/uploadify/swfobject.js"></script>
<script type="text/javascript" src="pcJs/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="pcJs/common.js"></script>