<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<title>实时体温数据图</title>
<%@ include file="../common/script.jsp"%>
<script type="text/javascript" src="${ctx }/highcharts/highcharts.js"></script>
<script type="text/javascript" src="${ctx }/highcharts/exporting.js"></script>
<script>
$(function () {
	queryData();
});
/*定义图表*/  
function showChart(xset,yset,lowSet,highSet){
    var chart = new Highcharts.Chart({
        chart: {  
            renderTo: 'linecontainer',  
            type: 'line',
            <c:if test="${patient.endTime==null}">
            events : {
				load : function() {
					// set up the updating of the chart each second
					var series = this['series'];
					var newParams={'lastTime':'','equipNo':'${patient.equipNo}'};
					setInterval(function() {
						$.post('newPoint.action',newParams,function(data,statu){
							if(statu=='success'){
								if(data.data.length>0){
									var x=data.data[0];
									var y=data.data[1];
									
									newParams['lastTime']=x;
									series[0].addPoint([x,y],true,true);
									series[1].addPoint([x,37.5],true,true);
									series[2].addPoint([x,38.5],true,true);
								}
							}
						})
					}, 1000);
				}
			}
            </c:if>
        },  
        title:{
        	text:'实时体温数据图'
        },
        xAxis: {
            categories: xset,
            labels: {rotation: -50, align: 'right', style: { font: 'normal 10px Verdana, sans-serif'}}
        },
        yAxis: {  
            title: {  
                text: '温度 (°C)'  
            },
            plotLines: [{  
                value: 0,  
                width: 1,  
                color: '#808080'  
            }]  
        },  
        tooltip: {  
            formatter: function() {  
                    return '<b>'+ this.series.name +'</b><br/>'+  
                    this.x +': '+ this.y;  
            }  
        },  
        legend: {  
            layout: 'vertical',  
            align: 'right',  
            verticalAlign: 'top',  
            x: -10,  
            y: 100,  
            borderWidth: 0  
        },  
        series: [{  
            name: '体温数据',  
            data: yset  
        },{
        	name:'低烧线',
        	data:lowSet
        },{
        	name:'高烧线',
        	data:highSet
        }]  
    });  
} 
function queryData(){
	var params={'equipNo':'${param.equipNo}','rows':20};
	var timeInfo=$("#query_startTime").val();
	if(timeInfo){
		params['startTime']=timeInfo;
	}
	timeInfo=$("#query_endTime").val();
	if(timeInfo){
		params['startTime']=timeInfo;
	}
	$.post('query.action',params,function(result){
        if (result.success){
        	var xset = [];//X轴数据集  
            var yset = [];//Y轴数据集
            var lowSet=[];
            var highSet=[];
            var point;
        	$.each(result.data,function(i,item){  
                $.each(item,function(k,v){
                    xset.push(k);
                    if(v>38.5){
                    	point={
                    			y:v,
                    			'color':'#FF6600'
                    	};
                    	yset.push(point);
                    }else if(v>37.5){
                    	point={
                    			y:v,
                    			'color':'#FF0000'
                    	};
                    	yset.push(point);
                    }
                    else{
                    	yset.push(v);
                    }
                    lowSet.push(37.5);
                    highSet.push(38.5)
                });
            })
            showChart(xset,yset,lowSet,highSet);
        } else {
        	$.messager.show({ // show error message
        		title: '错误',
        		msg: result.errorMsg
        	});
        }
    },'json');
}
function printData(){
	window.open('print.action');
}
function exportData(){
	window.open('export.action?id=${param.id}&distance=7');
}
</script>
</head>
<body>
	<div id="toolbar">
		<span style="font-size: 12px">开始时间:</span>
		<input class="f1 easyui-datetimebox" name="startTime" id="query_startTime" />&nbsp;&nbsp;-&nbsp;&nbsp;
		<span style="font-size: 12px">结束时间:</span>
		<input class="f1 easyui-datetimebox" name="endTime" id="query_endTime" />
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" onclick="queryData()">查询</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" onclick="exportData()">体温表</a>
	</div>
	<div id="linecontainer" style="min-width: 700px; height: 400px"></div>
</body>
</html>