<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<title>医用体温表格</title>
<%@ include file="../common/script.jsp"%>
<style type="text/css">
.data-table{
	width:980px;
}
.data-table table{
	width: 100%;
	BORDER-COLLAPSE: collapse;
	font-size:12px;
}
.data-table table td{
	BORDER-COLLAPSE: collapse;
	font-size:12px;
}
</style>
</head>
<body style="font-size: 13px;">
<a href="javascript:void(0)" class="easyui-linkbutton" onclick="preview2(1);" iconCls="icon-print">打印</a>
<a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="preview(1)">打印预览</a>
<br/>
<!--startprint1-->
<canvas id="c1" width="993" height="1008"></canvas>
<!--endprint1-->
<script type="text/javascript">
$(function(){
	//javascript:window.print();
	var oC =document.getElementById('c1');
	var oGC = oC.getContext('2d');

	var yImg = new Image();
	yImg.onload = function(){
		draw(this);

		oGC.font = '12px arial';
		//oGC.fillText('${hospitalName }', 500,6);
		oGC.fillText('${patient.name }', 50,36);
		oGC.fillText('${patient.sex }', 300,36);
		oGC.fillText('${patient.age }', 540,36);
		oGC.fillText('<fmt:formatDate value="${patient.startTime }" pattern="yyyy-MM-dd"/>', 810,36);

		oGC.fillText('${user_attr.realName }', 50,58);
		oGC.fillText('${bed.name }', 300,58);
		oGC.fillText('<fmt:formatDate value="${patient.startTime }" pattern="yyyyMMddHHmmss"/>', 553,58);
		
		var xval=140;
		<c:forEach items="${resultDateList }" var="st">
		oGC.fillText('${st }', xval,79);
		xval+=125;
		</c:forEach>
		
		var baseX=115,baseY=145;
		var x,y;
		var lineArr=new Array();
		var index=0;
		<c:forEach var="xval" begin="0" end="41">
			<c:forEach var="yval" begin="0" end="42">
				<c:if test="${resultArray[xval][yval]==1 }">
				//画一个实心圆
				<c:choose>
				<c:when test="${xval==1}">
				x=baseX+${xval}*23;
				</c:when>
				<c:otherwise>
				x=baseX+${xval}*21;
				</c:otherwise>
				</c:choose>
				
				<c:choose>
				<c:when test="${yval==1}">
				y=baseY+${yval}*18;
				</c:when>
				<c:otherwise>
				y=baseY+${yval}*16.1;
				</c:otherwise>
				</c:choose>
				
				oGC.beginPath();
				//oGC.arc(x,y,5,0,360,false);
				oGC.fillText('X',x-4,y+4);
				oGC.fill();
				lineArr[index++]=[x,y]
				</c:if>
			</c:forEach>
		</c:forEach>
		oGC.closePath();

		oGC.lineWidth = 1;//设置线宽
		oGC.beginPath();
		for(var i=0;i<lineArr.length;i++){
			if(i==0){
				oGC.moveTo(lineArr[i][0],lineArr[i][1]);
			}
			else{
				oGC.lineTo(lineArr[i][0],lineArr[i][1]);
			}
		}
		//oGC.closePath();//可以把这句注释掉再运行比较下不同
		oGC.stroke();//画线框
	};
	yImg.src = '${ctx}/images/tiwen.png';
	function draw(obj){
		oGC.drawImage(obj,0,0);
	}
});

function preview2(oper)         
{  
	if (oper < 10)  
	{  
		$('a').css('display','none');
		window.print();
		$('a').css('display','inline-block');
		return;
		bdhtml=window.document.body.innerHTML;//获取当前页的html代码  
		sprnstr="<!--startprint"+oper+"-->";//设置打印开始区域  
		eprnstr="<!--endprint"+oper+"-->";//设置打印结束区域  
		prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr)+18); //从开始代码向后取html  
  
		prnhtmlprnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));//从结束代码向前取html  
		window.document.body.innerHTML=prnhtml;  
		alert(prnhtml);
		window.print();  
		window.document.body.innerHTML=bdhtml;  
	} else {  
		window.print();  
	}  
}  
function preview(oper)         
{
	var canvas =document.getElementById('c1');
    //生成base64图片数据  
    var dataUrl = canvas.toDataURL();  
    var newImg = document.createElement("img");  
    newImg.src = dataUrl;  
    /* document.body.appendChild(newImg);  */  
    /* window.open(newImg.src); */  
    var printWindow = window.open(newImg.src);  
    // printWindow.document.write();   
    printWindow.document.write('<img src="'+newImg.src+'" />')  
    printWindow.print();  
}  
</script>
</body>
</html>