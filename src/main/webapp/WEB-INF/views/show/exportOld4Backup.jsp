<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<title>医用体温表格</title>
<%@ include file="../common/script.jsp"%>
<style type="text/css">
.data-table{
	width:980px;
}
.data-table table{
	width: 100%;
	BORDER-COLLAPSE: collapse;
	font-size:12px;
}
.data-table table td{
	BORDER-COLLAPSE: collapse;
	font-size:12px;
}
</style>
</head>
<body style="font-size: 13px;">
<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:window.print();" iconCls="icon-print">打印</a>
<a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-print" onclick="preview(1)">打印预览</a>
<!--startprint1-->
<table style="width:980px;">
	<tr>
		<td colspan="4" align="center"><strong>体&nbsp;&nbsp;&nbsp;&nbsp;温&nbsp;&nbsp;&nbsp;&nbsp;表</strong></td>
	</tr>
	<tr>
		<td width="25%">姓名：${patient.name }</td>
		<td width="25%">性别：${patient.sex }</td>
		<td width="25%">年龄：${patient.age }</td>
		<td width="25%">入院日期：<fmt:formatDate value="${patient.startTime }" pattern="yyyy-MM-dd"/></td>
	</tr>
	<tr>
		<td width="25%">科室: ${user_attr.realName }</td>
		<td width="25%">床号：${bed.name }</td>
		<td colspan="2">住院号：<fmt:formatDate value="${patient.startTime }" pattern="yyyyMMddHHmmss"/></td>
	</tr>
</table>
<div class="data-table">
<table style="width:100%;text-align: center" border="1" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3">日期</td>
		<c:forEach items="${resultDateList }" var="st">
		<td colspan="6">${st }</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">住院日数</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			${ind.count }
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">术后/产后日数</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td rowspan="44">
			<table style="height:100%;border: 0px">
				<tr><td style="height:60px">肛<br/>表<br/>O</td></tr>
				<tr><td style="height:102px">口<br/>表<br/>●</td></tr>
				<tr><td style="height:102px">腋<br/>表<br/>X</td></tr>
				<tr><td style="height:102px">心<br/>率<br/>O</td></tr>
				<tr><td style="height:102px">脉<br/>博<br/>●</td></tr>
				<tr><td style="height:178px"></td></tr>
			</table>
		</td>
		<td rowspan="4" valign="middle" style="border-bottom: 0px;">
				脉<br/>博<br/>次<br/>分
		</td>
		<td rowspan="4" valign="middle" style="border-bottom: 0px;">
				体<br/>温<br/>℃
		</td>
	<c:forEach var="i" begin="1" end="${distance }">
						<td>02</td>
						<td>06</td>
						<td>10</td>
						<td>14</td>
						<td>18</td>
						<td>22</td>
					</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="1" end="${6*distance }">
						<td>&nbsp;</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="1" end="${6*distance }">
						<td>&nbsp;</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="1" end="${6*distance }">
						<td>&nbsp;</td>
		</c:forEach>
	</tr>
	<c:forEach var="tw" begin="0" end="7">
	<tr>
		<td rowspan="5" valign="top" style="border-bottom: 0px;border-top: 0px;">${180-tw*20 }</td>
		<td rowspan="5" valign="top" style="border-bottom: 0px;border-top: 0px;">${42-tw }</td>
		<c:forEach var="i" begin="0" end="${6*distance-1 }">
		<td>
		<c:choose>
			<c:when test="${resultArray[i][tw*5]==1 }">
				●
			</c:when>
			<c:otherwise>
				&nbsp;
			</c:otherwise>
		</c:choose>
		</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="0" end="${6*distance-1 }">
		<td>
		<c:choose>
			<c:when test="${resultArray[i][tw*5+1]==1 }">
				●
			</c:when>
			<c:otherwise>
				&nbsp;
			</c:otherwise>
		</c:choose>
		</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="0" end="${6*distance-1 }">
		<td>
		<c:choose>
			<c:when test="${resultArray[i][tw*5+2]==1 }">
				●
			</c:when>
			<c:otherwise>
				&nbsp;
			</c:otherwise>
		</c:choose>
		</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="0" end="${6*distance-1 }">
		<td>
		<c:choose>
			<c:when test="${resultArray[i][tw*5+3]==1 }">
				●
			</c:when>
			<c:otherwise>
				&nbsp;
			</c:otherwise>
		</c:choose>
		</td>
		</c:forEach>
	</tr>
	<tr>
		<c:forEach var="i" begin="0" end="${6*distance-1 }">
		<td>
		<c:choose>
			<c:when test="${resultArray[i][tw*5+4]==1 }">
				●
			</c:when>
			<c:otherwise>
				&nbsp;
			</c:otherwise>
		</c:choose>
		</td>
		</c:forEach>
	</tr>
	</c:forEach>
	<tr>
		<td colspan="3" width="10%">呼吸</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">血压(mmHg)</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">体重(kg)</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">尿量(ml/次)</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">大便(次)</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%">身高(cm)</td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%"></td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%"></td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%"></td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
	<tr>
		<td colspan="3" width="10%"></td>
		<c:forEach items="${resultDateList }" var="st" varStatus="ind">
		<td colspan="6">
			&nbsp;
		</td>
		</c:forEach>
	</tr>
</table>
</div>
<!--endprint1-->
<script type="text/javascript">
function preview(oper)         
{  
	if (oper < 10)  
	{  
		bdhtml=window.document.body.innerHTML;//获取当前页的html代码  
		sprnstr="<!--startprint"+oper+"-->";//设置打印开始区域  
		eprnstr="<!--endprint"+oper+"-->";//设置打印结束区域  
		prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr)+18); //从开始代码向后取html  
  
		prnhtmlprnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));//从结束代码向前取html  
		window.document.body.innerHTML=prnhtml;  
		window.print();  
		window.document.body.innerHTML=bdhtml;  
	} else {  
		window.print();  
	}  
}  
</script>
</body>
</html>