<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>WLSMS</title>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/bed/index.css">
<link rel="stylesheet" href="${ctx}/static/js/layui-v2.1.7/css/layui.css" media="all">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<link rel="shortcut icon" href="../images/c_temperature.png"/>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/degreelisten3.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css"> 
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<%-- <script src="${ctx }/highcharts/highstock-all.js"></script> --%>
<%-- <link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css"> 
<script src="${ctx }/highcharts/modules/exporting.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/static/js/layer/layer.js"></script> --%>
<script src="${ctx }/static/js/bed/bed.js"></script><!-- _20180408 -->
<%-- <script src="${ctx }/static/js/chartManage.js"></script> --%>
<script src="${ctx }/static/js/echarts.min.js"></script>
<script src="${ctx }/static/js/commontool.js"></script>
<script src="${ctx }/webapp/highcharts/highcharts.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/static/js/layui-v2.1.7/layui.js"></script>
<script type="text/javascript" src="${ctx}/static/js/layui-v2.1.7/layui.all.js"></script>
<script type="text/javascript" src="${ctx}/static/js/bed/commonUtil.js"></script>
<style>
.leftdiv{  
    float:left;  
    width:1000px;  
}  

.centerdiv{  
    float:left;  
    width:20px;  
    border-right: 1px solid black;  
    padding-bottom:1000px;  /*关键*/  
    margin-bottom:-1200px;  /*关键*/  
}  
</style>
</head>
<body class="layui-layout-body">
<audio id="snd" src="" autoplay="true" width=0  height=0 loop="true"></audio>
<div class="layui-layout layui-layout-admin">
  <div class="layui-header layui-bg-cyan">
    <div class="layui-logo" style="font-size:24px;width:300px">无线生命体征监测客户端</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left" style="left: 300px;">
      <li class="layui-nav-item layui-this"><a href="index.action" style="font-size:16px"><img src="${ctx }/images/icon_temperature_monitor.png" alt="体征监测">&nbsp;体征监测</a></li>
      <%-- <li class="layui-nav-item"><a href="index.action" style="font-size:16px"><img src="${ctx }/images/icon_files_manage.png" alt="档案管理">&nbsp;档案管理</a></li> --%>
      <!-- <li class="layui-nav-item">
	    <a href="javascript:;">解决方案</a>
	    <dl class="layui-nav-child">
	      <dd><a href="">移动模块</a></dd>
	      <dd><a href="">后台模版</a></dd>
	      <dd class="layui-this"><a href="">选中项</a></dd>
	      <dd><a href="">电商平台</a></dd>
	    </dl>
	  </li> -->
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item">
        <a href="javascript:;"><img src="${ctx }/images/user-icon.jpg" class="layui-nav-img">${user_attr.realName }</a>
        <!-- <dl class="layui-nav-child">
          <dd><a href="">基本资料</a></dd>
          <dd><a href="">安全设置</a></dd>
        </dl> -->
      </li>
      <li class="layui-nav-item"><a href="../home/logOut.action">退出</a></li>
    </ul>
  </div>
  
  <!-- <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      左侧导航区域（可配合layui已有的垂直导航）
      <ul class="layui-nav layui-nav-tree"  lay-filter="test">
        <li class="layui-nav-item layui-nav-itemed">
          <a class="" href="javascript:;">所有商品</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">列表一</a></dd>
            <dd><a href="javascript:;">列表二</a></dd>
            <dd><a href="javascript:;">列表三</a></dd>
            <dd><a href="">超链接</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">解决方案</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">列表一</a></dd>
            <dd><a href="javascript:;">列表二</a></dd>
            <dd><a href="">超链接</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item"><a href="">云市场</a></li>
        <li class="layui-nav-item"><a href="">发布商品</a></li>
      </ul>
    </div>
  </div> -->
  
  <div class="layui-body" style="left:0px;background:#3a3a42;s">
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
    	<ul id="bedList" style="list-style:none;padding:0px;margin:0px;">
			<%-- <li>
				<div class="bedDiv">
					<div class="bedDiv1">
						<div class="bedNameDiv">
							<span>孙悟空-806床</span>
						</div>
						<div class="bedStatusDiv">
							<ul>
								<li><div class="statusDiv">
									<img class="statusIcon" src="${ctx }/images/i.png">
									<span>病人异常</span>
								</div></li>
							</ul>
						</div>
					</div>
					<div class="bedDiv2" id="chartDiv">
						
					</div>
					<div class="bedDiv3">
						<div class="indexDiv"><span class="indexValue breathRate">167</span><span class="unit">RPM</span></div>
					</div>
					<div class="bedDiv4">
						<div class="indexDiv"><span class="indexValue temperature">36.89</span><span class="unit">℃</span></div>	
					</div>
					<div class="bedDiv5">
						<div class="indexDiv"><span class="indexValue bloodPressure">200/100</span><span class="unit">mmHg</span></div>
					</div>
					<div class="bedDiv6">
						<div class="indexDiv"><span class="indexValue spo2">99</span><span class="unit">%</span></div>
					</div>
					<div class="bedDiv7">
						<div class="indexDiv"><span class="indexValue heartRate">82</span><span class="unit">RPM</span></div>
					</div>
				</div>
			</li> --%>
			<c:forEach items="${bedList}" var="item" varStatus="status">
				<%-- <c:if test="${item.status=='empty' }"></c:if> --%>
				<c:choose>
				   <c:when test="${item.status=='empty' }">  
  						<li>
							<div class="bedDiv" bedIndex="${status.index }" bedid="${item.id}"  status="empty" mac="${item.concentratorNo}">
								<div class="bedDiv1">
									<div class="bedNameDiv">
										<!-- <span onclick="QueryLook()">查看</span> -->
										<span>${item.name}</span>
									</div>
									<div class="bedStatusDiv">
										<span onclick="addPatient(this)" class="btnAddPatient">病人入住</span>
									</div>
								</div>
								<div class="bedDiv2 chartDiv" id="">
									<canvas class="ecgCanvas"></canvas>
								</div>
								<div class="ecgChannel" style="display:none;position:absolute;color:#00FF00">
									 <select>
									 	<option value="0">Ⅰ</option>
									 	<option value="1">Ⅱ</option>
									 	<option value="2">Ⅴ</option>
									 </select>
								</div>
								<div class="ecgGain" style="display:none;position:absolute;color:#00FF00">
									<select>
										 	<option value="0">1/1</option>
										 	<option value="1" selected>1/2</option>
										 	<option value="2">1/4</option>
										 </select>
								</div>
								<div class="sign1mv" style="display:none;position:absolute;margin-left:85mm;margin-top:5px;color:#00FF00;font-size:10mm"><span style="font-size:12px;margin-top:0px;display:block">1mv</span><div class="flag41mv"></div></div>
								<div class="bedDiv3">
									<div class="indexDiv"><span class="indexValue breathRate">--</span><span class="unit">RPM</span></div>
								</div>
								<div class="bedDiv4">
									<div class="indexDiv"><span class="indexValue temperature">--</span><span class="unit">℃</span></div>	
								</div>
								<div class="bedDiv5">
									<div class="indexDiv">
									<span class="bpUpdateTime">--:--</span>
									<span class="indexValue bloodPressure">--/--</span><span class="unit">mmHg</span></div>
								</div>
								<div class="bedDiv6">
									<div class="indexDiv"><span class="indexValue spo2">--</span><span class="unit">%</span></div>
								</div>
								<div class="bedDiv7">
									<div class="indexDiv"><span class="indexValue heartRate">--</span><span class="unit">BPM</span></div>
								</div>
						</div>
						</li>
				   </c:when>
				   <c:otherwise> 
				   		<li>
							<div class="bedDiv" bedIndex="${status.index }"  bedid="${item.id}"  mac="${item.concentratorNo}">
							<div class="bedDiv1">
								<div class="bedNameDiv">
									<!-- <span onclick="QueryLook()">查看</span> -->
									<span class="patientNameSpan" patientId="${item.patient.id}" startTime="${item.patient.listenStartTime}">${item.patient.name}</span>
									<span>/</span>
									<span class="bedNameSpan">${item.name}</span>
								</div>
								<div class="bedStatusDiv">
									<ul>
										<%-- <li><div class="statusDiv">
											<img class="statusIcon" src="${ctx }/images/i.png">
											<span>病人异常</span>
										</div></li>
										<li><div class="statusDiv">
											<img class="statusIcon" src="${ctx }/images/i.png">
											<span>病人异常</span>
										</div></li>
										<li><div class="statusDiv">
											<img class="statusIcon" src="${ctx }/images/i.png">
											<span>病人异常</span>
										</div></li> --%>
									</ul>
								</div>
							</div>
							<div class="bedDiv2 chartDiv" id="">
								<canvas class="ecgCanvas"></canvas>
							</div>
							<div class="ecgChannel" style="position:absolute;color:#00FF00">
								<select>
									 	<option value="0">Ⅰ</option>
									 	<option value="1">Ⅱ</option>
									 	<option value="2">Ⅴ</option>
									 </select>
							</div>
							<div class="ecgGain" style="position:absolute;color:#00FF00">
								<select>
									 	<option value="0">1/1</option>
									 	<option value="1" selected>1/2</option>
									 	<option value="2">1/4</option>
									 </select>
							</div> 
							<div class="sign1mv" style="position:absolute;margin-left:85mm;margin-top:5px;color:#00FF00;font-size:10mm"><span style="font-size:12px;margin-top:0px;display:block">1mv</span><div class="flag41mv"></div></div>
							<div class="bedDiv3">
								<div class="indexDiv"><span class="indexValue breathRate">--<%-- ${item.ecg.resp} --%></span><span class="unit">RPM</span></div>
							</div>
							<div class="bedDiv4">
								<div class="indexDiv"><span class="indexValue temperature">--<%-- ${item.temperature.measureNum} --%></span><span class="unit">℃</span></div>	
							</div>
							<div class="bedDiv5">
								<div class="indexDiv">
								<span class="bpUpdateTime" title="">--:--</span>
								<span class="indexValue bloodPressure">--/--<%-- ${item.bloodPressure.highPressure}/${item.bloodPressure.lowPressure} --%></span>
								<span class="unit">mmHg</span>
								</div>
							</div>
							<div class="bedDiv6">
								<div class="indexDiv"><span class="indexValue spo2">--<%-- ${item.oximetry.spo2} --%></span><span class="unit">%</span></div>
							</div>
							<div class="bedDiv7">
								<div class="indexDiv"><span class="indexValue heartRate">--<%-- ${item.oximetry.pulserate} --%></span><span class="unit">BPM</span></div>
							</div>
							
						</div>
						</li>
				   </c:otherwise>
				</c:choose>
				
				
			</c:forEach>
     	</ul>
     	<!-- 数据回顾 -->
     	<div id="container">
     		<span style="text-align: center;display:block;font-size:30px">25床患者</span>
     		<div class="leftdiv">
				<div id="containerHR" style="width: 950px; height: 200px; float:left;"></div>
				<div id="containerSPO2" style="width: 950px; height: 200px; float:left;"></div>
				<div id="containerRR" style="width: 950px; height: 200px; float:left;"></div>
				<div id="containerNBP" style="width: 950px; height: 200px; float:left;"></div>
				<div id="containerTEMP" style="width: 950px; height: 200px; float:left;"></div>
			</div>
			<div class="centerdiv"></div>
			<div style="text-align: center;">
				<span style="font-weight:bold">起始时间：</span>
                <input name="roomtime" id="startTime" style=" cursor: pointer" class="Wdate" type="text"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:false})">
			</div>
			
			<div style="text-align: center;margin-top:20px">
				<span style="font-weight:bold">结束时间：</span>
                <input name="roomtime" id="startTime" style=" cursor: pointer" class="Wdate" type="text"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:false})">
			</div>
			
			<div style="text-align: center;margin-top:20px">
				<span style="font-weight:bold;padding-left:10px">分辨率：</span>
               	<select  style="width:165px;padding-left:20px">
				  <option value ="volvo">1 min</option>
				</select>
			</div>
			
			<div style="text-align: center;margin-top:20px">
				<span style="font-weight:bold;">视图模式：</span>
               	<select id="test" style="width:165px;padding-left:20px" onchange="queryvolvo()">
				  <option value ="zhe">折线图</option>
				  <option value ="tong">统计表</option>
				</select>
			</div>
			
			<div style="text-align: center;margin-top:20px">
				<span style="font-weight:bold;">浏览模式：</span>
               	<select  style="width:165px;padding-left:20px">
				  <option value ="volvo">滑动（鼠标点击拖动）</option>	 
				</select>
			</div>
			
			<div style="text-align: center;margin-top:20px">
				<span style="font-weight:bold;">打印记录：</span>
				<input style="width:165px;" type="submit" value="打印" />
			</div>
		</div>
	   	<!-- 数据回放-表格 -->
	   	<div id="StatisticalChart">
     		<table class="layui-hide" id="Statistics" ></table>
		</div>
    </div>
  </div>
  
  <div class="layui-footer" style="left:0px;color:black;height:44px">
    <!-- 底部固定区域 -->
    © www.ptorigin.com.cn - 铂元智能科技
  </div>
</div>
<script>
//0表示显示脉搏，1表示显示心率
var showPulseOrHr="${showPulseOrHr}";
var deviceCaps="${deviceCaps}";
var deviceCapsHeight="${deviceCapsHeight}";
var ctx="${ctx }";
var websocketUrl="${websocketUrl}";
//由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：
;!function(){
  var layer = layui.layer
  ,form = layui.form;
  var element = layui.element;
  form.render();
//自定义验证规则
  form.verify({
    name: function(value){
      if(value.length < 1){
        return '至少得1个字符啊';
      }	
    }
    ,pass: [/(.+){6,12}$/, '密码必须6到12位']
    ,number: [/^\d+\.?\d*$/, '必须为正数']
    ,integer: [/^\d+$/, '必须为整数']
    ,content: function(value){
      layedit.sync(editIndex);
    }
  });
  
  form.on('submit(updatePatient)',function(data){
	  $.ajax({
			url:"../patient/updateBasicInfo.action",
			data:data.field,
			type:"post",
			dataType:"json",
			success:function(data){
				
			}
		})
		layer.closeAll();
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
	})
}();

//隐藏查看历史记录div
$("#container").hide()
$("#StatisticalChart").hide()



/* var myChart = echarts.init(document.getElementById('chartDiv'));
var option = {	    
	    xAxis:  {
	        type: 'category',
	        boundaryGap: false,
	        data: ['','','','','','','','','','','','','','','','','','','','',''
	               ,'','','','','','','','','','','','','','','','','','','',''
	               ,'','','','','','','','','','','','','','','','','','','','']
	    },
	    yAxis: {
	        type: 'value',
	        axisLabel: {
	            formatter: '{value}'
	        }
	    },
	    series: [
	        {
	            name:'心电',
	            type:'line',
	            data:[8, 8, 15, 1, 9,8,8, 16, 2,8, 8, 15, 1,8,8, 9, 16, 1,8, 8, 15, 1, 9, 16,1],
	        },
	        
	    ]
	};
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option); */
</script> 
</body>
<%-- <script src="${ctx }/static/js/bed/bed.js"></script> --%>
</html>