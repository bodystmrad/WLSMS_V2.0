<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>温度持续监测传感器客户端</title>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/degreelisten.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css"> 
<link rel="shortcut icon" href="../images/c_temperature.png"/>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<script src="${ctx }/highcharts/highstock-all.js"></script>
<%-- <script src="${ctx }/modules/exporting.js"></script> --%>
<script src="${ctx }/highcharts/modules/exporting.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/static/js/layer/layer.js"></script>
<style>
</style>
</head>
<body>
<audio id="snd" src="" autoplay="true" width=0  height=0 loop="true"></audio>

	<div id="page">
		<div class="header">
			<div class="headermain clearfix" style="width:500px">
				<!-- <div class="logo"></div> -->
				<div style="position:absolute;top:10px;left:20px;background: url('..${logoPath }') #52b5ff no-repeat;width: 200px; height: 40px;"></div>
				<center style="height: 60px;">
					<span
						style="font-size: 24px; color: #FFFFFF; top: 25%; position: relative;">温度持续监测传感器客户端</span>
						<span style="font-size:12px;color:#FFFFFF;float:right;margin-top:30px;margin-right:120px;">v1.1.1</span>
				</center>
				<%-- div class="logininfo"><span>${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }"></div> --%>
			</div>
			
		</div>
		<div class="main">
			<div class="menulist">
				<ul style="padding: 0px; margin: 0px">
					<li>
						<div class="mainmenu">
							<span class="mainmenucontent"
								style="background: url(../images/icon_temperature_monitor.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
								温度监控 </span>
							<hr style="margin-top: -10px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						</div>
					</li>

					<li>
						<div id="showunnormal" class="menulv2">
							<span id="unnormalTemperature" class="highandlowcount"
								style="display: block; width: 120px; text-align: center; font-size: 60px; color: orange; line-height: 40px; padding-top: 40px">4</span><br />
							<span
								style="display: block; width: 120px; text-align: center; font-size: 14px; color: #FFFFFF">体温异常</span>
						</div>
					</li>

					<li>
						<div id="shownormal" class="menulv2 menuclicked">
							<span id="unnormalTemperature"
								style="display: block; width: 120px; text-align: center; font-size: 60px; color: green; line-height: 40px; padding-top: 40px">
								<span class="notemptycount">18</span><span style="font-size: 25px; color: #FFFFFF">/<span class="totalcount">20</span></span>
							</span><br /> 
							<span style="display: block; width: 120px; text-align: center; font-size: 14px; color: #FFFFFF">床位信息</span>
						</div>
					</li>

					<li>
						<div id="docManage" class="mainmenu">
							<hr
								style="margin-top: 0px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
							<span class="mainmenucontent"
								style="background: url(../images/icon_files_manage.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
								档案管理 </span>
							<hr
								style="margin-top: -11px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						</div>
					</li>
					<li>
						<div id="concentratorManage" class="mainmenu">
							<hr
								style="margin-top: 0px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
							<span class="mainmenucontent"
								style="background: url(../images/icon_setting.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
								设备管理 </span>
							<hr
								style="margin-top: -11px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						</div>
					</li>
				</ul>
				<div style="width:120px;height:50px;position:absolute;bottom:20px;text-align:center;">
					<span style="font-size:14px;">账号：${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }">
					<hr style="margin-top: 5px;  width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
					<a href="../home/logOut.action" style="text-decoration:none;font-size:14px;color:#FFFFFF;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</a><!-- <span onclick="logOut();" style="cursor:pointer;font-size:14px;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</span> -->
				</div>
			</div>
			<!-- 主要显示区域 -->
			<div class="mainArea" style="position:fixed;left:120px;top:60px">
				<div class="pageConcentratorManage">
					<div class="divheader4Concentrator">
						<div
							style="font-size: 24px; color: #33A684; margin-left: 30px; display: block; position: relative; top: 26px">
							设备管理<font id="concentratorNum4Doc" style="font-size: 14px;"
								color="#666666">当前个集中器</font>
						</div>
						<div
							style="margin-right: 30px; position: relative; right: 30px; float: right; color: #3CB397;cursor:pointer;"
							class="btnDiv4AddConcentrator"  onclick="addConcentrator()">
							<span class="btnSpan4AddConcentrator"
								style="background: url(../images/icon_add_patient_small.png) no-repeat;">添加集中器</span>
						</div>
						<hr
							style="width: auto; height: 1px; border: none; margin: 46 30 0 30; position: relative; border-top: 1px solid #33A684;" />
					</div>
					<center>
					<table class="concentratorlistbox" border="1" cellspacing="0" style="margin: 18 0 18 30;  position: relative;" cellpadding="0">
						<tr style="background: #D5F5DF;">
							<td align="center"><span class="tab_b">病床号码</span></td>
							<td align="center"><span class="tab_b">集中器号码</span></td>
							<td align="center"><span class="tab_b">功能操作</span></td>
						</tr>
					</table>
					</center>
				</div>
				
		</div>
	</div>
	<script>
	
	</script>
	<div class="changeBedDialog">
		<!-- <div id="container" style="width: 650px; height: 400px; margin: 10px  auto 0px auto; "></div> -->
		<div style="width: 280px; height: 34px;margin:26 20 0 20;">
		<span  style="margin-left:0px;font-weight: bold; font-size: 18px; color: #3CB397">转科室</span>
		<hr style="width: 280px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
	    </div>
		<div style="margin:10px;">
		<span style="margin:10px;margin-left:30px;font-size: 14px;color:#666666;">请选择科室：</span><select id="selDepartment" onchange="InitBedsByDepartment();" style="width:160px;"></select>
		<br>
		</div>
		<div style="margin:10px;">
		<span style="margin:10px;margin-left:30px;font-size: 14px;color:#666666;">请选择床位：</span><select id="selBed" style="width:160px;"></select>
		</div>
		<!-- <div style="margin:10px;">
		<div style="padding:5px;"><input type="button" onclick="sureChangeDepartment();" value="确认"><input type="button" onclick="$('.changeBedDialog').hide();$('.modelbg').hide();" value="关闭"></div>
		</div> -->
		<div onclick="$('.changeBedDialog').hide();$('.modelbg').hide();" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;
				position:relative;margin-left:122px;margin-top:26px;padding-top:4px;">关闭</div>
			<div onclick="sureChangeDepartment()" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;
				position:relative;margin-left:212px;margin-top:-26px;padding-top:4px;float:left">确认</div>
	</div>
	<div class="modelbg"></div>
	<div class="addconcentratorbox" style="height:200px">
		<div style="width: 280px; height: 34px;margin:26 20 0 20;">
		<span class="title4Concentrator" style="margin-left:0px;font-weight: bold;float:left; font-size: 18px; color: #3CB397">添加集中器</span>
		<hr style="width: 280px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
	    </div>
		<div class="text" >
			<table class="persontab" style="margin-right:30px;">
				<tr><td style="text-align:right;padding-right:30px;font-size: 14px;color:#666666;"><font color="red">*&nbsp;</font>病床ID:</td><td><input class="bedid4Concentrator" type="text"></td></tr>
				<tr><td style="text-align:right;padding-right:30px;font-size: 14px;color:#666666;">集中器ID:</td><td><input class="concentratorNo" onkeyup="value=value.replace(/[^0-9a-fA-F]/g,'')" type="text" maxlength="12"></td></tr>
			</table>
		</div>
		<div onclick="$('.modelbg').hide();$('.addconcentratorbox').hide();" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;
				position:relative;margin-left:122px;margin-top:32px;padding-top:4px;">关闭</div>
			<div onclick="$('.modelbg').hide();sureAddConcentrator();" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;
				position:relative;margin-left:212px;margin-top:-26px;padding-top:4px;float:left">确认</div>
		<!-- <a href="javascript:void(0)" class="" onclick="$('.modelbg').hide();$('.putbedidbox').hide();addbed(nowaddBed);">确认</a><a href="javascript:void(0)" class="" onclick="$('.modelbg').hide();$('.putbedidbox').hide();">关闭</a> -->
	</div>
	
	<div class="putbedidbox" style="height:170px">
		<div style="width: 280px; height: 34px;margin:26 20 0 20;">
		<span style="margin-left:0px;font-weight: bold;float:left; font-size: 18px; color: #3CB397">添加床位</span>
		<hr style="width: 280px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
	    </div>
		<div class="text" >
			<!-- <div class="title">请填写病床ID
			</div> -->
			<table class="persontab" style="margin-right:30px;">
				<tr><td style="text-align:right;padding-right:30px;font-size: 14px;color:#666666;"><font color="red">*&nbsp;</font>病床ID:</td><td><input id="id4UpConcentrator" type="hidden"><input class="addbedid" type="text"></td></tr>
				<tr style="display:none"><td style="text-align:right;padding-right:30px;font-size: 14px;color:#666666;">集中器ID:</td><td><input class="concentratorNo" onkeyup="value=value.replace(/[^0-9a-fA-F]/g,'')" type="text" maxlength="12"></td></tr>
			</table>
		</div>
		<div onclick="$('.modelbg').hide();$('.putbedidbox').hide();" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;
				position:relative;margin-left:122px;margin-top:-12px;padding-top:4px;">关闭</div>
			<div id="addbed" onclick="$('.modelbg').hide();addbed(nowaddBed);" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;
				position:relative;margin-left:212px;margin-top:-26px;padding-top:4px;float:left">确认</div>
		<!-- <a href="javascript:void(0)" class="" onclick="$('.modelbg').hide();$('.putbedidbox').hide();addbed(nowaddBed);">确认</a><a href="javascript:void(0)" class="" onclick="$('.modelbg').hide();$('.putbedidbox').hide();">关闭</a> -->
	</div>
		<div class="bedmoreinfo">
			<div style="width: 910px; height: 34px; margin: 26 20 0 20;">
				<span id="titie4ChartForm"
					style="margin-left: 0px; font-weight: bold; font-size: 18px; color: #3CB397">温度监控详情</span>
				<hr
					style="width: 910px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
			</div>
			<div id="maincontent4chart" style="margin: 10px; width: 910px; height: 570px;">
			<div style="float:left;">
				<div id="container" style="width: 700px; height: 440px; margin: 0px auto 0px auto;"></div>
				<a class="PreviousDay" href="javascript:void(0);" onclick="PreviousDay()"></a>
				<a class="NextDay" href="javascript:void(0);" onclick="NextDay()"></a>
				<div style="width: 700px; top: -15px; left: 17px; position: relative; font-size: 12px; height: 16px; text-align: center">
					<div id="day1" style="border: solid 1px #ccc; display: inline-block; width: 300px; border-right: none">2015.11.18</div>
					<div id="day2" style="border: solid 1px #ccc; display: inline-block; width: 300px;">2015.11.19</div>
				</div>
				<div class="personid" style="margin-top:-38px"></div>
				<div style="width:700px;height:26px;margin-top:10px;">
					<div onclick="showPrint()" style="width:120px;height:24px;padding-top:4px;float:left;margin-right:10px;cursor:pointer;border-radius: 14px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;">打印体温表</div>
					<div onclick="patientOut()" style="width:80px;height:24px;padding-top:4px;float:left;margin-right:10px;cursor:pointer;border-radius: 14px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;">出院</div>
					<div onclick="changeDepartment();" style="width:90px;height:24px;padding-top:4px;float:left;margin-right:10px;cursor:pointer;border-radius: 14px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;">转科室</div>
					<div><select class="select4Time" onchange="ChangeTime()"><option value="5">5分钟</option><option value="10">10分钟</option><option value="30">30分钟</option><option value="60">1小时</option><option value="120">2小时</option></select></div>
					<!-- <div onclick="$('.bedmoreinfo').hide();$('.modelbg').hide();" style="width:80px;height:24px;padding-top:4px;float:right;cursor:pointer;border-radius: 14px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;">关闭</div> -->
					<!-- <input type="button" class="showinfo" onclick="" value="打印体温表">
					<input type="button" class="listenbt" onclick="patientOut()" value="出院">
					<input type="button" onclick="changeDepartment();" value="转科室">
					<input type="button" onclick="$('.bedmoreinfo').hide();$('.modelbg').hide();" value="关闭"> -->
				</div>
			</div>
			
			<!-- 体温表格 -->
				<div id="tablecontainer"  style="overflow-x:hidden;overflow-y:auto;margin:15 0 0 0px;float:right;width:200;height: 510px;"  >
					
					<table class="ttTable">
					<!-- <thead style="position:absolute;top:58px"><td style="width:100px">时间</td><td style="width:100px">温度</td></thead>
						<tr><td>123123</td><td>36.66</td></tr> -->
					</table>
				</div>
				<div onclick="$('.bedmoreinfo').hide();$('.modelbg').hide();" style="margin-top:10px;width:80px;height:24px;padding-top:4px;float:right;cursor:pointer;border-radius: 14px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;">关闭</div>
			</div>
			
		</div>
		<div class="addpersonbox" mac="">
		<div style="width: 320px; height: 34px;margin:26 20 0 20;">
		<span id="titie4AddOrUpdatePatient" style="margin-left:0px;font-weight: bold; font-size: 18px; color: #3CB397">添加患者</span>
		<hr style="width: 320px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
	    </div>
	    <!-- <span style="display:block;position:absolute;top:80px;left:50px;font-size:16px;color:#000;font-weight:bold;">患者信息</span> -->
	    <table  style="text-align:right;width:290px;font-size: 14px;color:#666666;margin-left:0px;margin-top:10px">
				<tr><td><font color="red"></font>病人姓名</td><td><input id="patientid" type="hidden"><input name="name" type="text"></td></tr>
				<tr><td><font color="red"></font>性别</td><td>&nbsp;<input name="sex" style="width:13px;height:13px;" type="radio" value="男"> &nbsp;男 &nbsp;&nbsp;<input name="sex" type="radio" style="width:13px;height:13px;" value="女">&nbsp;女</td></tr>
				<tr><td><font color="red"></font>年龄</td><td><input name="age" type="text" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
				
				<tr><td><font color="red">*&nbsp;</font>贴片号码</td><td><input name="cardname" onkeyup="value=value.replace(/[^0-9a-fA-F]/g,'')" type="text" maxlength="12"></td></tr>
				<tr><td><font color="red">*&nbsp;</font>低温报警</td><td><input id="lowc" name="lowc" maxlength="5" onkeyup="value=value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'')"  type="text"><!-- <select name="lowc"><option>35</option><option>36</option><option>37</option><option>38</option><option>39</option></select> ~ <select name="highc"><option>41</option><option>40</option><option>39</option><option>38</option><option>37</option></select> --></td></tr>
				<tr><td><font color="red">*&nbsp;</font>高温Ⅰ报警</td><td><input id="highc" name="highc" maxlength="5" onkeyup="value=value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'')" type="text"></td></tr>
				<tr><td><font color="red">*&nbsp;</font>高温Ⅱ报警</td><td><input id="superHighDegree" maxlength="5" onkeyup="value=value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'')" name="superHighDegree" type="text"></td></tr>
				<tr><td><font color="red">*&nbsp;</font>病床号</td><td><input id="bedid" name="bedid" type="text"><input id="idofbed" name="idofbed" type="hidden"></td></tr>
				<tr><td><font color="red">&nbsp;</font>入院时间</td><td><input name="roomtime" id="startTime" style=" cursor: pointer" class="Wdate" type="text"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:false})"></td></tr>
				<tr><td>病案号码</td><td><input name="sicknum" type="text"></td></tr>
				<tr><td>医师诊断</td><td><input name="sickname" type="text"></td></tr>
				<tr><td>主管医生</td><td><input name="doctor" type="text"></td></tr>
				<tr><td>身份证</td><td><input name="personid" type="text" maxlength="18" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
				<tr><td>住址</td><td><input name="address" type="text"></td></tr>
				<tr><td>就医类型</td><td><input name="sicktype" type="text"></td></tr>
			</table>
			<div onclick="cancelAddPatient()" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;
				position:relative;margin-left:172px;margin-top:12px;padding-top:4px;">关闭</div>
			<div id="yes33" onclick="sureAddPersonCom()" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;
				position:relative;margin-left:262px;margin-top:-26px;padding-top:4px;float:left">确认</div>
	</div>
	<script src="${ctx }/static/js/main.js"></script>
	<script src="${ctx }/static/js/chartManage.js"></script>
</body>
</html>