<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>WLSMS</title>
<%-- <link rel="stylesheet" type="text/css" href="${ctx }/static/css/bed/index.css"> --%>
<link rel="stylesheet" href="${ctx}/static/js/layui-v2.1.7/css/layui.css" media="all">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<%-- <link rel="stylesheet" type="text/css" href="${ctx }/static/css/degreelisten.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css"> --%>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/bed/addBed.css">
<link rel="shortcut icon" href="../images/c_temperature.png"/>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>

<style>
</style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
  <div class="layui-header layui-bg-cyan">
    <div class="layui-logo" style="margin-left:10px;font-size:24px;width:300px">无线生命体征监测后台管理</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left" style="left: 300px;">
      <li class="layui-nav-item"><a href="../system/systemSetting.action" style="font-size:16px"><img src="${ctx }/images/icon_setting.png" alt="系统设置">&nbsp;系统设置</a></li>
      <li class="layui-nav-item layui-this"><a href="../bed/addBed.action" style="font-size:16px"><img src="${ctx }/images/icon_files_manage.png" alt="添加床位">&nbsp;添加床位</a></li>
      
    </ul>
  </div>
  
  <div class="layui-body" style="left:0px;background:#3a3a42;">
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
     	<ul id="bedList" style="list-style:none;padding:0px;margin:0px;">
			<c:forEach items="${bedList }" var="item" varStatus="status">
				<li class="bedLi" bedName="${item.name }" id="${item.id }">
     			<div class="bedDiv">
     				<div class="bedTopDiv">
     					<span class="spanBedName">${item.name }</span>
     					<span class="spanMac">${item.concentratorNo }</span>
     				</div>
     				<div class="bedBottomDiv">
     					<a><span class="spanEditBed" bedId="${item.id }" onclick="editBed(this);">修改</span></a>
     					<a><span class="spanDelBed" bedId="${item.id }" onclick="delBed(this);">删除</span></a>
     				</div>
     			</div>
			</li>
			</c:forEach>
			<!-- <li class="bedLi" bedName="'+dom.bedid+'" id="'+dom.idofbed+'">
     			<div class="bedDiv">
     				<div class="bedTopDiv">
     					<span class="spanBedName">一号床</span>
     					<span class="spanMac">798696BDCDDD</span>
     				</div>
     				<div class="bedBottomDiv">
     					<a><span class="spanEditBed">修改</span></a>
     					<a><span class="spanDelBed">删除</span></a>
     				</div>
     			</div>
			</li> -->
			<li class="bedLi" style="background-color:#FFFFFF;">
     			<div class="bedDiv">
     				<div class="bedTopDiv" onclick="addBed();" style="background:url(${ctx }/images/icon_add_sickbed_big.png) no-repeat center;cursor:pointer;">
     				</div>
     				<div class="bedBottomDiv">
     					<a onclick="javascript:addBed();return false;"><span class="spanAddBed">添加床位</span></a>
     				</div>
     			</div>
			</li>
			
     	</ul>



    </div>
  </div>
  
  <div class="layui-footer" style="left:0px;">
    <!-- 底部固定区域 -->
    © www.ptorigin.com.cn - 铂元智能科技
  </div>
</div>
<script type="text/javascript" src="${ctx}/static/js/layui-v2.1.7/layui.all.js"></script>
<script>
//由于模块都一次性加载，因此不用执行 layui.use() 来加载对应模块，直接使用即可：

;!function(){
	var layer = layui.layer
	  ,form = layui.form;
	  var element = layui.element;
	  form.render();
}();
</script> 
<script src="${ctx }/static/js/bed/addBed.js"></script>
</body>
</html>