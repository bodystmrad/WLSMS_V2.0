<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>体温自动监控系统</title>
<link rel="stylesheet" type="text/css"
	href="${ctx }/static/css/bed/degreelisten.css">
 	<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css"> 
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<script src="${ctx }/highcharts/highcharts.js"></script>
<script src="${ctx }/modules/exporting.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/static/js/layer/layer.js"></script>
<style>
</style>
</head>
<body>
	<div id="page">
		<div class="header">
			<div class="headermain clearfix">
				<!-- <div class="logo"></div> -->
				<center style="height: 60px">
					<span
						style="font-size: 24px; color: #FFFFFF; top: 25%; position: relative;">体温自动监控系统</span>
				</center>
				<%-- div class="logininfo"><span>${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }"></div> --%>
			</div>
		</div>
		<div class="main">
			<div class="menulist">
				<ul style="padding: 0px; margin: 0px">
					<li>
						<div class="mainmenu">
							<span class="mainmenucontent"
								style="background: url(../images/icon_temperature_monitor.png) no-repeat; padding-left: 25px; line-height: 60px;">
								温度监控 </span>
							<hr style="margin-top: -10px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						</div>
					</li>

					<li>
						<div id="showunnormal" class="menulv2">
							<span id="unnormalTemperature" class="highandlowcount"
								style="display: block; width: 120px; text-align: center; font-size: 60px; color: orange; line-height: 40px; padding-top: 40px">4</span><br />
							<span
								style="display: block; width: 120px; text-align: center; font-size: 14px; color: #FFFFFF">体温异常</span>
						</div>
					</li>

					<li>
						<div id="shownormal" class="menulv2 menuclicked">
							<span id="unnormalTemperature"
								style="display: block; width: 120px; text-align: center; font-size: 60px; color: green; line-height: 40px; padding-top: 40px">
								<span class="notemptycount">18</span><span style="font-size: 25px; color: #FFFFFF">/<span class="totalcount">20</span></span>
							</span><br /> 
							<span style="display: block; width: 120px; text-align: center; font-size: 14px; color: #FFFFFF">床位信息</span>
						</div>
					</li>

					<li>
						<div id="docManage" class="mainmenu">
							<hr
								style="margin-top: 0px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
							<span class="mainmenucontent"
								style="background: url(../images/icon_files_manage.png) no-repeat; padding-left: 25px; line-height: 60px;">
								档案管理 </span>
							<hr
								style="margin-top: -11px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						</div>
					</li>
				</ul>
				<div style="width:120px;height:50px;position:absolute;bottom:20px;text-align:center;">
					<span style="font-size:14px;">账号：${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }">
					<hr style="margin-top: 5px;  width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
					<a href="../home/logOut.action" style="text-decoration:none;font-size:14px;color:#FFFFFF;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</a><!-- <span onclick="logOut();" style="cursor:pointer;font-size:14px;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</span> -->
				</div>
			</div>
			<!-- 主要显示区域 -->
			<div class="mainArea" style="position:fixed;left:120px;top:60px">
			<!-- <div  style="height:92px;width:100%;margin-left:30px;">
				<ul style="list-style:none;padding:0px;margin:0px;">
					<li class="" style="height:48px;width:140px;margin:24 0 20 0;display:inline;float:left;" >
						<span style=" width: 140px; text-align: center; font-size: 48px; color: orange;">
								<span class="highcount">1</span><span style="font-size: 14px; color: #FFFFFF">位患者体温过高</span>
						</span>
 					</li>
 				</ul>
			</div>
			<div style="height:auto;width:100%;margin-left:20px">
				<ul id="bedList4high" style="list-style:none;padding:0px;margin:0px;"></ul>
			</div>
			
			<div  style="height:48px;width:100%;margin-left:30px;margin-top:54px;margin-bottom:20px;position:relative;float:left">
				<ul style="list-style:none;padding:0px;margin:0px;">
 					<li class="" style="height:48px;width:140px;margin:0;display:inline;float:left;backround:red">
 						<span style=" width: 140px; text-align: center; font-size: 48px; color: #1295FE;">
							<span class="lowcount">2</span><span style="font-size: 14px; color: #FFFFFF">位患者体温过低</span>
						</span>
 					</li>
 				</ul>
			</div>
			<div style="height:auto;width:100%;margin-left:20px;margin-top:20px">
				<ul id="bedList4low" style="list-style:none;padding:0px;margin:0px;"></ul>
			</div> -->
			 <!-- 床位统计信息以及按钮等 -->
			<div id="bedstatistic" style="height:130px;width:100%;margin-left:10px;">
			<ul style="list-style:none;padding:0px;margin:0px;">
			<li class="statisticLi" >
			<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: orange;">
								<span class="highcount"></span><span style="font-size: 14px; color: #FFFFFF">位</span>
							</span>
							<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者体温过高</span>
 			</li>
 			<li class="statisticLi">
 			<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #1295FE;">
								<span class="lowcount"></span><span style="font-size: 14px; color: #FFFFFF">位</span>
							</span>
							<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者体温过低</span>
 			</li>
 			
 			<li class="statisticLi">
 			<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;">
								<span class="notemptycount"></span><span style="font-size: 14px; color: #FFFFFF">位</span>
							</span>
							<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者入住</span>
 			</li>
 			
 			<li class="statisticLi">
 			<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;">
								<span class="emptycount"></span><span style="font-size: 14px; color: #FFFFFF">个</span>
							</span>
							<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">床位空置</span>
 			</li>
 			
 			<li class="statisticLi">
 			<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;">
								<span class="totalcount"></span><span style="font-size: 14px; color: #FFFFFF">个</span>
							</span>
							<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">当前总计床位</span>
 			</li>
 			
 			<li class="btnAddBedLi4Deg">
 			<div class="btnDiv4Deg" onclick="addBed()">
				<span class="btnSpan4Deg" style="background: url(../images/icon_add_sickbed_small.png) no-repeat;">添加床位</span>
			</div>
 			</li>
 			
 			<li class="btnAddBedLi4Deg">
 			<div class="btnDiv4Deg" onclick="addPatient4btn()">
				<span class="btnSpan4Deg" style="background: url(../images/icon_add_patient_small.png) no-repeat;">患者入住</span>
			</div>
 			</li>
 			</ul>
			</div>
			<!-- 床位列表区域 -->
			<div class="bedsArea">
				<ul id="bedList" style="list-style:none;padding:0px;margin:0px;">
				<!-- 非空床位 -->
				<li class="bedLi">
					<div class="normal4bed">
						<!-- <select class="selectNum">
							<option>0.1</option>
							<option>0.2</option>
							<option>0.3</option>
							<option>0.4</option>
						</select> -->
						<input type="text" style="width:40px;" value="0">
						<a class="btnCorrect" onclick="correctNum(this);">修正</a>
						<span class="curDegreeSpan" >36.88℃</span>
						<span class="patientNameSpan">宋志伟</span><br>
						<span class="patientAgeSpan">男  28岁</span>
					</div>
					<div class="divbed_bedNum">
						<div class="div4BedNum">
							<span class="span4BedNum">床位号：</span>
							<span class="span4BedNum">2001</span>
						</div>
						<div>
							<div class="smallbtnRecInBedList4div" >
								<span class="span4Rec">记录</span>
							</div>
							<div class="smallbtnDelInBedList4div" >
								<span class="span4Del">删除</span>
							</div>
						</div>
					</div>
				</li >
				
				<!-- 空床位 -->
				<li class="bedLi">
					<div class="addPatient4bed">
						<span class="addPatientSpan">患者入住</span>
					</div>
					<div class="divbed_bedNum">
						<div class="div4BedNum">
							<span class="span4BedNum">床位号：</span>
							<span class="span4BedNum">2001</span>
						</div>
						<div>
							<div class="smallbtnDelInEmptyBed4div" >
								<span class="span4Del">删除</span>
							</div>
						</div>
					</div>
				</li>
				
				<!-- 添加床位 -->
				<li class="bedLi">
					<div class="addBed4bed"></div>
					<div class="divbed_bedNum" style="position:relative;">
						<div id="addBedInList4div" class="addBedInBedsList" >
						<center>
				 			<span id="addBedInLists4pan">添加床位</span></center>
						</div>
					</div>
				</li>
				</ul>
			</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			//alert(document.body.scrollHeight);
			$(".menulist").css("height",document.body.scrollHeight-60);
			$(".mainArea").css("height",document.body.scrollHeight-60).css("width",document.body.scrollWidth-120);
			getBedsData();
			startAutoRefresh();//自动刷新页面
		});
		window.onresize = function(){
			location.reload(true);
		}
		//开始自动刷新
		function startAutoRefresh(){
			autorefresh = setInterval("getBedsData()", 5000);//自动刷新页面
		}
		//停止页面自动刷新
		function stopAutoRefresh() {
			clearTimeout(autorefresh);
		}
		function correctNum(ele){//修正获取的温度数据
			var num4correct=$(ele).prev().val();
			var curbed=$(ele).parents(".bedLi");
			currentBedName=$(curbed).attr("bedid");
			var nowaboutinfo = bedsOtherinfo[currentBedName];
			var nowaboutinfoBed = bedsBasicinfo[currentBedName];
			var deviceNo=nowaboutinfoBed.num;
			$.ajax({
				type:"post",
				url:"../patient/correctDegree.action",
				dataType:"json",
				data:{strMac:deviceNo,correctNum:num4correct},
				success:function(data){
					alert("操作成功！");

				}
			})
		}
		$(".menulv2").click(function() {
			$(".menulv2").removeClass("menuclicked");
			$("#docManage").removeClass("menuclicked");
			$(this).addClass("menuclicked");
		});
		$("#docManage").click(function() {
			stopAutoRefresh();
			$(this).addClass("menuclicked");
			$(".menulv2").removeClass("menuclicked");
		});
		$("#showunnormal").click(function(){//显示温度异常页面
			stopAutoRefresh();
			$(".mainArea").empty();
			$(".mainArea").html('<div  style="height:92px;width:100%;margin-left:30px;"><ul style="list-style:none;padding:0px;margin:0px;">'+
			'<li class="" style="height:48px;width:140px;margin:24 0 20 0;display:inline;float:left;" ><span style=" width: 140px; text-align: center; font-size: 48px;'+
			' color: orange;"><span class="highcount">1</span><span style="font-size: 14px; color: #FFFFFF">位患者体温过高</span></span></li></ul></div>'+
			'<div style="height:auto;width:100%;margin-left:20px"><ul id="bedList4high" style="list-style:none;padding:0px;margin:0px;"></ul></div>'+
			'<div  style="height:48px;width:100%;margin-left:30px;margin-top:54px;margin-bottom:20px;position:relative;float:left"><ul style="list-style:none;padding:0px;'+
			'margin:0px;"><li class="" style="height:48px;width:140px;margin:0;display:inline;float:left;backround:red"><span style=" width: 140px; text-align: center;'+
			' font-size: 48px; color: #1295FE;"><span class="lowcount">2</span><span style="font-size: 14px; color: #FFFFFF">位患者体温过低</span></span></li></ul></div>'+
			'<div style="height:auto;width:100%;margin-left:20px;margin-top:20px"><ul id="bedList4low" style="list-style:none;padding:0px;margin:0px;"></ul></div>');
			getBedsDataOfUnnormal();
		});
		$("#shownormal").click(function(){//显示温度监控页面（床位列表）
			startAutoRefresh();
			$(".mainArea").empty();
			$(".mainArea").html('<div id="bedstatistic" style="height:130px;width:100%;margin-left:10px;"><ul style="list-style:none;padding:0px;'+
			'margin:0px;"><li class="statisticLi" ><span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: orange;">'+
			'<span class="highcount"></span><span style="font-size: 14px; color: #FFFFFF">位</span></span><span style="display: block; width: 140px; '+
			'text-align: center; font-size: 14px; color: #FFFFFF">患者体温过高</span></li><li class="statisticLi"><span style="margin-left:46px; width: 140px;'+
			' text-align: center; font-size: 48px; color: #1295FE;"><span class="lowcount"></span><span style="font-size: 14px; color: #FFFFFF">位</span></span>'+
			'<span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者体温过低</span></li><li class="statisticLi">'+
			'<span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;"><span class="notemptycount"></span><span style="font-size: 14px;'+
			' color: #FFFFFF">位</span></span><span style="display: block; width: 140px; text-align: center; font-size: 14px; color: #FFFFFF">患者入住</span></li>'+
			'<li class="statisticLi"><span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px; color: #FFFFFF;">'+
			'<span class="emptycount"></span><span style="font-size: 14px; color: #FFFFFF">个</span></span><span style="display: block; width: 140px; text-align: center;'+
			' font-size: 14px; color: #FFFFFF">床位空置</span></li><li class="statisticLi"><span style="margin-left:46px; width: 140px; text-align: center; font-size: 48px;'+
			' color: #FFFFFF;"><span class="totalcount"></span><span style="font-size: 14px; color: #FFFFFF">个</span></span><span style="display: block; width: 140px; '+
			'text-align: center; font-size: 14px; color: #FFFFFF">当前总计床位</span></li><li class="btnAddBedLi4Deg"><div class="btnDiv4Deg" onclick="addBed()"><span class="btnSpan4Deg" '+
			'style="background: url(../images/icon_add_sickbed_small.png) no-repeat;">添加床位</span></div></li><li class="btnAddBedLi4Deg">'+
			'<div class="btnDiv4Deg" onclick="addPatient4btn()"><span class="btnSpan4Deg" style="background: url(../images/icon_add_patient_small.png) no-repeat;">患者入住</span></div></li></ul></div>'+
			'<div class="bedsArea"><ul id="bedList" style="list-style:none;padding:0px;margin:0px;"></ul></div>');
			getBedsData();
		});
		$("#docManage").click(function(){
			$(".mainArea").empty();
		});
		//温度监控页面相关
		var bedsinfo = {};//当前所有病床的信息
		var bedsBasicinfo = {};
		var bedsOtherinfo = {};//当前所有病床的其他信息
		var chartObj = {};//当前chart表对象
		var nowDelBed = {};//当前要删除的病房
		var nowAddPersonBed = {};//当前要添加病人的病床
		var nowStopBed = {};//当前要停止监控的病床
		var nowaddBed = {};//当前要添加的病床
		var startHour=2;//折线图从几时开始
		var outpatientid="";//当前要出院的病人
		var currentBedId="";//鼠标悬停后存储当前床位id
		var currentBedName="";//鼠标悬停后存储当前床位名称
		var autorefresh;
		function getClientHeight(){
			return  document.body.scrollHeight > document.documentElement.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight;
		}
		function getClientWidth(){
			return  document.body.scrollWidth > document.documentElement.scrollWidth?document.body.scrollWidth:document.documentElement.scrollWidth;
		}
		
		//去后台请求所有病床信息数据
		function getBedsData(){
			//alert('31231');
			$.ajax({
				type:"post",
				url:"listAll.action",//"bed/listAll.action",//"tiwen/bedinfo.json",
				dataType:"json",
				data:{},
				success:function(data){
					$(".totalcount").html(data.total);
					bedsinfo = data.data;
					bedsOtherinfo = data.otherdata;
					putBedsData(bedsinfo);
					/* listenAddBed();
					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */
				},
				error:function(data){
					/* listenAddBed();
					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */
				}
			})}
		
		
		function getBedsDataOfUnnormal(){
			$.ajax({
				type:"post",
				url:"listAll.action",//"bed/listAll.action",//"tiwen/bedinfo.json",
				dataType:"json",
				data:{},
				success:function(data){
					$(".totalcount").html(data.total);
					bedsinfo = data.data;
					bedsOtherinfo = data.otherdata;
					putBedsDataOfUnnormal(bedsinfo);
					/* listenAddBed();
					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */
				},
				error:function(data){
					/* listenAddBed();
					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */
				}
			})}
		
		function delBed(ele){
			nowDelBed = $(ele).parents(".bedLi");
			var bedid = $(nowDelBed).attr("id");
			var index = layer.open({
			    type: 2,
			    area: ['360px', '240px'],
			    fix: true, //不固定
			    maxmin: false,
			    content: ['../static/layerHtml/delBedConfim.html', 'no'],
			    scrollbar: false,
			    closeBtn: 0,
			    title:false,
			    shade: [0.5,'#000000'] //0.1透明度的白色背景
			}); 
			layer.style(index, {
				'border-radius': '4px'
			});   
			return;
			$.ajax({
				type:"post",
				url:"deleteByName.action",//"returnTrue.json",//此处返回true或false
				dataType:"json",
				data:{bedid:bedid},
				success:function(data){
					/* var thisId = $(nowDelBed).attr("id");
					if($(nowStopBed).parents(".abnormalbeds").length != 0){
						$(nowStopBed).remove();
					}
					$(".normalbeds").find("#"+thisId+"").remove();
					$(".abnormalbeds").find("#"+thisId+"").remove();

					$(nowDelBed).remove();
					$(".modelbg").hide();
					$(".confirmbox").hide();
					setTimeout(function(){listenAddBed();},1000)

 					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */ 
				}
			})
		}
		
		//放入病床信息数据
		function putBedsData(data){
			var highcount = 0;//高温病人数量
			var lowcount=0;//低温病人数量
			var emptycount=0;//空床数量
			var notemptycount=0;//非空床数量
			$("#bedList").empty();
			$(data).each(function(index,dom) {
				//alert(dom.bedstate);
				if(dom.bedstate == "empty"){
					emptycount++;
					$("#bedList").append('<li class="bedLi" bedName="'+dom.bedid+'" id="'+dom.idofbed+'" patientid=""><div class="addPatient4bed" onclick="addPatient(this)" bedName="'+dom.bedid+'" id="'+dom.bedid+'"><span class="addPatientSpan">'+
							'患者入住</span></div><div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">'+
							'床位号：</span><span class="span4BedNum">'+dom.bedid+'</span></div><div><div class="smallbtnDelInEmptyBed4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
				}
				else{
					notemptycount++;
					//alert(dom.state);
					var tempTypeText = "";
					var strbedclassname="";
					if(dom.state == "high"){
						tempTypeText = "高温";
						highcount++;
						strbedclassname="high4bed";
					}else if(dom.state == "low"){
						tempTypeText = "低温";
						lowcount++;
						strbedclassname="low4bed";
					}else{
						tempTypeText = "正常";
						dom.state="normal";
						strbedclassname="normal4bed";
					}
					//获取贴片的工作状态，如出现故障则在病床号后边加叹号作为标志
					var statusOfDevice="";
					if(dom.deviceStatus==0){
						statusOfDevice="(!)";
					}
					
					//alert(dom.deviceStatus);
					//alert(dom.degree+"sss");
					//alert(dom.patientid);
					bedsBasicinfo[dom.bedid]=dom;
					var nowdegree=(dom.degree==undefined)?'33.00':dom.degree;
					var strpersonalinfo=dom.personinfo;
					//var seximgurl=(strpersonalinfo.indexOf("-女-")>0)?"../images/woman.ico":"../images/man.ico";
					var sexstr=(strpersonalinfo.indexOf("-女-")>0)?"女":"男";
					$("#bedList").append('<li class="bedLi" id="'+dom.idofbed+'" bedid="'+dom.bedid+'" roomtime="'+dom.roomtime+'" patientid="'+dom.patientid+'"><div class="'+strbedclassname+'"><input type="text" style="width:40px;" value="'+dom.num4Correct+'"><a class="btnCorrect" onclick="correctNum(this);">修正</a><span class="curDegreeSpan" >'+nowdegree+'℃</span>'+
							'<span class="patientNameSpan">'+dom.name+'</span><br><span class="patientAgeSpan">'+sexstr+' '+dom.age+'</span></div>'+
							'<div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">床位号：</span><span class="span4BedNum">'+dom.bedid+statusOfDevice+'</span>'+
							'</div><div><div class="smallbtnRecInBedList4div" onclick="showChart(this)"><span class="span4Rec">记录</span></div><div class="smallbtnDelInBedList4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
					//获取贴片的电量
					var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10){
						//setTimeout("alert('"+dom.bedid+"床电量不足，剩余电量为："+powerNum+"%')",500);
					}
				}
				
			})
			$("#bedList").append('<li class="bedLi addbedbtn" onclick="addBed()"><div class="addBed4bed"></div><div class="divbed_bedNum" style="position:relative;">'+
			'<div id="addBedInList4div" class="addBedInBedsList"><center><span id="addBedInLists4pan">添加床位</span></center>'+
			'</div></div></li>');
			$(".highcount").html(highcount);
			$(".lowcount").html(lowcount);
			$(".emptycount").html(emptycount);
			$(".notemptycount").html(notemptycount);
			$(".highandlowcount").html(highcount+lowcount);
		}
		//获取异常体温床位数据并显示
		function putBedsDataOfUnnormal(data){
			var highcount = 0;//高温病人数量
			var lowcount=0;//低温病人数量
			var emptycount=0;//空床数量
			var notemptycount=0;//非空床数量
			$("#bedList").empty();
			$(data).each(function(index,dom) {
				//alert(dom.bedstate);
				if(dom.bedstate == "empty"){
					emptycount++;
				}
				else{
					notemptycount++;
					//alert(dom.state);
					var tempTypeText = "";
					var strbedclassname="";
					var strbedlistname="";
					if(dom.state == "high"){
						tempTypeText = "高温";
						highcount++;
						strbedclassname="high4bed";
						strbedlistname="#bedList4high";
					}else if(dom.state == "low"){
						tempTypeText = "低温";
						lowcount++;
						strbedclassname="low4bed";
						strbedlistname="#bedList4low";
					}else{
						tempTypeText = "正常";
						dom.state="normal";
						strbedclassname="normal4bed";
						strbedlistname="#bedList";
					}
					//获取贴片的工作状态，如出现故障则在病床号后边加叹号作为标志
					var statusOfDevice="";
					if(dom.deviceStatus==0){
						statusOfDevice="(!)";
					}
					
					//alert(dom.deviceStatus);
					//alert(dom.degree+"sss");
					//alert(dom.patientid);
					bedsBasicinfo[dom.bedid]=dom;
					var nowdegree=(dom.degree==undefined)?'33.00':dom.degree;
					var strpersonalinfo=dom.personinfo;
					//var seximgurl=(strpersonalinfo.indexOf("-女-")>0)?"../images/woman.ico":"../images/man.ico";
					var sexstr=(strpersonalinfo.indexOf("-女-")>0)?"女":"男";
					if(strbedlistname!="#bedList")
					$(strbedlistname).append('<li class="bedLi" id="'+dom.idofbed+'" bedid="'+dom.bedid+'" roomtime="'+dom.roomtime+'" patientid="'+dom.patientid+'"><div class="'+strbedclassname+'"><span class="curDegreeSpan" >'+nowdegree+'℃</span>'+
							'<span class="patientNameSpan">'+dom.name+'</span><br><span class="patientAgeSpan">'+sexstr+' '+dom.age+'</span></div>'+
							'<div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">床位号：</span><span class="span4BedNum">'+dom.bedid+statusOfDevice+'</span>'+
							'</div><div><div class="smallbtnRecInBedList4div" onclick="showChart(this)"><span class="span4Rec">记录</span></div><div class="smallbtnDelInBedList4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
					//获取贴片的电量
					var powerNum=dom.powerNum;
					if(powerNum!=undefined&&powerNum<=10){
						//setTimeout("alert('"+dom.bedid+"床电量不足，剩余电量为："+powerNum+"%')",500);
					}
				}
				
			})
			$(".highcount").html(highcount);
			$(".lowcount").html(lowcount);
			$(".emptycount").html(emptycount);
			$(".notemptycount").html(notemptycount);
			$(".highandlowcount").html(highcount+lowcount);
		}
		
		//添加床位
		function addBed(ele){
			nowaddBed = ele;
			$('.modelbg').show();
			$(".putbedidbox").show();
		}
		//添加病床
		function addbed(){
/* 			alert(nowaddBed);
			alert($(obj).prop("className"));return; */
			$.ajax({
				type:"post",
				url:"add.action",//"tiwen/addbed.json",
				dataType:"json",
				data:{"bedid":$(".addbedid").val()},
				success:function(data){
					if(data.success == false){ 
						alert("添加失败，请确认要添加的病床号没有重复！");
						return;
					}
					getBedsData();return;
					$(".addbedbtn").before('<li class="bedLi" bedName="'+data.bedid+'" id="'+data.idofbed+'" patientid=""><div class="addPatient4bed" onclick="addPatient(this)" bedName="'+data.bedid+'" id="'+data.bedid+'"><span class="addPatientSpan">'+
							'患者入住</span></div><div class="divbed_bedNum"><div class="div4BedNum"><span class="span4BedNum">'+
							'床位号：</span><span class="span4BedNum">'+data.bedid+'</span></div><div><div class="smallbtnDelInEmptyBed4div">'+
							'<span class="span4Del" onclick="delBed(this);">删除</span></div></div></div></li>');
/* 					listenAddBed();
					listenAddPerson();
					listenBedHover();
					listenListenBt();
					listenCloseTishi();
					listenShowPrint();
					listenDelBed(); */
				},
				error:function(data){alert("添加失败");}
			})
		}
		function addPatient(ele){
			nowAddPersonBed = $(ele).parents(".bedLi");
			openAddPerson();
			console.dir(nowAddPersonBed);
			$("#bedid").val(ele.getAttribute("bedName"));
			
		}
		function addPatient4btn(){
			openAddPerson();		
		}
		function cancelAddPatient(){
			$('.addpersonbox').hide();$('.modelbg').hide();
		} 
		//点击记录显示折线图
		function showChart(ele){
			var tempTimer;
			var curbed=$(ele).parents(".bedLi");
			currentBedName=$(curbed).attr("bedid");
			var nowaboutinfo = bedsOtherinfo[currentBedName];
			var nowaboutinfoBed = bedsBasicinfo[currentBedName];
			console.dir(bedsBasicinfo);
			currentBedId=nowaboutinfoBed.idofbed;//将当前床位id存到全局变量中
			//alert(nowaboutinfo.personid);
			//给chat赋值
			var contentInfo="<table style='width:100%'>";
			contentInfo+="<tr><td>监控范围："+nowaboutinfoBed.range+"℃</td><td>监控时间："+nowaboutinfoBed.listentime+"</td><td>贴片号码："+nowaboutinfoBed.num+"</td>";
			contentInfo+="<tr><td>剩余时间：约"+nowaboutinfoBed.surplustime+"小时</td><td>病人信息："+nowaboutinfoBed.personinfo+"</td><td>医师诊断："+nowaboutinfoBed.diagnosis+"</td>";
			contentInfo+="<tr><td>入院时间："+nowaboutinfoBed.roomtime+"</td><td>病案号码："+nowaboutinfoBed.sicknum+"</td><td>主治医师："+nowaboutinfoBed.doctor+"</td>";
			contentInfo= contentInfo+"</table>";
			$(".bedmoreinfo > .personid").html(contentInfo);
			outpatientid=$(curbed).attr("patientid");
			var str =$(curbed).attr("roomtime");
			// 转换日期格式
			str = str.replace(/-/g, '/'); // "2010/08/01";
			$("#day1").text($(curbed).attr("roomtime"));
			// 创建日期对象
			var date = new Date(str);
			// 加一天
			date.setDate(date.getDate()+1);//获取AddDayCount天后的日期
		    var y = date.getFullYear();
		    var m = date.getMonth()+1;//获取当前月份的日期
		    var d = date.getDate();
			$("#day2").text(y+"-"+m+"-"+d);
			//chartObj.xAxis[0].setCategories(nowaboutinfo.times);
			chartObj.series[0].setData(nowaboutinfo.degrees);
			//chartObj.series[0].setData([35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,37,37,37,37,37]);
			
			startHour=nowaboutinfo.times;
			//alert(startHour);
			if(startHour==null||startHour==undefined)startHour=2;
			//****************************************************如果温度数据数量不够一天则显示一天的日期
			if(nowaboutinfo.degrees)
			if (nowaboutinfo.degrees.length>0&&(startHour+(nowaboutinfo.degrees.length-1)*2)<=24) {
				$("#day2").css("display","none");
				$("#day1").css("width","600px");

			}else{
				$("#day2").css("display","inline-block");
				$("#day1").css("width","300px");
			}
			//****************************************************

			if(tempTimer){
				clearTimeout(tempTimer);
			}
			var prototype=curbed;
			tempTimer = setTimeout(function(){
				var left = prototype.offsetLeft;
				var right =  getClientWidth() - prototype.offsetLeft - prototype.offsetWidth;
				var top = prototype.offsetTop;
				var bottom = getClientHeight() - prototype.offsetTop;
				var infoLeft = 0;
				if(left > right){
					infoLeft = left - $(".bedmoreinfo").width();
				}else{
					infoLeft = left+prototype.offsetWidth;
				}
				$('.modelbg').show();
				$(".bedmoreinfo").css({"left":getClientWidth()/2-325+"px","top":getClientHeight()/2-259+"px"}).show();//.show()
			},500)
		}
		//温度折线图相关
		$(function () {
			chartObj =  new Highcharts.Chart({
				chart: {
					spacingTop: 20,
					renderTo: 'container',
					//defaultSeriesType: 'line' //图表类别，可取值有：line、spline、area、areaspline、bar、column等
				},
				title: {
					text: '',
				},
				xAxis: {
					tickmarkPlacement:'between',//'on',
					tickInterval: 1,
					minorTickInterval: 0.5,
					gridLineWidth: 2,
					labels:{
						formatter:function(){
							if((this.value*2+startHour)>24){
								return (this.value*2+startHour)-24 ;
							}else{
								return this.value*2+startHour ;
							}

						}
					}
				},
				yAxis: {
					title: {
						text: ''
					},
					minorTickInterval:"auto",
					minorGridLineWidth: 1,
					gridLineWidth: 2,//副格线的宽度
					labels:{},
					min: 35,
					max: 42,                        //显示的最大值
					maxPadding: 2,
				},
				tooltip: {
					valueSuffix: '°C'
				},
				legend: {
					enabled:false
				},
				exporting:{enabled:false},
				series: [{
					name: '',
					data: [35,36,37,35,36,37,39,38,36,37,39,37,37]
				}],
				credits:{enabled:false}
			});
		});
	</script>
	<div class="modelbg"></div>
	<div class="putbedidbox">
		<div class="text" >
			<div class="title">请填写病床ID
			</div>
			<table class="persontab">
				<tr><td>病床ID</td><td><input class="addbedid" type="text"></td></tr>
			</table>
		</div>
		<a href="javascript:void(0)" class="" onclick="$('.modelbg').hide();$('.putbedidbox').hide();addbed(nowaddBed);">确认</a><a href="javascript:void(0)" class="" onclick="$('.modelbg').hide();$('.putbedidbox').hide();">关闭</a>
	</div>
	<div class="bedmoreinfo">
		<div id="container" style="width: 650px; height: 400px; margin: 10px  auto 0px auto; "></div>
		<div  style="width: 650px; top:-15px; left: 17px; position: relative; font-size: 12px; height: 16px;text-align: center"><div id="day1" style="border:solid 1px #ccc;display: inline-block; width: 300px; border-right: none">2015.11.18</div><div id="day2" style="border:solid 1px #ccc;display: inline-block; width: 300px;">2015.11.19</div> </div>
		<div class="personid" style="margin-top: -8px">
		</div>
		<div style="padding:5px;"><input type="button" class="showinfo" onclick="" value="打印体温表" ><input type="button" class="listenbt" onclick="" value="出院"><input type="button" onclick="changeDepartment();" value="转科室"><input type="button" onclick="$('.bedmoreinfo').hide();$('.modelbg').hide();" value="关闭"></div>
	</div>
	<div class="addpersonbox">
		<div style="width: 320px; height: 34px;margin:26 20 0 20;">
		<span style="margin-left:0px;font-weight: bold; font-size: 18px; color: #3CB397">添加患者</span>
		<hr style="width: 320px; height: 1px; border: none; border-top: 1px solid #3CB397;" />
	    </div>
	    <span style="display:block;position:absolute;top:80px;left:50px;font-size:16px;color:#000;font-weight:bold;">患者信息</span>
	    <table  style="text-align:right;width:290px;font-size: 14px;color:#666666;margin-left:0px;margin-top:36px">
				<tr><td>病人姓名</td><td><input id="patientid" type="hidden"><input name="name" type="text"></td></tr>
				<tr><td>性别</td><td>&nbsp;<input name="sex" style="width:13px;height:13px;" type="radio" value="男"> &nbsp;男 &nbsp;&nbsp;<input name="sex" type="radio" style="width:13px;height:13px;" value="女">&nbsp;女</td></tr>
				<tr><td>年龄</td><td><input name="age" type="text" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
				<tr><td>入院时间</td><td><input name="roomtime" id="startTime" style=" cursor: pointer" class="Wdate" type="text"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:false})"></td></tr>
				<tr><td>病案号码</td><td><input name="sicknum" type="text"></td></tr>
				<tr><td>医师诊断</td><td><input name="sickname" type="text"></td></tr>
				<tr><td>主管医生</td><td><input name="doctor" type="text"></td></tr>
				<tr><td>病床号</td><td><input id="bedid" name="bedid" type="text"><input id="idofbed" name="idofbed" type="hidden"></td></tr>
				<tr><td>身份证</td><td><input name="personid" type="text" maxlength="18" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
				<tr><td>住址</td><td><input name="address" type="text"></td></tr>
				<tr><td>就医类型</td><td><input name="sicktype" type="text"></td></tr>
			</table>
	    <span style="display:block;position:absolute;top:520px;left:50px;font-size:16px;color:#000;font-weight:bold;">贴片信息</span>
		<table class="" style="text-align:right;width:290px;font-size: 14px;color:#666666;margin-left:0px;margin-top:60px">
				<tr><td>贴片号码</td><td><input name="cardname" type="text" maxlength="12"></td></tr>
				<tr><td>温度监控范围</td><td><select name="lowc"><option>35</option><option>36</option><option>37</option><option>38</option><option>39</option></select> ~ <select name="highc"><option>41</option><option>40</option><option>39</option><option>38</option><option>37</option></select></td></tr>
			</table>
			<div onclick="cancelAddPatient()" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#666666;border:1px solid #666666;
				position:relative;margin-left:172px;margin-top:32px;padding-top:4px;">关闭</div>
			<div id="yes33" onclick="sureAddPersonCom()" style="cursor:pointer;width:80px;height:20px;border-radius: 15px;text-align:center;font-size:14px;color:#3CB397;border:1px solid #3CB397;
				position:relative;margin-left:262px;margin-top:-26px;padding-top:4px;float:left">确认</div>
		<!-- <div class="text" >
			<div class="title">添加病人</div>
			<div class="nav"><a href="javascript:void(0)" class="checked personbt" onclick="$('.persontab').show();$('.tiepiantab').hide();$('.tiepianbt').removeClass('checked');$(this).addClass('checked')">病人信息</a>
				<a href="javascript:void(0)" class="tiepianbt" onclick="$('.persontab').hide();$('.tiepiantab').show();$('.personbt').removeClass('checked');$(this).addClass('checked')">贴片信息</a> </div>
			<table class="persontab">
				<tr><td>病人姓名</td><td><input id="patientid" type="hidden"><input name="name" type="text"></td></tr>
				<tr><td>性别</td><td>&nbsp;<input name="sex" type="radio" value="男"> &nbsp;男 &nbsp;&nbsp;<input name="sex" type="radio" value="女">&nbsp;女</td></tr>
				<tr><td>年龄</td><td><input name="age" type="text" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
				<tr><td>入院时间</td><td><input name="roomtime" id="startTime" style=" cursor: pointer" class="Wdate" type="text"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:false})"></td></tr>
				<tr><td>病案号码</td><td><input name="sicknum" type="text"></td></tr>
				<tr><td>医师诊断</td><td><input name="sickname" type="text"></td></tr>
				<tr><td>主管医生</td><td><input name="doctor" type="text"></td></tr>
				<tr><td>病床号</td><td><input id="bedid" name="bedid" type="text"><input id="idofbed" name="idofbed" type="hidden"></td></tr>
				<tr><td>身份证</td><td><input name="personid" type="text" maxlength="18" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
				<tr><td>住址</td><td><input name="address" type="text"></td></tr>
				<tr><td>就医类型</td><td><input name="sicktype" type="text"></td></tr>
			</table>
			<table class="tiepiantab" style="display: none">
				<tr><td>贴片号码</td><td><input name="cardname" type="text" maxlength="12"></td></tr>
				<tr><td>温度监控范围</td><td><select name="lowc"><option>35</option><option>36</option><option>37</option><option>38</option><option>39</option></select> ~ <select name="highc"><option>41</option><option>40</option><option>39</option><option>38</option><option>37</option></select></td></tr>
			</table>
		</div>
		<a href="javascript:void(0)" class="" onclick="sureAddPerson()">确认</a><a href="javascript:void(0)" class="" onclick="$('.addpersonbox').hide();$('.modelbg').hide()">关闭</a> -->
	</div>
</body>
</html>