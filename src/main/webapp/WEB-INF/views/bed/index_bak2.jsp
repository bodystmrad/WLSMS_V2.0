<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd“>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<title>WLSMS</title>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/bed/index.css">
<script type="text/javascript" src="${ctx}/static/js/layui-v2.1.7/layui.all.js"></script>
<link rel="stylesheet" href="${ctx}/static/js/layui-v2.1.7/css/layui.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<link rel="shortcut icon" href="../images/c_temperature.png"/>
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<script src="${ctx }/highcharts/highstock-all.js"></script>
<%-- <link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css"> 
<script src="${ctx }/highcharts/modules/exporting.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/static/js/layer/layer.js"></script> --%>
<script src="${ctx }/static/js/bed/bed.js"></script>
<script src="${ctx }/static/js/chartManage.js"></script>
<style>
</style>
</head>
<body>
<audio id="snd" src="" autoplay="true" width=0  height=0 loop="true"></audio>

<div id="page">
	<div class="header">
		<div class="headermain clearfix" style="width:500px">
			<!-- <div class="logo"></div> -->
			<div style="position:absolute;top:10px;left:20px;background: url('..${logoPath }') #52b5ff no-repeat;width: 200px; height: 40px;"></div>
			<center style="height: 60px;">
				<span
					style="font-size: 24px; color: #FFFFFF; top: 25%; position: relative;">无线生命体征监测客户端</span>
					<span style="font-size:12px;color:#FFFFFF;float:right;margin-top:30px;margin-right:120px;">v1.1.1</span>
			</center>
			<%-- div class="logininfo"><span>${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }"></div> --%>
		</div>
		
	</div>
	<div class="main">
		<div class="menulist">
			<ul style="padding: 0px; margin: 0px">
				<li>
					<div class="mainmenu">
						<span class="mainmenucontent"
							style="background: url(../images/icon_temperature_monitor.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
							体征监控 </span>
						<hr style="margin-top: -10px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
					</div>
				</li>

				<li>
					<div id="showunnormal" class="menulv2">
						<span id="unnormalTemperature" class="highandlowcount"
							style="display: block; width: 120px; text-align: center; font-size: 60px; color: orange; line-height: 40px; padding-top: 40px">4</span><br />
						<span
							style="display: block; width: 120px; text-align: center; font-size: 14px; color: #FFFFFF">体征异常</span>
					</div>
				</li>

				<li>
					<div id="shownormal" class="menulv2 menuclicked">
						<span id="unnormalTemperature"
							style="display: block; width: 120px; text-align: center; font-size: 60px; color: green; line-height: 40px; padding-top: 40px">
							<span class="notemptycount">18</span><span style="font-size: 25px; color: #FFFFFF">/<span class="totalcount">20</span></span>
						</span><br /> 
						<span style="display: block; width: 120px; text-align: center; font-size: 14px; color: #FFFFFF">床位信息</span>
					</div>
				</li>

				<li>
					<div id="docManage" class="mainmenu">
						<hr
							style="margin-top: 0px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						<span class="mainmenucontent"
							style="background: url(../images/icon_files_manage.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
							档案管理 </span>
						<hr
							style="margin-top: -11px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
					</div>
				</li>
				<li>
					<div id="concentratorManage" class="mainmenu">
						<hr
							style="margin-top: 0px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
						<span class="mainmenucontent"
							style="background: url(../images/icon_setting.png) no-repeat 0px 3px; padding-left: 25px; line-height: 60px;">
							设备管理 </span>
						<hr
							style="margin-top: -11px; margin-left: 5px; width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
					</div>
				</li>
			</ul>
			<div style="width:120px;height:50px;position:absolute;bottom:20px;text-align:center;">
				<span style="font-size:14px;">账号：${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }">
				<hr style="margin-top: 5px;  width: 100px; height: 1px; border: none; border-top: 1px solid #555555;" />
				<a href="../home/logOut.action" style="text-decoration:none;font-size:14px;color:#FFFFFF;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</a><!-- <span onclick="logOut();" style="cursor:pointer;font-size:14px;padding-left:20px;background:url(../images/icon_logout.png) no-repeat 0 3px">退出登录</span> -->
			</div>
		</div>
		<!-- 主要显示区域 -->
		<div class="mainArea" style="position:fixed;left:120px;top:60px">
			<div class="pageConcentratorManage">
				
				
			</div>
			
	</div>
</div>
</div>
<div class="modelbg"></div>	
	
</body>
</html>