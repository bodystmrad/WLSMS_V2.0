<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>档案管理</title>
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/normalize.css">
<link rel="stylesheet" type="text/css" href="${ctx }/static/css/common.css">
<script type="text/javascript" src="${ctx }/static/js/jquery.min.js"></script>
<script src="${ctx }/static/js/common.js"></script>
<script src="${ctx }/static/js/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx }/highcharts/highcharts.js"></script>
<script src="${ctx }/modules/exporting.js"></script>
<style>
.main {
	width: 980px;
	margin: 0px auto;
}

.roombox {
	margin: 10px 0px;
}

.roombox .title {
	
}

.roombox .bedbox li {
	border: solid 1px #ccc;
	height: 200px;
	width: 100px;
	margin: 10px 0px;
	float: left;
	width: 313px;
	height: 500px;
	margin-right: 11px;
}

.bedmoreinfo {
	width: 651px;
	height: 500px;
	border: solid 1px #ccc;
	display: none;
	position: absolute;
	background: #fff;
	border-left: none
}

#page {
	height: 100%;
	width: 100%;
	background: none;
	font-family: "微软雅黑";
	position: fixed;
	left: 0px;
	top: 0px;
	z-index: 0
}

.fr {
	float: right
}

.tab_list {
	margin: 0 auto;
	width: 964px;
	padding-right: 16px;
}

.tab_head {
	width: 964px;
	height: 40px;
	padding-right: 16px;
}

.tab_body {
	width: 964px;
	padding-right: 16px;
}

.tab_a {
	font-size: 16px;
	color: #0191ff;
	line-height: 40px;
}

.tab_b {
	font-size: 14px;
	color: #667078;
	line-height: 30px;
}

.tab_c {
	font-size: 12px;
	color: #383838;
	line-height: 40px;
}

.tab_d {
	font-size: 14px;
	color: #383838;
	line-height: 22px;
}

.tab_e {
	font-size: 12px;
	color: #0191ff;
	line-height: 30px;
	cursor: pointer
}

.tab_f {
	font-size: 12px;
	color: #0191ff;
	line-height: 30px;
	cursor: pointer
}

.add_one {
	padding: 2px 4px;
	font-size: 12px;
	color: #0191ff;
	border: 1px solid #0191ff;
	margin-top: 10px
}

.page_number {
	width: 22px;
	height: 22px;
	border: 1px solid #0191ff;
	margin-left: 8px;
	text-align: center
}

.page_number span {
	display: block;
	cursor: pointer
}

.page_gray {
	width: 22px;
	height: 22px;
	border: 1px solid #d2dde5;
	margin-left: 8px;
	text-align: center
}

.page_gray span {
	display: block;
	cursor: pointer
}

.page_ge {
	width: 964px;
	height: 40px;
	padding-right: 16px;
	margin-top: 30px;
}
</style>
<script>
        var pageSize = 15; //每页显示个数
        var nowpage = 1; //当前第几页
        var totalPages = 1;
        var nowUpdId = 0;
        var nowDelId=0;
        var nowDelBedName="";
        var tempTimer;
        function getClientHeight(){
			return  document.body.scrollHeight > document.documentElement.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight;
		}
		function getClientWidth(){
			return  document.body.scrollWidth > document.documentElement.scrollWidth?document.body.scrollWidth:document.documentElement.scrollWidth;
		}
        

        $(function(){
         getListData();
         function addlisten(){
             $(".pagenumbox").find(".fr").off("click")
             $(".pagenumbox").find(".fr").click(function(){
                   nowpage = $(this).text();
                   getListData();
             })
             //监听修改按钮
             $(".tab_e").off("click")
             $(".tab_e").click(function(){
                 nowUpdId = $(this).attr("personnelId");
              $(".modelbg").show();
              //$(".putpwdbox").show();
              $("#title").text("修改病人");
              surePutPwd();
             })
             //监听删除按钮
             $(".tab_f").off("click")
             $(".tab_f").click(function(){
            	 nowDelId=$(this).attr("personnelId");
            	 nowDelBedName=$(this).attr("bedName");
              $(".modelbg").show();
              $(".confirmbox").show();
             })
             //监听确认删除病人
             $(".yes").off("click")
             $(".yes").click(function(){
              delPerson();
             })
             $(".cancel").off("click")
             $(".cancel").click(function(){
              $(".modelbg").hide();
              $(".confirmbox").hide();
             })
             $(".prevPageBt").off("click");
             $(".prevPageBt").click(function(){
                  if(nowpage == 1){
                      return;
                  }else{
                     nowpage--;
                  }
              getListData();
             })
             $(".nextPageBt").off("click");
             $(".nextPageBt").click(function(){
               if(nowpage == totalPages){
                   return;
               }else{
                   nowpage++;
               }
              getListData();
             })
         }

         function delPerson(){
          $.ajax({
            type:"post",
            url:"delete.action",//此处返回true或false
            dataType:"json",
            data:{id:nowDelId,bedname:nowDelBedName},
            success:function(data){
               $(".modelbg").hide();
               $(".confirmbox").hide();
               location.reload(true);
            }
          })
         }

         function putPageNum(){
            $(".pagenumbox").empty();
            var startPage = 1;
             if(nowpage > 3){
                if((totalPages - nowpage) >= 2){
                   startPage = nowpage-2;
                }else{
                   startPage = nowpage-(4-(totalPages - nowpage));
                }

             }else{
                startPage = 1;
             }
             // $(".page_ge").
             var forcount = 5;
             if(totalPages >= 5){
              forcount = 5;
             }else{
              forcount = totalPages;
             }

             for(var i = 0; i < forcount; i++){
                 var tempPage = startPage+forcount-1-i;
                 var tempClass = "page_gray";
                 if(tempPage == nowpage){
                  tempClass =  "page_number"
                 }else{
                  tempClass = "page_gray";
                 }
                 $(".pagenumbox").append(' <div class="'+tempClass+' fr">'+
                                               '<span class="tab_d">'+tempPage+'</span>'+
                                            '</div>')
             }
         }

         function getListData(){
             $.ajax({
              type:"post",
              url:"../patient/list.action",//此处返回true或false
              dataType:"json",
              data:{"pageNo":nowpage,"pageSize":pageSize,"userId":""+$("#user_attr_id").val()+""},
              success:function(data){
                   if(data.total%pageSize != 0){
                       totalPages = parseInt(data.total/pageSize) +1;
                   }else{
                       totalPages =  parseInt(data.total/pageSize);
                   }
               putPageNum();
                   putData(data.rows);
                   $("#patientCount").text("共"+data.total+"位病人")
              }
             })
         }

         function putData(data){
        	
               $(".listbox").empty();
               $(".listbox").append('  <tr style=" background:#e4eff7;">'+
               '<td align="center"><span class="tab_b">病床号码</span></td>'+
               '<td align="center"><span class="tab_b">患者姓名</span></td>'+
               '<td align="center"><span class="tab_b">性别/年龄</span></td>'+
               '<td align="center"><span class="tab_b">医师诊断</span></td>'+
               '<td align="center"><span class="tab_b">病案号码</span></td>'+
               '<td align="center"><span class="tab_b">温度监控起始时间</span></td>'+
               '<td align="center"><span class="tab_b">贴片号码</span></td>'+
               '<td align="center"><span class="tab_b">实时温度</span></td>'+
               '<td align="center"><span class="tab_b">功能炒作</span></td>'+
               '</tr>');

                $(data).each(function(index,dom){
                 $(".listbox").append('  <tr style=" border-bottom:1px solid #e4ebf1">'+
                 '<td align="center"><span class="tab_b">'+dom.bedid+'</span></td>'+
                 '<td align="center"><span class="tab_b" style="cursor:pointer;" onclick="showDegree(this);" bedid="'+dom.bedid+'">'+dom.name+'</span></td>'+
                 '<td align="center"><span class="tab_b">'+dom.sex+'/'+dom.age+'</span></td>'+
                 '<td align="center"><span class="tab_b">'+dom.diagnosis+'</span></td>'+
                 '<td align="center"><span class="tab_b">'+dom.sickid+'</span></td>'+
                 '<td align="center"><span class="tab_b">'+dom.startTime+'</span></td>'+
                 '<td align="center"><span class="tab_b">'+dom.cardnum+'</span></td>'+
                 '<td align="center"><span class="tab_b">'+dom.degree+'</span></td>'+
                 '<td align="center"><span class="tab_e" personnelId="'+dom.id+'">修改</span>&nbsp;<span class="tab_f" bedName="'+dom.bedid+'"  personnelId="'+dom.id+'">删除</span></td>'+
                 '</tr>');
                })
               addlisten()
         }
        })
	</script>
	<script type="text/javascript">
	var bedsinfo = {};//当前所有病床的信息
	var bedsBasicinfo = {};
	var bedsOtherinfo = {};//当前所有病床的其他信息
	var chartObj = {};//当前chart表对象
	var nowDelBed = {};//当前要删除的病房
	var nowAddPersonBed = {};//当前要添加病人的病床
	var nowStopBed = {};//当前要停止监控的病床
	var nowaddBed = {};//当前要添加的病床
	var startHour=2;//折线图从几时开始
	var outpatientid="";//当前要出院的病人
		$(function () {
			chartObj =  new Highcharts.Chart({
				chart: {
					spacingTop: 20,
					renderTo: 'container',
					//defaultSeriesType: 'line' //图表类别，可取值有：line、spline、area、areaspline、bar、column等
				},
				title: {
					text: '',
				},
				xAxis: {
					tickmarkPlacement:'between',//'on',
					tickInterval: 1,
					minorTickInterval: 0.5,
					gridLineWidth: 2,
					labels:{
						formatter:function(){
							if((this.value*2+startHour)>24){
								return (this.value*2+startHour)-24 ;
							}else{
								return this.value*2+startHour ;
							}

						}
					}
				},
				yAxis: {
					title: {
						text: ''
					},
					minorTickInterval:"auto",
					minorGridLineWidth: 1,
					gridLineWidth: 2,//副格线的宽度
					labels:{},
					min: 35,
					max: 42,                        //显示的最大值
					maxPadding: 2,
				},
				tooltip: {
					valueSuffix: '°C'
				},
				legend: {
					enabled:false
				},
				exporting:{enabled:false},
				series: [{
					name: '',
					data: [35,36,37,35,36,37,39,38,36,37,39,37,37]
				}],
				credits:{enabled:false}
			});
		});
		function showDegree(target){
			//***********************************************
			$.ajax({
				type:"post",
				url:"../bed/listAll.action",//"bed/listAll.action",//"tiwen/bedinfo.json",
				dataType:"json",
				data:{},
				success:function(data){
					//$(".totalcount").html(data.total)
					bedsinfo = data.data;
					bedsOtherinfo = data.otherdata;
					//alert(bedsOtherinfo[1000]);
					$(bedsinfo).each(function(index,dom) {
						//alert(dom.bedstate);
						if(dom.bedstate == "empty"){
							
						}
						else{
							//alert(dom.range);
							bedsBasicinfo[dom.bedid]=dom;
						}
						});
					//*******************************************
					var prototype = $(this);//.parent()[0];
					var bedid = $(target).attr("bedid");//id
					//alert(bedid);
					var nowaboutinfo = bedsOtherinfo[bedid];
					var nowaboutinfoBed = bedsBasicinfo[bedid];
					
					
					//alert(nowaboutinfo.personid);
					//给chat赋值
					var contentInfo="<table style='width:100%'>";
					contentInfo+="<tr><td>监控范围："+nowaboutinfoBed.range+"℃</td><td>监控时间："+nowaboutinfoBed.listentime+"</td><td>贴片号码："+nowaboutinfoBed.num+"</td>";
					contentInfo+="<tr><td>剩余时间：约"+nowaboutinfoBed.surplustime+"小时</td><td>病人信息："+nowaboutinfoBed.personinfo+"</td><td>医师诊断："+nowaboutinfoBed.diagnosis+"</td>";
					contentInfo+="<tr><td>入院时间："+nowaboutinfoBed.roomtime+"</td><td>病案号码："+nowaboutinfoBed.sicknum+"</td><td>主治医师："+nowaboutinfoBed.doctor+"</td>";
					contentInfo= contentInfo+"</table>";
					$(".bedmoreinfo > .personid").html(contentInfo);
					//outpatientid=$(prototype).attr("patientid");
					outpatientid=nowaboutinfoBed.patientid;
					//var str =$(prototype).attr("roomtime");
					var str=nowaboutinfoBed.roomtime
					// 转换日期格式
					str = str.replace(/-/g, '/'); // "2010/08/01";
					$("#day1").text($(prototype).attr("roomtime"));
					// 创建日期对象
					var date = new Date(str);
					// 加一天
					date.setDate(date.getDate()+1);//获取AddDayCount天后的日期
				    var y = date.getFullYear();
				    var m = date.getMonth()+1;//获取当前月份的日期
				    var d = date.getDate();
					$("#day2").text(y+"-"+m+"-"+d);
					//chartObj.xAxis[0].setCategories(nowaboutinfo.times);
					chartObj.series[0].setData(nowaboutinfo.degrees);
					//chartObj.series[0].setData([35,36,37,35,36,37,39,38,36,37,39,37,37,35,36,37,37,37,37,37,37,37,37,37]);
					startHour=nowaboutinfo.times;
					//alert(startHour);
					if(startHour==null||startHour==undefined)startHour=2;
					//****************************************************如果温度数据数量不够一天则显示一天的日期
					if (nowaboutinfo.degrees.length>0&&(startHour+(nowaboutinfo.degrees.length-1)*2)<=24) {
						$("#day2").css("display","none");
						$("#day1").css("width","600px");

					}else{
						$("#day2").css("display","inline-block");
						$("#day1").css("width","300px");
					}
					//****************************************************

					if(tempTimer){
						clearTimeout(tempTimer);
					}
					tempTimer = setTimeout(function(){
						var left = prototype.offsetLeft;
						var right =  getClientWidth() - prototype.offsetLeft - prototype.offsetWidth;
						var top = prototype.offsetTop;
						var bottom = getClientHeight() - prototype.offsetTop;
						var infoLeft = 0;
						if(left > right){
							infoLeft = left - $(".bedmoreinfo").width();
						}else{
							infoLeft = left+prototype.offsetWidth;
						}
						$(".bedmoreinfo").css({"left":infoLeft+"px","top":top+"px"}).show()
					},500)
				}
			})
			//***********************************************
			
		}
	</script>
</head>
<body>
	<div id="page">
		<div class="header clearfix">
			<div class="headermain clearfix">
				<div class="logo"><span style="font-size:8px;margin-left:70px;color:white;margin-top:40px;display:block;" >v1.1.1.20151129_ release</span></div>
				<div class="nav">
					<a href="${ctx }/bed/degreelisten.action">温度监控</a><a
						href="javascript:void(0)" class="checked">档案管理</a>
					<!-- <a href="deviceManage.html">设备管理</a> -->
				</div>
				<div class="logininfo">
					<span>${user_attr.realName }</span><input id="user_attr_id" type="hidden" value="${user_attr.id }">
				</div>
			</div>
		</div>
		<div class="tab_list">
		<div class="tab_head">
			<img src="../images/nu.png"
				style="margin-bottom: -2px; margin-right: 5px;"><span
				id="patientCount" class="tab_a">共位病人</span> <span
				class="add_one fr" style="cursor: pointer" onclick="openAddPerson()">添加病人</span>
		</div>
		<div class="tab_body">
			<table class="listbox" width="964" border="0" cellspacing="0"
				cellpadding="0">
				<tr style="background: #e4eff7;">
					<td align="center"><span class="tab_b">病床号码</span></td>
					<td align="center"><span class="tab_b">患者姓名</span></td>
					<td align="center"><span class="tab_b">性别/年龄</span></td>
					<td align="center"><span class="tab_b">医师诊断</span></td>
					<td align="center"><span class="tab_b">病案号码</span></td>
					<td align="center"><span class="tab_b">温度监控起始时间</span></td>
					<td align="center"><span class="tab_b">贴片号码</span></td>
					<td align="center"><span class="tab_b">实时温度</span></td>
					<td align="center"><span class="tab_b">功能炒作</span></td>
				</tr>
			</table>
			<div class="page_ge">
				<div class="page_gray fr nextPageBt">
					<span class="tab_d">></span>
				</div>
				<div class="pagenumbox">
					<div class="page_gray fr">
						<span class="tab_d">5</span>
					</div>
					<div class="page_gray fr">
						<span class="tab_d">4</span>
					</div>
					<div class="page_gray fr">
						<span class="tab_d">3</span>
					</div>
					<div class="page_gray fr">
						<span class="tab_d">2</span>
					</div>
					<div class="page_number fr">
						<span class="tab_d">1</span>
					</div>
				</div>
				<div class="page_gray fr prevPageBt">
					<span class="tab_d"><</span>
				</div>

			</div>
		</div>
	</div>
</div>


<div class="bedmoreinfo" style="margin:200px;">
		<div id="container" style="width: 650px; height: 400px; margin: 10px  auto 0px auto; "></div>
		<div  style="width: 650px; top:-15px; left: 17px; position: relative; font-size: 12px; height: 16px;text-align: center"><div id="day1" style="border:solid 1px #ccc;display: inline-block; width: 300px; border-right: none">2015.11.18</div><div id="day2" style="border:solid 1px #ccc;display: inline-block; width: 300px;">2015.11.19</div> </div>
		<div class="personid" style="margin-top: -8px">
		</div>
		<div style="padding:5px;"><input type="button" class="showinfo" onclick="" value="打印体温表" ><!-- <input type="button" class="listenbt" onclick="" value="出院"><input type="button" onclick="" value="转科室"> --><input type="button" onclick="$('.bedmoreinfo').hide();" value="关闭"></div>
	</div>


<div class="modelbg"></div>
<div class="confirmbox">
 <div class="text" >确认删除吗？</div>
 <a href="javascript:void(0)" class="yes">确认</a><a href="javascript:void(0)" class="cancel">取消</a>
</div>
<div class="addpersonbox">
 <div class="text" >
  <div class="title" id="title">添加病人</div>
  <div class="nav"><a href="javascript:void(0)" class="checked personbt" onclick="$('.persontab').show();$('.tiepiantab').hide();$('.tiepianbt').removeClass('checked');$(this).addClass('checked')">病人信息</a>
   <a href="javascript:void(0)" class="tiepianbt" onclick="$('.persontab').hide();$('.tiepiantab').show();$('.personbt').removeClass('checked');$(this).addClass('checked')">贴片信息</a> </div>
  <table class="persontab">
   <tr><td>病人姓名</td><td><input id="patientid" type="hidden"><input name="name" type="text" ></td></tr>
   <tr><td>性别</td><td> &nbsp;<input name="sex" type="radio" value="男"> &nbsp;男 &nbsp;  &nbsp;<input name="sex" type="radio" value="女">&nbsp; 女</td></tr>
   <tr><td>年龄</td><td><input name="age" type="text"　style="IME-MODE: disabled;" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')" ></td></tr>
   <tr><td>入院时间</td><td><input name="roomtime" id="startTime" style=" cursor: pointer" class="Wdate" type="text"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:false})"><!-- <input name="roomtime" type="text"> --></td></tr>
   <tr><td>病案号码</td><td><input name="sicknum" type="text"></td></tr>
   <tr><td>医师诊断</td><td><input name="sickname" type="text"></td></tr>
   <tr><td>主管医生</td><td><input name="doctor" type="text"></td></tr>
   <tr><td>病床号</td><td><input name="bedid" type="text"><input id="idofbed" name="idofbed" type="hidden"></td></tr>
   <tr><td>身份证</td><td><input name="personid" type="text" maxlength="18" onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')"></td></tr>
   <tr><td>住址</td><td><input name="address" type="text"></td></tr>
   <tr><td>就医类型</td><td><input name="sicktype" type="text"></td></tr>
  </table>
  <table class="tiepiantab" style="display: none">
   <tr><td>贴片号码</td><td><input name="cardname" type="text" maxlength="12"></td></tr>
   <tr><td>温度监控范围</td><td><select name="lowc"><option >35</option><option>36</option><option>37</option><option>38</option><option>39</option></select> ~ <select name="highc"><option>41</option><option>40</option><option>39</option><option>38</option><option>37</option></select></td></tr>
  </table>
 </div>
 <a href="javascript:void(0)" class="" onclick="sureAddPersonCom()">确认</a><a href="javascript:void(0)" class="" onclick="$('.addpersonbox').hide();$('.modelbg').hide()">关闭</a>
</div>

<div class="updpwdbox">
 <div class="text" >
  <div class="title">修改密码</div>
  <table class="persontab">
   <tr><td>原密码</td><td><input name="oldpwd" type="password"></td></tr>
   <tr><td>新密码</td><td><input name="newpwd" type="password"></td></tr>
   <tr><td>确认新密码</td><td><input name="surenewpwd" type="password"></td></tr>
  </table>
 </div>
 <a href="javascript:void(0)" class="" onclick="updpwd()">确认</a><a href="javascript:void(0)" class="" onclick="closeupdpwd()">关闭</a>
</div>

<div class="putpwdbox">
 <div class="text" >
  <div class="title">修改信息需要管理员密码
  </div>
  <table class="persontab">
   <tr><td>输入密码</td><td><input name="oldpwd" type="password"></td></tr>
  </table>
 </div>
 <a href="javascript:void(0)" class="" onclick="surePutPwd()">确认</a><a href="javascript:void(0)" class="" onclick="closePutPwd()">关闭</a>
</div>
	
</body>
<script>
//修改病人信息
 function surePutPwd(){
	 
     //cleanForm();
     //添加病人公用
         $.ajax({
             type:"post",
             url:"getPatientById.action",//添加病人信息
             dataType:"json",
             data:{id:nowUpdId},
             success:function(data){
            	 $("#patientid").val(data.id);
                  $(".addpersonbox").find("input[name=name]").val(data.name);//病人姓名
                $(".addpersonbox").find("input[name=sex][value="+data.sex+"]").click();//性别
                  $(".addpersonbox").find("input[name=age]").val(data.age);//年龄
                $(".addpersonbox").find("input[name=roomtime]").val(data.roomtime);//入院时间
                $(".addpersonbox").find("input[name=sicknum]").val(data.sickid);//病案号码
                 $(".addpersonbox").find("input[name=sickname]").val(data.sickname);//所患疾病
                 $(".addpersonbox").find("input[name=doctor]").val(data.doctor);//主管医生
                 $(".addpersonbox").find("input[name=roomid]").val(data.roomid);//病房号
                $(".addpersonbox").find("input[name=bedid]").val(data.bedid);//病床号
                $("#idofbed").val(data.idofbed);//病床id
                 $(".addpersonbox").find("input[name=cardname]").val(data.cardname);//贴片号码
                $(".addpersonbox").find("select[name=highc]").val(data.highc);//温度监控范围 最低温度
                $(".addpersonbox").find("select[name=lowc]").val(data.lowc);//温度监控范围 最高温度
                $(".addpersonbox").find("input[name=count]").val(data.count);//温度采集间隔数字
                 $(".addpersonbox").find("input[name=sicktype]").val(data.sicktype);//就医类型
                $(".addpersonbox").find("input[name=personid]").val(data.personid);//身份证
                $(".addpersonbox").find("input[name=address]").val(data.address);//地址
                 $(".putpwdbox").hide();
                 $(".addpersonbox").show();
             }
         })
 }
 function closePutPwd(){
   $(".putpwdbox").hide();
  $(".modelbg").hide();
 }
</script>
</html>