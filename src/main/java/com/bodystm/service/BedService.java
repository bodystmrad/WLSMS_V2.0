package com.bodystm.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Bed;
import com.bodystm.bean.Patient;
import com.bodystm.dao.BedDao;
import com.bodystm.dao.PatientDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 病人信息的业务类
 * 
 * @author ggeagle
 *
 */
@Service
@Transactional
public class BedService extends CommonService<Bed, String> {
	@Resource
	private BedDao bedDao;

	@Resource
	private PatientDao patientDao;

	@Override
	public void saveEntity(Bed bed) {
		super.saveEntity(bed);
		Patient patient = bed.getPatient();
		if (patient != null) {
			//patient = new Patient();
			//patient.setBed(bed);//(bed.getId());***************************************
			this.patientDao.save(patient);
		}
	}
	
	public Bed getBedByName(String bedName,String userId){
		return bedDao.getBedByName(bedName,userId);
	}
	
	public void outBed(String ids) {
		if (StringUtils.isNotBlank(ids)) {
			String[] idValues = StringUtils.split(ids, ',');
			List<Patient> patientList = null;
			for (String idv : idValues) {
				patientList = this.patientDao.find(PatientDao.PATIENT_SQL, idv);
				if (patientList != null) {
					for (Patient p : patientList) {
						p.setEndTime(new Date());
						this.patientDao.save(p);
					}
				}
			}
		}
	}

	@Override
	public Bed getEntity(String id) {
		Bed bed = super.getEntity(id);
		List<Patient> patientList = this.patientDao.find(
				PatientDao.PATIENT_SQL, id);
		if (patientList != null && patientList.size() > 0) {
			bed.setPatient(patientList.get(0));
		}
		return bed;
	}
	

	public Bed getPatientIdBybed(String mac) {
		return bedDao.getPatientIdBybed(mac);
	}

	@Override
	protected HibernateDao<Bed, String> getHibernateDao() {
		return this.bedDao;
	}
	
	public void deleteByIdofBedbysql(String bedId) {
		this.bedDao.deleteByIdofBedbysql(bedId);
	}
	
	public List<Bed> getNotEmptyBeds4CheckConcentrator(String userId){
		return this.bedDao.getNotEmptyBeds4CheckConcentrator(userId);
	}
	
	/**
	 * 限制床位条数-测试
	 * @return
	 */
	public List<Bed> query(){
		return this.bedDao.query();
	}
	
}