package com.bodystm.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Alarm;
import com.bodystm.bean.AlarmRecord;
import com.bodystm.dao.AlarmRecordDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

@Service
@Transactional
public class AlarmRecordService extends CommonService<AlarmRecord, String>{
	
	@Resource
	private AlarmRecordDao alarmRecordDao;
	
	@Override
	protected HibernateDao<AlarmRecord, String> getHibernateDao() {
		return this.alarmRecordDao;
	}
}
