package com.bodystm.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Patient;
import com.bodystm.bean.PatientBedUser;
import com.bodystm.dao.PatientBedDao;
import com.bodystm.dao.PatientDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 病人信息的业务类
 * 
 * @author ggeagle
 *
 */
@Service
@Transactional
public class PatientBedService extends CommonService<PatientBedUser, String> {

	public Patient findByBedIdAndEndTimeIsNull(String bedId) {
		return this.getHibernateDao().findUnique(PatientDao.PATIENT_SQL, bedId);
	}

	@Resource
	private PatientBedDao patientBedDao;

	@Override
	protected HibernateDao<PatientBedUser, String> getHibernateDao() {
		return this.patientBedDao;
	}
	public void deleteById(String personalId) {
		this.patientBedDao.deleteById(personalId);
	}
	public List<Patient> getPatientsOfDepartment(String userId){
		return this.patientBedDao.getPatientsOfDepartment(userId);
	}
	public PatientBedUser getPatientBedUserByPatientAnddBed(String patientId,String bedId){
		return this.patientBedDao.getPatientBedUserByPatientAnddBed(patientId,bedId);
	}
	public PatientBedUser getPatientBedUserByBedName(String bedId){
		return this.patientBedDao.getPatientBedUserByBedName(bedId);
	}
	public void deleteByPatientId(String patientId){
		this.patientBedDao.deleteByPatientId(patientId);
	}
	public void deleteByBedIdAndPatientId(String bedId,String patientId){
		this.patientBedDao.deleteByBedIdAndPatientId(bedId,patientId);
	}
	public PatientBedUser getPatientBedUserByMac(String strMac){
		return this.patientBedDao.getPatientBedUserByMac(strMac);
	}
	public List<PatientBedUser> getCurrentPatients(){
		List<PatientBedUser> lstpatientBedUser=patientBedDao.getCurrentPatients();
		return lstpatientBedUser;
	}
}