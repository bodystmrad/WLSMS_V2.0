package com.bodystm.service;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.PatientEquip;
import com.bodystm.dao.PatientEquipDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 病人信息的业务类
 * 
 * @author ggeagle
 *
 */
@Service
@Transactional
public class PatientEquipService extends CommonService<PatientEquip, String> {

	@Resource
	private PatientEquipDao patientEquipDao;

	@Override
	protected HibernateDao<PatientEquip, String> getHibernateDao() {
		return this.patientEquipDao;
	}
	public void deleteById(String personalId) {
		this.patientEquipDao.deleteById(personalId);
	}

	public List<PatientEquip> getPatientEquipByPatient(String patientId){
		return this.patientEquipDao.getPatientEquipByPatient(patientId);
	}
	public List<PatientEquip> getPatientEquipByMac(String strMac){
		return this.patientEquipDao.getPatientEquipByMac(strMac);
	}
	public void deleteByPatientId(String patientId){
		this.patientEquipDao.deleteByPatientId(patientId);
	}
	public void deleteAllByMac(String mac){
		this.patientEquipDao.deleteAllByMac(mac);
	}
}