package com.bodystm.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Concentrator;
import com.bodystm.dao.ConcentratorDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

@Service
@Transactional
public class ConcentratorService extends CommonService<Concentrator,String>{

	@Resource
	private ConcentratorDao concentratorDao;
	
	@Override
	protected HibernateDao<Concentrator, String> getHibernateDao() {
		// TODO Auto-generated method stub
		return this.concentratorDao;
	}

}
