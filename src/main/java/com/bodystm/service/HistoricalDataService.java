package com.bodystm.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Bed;
import com.bodystm.bean.HistoricalData;
import com.bodystm.bean.Patient;
import com.bodystm.dao.HistoricalDataDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 历史数据回顾
 * @author Administrator
 *
 */
@Service
@Transactional
public class HistoricalDataService extends CommonService<HistoricalData, String>{

	@Autowired
	private HistoricalDataDao HistoricalData;
	
	@Override
	protected HibernateDao<HistoricalData, String> getHibernateDao() {
		return null;
	}
	
	public List<HistoricalData> getHistoricalDataByPatientId(String Patient_Id,String Starttime, String Endtime,String Resolution){
		return HistoricalData.getHistoricalDataByPatientId(Patient_Id,Starttime,Endtime,Resolution);
	}
	
}
