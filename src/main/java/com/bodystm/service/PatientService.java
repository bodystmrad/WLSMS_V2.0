package com.bodystm.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Patient;
import com.bodystm.dao.PatientDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 病人信息的业务类
 * 
 * @author ggeagle
 *
 */
@Service
@Transactional
public class PatientService extends CommonService<Patient, String> {

	public Patient findByBedIdAndEndTimeIsNull(String bedId) {
		try {
			return this.getHibernateDao().findUnique(PatientDao.PATIENT_SQL, bedId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Resource
	private PatientDao patientDao;

	@Override
	protected HibernateDao<Patient, String> getHibernateDao() {
		return this.patientDao;
	}
	/**
	 * 根据身份证号删除病人
	 * @param personnelId
	 * @return
	 */
	public void deleteById(String personalId) {
		this.patientDao.deleteById(personalId);
	}
	
	public void deleteByBedId(String bedId) {
		this.patientDao.deleteByBedId(bedId);
	}
	public Patient getPatientByMac(String strMac) {
		try {
			return this.getHibernateDao().findUnique(PatientDao.FindPatientByMac_SQL, strMac);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}