package com.bodystm.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.BloodPressure;
import com.bodystm.bean.Patient;
import com.bodystm.dao.BloodPressureDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

@Service
@Transactional
public class BloodPressureService extends CommonService<BloodPressure, String> {
	@Resource
	private JdbcTemplate jdbcTemplate;

	@Resource
	private BloodPressureDao bloodPressureDao;

	public BloodPressure findLatestByEquipNoAndMeasureTime(String equipNo) {
		Query query = this.bloodPressureDao.createQuery("From BloodPressure Where equipNo=? order by measureTime desc", equipNo);
		return (BloodPressure) query.setMaxResults(1).uniqueResult();
	}
	
	/**
	 * 根据mac地址获取所有的血压数据（升序）
	 * 
	 * @param equipNo
	 * @return
	 */
	public List<BloodPressure> getAllByEquipNo(String equipNo) {
		try {
			// return temperatureDao.getAllByEquipNo(equipNo);
			Query query = this.bloodPressureDao.createQuery("From BloodPressure Where equipNo=? order by measureTime asc",
					equipNo);
			List<BloodPressure> bloodPressures = (List<BloodPressure>) query.list();
			return bloodPressures;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public void save(BloodPressure bloodPressure) {
		super.saveEntity(bloodPressure);
	}
	
	public Date getLatestDate(final Patient patient, final int distance) {
		// 鏌ヨ鏈�ぇ鐨勬椂闂�
		StringBuilder maxDateSql = new StringBuilder("SELECT max(measureTime) FROM BloodPressure where equipNo=?");
		List<Object> paramList = new ArrayList<Object>();
		paramList.add(patient.getEquipNo());
		if (patient.getEndTime() != null) {
			maxDateSql.append(" and measureTime<=?");
			paramList.add(patient.getEndTime());
		}
		Query query = this.bloodPressureDao.createQuery(maxDateSql.toString());
		int index = 0;
		for (Object obj : paramList) {
			query.setParameter(index++, obj);
		}
		Date date = (Date) query.uniqueResult();
		return date;
	}
	
	static final String EXPORT_SQL = "SELECT pulserate,measureTime FROM T_BloodPressure where equipNo=? and measureTime>=DATE_SUB(?, INTERVAL ? DAY) GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";//by rxz 20160219

	public List<Object> getLatestRecord(final Patient patient, final Date maxMeasureDate, final int distance) {
		List<Object> list = null;
		StringBuilder queryBuilder = new StringBuilder(EXPORT_SQL);
		List<Object> paramList = new ArrayList<Object>();
		paramList.add(patient.getEquipNo());
		paramList.add(maxMeasureDate);
		paramList.add(distance);
		if (patient.getEndTime() != null) {
			queryBuilder.append(" and measureTime<=?");
			paramList.add(patient.getEndTime());
		}
		SQLQuery resultQuery = this.bloodPressureDao.getSession().createSQLQuery(queryBuilder.toString());
		int index = 0;
		for (Object obj : paramList) {
			resultQuery.setParameter(index++, obj);
		}
		list = resultQuery.list();
		return list;
	}
	
	@Override
	protected HibernateDao<BloodPressure, String> getHibernateDao() {
		return this.bloodPressureDao;
	}
	public BloodPressure getLatestBloodPressure(final String equipNo){
		Query query = this.getHibernateDao().createQuery("FROM BloodPressure where equipNo=? order by measureTime desc", equipNo);
		BloodPressure bloodPressure = (BloodPressure) query.setMaxResults(1).uniqueResult();
		return bloodPressure;
	}
	
public List<BloodPressure> find24ByEquipNoAndMeasureTime(String equipNo) {
		
		try {
			return bloodPressureDao.find24ByEquipNoAndMeasureTime(equipNo);//,measureTime);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

public BloodPressure getLatestPoint(final Date lastDate, final String equipNo) {
	Query query = this.getHibernateDao().createQuery("FROM BloodPressure where equipNo=? and measureTime>=? order by measureTime desc", equipNo, lastDate);
	BloodPressure bloodPressure = (BloodPressure) query.setMaxResults(1).uniqueResult();
	return bloodPressure;
}

/**
 * 根据集中器设备号获取前一天的温度数据
 * @param strConcentratorNo
 * @return
 */
public List<BloodPressure> getListInOneDayByConcentratorNo(String strConcentratorNo){
	Calendar calendar = Calendar.getInstance();
	Date date=new Date();
	calendar.setTime(date);
	calendar.add(Calendar.DAY_OF_MONTH, -1);
	date = calendar.getTime();
	Query query = this.bloodPressureDao.createQuery("From BloodPressure Where concentratorNo=? and measureTime> ? order by measureTime desc",strConcentratorNo, date);
		List<BloodPressure> bloodPressures= (List<BloodPressure>)query.list();
		return bloodPressures;
}
}