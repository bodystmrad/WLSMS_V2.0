package com.bodystm.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bodystm.bean.Patient;
import com.bodystm.bean.PatientDepartment;
import com.bodystm.dao.PatientDao;
import com.bodystm.dao.PatientDepartmentDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 病人信息的业务类
 * 
 * @author ggeagle
 *
 */
//@Service
//@Transactional
public class PatientDepartmentService extends CommonService<PatientDepartment, String> {

	public Patient findByBedIdAndEndTimeIsNull(String bedId) {
		return this.getHibernateDao().findUnique(PatientDao.PATIENT_SQL, bedId);
	}

	@Resource
	private PatientDepartmentDao patientDepartmentDao;

	@Override
	protected HibernateDao<PatientDepartment, String> getHibernateDao() {
		return this.patientDepartmentDao;
	}
	public void deleteById(String personalId) {
		this.patientDepartmentDao.deleteById(personalId);
	}
}