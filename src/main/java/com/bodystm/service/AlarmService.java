package com.bodystm.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bodystm.bean.Alarm;
import com.bodystm.bean.AlarmRecord;
import com.bodystm.bean.Bed;
import com.bodystm.bean.Patient;
import com.bodystm.dao.AlarmDao;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 报警功能业务层
 * 
 * @author Administrator
 *
 */
@Service
@Transactional
public class AlarmService extends CommonService<Alarm, String> {

	@Resource
	private AlarmDao alarmDao;

	@Override
	protected HibernateDao<Alarm, String> getHibernateDao() {
		return this.alarmDao;
	}
	
	@Override
	public void saveEntity(Alarm Alarm) {
		super.saveEntity(Alarm);
	}
	
	/**
	 * 查询报警设置初始化参数
	 * 
	 * @param Patient_Id
	 * @return
	 */
	public List<Alarm> getAlarmByPatientId(String Patient_Type) {
		List<Alarm> alarmByPatientId = alarmDao.getAlarmByPatientId(Patient_Type);
		return alarmByPatientId;
	}

	/**
	 * 查询报警记录
	 * 
	 * @param Patient_Id
	 * @return
	 */
	public List<AlarmRecord> getAlarmRecordByPatientId(String Patient_Id) {
		List<AlarmRecord> AlarmRecord = alarmDao.getAlarmRecordByPatientId(Patient_Id);
		return AlarmRecord;
	}
	
}
