package com.bodystm.service;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bodystm.bean.EquipInfo;
import com.bodystm.dao.EquipDao;
import com.mimosa.common.dao.HibernateDao;

/**
 * 设备信息
 * @author fy
 *
 */
@Service
@Transactional
public class EquipService extends HibernateDao<EquipInfo, String> {

	@Resource
	private EquipDao equipdao;
	
	/**
	 * 查询设备信息
	 * @param patientId
	 * @return
	 */
	public List<EquipInfo> getEquipInfoByPatientId(String patientId){
		return this.equipdao.getEquipInfoByPatientId(patientId);
	}	
}
