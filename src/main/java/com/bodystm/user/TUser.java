package com.bodystm.user;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.springframework.format.annotation.DateTimeFormat;

import com.bodystm.bean.Department;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.types.SexType;
import com.mimosa.util.config.YesNoType;


/**
 * @author rxz
 */
//@Entity
//@Table(name = "T_USER")
//@Inheritance(strategy = InheritanceType.JOINED)
public class TUser extends BaseEntity {
	public static final String PROPERTY_USER_ID = "userId";
	public static final String PROPERTY_USER_NAME = "userName";
	public static final String PROPERTY_USER_PASS = "userPass";
	public static final String PROPERTY_STATE = "state";

	@Column(length = 64)
	private String userName; // 鐢ㄦ埛濮撳悕

	@Column(length = 64)
	private String userPass; // 瀵嗙爜
	
	@Column(length = 64)
	private String realName;
	// 鏃у瘑鐮�鐢ㄤ簬瀵嗙爜淇敼
	@Transient
	private String oldPass;

	@OneToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private Department department;

	/**
	 * 鐢ㄦ埛绫诲瀷瀛楁
	 */
	@Column(length = 16, nullable = true)
	private String userType;




	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


	@Column(length = 8, nullable = true)
	private String state = YesNoType.Y.name();


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}


	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	public String getOldPass() {
		return oldPass;
	}

	public void setOldPass(String oldPass) {
		this.oldPass = oldPass;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
}