package com.bodystm.user;

/**
 * 鐢ㄦ埛绫诲瀷
 * 
 * @author ggeagle
 *
 */
public enum UserType {
	/**
	 * 科室用户
	 */
	NORMAL,
	/**
	 * 管理员
	 */
	ADMIN;
}