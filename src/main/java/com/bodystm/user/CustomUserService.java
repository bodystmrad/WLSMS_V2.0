package com.bodystm.user;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.hash.Hashing;
import com.mimosa.common.system.role.TRole;
import com.mimosa.common.system.role.TRoleDao;
import com.mimosa.common.system.role.TUserRoleDao;
import com.mimosa.common.system.user.TUser;
import com.mimosa.common.system.user.UserService;
import com.mimosa.common.util.UUIDUtil;
import com.mimosa.util.constant.StringConstUtil;

@Service
@Transactional
public class CustomUserService extends UserService {
	/**
	 * 日志记录
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void updateEntity(TUser entity) {
        //setDefaultProperty(entity);
		//TUser user=getEntity(entity.getId());
        if (StringUtils.isNotBlank(entity.getUserPass())){//&&!entity.getUserPass().equals(user.getUserPass())) {
			String password = entity.getUserPass();
			password = Hashing.md5()
					.hashString(password, StandardCharsets.UTF_8).toString();
			entity.setUserPass(password);
		}
		this.getHibernateDao().save(entity);
	}

}