package com.bodystm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.PatientEquip;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class PatientEquipDao extends HibernateDao<PatientEquip, String> {
	public static final String PATIENT_SQL = "From Patient Where bed.id=? and endTime is null";
	public void deleteById(String id) 
	{
		logger.info(id);
		Session session = super.getSession();
		String sql="delete from t_patient_equip where id='"+id+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}

	public List<PatientEquip> getPatientEquipByPatient(String patientId){
		List<PatientEquip> lstpatientequip=this.find("from PatientEquip t where t.patientID=? order by createTime ASC",patientId);
		if(lstpatientequip.size()>0) return lstpatientequip;
		return null;
	}
	public List<PatientEquip> getPatientEquipByMac(String strMac){
		List<PatientEquip> lstpatientequip=this.find("from PatientEquip t where t.equipNo=? order by createTime ASC",strMac);
		if(lstpatientequip.size()>0) return lstpatientequip;
		return null;
	}
	public void deleteByPatientId(String patientId){
		logger.info(patientId);
		Session session = super.getSession();
		String sql="delete from t_patient_equip where patientID='"+patientId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	public void deleteAllByMac(String mac){
		Session session = super.getSession();
		String sql="delete from t_patient_equip where equipNo='"+mac+"'";
		int n=session.createSQLQuery(sql).executeUpdate();
		if(n>0){
			logger.info("删除该设备对应的关系数据（旧的）:"+mac);
		}
		logger.info(sql);
	}

}