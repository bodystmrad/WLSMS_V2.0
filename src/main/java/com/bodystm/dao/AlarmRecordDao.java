package com.bodystm.dao;

import org.springframework.stereotype.Repository;

import com.bodystm.bean.AlarmRecord;
import com.bodystm.bean.Concentrator;
import com.mimosa.common.dao.HibernateDao;
@Repository
public class AlarmRecordDao extends HibernateDao<AlarmRecord,String>{

}
