package com.bodystm.dao;

import org.springframework.stereotype.Repository;

import com.bodystm.bean.Concentrator;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class ConcentratorDao extends HibernateDao<Concentrator,String>{

}
