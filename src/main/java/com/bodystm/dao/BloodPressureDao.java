package com.bodystm.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.bodystm.bean.BloodPressure;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class BloodPressureDao extends HibernateDao<BloodPressure, String> {
//	public List<Temperature> find24ByEquipNoAndMeasureTime(String equipNo, Date measureTime){
//		try {
//			Session session = super.getSession();
//			String sql="select * from t_temperature where equipNo='"+equipNo+"' and measureTime>= '"+measureTime+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
//			SQLQuery sqlquery=session.createSQLQuery(sql);
//			sqlquery.setMaxResults(48);
//			List<Temperature> list=session.createSQLQuery(sql).addEntity(Temperature.class).list();
//			return list;
//		} catch (HibernateException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//	}
	public List<BloodPressure> find24ByEquipNoAndMeasureTime(String equipNo){//, Date measureTime){
		try {
			Session session = super.getSession();
			String sql="select * from t_bloodpressure where equipNo='"+equipNo+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			SQLQuery sqlquery=session.createSQLQuery(sql);
			//sqlquery.setMaxResults(48);
			List<BloodPressure> list=session.createSQLQuery(sql).addEntity(BloodPressure.class).list();
			return list;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}