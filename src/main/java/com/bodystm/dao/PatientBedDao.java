package com.bodystm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.Patient;
import com.bodystm.bean.PatientBedUser;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class PatientBedDao extends HibernateDao<PatientBedUser, String> {
	public static final String PATIENT_SQL = "From Patient Where bed.id=? and endTime is null";
	public void deleteById(String id) 
	{
		logger.info(id);
		Session session = super.getSession();
		String sql="delete from t_patient_bed where id='"+id+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	/*
	 * ��ȡĳ���������еĲ���
	 */
	public List<Patient> getPatientsOfDepartment(String userId){
		logger.info(userId);
		Session session = super.getSession();
		String sql="select * from t_patient__bed_user t where t.tUser.id='"+userId+"'";
		List<PatientBedUser> lstPatientBedUser=this.find("from PatientBedUser t where t.tUser.id =? order by t.lastUpdateTime DESC", userId);
		//session.createSQLQuery(sql);
		//logger.info(sql);
		List<Patient> lstPatient=new ArrayList<Patient>();
		if(lstPatientBedUser.size()==0) return lstPatient;
		for(PatientBedUser item:lstPatientBedUser){
			lstPatient.add(item.getPatient());
		}
		return lstPatient;
	}
	public PatientBedUser getPatientBedUserByPatientAnddBed(String patientId,String bedId){
		List<PatientBedUser> lstpatientBedUser=this.find("from PatientBedUser t where t.patient.id=? and t.bed.id=? and t.status=1 and t.statusOfDepartment=1 order by createTime Desc",patientId,bedId);
		if(lstpatientBedUser.size()>0) return lstpatientBedUser.get(0);
		return null;
	}
	
	public PatientBedUser getPatientBedUserByBedName(String bedName){
		List<PatientBedUser> lstpatientBedUser=this.find("from PatientBedUser t where t.bed.name=? and t.status=1 and t.statusOfDepartment=1 order by createTime Desc",bedName);
		if(lstpatientBedUser.size()>0) return lstpatientBedUser.get(0);
		return null;
	}
	
	public PatientBedUser getPatientBedUserByMac(String strMac){
		List<PatientBedUser> lstpatientBedUser=this.find("from PatientBedUser t where t.patient.equipNo=? order by createTime Desc",strMac);
		if(lstpatientBedUser.size()>0) return lstpatientBedUser.get(0);
		return null;
	}
	
	public void deleteByPatientId(String patientId){
		logger.info(patientId);
		Session session = super.getSession();
		String sql="delete from t_patient_bed_user where tPatient_ID='"+patientId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	public void deleteByBedIdAndPatientId(String bedId,String patientId){
		logger.info(bedId);
		Session session = super.getSession();
		String sql="delete from t_patient_bed_user where bed_id='"+bedId+"' or tPatient_ID='"+patientId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	/**
	 * ��ȡ��ǰ��λ-���˹�ϵ��Ϣ����ǰ�ǿմ�����Ϣ��
	 * @return
	 */
	public List<PatientBedUser> getCurrentPatients(){
		List<PatientBedUser> lstpatientBedUser=this.find("from PatientBedUser t where t.status=1");
		return lstpatientBedUser;
	}
}