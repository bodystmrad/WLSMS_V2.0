package com.bodystm.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.bodystm.bean.Oximetry;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class OximetryDao extends HibernateDao<Oximetry, String> {
	public List<Oximetry> find24ByEquipNoAndMeasureTime(String equipNo){//, Date measureTime){
		try {
			Session session = super.getSession();
			String sql="select * from t_oximetry where equipNo='"+equipNo+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			SQLQuery sqlquery=session.createSQLQuery(sql);
			//sqlquery.setMaxResults(48);
			List<Oximetry> list=session.createSQLQuery(sql).addEntity(Oximetry.class).list();
			return list;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}