package com.bodystm.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.PatientDepartment;
import com.mimosa.common.dao.HibernateDao;

//@Repository
public class PatientDepartmentDao extends HibernateDao<PatientDepartment, String> {
	public static final String PATIENT_SQL = "From Patient Where bed.id=? and endTime is null";
	public void deleteById(String id) 
	{
		logger.info(id);
		Session session = super.getSession();
		String sql="delete from t_patient_department where id='"+id+"'";
		session.createSQLQuery(sql);
		logger.info(sql);
	}
}