package com.bodystm.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.Alarm;
import com.bodystm.bean.AlarmRecord;
import com.bodystm.bean.Bed;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class AlarmDao extends HibernateDao<Alarm, String>{
	
	/**
	 * 通过病人类型获取初始化参数
	 * @param Patient_Id
	 * @return
	 */
	public List<Alarm> getAlarmByPatientId(String Patient_Type){
		Session session = super.getSession();
		String sql="select * from t_alarm where Patient_Type='"+Patient_Type+"'or Patient_Type=3";
		List<Alarm> list=session.createSQLQuery(sql).addEntity(Alarm.class).list();
		return list;
	}
	
	/**
	 * 通过病人ID查询报警详情
	 * @param Patient_Id
	 * @return
	 */
	public List<AlarmRecord> getAlarmRecordByPatientId(String Patient_Id){
		if(Patient_Id.length()==0) return null;
		Session session = super.getSession();
		String sql="select * from t_alarmrecord where patient_Id='"+Patient_Id+"'";
		List<AlarmRecord> list=session.createSQLQuery(sql).addEntity(AlarmRecord.class).list();
		return list;
	}
}
