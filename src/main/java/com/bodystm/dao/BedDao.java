package com.bodystm.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.Bed;
import com.bodystm.bean.HistoricalData;
import com.mimosa.common.dao.HibernateDao;


@Repository
public class BedDao extends HibernateDao<Bed, String> {
	
	public Bed getBedByName(String bedName,String userId){
		logger.info(bedName);
		if(bedName.length()==0) return null;
		Session session = super.getSession();
		String sql="select * from t_bed where name='"+bedName+"' and user_id='"+userId+"' and usable=1";
		List<Bed> list=session.createSQLQuery(sql).addEntity(Bed.class).list();
		if(list==null||list.size()<=0){
			return null;
		}else{
			return list.get(0);
		}
		
	}
	public void deleteByIdofBedbysql(String bedId) {
		logger.info(bedId);
		Session session = super.getSession();
		String sql="delete from t_bed where id='"+bedId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	
	public List<Bed> getNotEmptyBeds4CheckConcentrator(String userId){
		Session session = super.getSession();
		String sql="select * from t_bed where status='notempty' and (concentratorNo is not null or concentratorNo<>'') and user_id='"+userId+"'";
		List<Bed> list=session.createSQLQuery(sql).addEntity(Bed.class).list();
		if(list==null||list.size()<=0){
			return null;
		}else{
			return list;
		}
	}
	
	//通过mac地址查询床位获取病人id
	public Bed getPatientIdBybed(String mac){
		Session session = super.getSession();
		String sql="SELECT * FROM t_bed WHERE concentratorNo='"+mac+"'";
		List<Bed> Bedlist=session.createSQLQuery(sql).addEntity(Bed.class).list();
		if(Bedlist==null||Bedlist.size()<=0){
			return null;
		}else{
			return Bedlist.get(0);
		}
	}
	
	/**
	 *  限制床位条数-测试
	 * @return
	 */
	public List<Bed> query(){
		Session session = super.getSession();
		String sql="from Bed t where t.usable=1 order by id ASC";
		Query query = session.createQuery(sql);
		query.setMaxResults(2);
		List<Bed> list=(List<Bed>)query.list();		
		if(list==null||list.size()<=0){
			return null;
		}else{
			return list;
		}
	}
	
}