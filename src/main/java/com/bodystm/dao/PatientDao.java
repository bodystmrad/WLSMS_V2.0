package com.bodystm.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.Patient;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class PatientDao extends HibernateDao<Patient, String> {
//	public static final String PATIENT_SQL = "From Patient Where bed.id=? and endTime is null";
	public static final String PATIENT_SQL = "From Patient Where bedId=? and endTime is null";
	public static final String FindPatientByMac_SQL = "From Patient Where equipNo=? ";
	public void deleteById(String personalId) 
	{
		logger.info(personalId);
		Session session = super.getSession();
		String sql="delete from t_patient where personnelId='"+personalId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	public void deleteByBedId(String bedId) {
		logger.info(bedId);
		Session session = super.getSession();
		String sql="delete from t_patient where bed_id='"+bedId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}
	
}