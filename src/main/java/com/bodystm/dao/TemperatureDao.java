package com.bodystm.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.bodystm.bean.Temperature;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class TemperatureDao extends HibernateDao<Temperature, String> {
	public List<Temperature> find24ByEquipNoAndMeasureTime(String equipNo){//, Date measureTime){
		try {
			Session session = super.getSession();
			//String sql="select * from t_temperature where equipNo='"+equipNo+"' and measureTime>= '"+measureTime+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			String sql="select * from t_temperature where equipNo='"+equipNo+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			SQLQuery sqlquery=session.createSQLQuery(sql);
			//sqlquery.setMaxResults(48);
			List<Temperature> list=session.createSQLQuery(sql).addEntity(Temperature.class).list();
			return list;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 获取某个病人用过所有的贴片的温度数据，每个小时获取一个数
	 * @param strMacs
	 * @return
	 */
	public List<Temperature> find24ByMacsAndMeasureTime(String strMacs) {
		try {
			Session session = super.getSession();
			//String sql="select * from t_temperature where equipNo='"+equipNo+"' and measureTime>= '"+measureTime+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			String sql="select * from t_temperature where equipNo in ("+strMacs+") GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			SQLQuery sqlquery=session.createSQLQuery(sql);
			//sqlquery.setMaxResults(48);
			List<Temperature> list=session.createSQLQuery(sql).addEntity(Temperature.class).list();
			return list;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 获取某个病人用过所有的贴片的温度数据，每个小时获取一个数
	 * @param strMacs
	 * @return
	 */
	public List<Temperature> findAllByMacs(String strMacs) {
		try {
			Session session = super.getSession();
			//String sql="select * from t_temperature where equipNo='"+equipNo+"' and measureTime>= '"+measureTime+"' GROUP BY DATE_FORMAT(measureTime,'%y-%m-%d %H')";
			String sql="select * from t_temperature where equipNo in ("+strMacs+") order by measureTime Asc";
			SQLQuery sqlquery=session.createSQLQuery(sql);
			//sqlquery.setMaxResults(48);
			List<Temperature> list=session.createSQLQuery(sql).addEntity(Temperature.class).list();
			return list;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}