package com.bodystm.dao;

import java.util.List;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import com.bodystm.bean.EquipInfo;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class EquipDao extends HibernateDao<EquipInfo, String> {

	//设备信息查询
	public List<EquipInfo> getEquipInfoByPatientId(String Patient_Id) {
		Session session = super.getSession();
		String sql = "SELECT * FROM T_Equip WHERE Patient_id='" + Patient_Id + "'";
		List<EquipInfo> list = session.createSQLQuery(sql).addEntity(EquipInfo.class).list();
		if (list == null || list.size() <= 0) {
			return null;
		} else {
			return list;
		}
	}
}
