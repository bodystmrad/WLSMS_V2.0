package com.bodystm.dao;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bodystm.bean.HistoricalData;
import com.mimosa.common.dao.HibernateDao;

@Repository
public class HistoricalDataDao extends HibernateDao<HistoricalData, String> {

	//数据回顾
	public List<HistoricalData> getHistoricalDataByPatientId(String Patient_Id,String Starttime, String Endtime,String Resolution){
		Session session = super.getSession();
		String sql="SELECT * FROM t_historicaldata WHERE createTime BETWEEN'"+Starttime+"'AND'"+Endtime+"'AND DATE_FORMAT(createTime, '%i') % '"+Resolution+"'= 0 AND DATE_FORMAT(createTime, '%s') = 0 AND Patient_id='"+Patient_Id+"'";
		List<HistoricalData> list=session.createSQLQuery(sql).addEntity(HistoricalData.class).list();
		if(list==null||list.size()<=0){
			return null;
		}else{
			return list;
		}
	}
}
