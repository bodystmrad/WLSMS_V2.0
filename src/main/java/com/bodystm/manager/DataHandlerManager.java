package com.bodystm.manager;

import java.util.Hashtable;

import com.bodystm.bean.datadeal.BodystmProduct;
import com.bodystm.bean.datadeal.EcgAnalysis;
import com.bodystm.bean.datadeal.EcgAnalysis.EcgChannel;
import com.bodystm.bean.datadeal.OximetryAnalysis;

/**
 * 数据处理线程管理类
 * @author ehl
 *
 */
public class DataHandlerManager {
	public static Hashtable<String,OximetryAnalysis> hasOxiAynalysis=new Hashtable<String,OximetryAnalysis>();
	public static Hashtable<String,EcgAnalysis> hasEcgAynalysis=new Hashtable<String,EcgAnalysis>();
	/*public synchronized static void updateEcgChannelType(String mac,EcgChannel ecgChannel){
		if (hasEcgAynalysis.containsKey(mac)) {
			EcgAnalysis ecgAnalysis=hasEcgAynalysis.get(mac);
			ecgAnalysis.channelType=ecgChannel;
		}
	}*/
	public synchronized static void updateHasEcgAynalysis(String mac,EcgAnalysis ecgAnalysis){
		hasEcgAynalysis.put(mac, ecgAnalysis);
	}
	
	//==============华丽分割线======================
	
	//将所有类型的放到一起，key为mac+type
	public static volatile Hashtable<String,Object> hasDataAynalysis=new Hashtable<String,Object>();
	public synchronized static void updateHasDataAynalysis(String mac_type,Object dataAnalysis){
		hasDataAynalysis.put(mac_type, dataAnalysis);
	}
	public static EcgAnalysis getHasDataAynalysis(String mac_type){
		return (EcgAnalysis)DataHandlerManager.hasDataAynalysis.get(mac_type);
	}
	public synchronized static void updateEcgChannelType(String mac,EcgChannel ecgChannel){
		mac=mac+BodystmProduct.BodystmECG.ordinal();
		if (hasDataAynalysis.containsKey(mac)) {
			EcgAnalysis ecgAnalysis=(EcgAnalysis)hasDataAynalysis.get(mac);
			ecgAnalysis.channelType=ecgChannel;
		}/*else{
			EcgAnalysis ecgAnalysis=new EcgAnalysis();
			ecgAnalysis.channelType=ecgChannel;
			hasDataAynalysis.put(mac, ecgAnalysis);
		}*/
	}
}
