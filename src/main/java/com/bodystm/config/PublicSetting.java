package com.bodystm.config;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class PublicSetting {
	public static final String BED_STATUS_EMPTY="empty";
	/**
	 * 界面上显示脉搏值或者心率值
	 * 0表示显示脉搏（RPM）；1表示显示心率（BPM）
	 */
	public static int ShowPulseOrHeartRate=0;
	public static String websocketUrl="ws://localhost:4000";
	public static boolean ifMeasureScreen=false;
	public static String dllPath;
	static{
		org.springframework.core.io.Resource resource = new ClassPathResource("/applicationContext.properties");//
        Properties props = null;
        String path="";
		try {
			props = PropertiesLoaderUtils.loadProperties(resource);
			path=props.getProperty("dllPath");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(null==path||"".equals(path.trim())){
			dllPath="Algorithm";
		}else{
			dllPath=path;
		}
		
	}
}
