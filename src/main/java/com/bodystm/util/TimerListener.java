package com.bodystm.util;

import java.util.Timer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.bodystm.dao.BedDao;
import com.bodystm.dao.HistoricalDataDao;

public class TimerListener implements ServletContextListener  {
	
	Timer timer=new Timer();
	//每五秒执行一次
	@Override
	public void contextInitialized(final ServletContextEvent event) {
		HistoricalDataDao bean = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext()).getBean(HistoricalDataDao.class);
		BedDao BedDao = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext()).getBean(BedDao.class);
		event.getServletContext().log("定时器已启动");
		timer.scheduleAtFixedRate(new SaveHistorData(bean,BedDao), 0, 5000);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		 if (timer != null) {  
            timer.cancel();  
            event.getServletContext().log("定时器销毁");  
	    }  
	}
}
