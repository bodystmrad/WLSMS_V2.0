package com.bodystm.util;

import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bodystm.bean.Bed;
import com.bodystm.bean.HistoricalData;
import com.bodystm.dao.BedDao;
import com.bodystm.dao.HistoricalDataDao;
import com.bodystm.server.RedisManager;

import redis.clients.jedis.Jedis;

public class SaveHistorData extends TimerTask {

	/**
	 * 日志记录器
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private HistoricalDataDao bean;
	
	private BedDao Bedben;
	
	public SaveHistorData(HistoricalDataDao bean,BedDao BedDao) {
		this.bean=bean;
		this.Bedben=BedDao;
	}

	@Override
	public void run() {
		
		try {
			Jedis jedis = RedisManager.getJedis();
			System.out.println("++++++++++++++++++++++++++++++++++++");
			HistoricalData historData = new HistoricalData();
			//通过mac地址查询床位病人id todo
			historData.setCreateTime(new Date());
			//血氧数据
			if(RedisManager.get("Spo2")!=null && RedisManager.get("Spo2")!="") {
				historData.setSpO2((Integer.valueOf(RedisManager.get("Spo2")).intValue()));
				jedis.del("Spo2");
			}
			if(RedisManager.get("pi")!=null && RedisManager.get("pi")!="") {
				historData.setPI((Float.valueOf(RedisManager.get("pi")).floatValue()));
				jedis.del("pi");
			}
			if(RedisManager.get("bpm")!=null && RedisManager.get("bpm")!="") {
				historData.setPR((Integer.valueOf(RedisManager.get("bpm")).intValue()));
				jedis.del("bpm");
			}
		
			//心电数据
			if(RedisManager.get("hr")!=null && RedisManager.get("hr")!="") {
				historData.setHr((Integer.valueOf(RedisManager.get("hr")).intValue()));
				jedis.del("hr");
				
			}
			//呼吸数据
			if(RedisManager.get("resp")!=null && RedisManager.get("resp")!="") {
				historData.setRr((Integer.valueOf(RedisManager.get("resp")).intValue()));
				jedis.del("resp");
			}
			//血压
			if(RedisManager.get("Sbp")!=null && RedisManager.get("Sbp")!="") {
				historData.setSbp((Integer.valueOf(RedisManager.get("Sbp")).intValue()));
				jedis.del("Sbp");
			}
			if(RedisManager.get("Dbp")!=null && RedisManager.get("Dbp")!="") {
				historData.setDbp((Integer.valueOf(RedisManager.get("Dbp")).intValue()));
				jedis.del("Dbp");
			}
			if(RedisManager.get("BP4PR")!=null && RedisManager.get("BP4PR")!="") {
				historData.setBP4PR((Integer.valueOf(RedisManager.get("BP4PR")).intValue()));
				jedis.del("BP4PR");
			}				
			//温度
			if(RedisManager.get("Temperature1")!=null && RedisManager.get("Temperature1")!="") {
				historData.setTemperature1((Float.valueOf(RedisManager.get("Temperature1")).floatValue()));
				jedis.del("Temperature1");
			}
			if(RedisManager.get("Temperature2")!=null && RedisManager.get("Temperature2")!="") {
				historData.setTemperature1((Float.valueOf(RedisManager.get("Temperature2")).floatValue()));
				jedis.del("Temperature2");
			}
			
			//保存数据
			String mac = RedisManager.get("mac");
			Bed Bed = Bedben.getPatientIdBybed(mac);
			System.out.println("--------------------------");
			if(Bed!=null) {
				jedis.del("mac");
				historData.setPatient_id(Bed.getPatient().getId());
				historData.setBed_id(Bed.getId());
				historData.setUser_id(Bed.getUser().getId());
				bean.save(historData);
			}
		}catch (Exception e) {
			logger.error(e.getMessage(),e.fillInStackTrace());
		}
	}
}
