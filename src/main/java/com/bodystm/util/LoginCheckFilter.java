package com.bodystm.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.mimosa.common.system.user.TUser;
import com.mimosa.common.system.user.UserContext;


/**
 * 绯荤粺鐨勮繃婊ゅ櫒绫�
 * 
 * ClassName: LoginFilter <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015骞�鏈�鏃�涓嬪崍10:57:01 <br/>
 *
 * @author Administrator
 * @version
 * @since JDK 1.7
 */
public class LoginCheckFilter implements Filter {

	@Override
	public void destroy(){
		
	}
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response=(HttpServletResponse)arg1;
//		response.setContentType("textml;charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin")); //"*"
		//允许跨域
	      response.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type,"
	            + " X-E4M-With,appId,token,userId");
	      response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	      response.setHeader("Access-Control-Max-Age", "0");
	      response.setHeader("Access-Control-Allow-Credentials", "true");
	      response.setHeader("XDomainRequestAllowed","1");
	      //设置cookie
	      /*Cookie[] cookies=request.getCookies();
	      StringBuilder sb=new StringBuilder();
	      if (null!=cookies&&cookies.length>0) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie=cookies[i];
				if (i<cookies.length-1) {
					sb.append(cookie.getName()+"="+cookie.getValue()+";");
				}else{
					sb.append(cookie.getName()+"="+cookie.getValue());
				}
			}
			response.setHeader("Set-Cookie", sb.toString());
		}*/
	      //2018年4月21日21:28:19 为了解决跨域访问session不能保持一致，客户端不能发送cookie到服务端的问题。临时注释掉登录过滤器
		/*String path = request.getServletPath();
		if (!StringUtils.startsWith(path, "/home/")
				) {
			TUser user = (TUser) request.getSession().getAttribute(
					UserContext.USER_ATTR);
			if (user == null) {
//				HttpServletResponse response = (HttpServletResponse) arg1;
				response.sendRedirect(request.getContextPath()
						+ "/home/index.action");
				return;
			} else {
				
			}
		}*/
	      String path = request.getServletPath();
			if (!StringUtils.startsWith(path, "/home/")&&!StringUtils.startsWith(path, "/bed/getCurBedsData.action")
					) {
				TUser user = (TUser) request.getSession().getAttribute(
						UserContext.USER_ATTR);
				if (user == null) {
					response.sendRedirect(request.getContextPath()
							+ "/home/index.action");
					return;
				} else {
					
				}
			}
		chain.doFilter(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		WebApplicationContext context = WebApplicationContextUtils
				.getRequiredWebApplicationContext(arg0.getServletContext());

	}
}