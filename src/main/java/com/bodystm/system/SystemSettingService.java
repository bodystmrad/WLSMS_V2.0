package com.bodystm.system;

import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mimosa.common.dao.HibernateDao;
import com.mimosa.common.service.CommonService;

/**
 * 病人信息的业务类
 * 
 * @author ggeagle
 *
 */
@Service
@Transactional
public class SystemSettingService extends CommonService<SystemSetting, String> {
	@Resource
	private SystemSettingDao systemSettingDao;

	@Override
	public void saveEntity(SystemSetting systemSetting) {
		super.saveEntity(systemSetting);
	}
	
	public SystemSetting getSystemSetting(){
		SystemSetting systemSetting=systemSettingDao.getSystemSetting();
		return systemSetting;
		/*if(systemSetting==null){
			systemSetting=new SystemSetting();
		}*/
	}



	/*@Override
	public SystemSetting getEntity(String id) {
		Bed bed = super.getEntity(id);
		List<Patient> patientList = this.patientDao.find(
				PatientDao.PATIENT_SQL, id);
		if (patientList != null && patientList.size() > 0) {
			bed.setPatient(patientList.get(0));
		}
		return bed;
	}*/

	@Override
	protected HibernateDao<SystemSetting, String> getHibernateDao() {
		return this.systemSettingDao;
	}
}