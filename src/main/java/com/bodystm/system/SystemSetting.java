package com.bodystm.system;

import java.text.NumberFormat;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.system.user.TUser;

/**
 * 鐥呬汉淇℃伅
 * 
 * @author ggeagle
 *
 */
@Table(name = "T_SystemSetting")
@Entity
public class SystemSetting extends BaseEntity {
	/**
	 * 医院logo图片路径（相对路径）
	 */
	private String hospitalLogoPath;
	private String hospitalLogoAbsolutePath;
	private String hospitalName;
	/**
	 * 采集温度数据时是否取平均值
	 * 默认为true，即取平均值
	 */
	private int ifGetAvgInDataGather=1;
	/**
	 * 采集温度数据时是否取平均值
	 * 静态版本，初始化时从数据库取出并给此字段赋值，如果取不到值默认为true
	 */
	@Transient
	public volatile static Boolean ifGetAvgInDataGather4Temp;
	
	/**
	 * 过期时间设置
	 */
	@Transient
	public volatile static int NUM4EXPIREHOURS=48;

	public String getHospitalLogoPath() {
		return hospitalLogoPath;
	}

	public void setHospitalLogoPath(String hospitalLogoPath) {
		this.hospitalLogoPath = hospitalLogoPath;
	}

	public String getHospitalLogoAbsolutePath() {
		return hospitalLogoAbsolutePath;
	}

	public void setHospitalLogoAbsolutePath(String hospitalLogoAbsolutePath) {
		this.hospitalLogoAbsolutePath = hospitalLogoAbsolutePath;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public int isIfGetAvgInDataGather() {
		return ifGetAvgInDataGather;
	}

	public void setIfGetAvgInDataGather(int ifGetAvgInDataGather) {
		this.ifGetAvgInDataGather = ifGetAvgInDataGather;
	}
	
}