package com.bodystm.system;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.mimosa.common.dao.HibernateDao;


@Repository
public class SystemSettingDao extends HibernateDao<SystemSetting, String> {
	
	public SystemSetting getSystemSetting(){
		Session session = super.getSession();
		String sql="select * from t_systemsetting";
		List<SystemSetting> list=session.createSQLQuery(sql).addEntity(SystemSetting.class).list();
		if(list==null||list.size()<=0){
			return null;
		}else{
			return list.get(0);
		}
		
	}
/*	public void deleteByIdofBedbysql(String bedId) {
		logger.info(bedId);
		Session session = super.getSession();
		String sql="delete from t_bed where id='"+bedId+"'";
		session.createSQLQuery(sql).executeUpdate();
		logger.info(sql);
	}*/
}