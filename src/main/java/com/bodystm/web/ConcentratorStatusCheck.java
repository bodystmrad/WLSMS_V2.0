package com.bodystm.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.bodystm.bean.Concentrator;
import com.bodystm.service.BedService;
import com.bodystm.service.ConcentratorService;
import com.bodystm.service.TemperatureService;


public class ConcentratorStatusCheck implements Runnable{
	
	//@Autowired
	//private ConcentratorService concentratorService;
	@Autowired
	private TemperatureService temperatureService;
	
	@Resource
	private BedService bedService;
	
	@Resource
	private ConcentratorService concentratorService;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		/*ConcentratorDevice concentratorDevice=null;
		List<Temperature> lstTemperature=null;
		String strMac="";
		//Calendar calendar= Calendar.getInstance();
		for(int i=0;i<DeviceSettings.lstMacs4Concentrator.size();i++){
			concentratorDevice=(ConcentratorDevice) DeviceSettings.hasDevice4Concentrator.get(DeviceSettings.lstMacs4Concentrator.get(i));
			if(concentratorDevice==null)continue;
			strMac=concentratorDevice.getConcentratorNo();
			lstTemperature=temperatureService.getListInOneYearByConcentratorNo(strMac);
			if(lstTemperature==null||lstTemperature.size()==0){
				concentratorDevice.setSignaled(false);
			}
			else{//如果一直在上数则清空静态变量
				//DeviceSettings.hasDevice4Concentrator.remove(DeviceSettings.lstMacs4Concentrator.get(i));
				//DeviceSettings.lstMacs4Concentrator.remove(i--);
			}
		}*/
		ConcentratorDevice concentratorDevice=null;
		List<Concentrator> lstConcentrators=concentratorService.getAll();
		List<String> lstCons=new ArrayList<String>();
		Date dateTemp=null;
		Date dateCur=new Date();
		for(Concentrator con : lstConcentrators){
			if(!lstCons.contains(con.getConcentratorNo()))
			lstCons.add(con.getConcentratorNo());
			if(!DeviceSettings.lstMacs4Concentrator.contains(con.getConcentratorNo())){
				DeviceSettings.lstMacs4Concentrator.add(con.getConcentratorNo());
				concentratorDevice=new ConcentratorDevice();
				concentratorDevice.setConcentratorNo(con.getConcentratorNo());
				concentratorDevice.setLastMsgTime(new Date());
				concentratorDevice.setBedName(con.getBedName());
				DeviceSettings.hasDevice4Concentrator.put(con.getConcentratorNo(), concentratorDevice);
			}else{
				concentratorDevice=(ConcentratorDevice) DeviceSettings.hasDevice4Concentrator.get(con.getConcentratorNo());
			}
			//检查各个集中器的状态
			
			dateTemp=concentratorDevice.getLastMsgTime();
			if(dateTemp!=null&&((dateCur.getTime()-dateTemp.getTime()) / (1000 * 60)>=720)){//超过12小时才算异常
				concentratorDevice.setSignaled(false);
				con.setStatus(0);
				concentratorService.saveEntity(con);
			}else if(con.getStatus()==0){
				con.setStatus(1);
				concentratorService.saveEntity(con);
			}
		}
		
//		Date dateTemp=null;
//		Date dateCur=new Date();
		for(int i=0;i<DeviceSettings.lstMacs4Concentrator.size();i++){
			if(!lstCons.contains(DeviceSettings.lstMacs4Concentrator.get(i))){
				DeviceSettings.lstMacs4Concentrator.remove(DeviceSettings.lstMacs4Concentrator.get(i));
				DeviceSettings.lstMacs4Concentrator.remove(i--);
				continue;
			}
			/*concentratorDevice=(ConcentratorDevice) DeviceSettings.hasDevice4Concentrator.get(DeviceSettings.lstMacs4Concentrator.get(i));
			dateTemp=concentratorDevice.getLastMsgTime();
			if(dateTemp!=null&&((dateCur.getTime()-dateTemp.getTime()) / (1000 * 60)>=60)){
				concentratorDevice.setSignaled(false);
			}*/
		}
	}

}
