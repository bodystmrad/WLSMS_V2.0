package com.bodystm.web;

import org.apache.commons.lang.StringUtils;

public class EasyUiMessage {
	private boolean success = true;
	private String errorMsg = StringUtils.EMPTY;
	private String data;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return String.format(
				"{\"success\":%s,\"errorMsg\":\"%s\",\"data\":%s}",
				this.isSuccess(), this.getErrorMsg(), this.getData());
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
