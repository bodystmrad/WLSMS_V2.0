package com.bodystm.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public  class ConcentratorDevice {
	private String bedName;//集中器对应的床号
	private String concentratorNo;
	/**
	 * 是否有信号（有数据上传）
	 */
	private boolean signaled=true;

	/*
	 * 是否弹出警示，集中器已经失去连接超过一段时间
	 */
	private boolean warningOrNot=true;
	/**
	 * 该设备上一次收到集中器连接消息的时间
	 */
	private Date lastMsgTime;

	public String getConcentratorNo() {
		return concentratorNo;
	}

	public void setConcentratorNo(String concentratorNo) {
		this.concentratorNo = concentratorNo;
	}

	public boolean isSignaled() {
		return signaled;
	}

	public void setSignaled(boolean signaled) {
		this.signaled = signaled;
	}

	public boolean isWarningOrNot() {
		return warningOrNot;
	}

	public void setWarningOrNot(boolean warningOrNot) {
		this.warningOrNot = warningOrNot;
	}

	public Date getLastMsgTime() {
		return lastMsgTime;
	}

	public void setLastMsgTime(Date lastMsgTime) {
		this.lastMsgTime = lastMsgTime;
	}

	public String getBedName() {
		return bedName;
	}

	public void setBedName(String bedName) {
		this.bedName = bedName;
	}
}
