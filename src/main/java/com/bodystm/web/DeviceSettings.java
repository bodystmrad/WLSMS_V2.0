package com.bodystm.web;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.bodystm.bean.Bed;
import com.bodystm.bean.Patient;
import com.bodystm.bean.PatientBedUser;

public  class DeviceSettings {
	
	/**
	 *key为设备的mac地址，value为Alarm对象 报警设置
	 */
	public static volatile Hashtable AlarmDevice= new Hashtable();  
	
	/**
	 * key为设备的mac地址，value为TemperatureDevice对象
	 */
	public static volatile Hashtable hasDevice= new Hashtable();  
	public static volatile List<String> lstMacs=new ArrayList<String>();
	
	public static volatile Hashtable hasDevice4Concentrator= new Hashtable();  
	public static volatile List<String> lstMacs4Concentrator=new ArrayList<String>();
	/**
	 * 获取当前病人对应的贴片设备的mac地址列表
	 */
	public static volatile List<String> lstMacs4CurrentPatients=new ArrayList<String>();
	/**
	 * 获取当前没有病人床位绑定的贴片设备的mac地址列表
	 */
	public static volatile List<String> lstMacs4NoPatients=new ArrayList<String>();
	public static volatile Hashtable<String,Float> hasMacTem4NoPatient= new Hashtable<String,Float>();
	/**
	 * 是否已经获取当前正在使用的贴片mac地址列表
	 */
	public static volatile boolean ifgotMacs4CurrentPatients=false;
	
	//20170105添加用于减少数据库读写频率，将相关信息存于内存中
	/**
	 * 存储设备状态等相关信息
	 */
	//public static volatile List<PatientBedUser> lstPBU4Macs=new ArrayList<PatientBedUser>();
	public static volatile Hashtable<String,PatientBedUser> hasDevice4PBUOfMacs= new Hashtable<String,PatientBedUser>();  
	/**
	 * 用于温度监测页面获取床位列表信息
	 * 床位增减时需要重启服务器保持数据更新 2018年2月22日11:23:44
	 */
	public static volatile List<Bed> lstBeds=new ArrayList<Bed>();
	/**
	 * 2018年3月2日11:13:01 改为用hashtable来存储
	 * mac-Bed
	 */
//	public static volatile Hashtable<String, Bed> hasBeds=new Hashtable<String, Bed>();
	public static volatile ConcurrentHashMap<String, Bed> hasBeds=new ConcurrentHashMap<String, Bed>();
	/**
	 * bedid-Patient
	 */
	public static volatile Hashtable<String,Patient> hasDevice4PatientOfBed= new Hashtable<String,Patient>();  
	/**
	 * 清空床位和相应病人信息，主要用于有床位增减或者病人增减或修改时清空，重新获取最新的床位信息到内存
	 */
	public static void clearBedPatientData(){
		hasDevice4PBUOfMacs.clear();
//		lstBeds.clear();
		hasDevice4PatientOfBed.clear();
	}
	//2018年1月2日15:20:54
	//lstBeds
}
