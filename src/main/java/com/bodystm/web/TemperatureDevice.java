package com.bodystm.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public  class TemperatureDevice {
	/**
	 * 设备的mac地址
	 */
	private String strMac;
	/**
	 * 该设备最后一次保存数据的时间
	 */
	private Date lastSavTime;
	/**
	 * 该设备要修正温度数据的数值
	 */
	private float num4Correct=0;
	/**
	 * 设备当前温度，即最后一次上传的温度
	 */
	private float currentDegree;
	/**
	 * 该设备最后一次数据（用于连续两个33显示问题相关）
	 */
	private float lastDegree=0;
	/**
	 * 该设备第一次保存数据的时间，用于判断是否超过50小时，超过50小时的数据则不再存储和显示，直接忽略
	 */
	private Date firstSavTime;
	//下边两个为偶然异常数据的处理相关
	/**
	 * 异常温度或者正常温度
	 */
	private float unusualDegree=0;
	/**
	 * 上次的异常时间
	 */
	private Date unusualTime;
	//用于做2分钟内的体温数据的平均数（去掉一个最高值和最低值）
	List<Float> dataIn2Min=new ArrayList<Float>();
	List<Date> date4DataIn2Min=new ArrayList<Date>();
	List<Float> dataIn20Min=new ArrayList<Float>();
	List<Date> date4DataIn20Min=new ArrayList<Date>();
	
	//报警相关
	/**
	 * 警报类型（0代表正常不报警，1代表低温报警，2代表高温报警，3代表超高温报警）
	 */
	int warningType=0;
	/**
	 * 是否报警，当温度恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	boolean warningOrNot=true;
	/**
	 * 当失去连接，持续一段时间不上数时是否报警，当温度恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	boolean warningOrNot4NoSign=true;
	/**
	 * 没电时是否提醒用户
	 */
	Boolean warningOrNot4NoPower;
	//预估稳定值相关
	/**
	 * 第一次接收到数据的时间
	 */
	private Date firstDataTime;
	boolean ifGatherData=false;//是否继续接收数据用于曲线拟合预估平衡值
	boolean ifEstimated=false;//是否已经拟合过
	boolean ifEstimatedOF3Min=false;//是否已经拟合过(老庞算法三分钟)
	float estimatedT=0;//拟合结果
	private Date firstDataTime4Reach33=null;//测试老庞算法用--第一次达到33度以上的温度的时间
	private float timefacter=0;//修正系数
	private float data0=0;
	private boolean ifFirstTFis0;//第一次拟合timefacter是否为0
	//private int index4Last33=0;//记录最后一一个33的温度值所在list中的序号
	private Date date4Last33;//记录最后一一个33的温度值对应的时间
//	private float TotalResult4estimate=0;//老梁算法4中之前几次估算的结果累加和
//	private int times4estimate=0;//老梁算法4中之前几次估算的次数
	List<Float> result4Latest3Times=new ArrayList<Float>();//老梁算法4中记录最近三次算法结果
	//float BCIn0To3=0.2f;//从上第一个大于33的温度开始计时往后三分钟进行补偿
	float estimatedT4LP3=0;//老庞算法的拟合结果
	private Date openArmsTime;//记录打开胳膊的时间，从此刻开始计时超过半小时则发出警告
	boolean warningOrNot4OpenArms=false;//是否报警（病人打开了胳膊）
	boolean ifReNormal4OpenArms=true;//是否从上次温差过大持续半小时开始温差正常过，正常过为true，则再次温差过大就报警，否则不报警
	boolean status4OpenArms=false;//是否打开了胳膊的标志，true代表打开了胳膊
	private float degree4LastMac=0;//上一个贴片最后的体温数据（用于更换贴片时温度数据平滑化）
	private int powerNum=99;//电池电量
	public String getStrMac() {
		return strMac;
	}
	public void setStrMac(String strMac) {
		this.strMac = strMac;
	}
	public Date getLastSavTime() {
		return lastSavTime;
	}
	public void setLastSavTime(Date lastSavTime) {
		this.lastSavTime = lastSavTime;
	}
	public float getNum4Correct() {
		return num4Correct;
	}
	public void setNum4Correct(float num4Correct) {
		this.num4Correct = num4Correct;
	}
	public float getCurrentDegree() {
		return currentDegree;
	}
	public void setCurrentDegree(float currentDegree) {
		this.currentDegree = currentDegree;
	}
	public float getLastDegree() {
		return lastDegree;
	}
	public void setLastDegree(float lastDegree) {
		this.lastDegree = lastDegree;
	}
	public Date getFirstSavTime() {
		return firstSavTime;
	}
	public void setFirstSavTime(Date firstSavTime) {
		this.firstSavTime = firstSavTime;
	}
	public float getUnusualDegree() {
		return unusualDegree;
	}
	public void setUnusualDegree(float unusualDegree) {
		this.unusualDegree = unusualDegree;
	}
	public Date getUnusualTime() {
		return unusualTime;
	}
	public void setUnusualTime(Date unusualTime) {
		this.unusualTime = unusualTime;
	}
	public List<Float> getDataIn20Min() {
		return dataIn20Min;
	}
	public void setDataIn20Min(List<Float> dataIn20Min) {
		this.dataIn20Min = dataIn20Min;
	}
	public List<Date> getDate4DataIn20Min() {
		return date4DataIn20Min;
	}
	public void setDate4DataIn20Min(List<Date> date4DataIn20Min) {
		this.date4DataIn20Min = date4DataIn20Min;
	}
	public int getWarningType() {
		return warningType;
	}
	public void setWarningType(int warningType) {
		this.warningType = warningType;
	}
	public boolean isWarningOrNot() {
		return warningOrNot;
	}
	public void setWarningOrNot(boolean warningOrNot) {
		this.warningOrNot = warningOrNot;
	}
	public boolean isWarningOrNot4NoSign() {
		return warningOrNot4NoSign;
	}
	public void setWarningOrNot4NoSign(boolean warningOrNot4NoSign) {
		this.warningOrNot4NoSign = warningOrNot4NoSign;
	}
	public Boolean getWarningOrNot4NoPower() {
		return warningOrNot4NoPower;
	}
	public void setWarningOrNot4NoPower(Boolean warningOrNot4NoPower) {
		this.warningOrNot4NoPower = warningOrNot4NoPower;
	}
	public Date getFirstDataTime() {
		return firstDataTime;
	}
	public void setFirstDataTime(Date firstDataTime) {
		this.firstDataTime = firstDataTime;
	}
	public boolean isIfGatherData() {
		return ifGatherData;
	}
	public void setIfGatherData(boolean ifGatherData) {
		this.ifGatherData = ifGatherData;
	}
	public boolean isIfEstimated() {
		return ifEstimated;
	}
	public void setIfEstimated(boolean ifEstimated) {
		this.ifEstimated = ifEstimated;
	}
	public float getEstimatedT() {
		return estimatedT;
	}
	public void setEstimatedT(float estimatedT) {
		this.estimatedT = estimatedT;
	}
	public Date getFirstDataTime4Reach33() {
		return firstDataTime4Reach33;
	}
	public void setFirstDataTime4Reach33(Date firstDataTime4Reach33) {
		this.firstDataTime4Reach33 = firstDataTime4Reach33;
	}
	public float getTimefacter() {
		return timefacter;
	}
	public void setTimefacter(float timefacter) {
		this.timefacter = timefacter;
	}
	public float getData0() {
		return data0;
	}
	public void setData0(float data0) {
		this.data0 = data0;
	}
	public boolean isIfFirstTFis0() {
		return ifFirstTFis0;
	}
	public void setIfFirstTFis0(boolean ifFirstTFis0) {
		this.ifFirstTFis0 = ifFirstTFis0;
	}
	public Date getDate4Last33() {
		return date4Last33;
	}
	public void setDate4Last33(Date date4Last33) {
		this.date4Last33 = date4Last33;
	}
	public boolean isIfEstimatedOF3Min() {
		return ifEstimatedOF3Min;
	}
	public void setIfEstimatedOF3Min(boolean ifEstimatedOF3Min) {
		this.ifEstimatedOF3Min = ifEstimatedOF3Min;
	}
//	public float getTotalResult4estimate() {
//		return TotalResult4estimate;
//	}
//	public void setTotalResult4estimate(float totalResult4estimate) {
//		TotalResult4estimate = totalResult4estimate;
//	}
//	public int getTimes4estimate() {
//		return times4estimate;
//	}
//	public void setTimes4estimate(int times4estimate) {
//		this.times4estimate = times4estimate;
//	}
	public List<Float> getResult4Latest3Times() {
		return result4Latest3Times;
	}
	public void setResult4Latest3Times(List<Float> result4Latest3Times) {
		this.result4Latest3Times = result4Latest3Times;
	}
	public float getEstimatedT4LP3() {
		return estimatedT4LP3;
	}
	public void setEstimatedT4LP3(float estimatedT4LP3) {
		this.estimatedT4LP3 = estimatedT4LP3;
	}
	public List<Float> getDataIn2Min() {
		return dataIn2Min;
	}
	public void setDataIn2Min(List<Float> dataIn2Min) {
		this.dataIn2Min = dataIn2Min;
	}
	public List<Date> getDate4DataIn2Min() {
		return date4DataIn2Min;
	}
	public void setDate4DataIn2Min(List<Date> date4DataIn2Min) {
		this.date4DataIn2Min = date4DataIn2Min;
	}
	public Date getOpenArmsTime() {
		return openArmsTime;
	}
	public void setOpenArmsTime(Date openArmsTime) {
		this.openArmsTime = openArmsTime;
	}
	public boolean isWarningOrNot4OpenArms() {
		return warningOrNot4OpenArms;
	}
	public void setWarningOrNot4OpenArms(boolean warningOrNot4OpenArms) {
		this.warningOrNot4OpenArms = warningOrNot4OpenArms;
	}
	public float getDegree4LastMac() {
		return degree4LastMac;
	}
	public void setDegree4LastMac(float degree4LastMac) {
		this.degree4LastMac = degree4LastMac;
	}
	public boolean isIfReNormal4OpenArms() {
		return ifReNormal4OpenArms;
	}
	public void setIfReNormal4OpenArms(boolean ifReNormal4OpenArms) {
		this.ifReNormal4OpenArms = ifReNormal4OpenArms;
	}
	public boolean isStatus4OpenArms() {
		return status4OpenArms;
	}
	public void setStatus4OpenArms(boolean status4OpenArms) {
		this.status4OpenArms = status4OpenArms;
	}
	public int getPowerNum() {
		return powerNum;
	}
	public void setPowerNum(int powerNum) {
		this.powerNum = powerNum;
	}

}
