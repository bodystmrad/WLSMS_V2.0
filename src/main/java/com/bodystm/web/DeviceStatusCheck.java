/**
 * 
 */
package com.bodystm.web;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

import com.bodystm.bean.PatientBedUser;
import com.bodystm.bean.Temperature;
import com.bodystm.dao.PatientBedDao;
import com.bodystm.service.PatientBedService;
import com.bodystm.service.TemperatureService;

/**
 * @author rxz
 *
 */
public class DeviceStatusCheck implements Runnable {
	@Autowired
	private PatientBedService patientBedUserService;

	@Autowired
	private PatientBedDao patientBedUserDao;
	
	@Autowired
	private TemperatureService temperatureService;
	
	@Autowired
	private Integer checkminutes = 10;
	//@PostConstruct
	@Override
	public void run() {
		// TODO Auto-generated method stub
		//��ȡ��ǰ�������в��˵Ĵ�λ����Ϣ
		List<PatientBedUser> lstpatientBedUser=patientBedUserDao.getCurrentPatients();
		Calendar calendar = Calendar.getInstance();
		Date date=new Date();
		Date NOW=new Date();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, -checkminutes);
		date = calendar.getTime();
		Temperature temperature=null;
		PatientBedUser pbu=null;
		boolean ifchangestatus=false;
		for(PatientBedUser patientBedUser:lstpatientBedUser){
			ifchangestatus=false;
			//temperatureService.query(patientBedUser.getPatient().getEquipNo(),new Date(),new date(),null);
			temperature=temperatureService.getLatestPoint(date,patientBedUser.getPatient().getEquipNo());
			if(patientBedUser.getStatusOfDevice()==null){
				patientBedUser.setStatusOfDevice(1);
				ifchangestatus=true;
			}
			//����ȡ����������ʾ���ֹ����ˣ���Ҫ�޸�patientBedUser���豸״̬
			if(temperature==null){
				if(patientBedUser.getStatusOfDevice()==1){
					patientBedUser.setStatusOfDevice(0);
					patientBedUserService.saveEntity(patientBedUser);
					ifchangestatus=true;
				}
			}else{
				if(patientBedUser.getStatusOfDevice()==0){
					//将该设备的失去信号提醒的标志位重置为true
					if(DeviceSettings.lstMacs.contains(patientBedUser.getPatient().getEquipNo())){
						TemperatureDevice temperatureDevice=(TemperatureDevice)DeviceSettings.hasDevice.get(patientBedUser.getPatient().getEquipNo());
						temperatureDevice.setWarningOrNot4NoPower(true);
					}
					patientBedUser.setStatusOfDevice(1);
					patientBedUserService.saveEntity(patientBedUser);
					ifchangestatus=true;
				}
			}
			//20170105 将设备状态信息存储到内存中
			if(ifchangestatus){
				pbu=DeviceSettings.hasDevice4PBUOfMacs.get(patientBedUser.getPatient().getEquipNo());
				if (pbu!=null) {
					pbu.setStatusOfDevice(patientBedUser.getStatusOfDevice());
				}
			}
			//检测 是否超过48小时
			String mac=patientBedUser.getPatient().getEquipNo();
			if(DeviceSettings.lstMacs.contains(mac)){
				TemperatureDevice td=(TemperatureDevice)DeviceSettings.hasDevice.get(mac);
				if(td!=null&&td.getFirstSavTime()!=null&&(NOW.getTime()-td.getFirstSavTime().getTime())/3600000>=50){
					if(td.getWarningOrNot4NoPower()==null){
						td.setWarningOrNot4NoPower(true);
					}
				}
			}
		}

	}

}
