/**
 * 
 */
package com.bodystm.web;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author rxz
 *
 */
public class ClearStatisticVar implements Runnable {
	@Override
	public void run() {
		TemperatureDevice temperatureDevice=null;
		Date dateTemp=null;
		Date dateCur=new Date();
		//Calendar calendar= Calendar.getInstance();
		for(int i=0;i<DeviceSettings.lstMacs.size();i++){
			temperatureDevice=(TemperatureDevice) DeviceSettings.hasDevice.get(DeviceSettings.lstMacs.get(i));
			dateTemp=temperatureDevice.getLastSavTime();
			if(dateTemp!=null&&((dateCur.getTime()-dateTemp.getTime()) / (1000 * 60 * 60 * 24)>=1)){
				DeviceSettings.hasDevice.remove(DeviceSettings.lstMacs.get(i));
				DeviceSettings.lstMacs.remove(i--);
			}
		}
		//清空设备状态数据，用来去掉一些过期无效的数据，防止内存占用越来越高
		DeviceSettings.hasDevice4PBUOfMacs.clear();
		DeviceSettings.lstBeds.clear();
		DeviceSettings.hasDevice4PatientOfBed.clear();
		//rxz 2017年4月20日23:57:46
		DeviceSettings.hasMacTem4NoPatient.clear();
	}

}
