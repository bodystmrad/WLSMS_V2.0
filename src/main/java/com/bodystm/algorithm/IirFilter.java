package com.bodystm.algorithm;
/**
 * 低通滤波
 * @author rxz
 *
 */
public class IirFilter {
	private final int LP_ORDER = 8;//数组大小
    private double[] lpxparam = new double[] { 0.0000490039037893543, 0.000392031230314834, 0.00137210930610192, 0.00274421861220384, 0.00343027326525480, 0.00274421861220384, 0.00137210930610192, 0.000392031230314834, 0.0000490039037893543 };
    private double[] lpyparam = new double[]{4.42843858639182,-9.08258501718593,11.0931736694875,-8.75400718135508,4.54553472458618,-1.51073700701629,0.292953949722513,-0.0253167240008113};
    private double[] lpyvalue = new double[LP_ORDER + 1];
    private double[] lpxvalue = new double[LP_ORDER];
    /// <summary>
    /// 构造函数
    /// </summary>
    public IirFilter()
    {

    }
    /// <summary>
    /// 低通滤波函数
    /// </summary>
    /// <param name="x">输入的某个点的y值</param>
    /// <returns>低通滤波过后的y值</returns>
    public double lowpassFilter(double x)
    {
        double y = x * lpxparam[0] + lpxvalue[0] * lpxparam[1] + lpxvalue[1] * lpxparam[2] + lpxvalue[2] * lpxparam[3] + lpxvalue[3] * lpxparam[4]
            + lpxvalue[4] * lpxparam[5] + lpxvalue[5] * lpxparam[6] + lpxvalue[6] * lpxparam[7] + lpxvalue[7] * lpxparam[8]
            + lpyvalue[0] * lpyparam[0] + lpyvalue[1] * lpyparam[1] + lpyvalue[2] * lpyparam[2] + lpyvalue[3] * lpyparam[3] + lpyvalue[4] * lpyparam[4] + lpyvalue[5] * lpyparam[5] + lpyvalue[6] * lpyparam[6] + lpyvalue[7] * lpyparam[7];
        lpxvalue[7] = lpxvalue[6];
        lpxvalue[6] = lpxvalue[5];
        lpxvalue[5] = lpxvalue[4];
        lpxvalue[4] = lpxvalue[3];
        lpxvalue[3] = lpxvalue[2];
        lpxvalue[2] = lpxvalue[1];
        lpxvalue[1] = lpxvalue[0];
        lpxvalue[0] = x;
        lpyvalue[7] = lpyvalue[6];
        lpyvalue[6] = lpyvalue[5];
        lpyvalue[5] = lpyvalue[4];
        lpyvalue[4] = lpyvalue[3];
        lpyvalue[3] = lpyvalue[2];
        lpyvalue[2] = lpyvalue[1];
        lpyvalue[1] = lpyvalue[0];
        lpyvalue[0] = y;
        return y;
    }
}
