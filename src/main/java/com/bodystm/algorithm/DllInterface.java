package com.bodystm.algorithm;

import java.net.URLDecoder;
import java.net.URLEncoder;

import com.bodystm.config.PublicSetting;
import com.sun.jna.Library;
import com.sun.jna.Native;

public interface DllInterface extends Library {
	//System.load("这里填写dll文件的完整路径")
//	public static String dllName = "src/main/resources/Algorithm";//"Algorithm";// 动态库名字
//	@SuppressWarnings("deprecation")
//	public static String dllName=URLDecoder.decode(DllInterface.class.getResource("/").getPath()).replaceAll("^\\/", "")+"Algorithm.dll";
	/*此种写法windows，linux通用*/
//	public static String dllName="Algorithm";//算法库要放到classes里，库文件名称为libAlgorithm.so linux环境下
//	public static String dllName =Library.class.getResource("").getPath().replaceFirst("/","")+"Algorithm.dll";
	public static String dllName=PublicSetting.dllPath;//"E:/Algorithm";
	DllInterface INSTANCE = (DllInterface) Native.loadLibrary(dllName, DllInterface.class);// 动态库实例
	
	//心电相关
	//MedFilter
	long MedFilter_C();
	double MedFilter_B(double x, int chanId, long c);
	boolean MedFilter_D(long addr);
	//iir
	//EcgHR ECG_HRAnalysis
	long Count_Ecg_Hr_C();
	int Count_Ecg_Hr_B(float HRdata,long c);
	boolean Count_Ecg_Hr_D(long addr);
//	float Breath(float[] input, int length);
	float Breath_B(float input[], int length, long c);
	long Breath_C();
	boolean Breath_D(long addr);
	//IirFilter
	long IIFilter_C();
	double  IIFilter_B(double x, int chanId, int nPowerFilterEnable ,long c);
	boolean IIFilter_D(long addr);
	long LowPassFilter4_C();
	double LowPassFilter4_B(double xn,long c);
	boolean LowPassFilter4_D(long addr);
	long laborpassFilter_C();
	double laborpassFilter_B(double x, int chanId, long c);
	boolean laborpassFilter_D(long addr);
	//MathTools
	long Resampling_C();
	int Resampling_B(float OldF, float NewF, double Data, int nChannel,long c);//, double[] ResValue
	boolean Resampling_D(long addr);
	
	//血氧相关
	//MedfilterBO
	long MedFilterBO_C();
	double MedFilterBO_B(double x, int chanId, long c);
	boolean MedFilterBO_D(long addr);
	//新血氧 2018年6月25日21:19:41
	 void SpO2Calculate4WW(float fRed[], float fIR[], int nLen, int nRetVal[], long addr);
	 long SpO2Algorithm_C();
	 boolean SpO2Algorithm_D(long addr);
	//LowPerf
	long butter_C();
	double butter_B(double xn, int ChanID,long c);
	boolean butter_D(long addr);
	long SSF_Calc_C();
	int SSF_Calc_B(double IredData[],long c);
	boolean SSF_Calc_D(long addr);
	//void LPSpO2Calc(double RedAC[], double RedDC[], double IredAC[], double IredDC[], int *SpO2, int *BPM, float *fPI);
	int LPSpO2Calc(double RedAC[], double RedDC[], double IredAC[], double IredDC[], float bpm_fPI[]);
	//void SSF_BPM(double SSF[], double Threshold, int *BPM, double *NewThreshold);
	
	//CNIBP_PWV
	int GetDeviceCaps4Bodystm_EX(int nType);
}
