package com.bodystm.algorithm;

public class LaborpassFilter {
	private final int LP_ORDER = 8;
    private final int HP_ORDER = 2;
    private final int L50_ORDER = 4;
    private final int L100_ORDER = 2;
    private final int L150_ORDER = 2;

    private double[] l50xparam = new double[] { 0.882648994989472, -2.25155689346435, 3.20117693882636, -2.25155689346435, 0.882648994989472 };
    private double[] l50yparam = new double[] { 2.39241998504052, -3.18735793926214, 2.11069380188819, -0.779116989543166 };
    private double[] l100xparam = new double[] { 0.991252270531138, 0.382491693995236, 0.991252270531138 };
    private double[] l100yparam = new double[] { -0.382491693995236, -0.982504541062275 };
    private double[] l150xparam = new double[] { 0.991252270531137, 1.74537864590384, 0.991252270531137 };
    private double[] l150yparam = new double[] { -1.74537864590384, -0.982504541062274 };

    private double[] l50xvalue = new double[L50_ORDER];
    private double[] l50yvalue = new double[L50_ORDER];
    private double[] l100xvalue = new double[L100_ORDER];
    private double[] l100yvalue = new double[L100_ORDER];
    private double[] l150xvalue = new double[L150_ORDER];
    private double[] l150yvalue = new double[L150_ORDER];
    public LaborpassFilter()
    {

    }

    public double laborpassFilter(double x)
    {
        double y = x * l50xparam[0] + l50xvalue[0] * l50xparam[1] + l50xvalue[1] * l50xparam[2] + l50xvalue[2] * l50xparam[3] + l50xvalue[3] * l50xparam[4]
            + l50yvalue[0] * l50yparam[0] + l50yvalue[1] * l50yparam[1] + l50yvalue[2] * l50yparam[2] + l50yvalue[3] * l50yparam[3];
        l50xvalue[3] = l50xvalue[2];
        l50xvalue[2] = l50xvalue[1];
        l50xvalue[1] = l50xvalue[0];
        l50xvalue[0] = x;

        l50yvalue[3] = l50yvalue[2];
        l50yvalue[2] = l50yvalue[1];
        l50yvalue[1] = l50yvalue[0];
        l50yvalue[0] = y;

        double y1 = y * l100xparam[0] + l100xvalue[0] * l100xparam[1] + l100xvalue[1] * l100xparam[2]
            + l100yvalue[0] * l100yparam[0] + l100yvalue[1] * l100yparam[1];

        l100xvalue[1] = l100xvalue[0];
        l100xvalue[0] = y;

        l100yvalue[1] = l100yvalue[0];
        l100yvalue[0] = y1;

        double y2 = y1 * l150xparam[0] + l150xvalue[0] * l150xparam[1] + l150xvalue[1] * l150xparam[2]
            + l150yvalue[0] * l150yparam[0] + l150yvalue[1] * l150yparam[1];

        l150xvalue[1] = l150xvalue[0];
        l150xvalue[0] = y1;

        l150yvalue[1] = l150yvalue[0];
        l150yvalue[0] = y2;
        return y2;
    }
}
