package com.bodystm.algorithm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.stat.SecondLevelCacheStatistics;

import com.bodystm.bean.datadeal.EcgAnalysis;

public class EcgHR {
	//HR_count
	byte  HR_flag,RR_flag;
	float[]  HRWaveData=new float[16];
	float  HR_Sum  = 0;
	int  Rindex  = 0;
	int  Average_number =0;
	int RR_number,Sum_number;
	float   Fix;
	int  a,b;
	volatile  int   Threshold  = 620;//501 351
	int[] Fixbuf=new int[3];
	int HRtime=0;

	int HRatio;
	//uint16_t HeartRate=0;
	int    HeartRate=0;
	 byte flagr;
	 
	 //滤波相关变量
	 List<Integer> dataIn3s=new ArrayList<Integer>();
	 List<Long> ms4DataIn3s=new ArrayList<Long>();
	/*
	 * Desc: HeartRate Calculator
	 * Params:
	 *      HRData: HeartRate Data
	 * Return:
	 *      HeartRate;
	 * Author:
	 *      TanLiFu
	 */
	public int Count_Ecg_Hr(float HRdata)//计算R波
	{
	  flagr=0;
	  byte j;

	  if (HR_flag<16)
	  {
	    HRWaveData[HR_flag]=HRdata;
	    HR_Sum += HRdata;//HRWaveData[HR_flag];
	    ++HR_flag;//+=HRWaveData[HR_flag++];
	  }
	  if (HR_flag==16)
	  {
	    //a=HR_Sum;
	    Fix = HRWaveData[7]*16 - HR_Sum;//a;
	    HR_Sum = HR_Sum-HRWaveData[0] - HRWaveData[1];

	    if ((Fix>=Threshold && ((HRWaveData[6]<HRWaveData[7]&&HRWaveData[7]>=HRWaveData[8])||(HRWaveData[7]<HRWaveData[8]&&HRWaveData[8]>=HRWaveData[9])))
	        ||(Fix<=-Threshold && ((HRWaveData[6]>=HRWaveData[7]&&HRWaveData[7]<HRWaveData[8]) ||(HRWaveData[7]>=HRWaveData[8]&&HRWaveData[8]<HRWaveData[9]))))
	    {
	      Rindex++;
	      Fixbuf[0]=Fixbuf[1];
	      Fixbuf[1]=Fixbuf[2];
	      if(Fix<0)Fix=0-Fix;
	      Fixbuf[2]=(int)Fix;
	      if (Fixbuf[0] != 0)
	      {
	        Threshold=(Fixbuf[0]+Fixbuf[1]+Fixbuf[2])*2/9;
	      }
	    }
	    for(j=2;j<=15;j++)
	    {
	      HRWaveData[j-2]=HRWaveData[j];
	    }
	    HR_flag-=2;
	  }

	  if((Rindex>0)&&(Rindex<3))
	    RR_number++;
	  if(Rindex==2)
	  {
	    if(RR_number<35)
	      Rindex=Rindex-1;            //RR不应期
	    else
	    {
	      RR_flag++;
	      Sum_number += RR_number;
	      Rindex=1;
	      RR_number = 0;
	    }
	  }
	  if(HRtime>599&& RR_flag==0)
	  {
	    HRtime=0;
	    flagr++;
	    if(flagr==2)//6S
	    {
	      flagr=0;
	      Threshold=500;
	      Fixbuf[0]=0;
	      Fixbuf[1]=0;
	      Fixbuf[2]=0;
	    }
	  }
	  if(HRtime>599 && RR_flag > 0)//3S
	  {
	    HRtime=0;
	    //   Average_number = Sum_number * 5 / RR_flag;
	    //   HRatio = 60000 / Average_number;
	    HRatio = (int)((60.0*EcgAnalysis.SAMPLE_RATE) * (long)(RR_flag) / Sum_number+0.5);
	    if(HRatio>300)HRatio=300;
	    // HRatio +=1;//7860数值偏高的8M晶振
	    // if(HRatio<70)HRatio +=1;//7790数值偏低的8M晶振
	    //else HRatio +=2;
	    RR_flag=0;
	    Sum_number=0;
	    HeartRate = HRatio;//(char)
	  }
	  HRtime++;

//	  return HeartRate;
	  return getAvgHr(HeartRate);
	}
	private double lastbpm=-1;
	public int getAvgHr(int hr){
		Long curMs=System.currentTimeMillis();	
		for(int i=0;i<ms4DataIn3s.size();i++){
			if((curMs-ms4DataIn3s.get(i))/1000>3){
				ms4DataIn3s.remove(i);
				dataIn3s.remove(i);
				i--;
			}else{
				break;
			}
		}

		ms4DataIn3s.add(curMs);
		dataIn3s.add(hr);
		if(ms4DataIn3s.size()<=1){
			
		}else if(ms4DataIn3s.size()<=2){
			hr=(dataIn3s.get(0)+dataIn3s.get(1))/2;
		}else{
			float maxHr=0;
			float minHr=0;
			float totalHr=0;
			for(int i=0;i<dataIn3s.size();i++){
				if(i==0){
					maxHr=dataIn3s.get(i);
					minHr=dataIn3s.get(i);
				}else{
					maxHr=dataIn3s.get(i)>maxHr?dataIn3s.get(i):maxHr;
					minHr=dataIn3s.get(i)<minHr?dataIn3s.get(i):minHr;
				}
				totalHr+=dataIn3s.get(i);
			}
			hr=(int)(totalHr-minHr-maxHr)/(dataIn3s.size()-2);
		}
		//平滑曲线
		if (lastbpm==-1) {
			lastbpm=hr;
		}
		double tmp=lastbpm*0.9+hr*0.1;
		hr=(int)Math.round(tmp);
		lastbpm=tmp;
		return hr;
	}
}
