package com.bodystm.algorithm;
/**
 * 连续无创血压pwv
 * @author ehl
 *
 */
public class CNIBP_PWV {
	public float[] m_fNIBP_PWV_1=new float[2500];
	public float[] m_fNIBP_PWV_2=new float[2500];
	public int curNum=0;
	public static final int NIBP_PWV_CalcBufferLen=2500;
	
	float fir[] = new float[]{ 0.0008f,0.0008f,0.0007f,0.0006f,0.0004f,-0.0000f,-0.0006f,-0.0013f,-0.0023f,-0.0034f,-0.0047f,-0.0059f,-0.0069f,-0.0075f,-0.0075f,-0.0067f,-0.0049f,-0.0019f,0.0023f,
			0.0077f,0.0144f,0.0220f,0.0304f,0.0393f,0.0482f,0.0567f,0.0643f,0.0708f,0.0757f,0.0788f,0.0798f,0.0788f,0.0757f,0.0708f,0.0643f,0.0567f,0.0482f,0.0393f,0.0304f,0.0220f,0.0144f,0.0077f,0.0023f,
			-0.0019f,-0.0049f,-0.0067f,-0.0075f,-0.0075f,-0.0069f,-0.0059f,-0.0047f,-0.0034f,-0.0023f,-0.0013f,-0.0006f,-0.0000f,0.0004f,0.0006f,0.0007f,0.0008f,0.0008f };//滤波系数；


			/*-------------------------------------------*
			*                    卷积                    *
			*                                           */
			float conv(float[] input1, int m, float[] input2, int n, int k)//, float *output)//输入数据1，数据1长度，输入数据2，数据2长度，卷积位置；
			{
			    int i, j;
			    float temp = 0;

			    //j = n - 1;
			    j = 0;
			    for (i = k; i >= 0; i--)
			    {
			        if (i<m&&j < n)
			        {
			            temp = input1[i] * input2[j] + temp;
			        }
			        j++;
			    }
			    return temp;

			}

			/*-------------------------------------------*
			*                    差分                    *
			*                                           */
			void Diff(float[] input, int length, float[] output)
			{
			    int i;
			    for (i = 0; i < length - 1; i++)
			    {
			        output[i] = input[i + 1] - input[i];
			    }

			}

			/*-------------------------------------------*
			*                 寻找参考值                 *
			*                                           */
			float Find_Value(float[] input, int length)
			{
			    int n = length / 15;                   //由大到小排序至15分之一处即可；15为经验值；
			    int i, j;
			    float temp;
			    float temp_input[]=new float[2500];                //500hz采样，5秒数据长度；
			    for (i = 0; i < length; i++)
			    {
			        temp_input[i] = input[i];
			    }
			    for (i = 0; i < n; i++)
			    {
			        for (j = i; j < length; j++)
			        {
			            if (temp_input[i] < temp_input[j])
			            {
			                temp = temp_input[i];
			                temp_input[i] = temp_input[j];
			                temp_input[j] = temp;
			            }
			        }
			    }
			    temp = temp_input[n - 1] / 2;          //参考峰值；
			    return temp;
			}
			/*-------------------------------------------*
			*                 找最大值                   *
			*                                           */
			int Find_Max(int []input, int length)
			{

			    int i;
			    int temp;
			    temp = input[0];

			    for (i = 0; i < length; i++)
			    {
			        if (temp < input[i])
			        {
			            temp = input[i];
			        }

			    }
			    return temp;
			}
			/*-------------------------------------------*
			*                 找最小值                   *
			*                                           */
			int Find_Min(int[] input, int length)
			{

			    int i;
			    int temp;
			    temp = input[0];

			    for (i = 0; i < length; i++)
			    {
			        if (temp > input[i])
			        {
			            temp = input[i];
			        }

			    }
			    return temp;
			}
			/*-------------------------------------------*
			*                 找峰值                     *
			*                                           */
			int PeakScan(float[] input1, float[] input2, int length, int[] index) //返回值k为峰的个数；
			{
			    int index1[] = new int[100];
			    int index2[] = new int[100];
			    int i, k;

			    k = 0;

			    float PeakValue1 = Find_Value(input1, length);
			    float PeakValue2 = Find_Value(input2, length);

			    for (i = 1; i < length - 1; i++)
			    {
			        if (input1[i] > input1[i - 1] && input1[i] > input1[i + 1] && input1[i] > PeakValue1)      //极大值需大于参考峰值；
			        {
			            index1[k] = i;
			        }

			        if (input2[i] > input2[i - 1] && input2[i] > input2[i + 1] && input2[i] > PeakValue2)      //极大值需大于参考峰值；
			        {
			            index2[k] = i;
			        }

			        if (index1[k] - index2[k]<5 && index1[k]>index2[k]&& index2[k]>0)
			        {
			            index[k] = index1[k] - index2[k];//1通道（指尖）-2通道（手腕）；
			            k = k + 1;
			        }

			        if (k == 100)                                                                              //5秒数据的峰值数不会超过100；
			        {
			            break;
			        }
			    }

			    return k;
			}



			/*-------------------------------------------*
			*               计算delta_T                  *
			*                                           */
			public float Calculate_delta_T(float[] PluseWave1, float[] PluseWave2, int length)                         //输入两通道数据及数据长度（两通道数据长度应相等）；
			{
			    int i, k;
			    int temp_Max;
			    int temp_Min;
			    int sum = 0;
			    float delta_T;
			    float PWV;                                                                                   //pulse wave velocity;
			    float dist = 15.5f;                                                                           //手腕到指尖的参考长度；个体差异大；
			    float filtered_Wave1[] = new float[2500];                                                          //500hz采样，5秒数据长度；
			    float filtered_Wave2[] = new float[2500];
			    float diffed_Wave1[] = new float[2500];
			    float diffed_Wave2[] = new float[2500];

			    int index[] = new int[100];

			    for (i = 29; i < 2500; i++)
			    {
			        filtered_Wave1[i - 29] = conv(PluseWave1, length, fir, 61, i);
			        filtered_Wave2[i - 29] = conv(PluseWave2, length, fir, 61, i);                           //滤波;
			    }

			    for (i = 0; i < length-1; i++)
			    {
			        diffed_Wave1[i] = filtered_Wave1[i + 1] - filtered_Wave1[i];                             //差分；
			        diffed_Wave2[i] = filtered_Wave2[i + 1] - filtered_Wave2[i];
			    }


			    k = PeakScan(diffed_Wave1, diffed_Wave2, length, index);                                     //计算时间差；返回值k为峰值个数；

			    for (i = 0; i < k; i++)
			    {
			        sum = sum + index[i];

			    }


			    if (k > 5)
			    {
			        temp_Max = Find_Max(index, k);
			        temp_Min = Find_Min(index, k);
			        delta_T = 2 * (float)(sum - temp_Max - temp_Min) / (float)(k - 2);                        //单位：毫秒
			    }
			    else if (k>0)
			    {
			        delta_T = 2 * (float)(sum) / (float)k;                                                  //单位：毫秒
			    }
			    else
			    {
			        delta_T = 0;                                                                            //错误；不能为0；
			    }

			    if (delta_T > 0)
			    {
			        PWV = dist * 10 / delta_T;                                                                  //波速，单位为m/s；
			    }
			    else
			    {
			        PWV = 0;                                                                  //错误；
			    }
			    return PWV;
			}
}
