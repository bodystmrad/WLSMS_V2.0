package com.bodystm.controller;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateFormatUtils;
import org.hibernate.Criteria;
import org.hibernate.annotations.OrderBy;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.bodystm.bean.Temperature;
import com.bodystm.service.TemperatureService;
import com.mimosa.util.config.Page;

@Controller
@RequestMapping("/equip")
public class EquipController {
	@Resource
	private TemperatureService temperatureService;

	@RequestMapping("/index.action")
	public String index() {
		return "equip/index";
	}
	
	@ResponseBody
	@RequestMapping("/list.action")
	public String list(Page<Temperature> page,String deviceNum) {
		DecimalFormat decimalFormat=new DecimalFormat(".00");//���췽�����ַ��ʽ�������С����2λ,����0����.
		page.setOrder(Page.DESC);
		page.setOrderBy("measureTime");
		if(StringUtils.isEmpty(deviceNum.trim())){
			page=this.temperatureService.findByPage(page);
		}
		else{
			page=this.temperatureService.findByPage(page,Restrictions.eq("equipNo", deviceNum));
		}
		//page=this.temperatureService.findByPage(page);
//		List<Temperature> temperatures = this.temperatureService.getListIn2Days();//getAll();
		List<Temperature> temperatures = page.getResult();
		StringBuilder builder = new StringBuilder("{\"total\":\"");
//		builder.append(temperatures.size());
		builder.append(page.getTotalCount());
		builder.append("\",\"rows\":[");
		Date date = null;
		long n=1;
		for (Temperature temperature : temperatures) {
			builder.append("{\"no\":\"").append(n++);
			//builder.append("{\"id\":\"").append(temperature.getId());
			date = temperature.getMeasureTime();
			builder.append("\",\"time\":\"");
			if (date != null) {
				builder.append(DateFormatUtils.format(date,
						"yyyy-MM-dd HH:mm:ss"));
			}
//			builder.append("\",\"degree1\":\"").append(
//					temperature.getMeasureNumOne());
			builder.append("\",\"degree1\":\"").append(decimalFormat.format(temperature.getMeasureNum()));
			builder.append("\",\"dian\":\"").append(
					temperature.getPowerNum());
			builder.append("\",\"device\":\"")
					.append(temperature.getConcentratorNo()).append("\"},");
		}
		if (temperatures.size() > 0) {
			builder.deleteCharAt(builder.length() - 1);
		}
		builder.append("]}");

		return builder.toString();
	}
	
	@ResponseBody
	@RequestMapping("/addFilter.action")
	public String addFilter(String strMac) {
		if(!com.bodystm.web.Filter4DataGather.lstMacs.contains(strMac))
		com.bodystm.web.Filter4DataGather.lstMacs.add(strMac);
		return "{\"success\":\"true\"}";
	}

}