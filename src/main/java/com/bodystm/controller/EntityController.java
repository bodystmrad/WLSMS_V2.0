package com.bodystm.controller;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mimosa.common.dao.PropertyFilter;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.service.CommonService;
import com.mimosa.common.view.EasyUiDataGrid;
import com.mimosa.util.config.Page;

public abstract class EntityController<T extends BaseEntity> {
	/**
	 * 鏃ュ織璁板綍鍣�
	 */
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	protected abstract CommonService<T, String> getEntityService();

	protected abstract String getEntityName();

	@SuppressWarnings("unchecked")
	protected List<PropertyFilter> getDefaultPropertyFilter() {
		return Collections.EMPTY_LIST;
	}

	protected String getMappingPath() {
		return StringUtils.EMPTY;
	}

	public static final String REDIRECT_URL = "redirect:index.action";

	@RequestMapping(value = { "/index" })
	public String index(Page<T> page, HttpServletRequest request, Model model) {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(request);
		filters.addAll(this.getDefaultPropertyFilter());
		if (StringUtils.isBlank(page.getOrderBy())) {
			page.setOrder(Page.DESC);
			page.setOrderBy("createTime");
		}
		Page<T> resultPage = this.getEntityService().findByPage(page, filters);
		model.addAttribute("resultPage", resultPage);
		return this.getMappingPath() + this.getEntityName() + "/index";
	}

	@RequestMapping(value = { "/list" })
	public String list(Page<T> page, HttpServletRequest request) {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(request);
		filters.addAll(this.getDefaultPropertyFilter());
		Page<T> pageResult = this.getEntityService().findByPage(page, filters);
		EasyUiDataGrid<T> grid = new EasyUiDataGrid<T>(pageResult);
		return grid.toString();
	}

	@RequestMapping(value = { "/save" })
	public String save(@ModelAttribute("preloadEntity") T entity) {
		if (StringUtils.isBlank(entity.getId())) {
			this.getEntityService().saveEntity(entity);
		} else {
			this.getEntityService().updateEntity(entity);
		}
		return REDIRECT_URL;
	}

	@RequestMapping(value = { "/delete" })
	public String delete(String id) {
		this.getEntityService().deleteEntity(id);
		return REDIRECT_URL;
	}

	@RequestMapping(value = { "/show" })
	public String show(String id, Model model) {
		if (StringUtils.isNotBlank(id)) {
			T t = this.getEntityService().getEntity(id);
			model.addAttribute("entity", t);
		}
		return this.getMappingPath() + this.getEntityName() + "/show";
	}

	@RequestMapping(value = { "/edit" })
	public String input(String id, Model model) {
		if (StringUtils.isNotBlank(id)) {
			T t = this.getEntityService().getEntity(id);
			model.addAttribute("entity", t);
		}
		return this.getMappingPath() + this.getEntityName() + "/edit";
	}

	@RequestMapping("/update")
	public String update(@ModelAttribute("preloadEntity") T entity) {
		this.getEntityService().saveEntity(entity);
		return REDIRECT_URL;
	}

	protected abstract T getNewEntity();

	@ModelAttribute("preloadEntity")
	public T getEntity(@RequestParam(value = "id", required = false) String id) {
		T entity = null;
		if (StringUtils.isNotBlank(id)) {
			entity = this.getEntityService().getEntity(id);
		} else {
			entity = this.getNewEntity();
		}
		return entity;
	}
}