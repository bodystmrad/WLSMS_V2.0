package com.bodystm.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bodystm.bean.Alarm;
import com.bodystm.bean.EquipInfo;
import com.bodystm.service.EquipService;
import com.bodystm.util.JsonDateValueProcessor;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping("/EquipInfo")
public class EquipInfoController {

	@Autowired
	private EquipService equipService;

	/**
	 * 查新设备信息
	 * 
	 * @param PatientId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/EquipInfo.action")
	public String queryEquipInfo(String PatientId) {
		if(PatientId==null || PatientId.length()==0) {
			return "{\"success\":false,\"msg\":病人ID不能为空！}";
		}
		JSONObject jsonObject = new JSONObject();
		// 时间格式处理
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
		// 验证该用户是否设置报警
		List<EquipInfo> EquipInfo = equipService.getEquipInfoByPatientId(PatientId);
		if (EquipInfo==null) {
			return "{\"success\":true,\"msg\":此病人没有报警信息！}";
		}
		JSONArray json = JSONArray.fromObject(EquipInfo, jsonConfig);
		jsonObject.put("RealList", json);
		return json.toString();
	}
}
