package com.bodystm.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bodystm.bean.Patient;
import com.bodystm.bean.Temperature;
import com.bodystm.service.TemperatureService;
import com.bodystm.web.EasyUiMessage;

@Controller
@RequestMapping("/gather")
public class GatherController {
	@Resource
	private TemperatureService temperatureService;

	@RequestMapping("/query.action")
	public String query(Patient patient) {
		List<Temperature> list = this.temperatureService.query(
				patient.getEquipNo(), patient.getStartTime(),
				patient.getEndTime(), null);
		return new EasyUiMessage().toString();
	}
}