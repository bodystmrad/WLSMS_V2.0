package com.bodystm.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bodystm.system.SystemSetting;
import com.bodystm.system.SystemSettingService;
import com.mimosa.util.ImgCompress;
import com.mimosa.util.config.Page;
import com.mimosa.util.constant.StringConstUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/system")
public class SystemSettingController{
	@Resource
	private SystemSettingService systemSettingService;
	
	@RequestMapping("/avg.action")
	public String avg() {
		return "system/setting4Avg";
	}
	@RequestMapping("/systemSetting.action")
	public String systemSetting() {
		return "system/systemSetting";
	}
	
	
	@ResponseBody
	@RequestMapping("/updateArg4Avg.action")
	public String updateArg4Avg(int ifGetAvg){
		JSONObject jsonObject=new JSONObject();
		try {
			SystemSetting systemSetting = systemSettingService.getSystemSetting();
			if (systemSetting == null) {
				systemSetting = new SystemSetting();
			}
			systemSetting.setIfGetAvgInDataGather(ifGetAvg);
			systemSettingService.saveEntity(systemSetting);
			SystemSetting.ifGetAvgInDataGather4Temp=ifGetAvg==1?true:false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			jsonObject.put("success", false);
		}
		jsonObject.put("success", true);
		return jsonObject.toString();
	}
	
	/**
	 * 获取系统参数（是否采集数据时取两分钟内平均值）
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getSystemSetting.action")
	public String getSystemSetting(){
		JSONObject jsonObject=new JSONObject();
		SystemSetting systemSetting = systemSettingService.getSystemSetting();
		if (systemSetting!=null) {
			jsonObject.put("success", true);
			jsonObject.put("ifAvg", systemSetting.isIfGetAvgInDataGather());
		}else{
			jsonObject.put("success", false);
			jsonObject.put("ifAvg", true);
		}
		return jsonObject.toString();
	}
	
	@RequestMapping(value = "/uploadLogo.action", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String uploadLogo(@RequestParam("file") MultipartFile file, String hospitalName,
			HttpServletRequest request) {
		boolean result = false;
		String imgPath = null;
		try {
			SystemSetting systemSetting = systemSettingService.getSystemSetting();
			if (systemSetting == null) {
				systemSetting = new SystemSetting();
			}
			if (StringUtils.isNotBlank(hospitalName)) {
				systemSetting.setHospitalName(hospitalName);
			}
			if (file.getSize() > 0) {
				// 文件上传路径
				ServletContext sc = request.getSession().getServletContext();
				String rootDir = sc.getRealPath("/");
				String suffix = StringUtils.substringAfter(file.getOriginalFilename(), StringConstUtil.DOT);
				String filePath = "/pictureupload/" + System.currentTimeMillis();
				String smallFilePath = filePath + "small";
				if (StringUtils.isNotBlank(suffix)) {
					filePath += StringConstUtil.DOT + suffix;
					smallFilePath += StringConstUtil.DOT + suffix;
				}
				try {
					File imgFile = new File(rootDir + filePath);
					FileUtils.writeByteArrayToFile(imgFile, file.getBytes());
					systemSetting.setHospitalLogoAbsolutePath(rootDir + filePath);
					systemSetting.setHospitalLogoPath(filePath);
					/*
					 * // 压缩图片 ImgCompress compress = new ImgCompress(imgFile);
					 * compress.resizeByHeight(200); compress.toFile(rootDir +
					 * smallFilePath);
					 * tTaskPicture.setSmallFilePath(smallFilePath);
					 * taskPictureService.saveEntity(tTaskPicture);
					 */
//					systemSettingService.saveEntity(systemSetting);
//					result = true;
					imgPath = filePath;
				} catch (IOException e) {

				}
			}
			systemSettingService.saveEntity(systemSetting);
			result = true;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject JSONObject = new JSONObject();
		JSONObject.put("success", result);
		JSONObject.put("imgPath", imgPath);
		return JSONObject.toString();
	}
}