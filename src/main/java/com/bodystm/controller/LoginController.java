package com.bodystm.controller;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mimosa.common.system.user.TUser;
import com.mimosa.common.system.user.UserContext;
import com.mimosa.common.system.user.UserService;
import com.mimosa.util.config.Page;

@Controller
@RequestMapping("/home")
public class LoginController{
	@Autowired
	private UserService userService;
	
	@RequestMapping("/index")
	public String index(Model model) {
		return "home/login";
	}


	
	@RequestMapping({ "/logIn" })
	public String logIn() {
		return "home/login";
	}

	@RequestMapping("/logIn!Check")
	public String logCheck(TUser user, HttpSession session) {
		TUser resultUser = this.userService.login(user.getUserName(), user.getUserPass());
		String result = StringUtils.EMPTY;
		if (resultUser != null) {
			session.setAttribute(UserContext.USER_ATTR, resultUser);
			if(resultUser.getUserName().toLowerCase().equals("admin")){
				result = "redirect:../user/index.action";
			}
			else{
				result = "redirect:../bed/index.action";
			}
					
		}else if(user.getUserName().equals("admin")&&user.getUserPass().equals("admin")){ 
			session.setAttribute(UserContext.USER_ATTR, user);
			result = "redirect:../user/index.action";
		}else {
			session.setAttribute("loginErr", "用户名或密码不正确");
			result = "redirect:logIn.action";
		}
		return result;
	}

	@RequestMapping("/logOut")
	public String logOut(HttpSession session) {
		try {
			session.invalidate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:logIn.action";
	}
}
