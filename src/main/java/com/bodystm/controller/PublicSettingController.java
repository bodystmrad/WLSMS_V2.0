package com.bodystm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bodystm.config.PublicSetting;

@Controller
@RequestMapping("/PublicSetting")
public class PublicSettingController {
	@ResponseBody
	@RequestMapping("/changeShowType.action")
	public void changeShowPulseOrHeartRate(int type){
		PublicSetting.ShowPulseOrHeartRate=type;
	}
}
