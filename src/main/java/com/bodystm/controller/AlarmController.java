package com.bodystm.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bodystm.bean.Alarm;
import com.bodystm.bean.AlarmRecord;
import com.bodystm.service.AlarmService;
import com.bodystm.util.JsonDateValueProcessor;
import com.bodystm.web.DeviceSettings;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@SuppressWarnings("unchecked")
@Controller
@RequestMapping("/Alarm")
public class AlarmController {

	@Autowired
	private AlarmService alarmService;

	/**
	 * 报警参数复位
	 * 
	 * @param PatienTtype
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/paramReset.action")
	public String paraReset(String PatienTtype) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = new JSONObject();
		// 时间格式处理
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
		List<Alarm> initAlarmData = alarmService.getAlarmByPatientId(PatienTtype);
		JSONArray json = JSONArray.fromObject(initAlarmData, jsonConfig);
		jsonObject.put("AlarmList", json);
		return jsonObject.toString();
	}

	/**
	 * 报警按钮设置-全局状态
	 * 
	 * @param state
	 */
	@ResponseBody
	@RequestMapping("/setupAlarm.action")
	public String SetupAlarm(String state) {
		// 将报警状态存入内存
		DeviceSettings.AlarmDevice.put("state", state);
		DeviceSettings.AlarmDevice.put("time", new Date());
		return "{\"success\":true}";
	}

	/**
	 * 保存报警基本参数
	 * 
	 * @param Alarm
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/save.action")
	// List<Alarm> list
	public String saveAlarm(List<Alarm> list, String mac, String Asphyxia, String volume) {
		try {
			// 将报警信息存入内存
			DeviceSettings.AlarmDevice.put(mac, list);
			DeviceSettings.AlarmDevice.put(mac + "Asphyxia", Asphyxia);
			DeviceSettings.AlarmDevice.put("volume", volume);
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"success\":false,\"msg\":系统繁忙稍后再试！}";
		}
		return "{\"success\":true}";
	}

	/**
	 * 查询报警设置
	 * 
	 * @param PatientId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/alarmSetup.action")
	public String getAlarmByID(String mac, String PatienTtype) {

		JSONObject jsonObject = new JSONObject();
		// 时间格式处理
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());

		// 验证该用户是否设置报警
		List<Alarm> list = (List<Alarm>) DeviceSettings.AlarmDevice.get(mac);

		if (list == null) {
			List<Alarm> initAlarmData = alarmService.getAlarmByPatientId(PatienTtype);
			JSONArray json = JSONArray.fromObject(initAlarmData, jsonConfig);
			jsonObject.put("Alarm", json);
		} else {
			JSONArray json = JSONArray.fromObject(list, jsonConfig);
			jsonObject.put("Alarm", json);
		}
		// 获取音量
		String volume = (String) DeviceSettings.AlarmDevice.get("volume");
		if (volume != null) {
			jsonObject.put("volume", volume);
		} else {
			jsonObject.put("volume", "20");
		}
		return jsonObject.toString();
	}

	/**
	 * 查询报警记录
	 * 
	 * @param PatientId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/alarmRecord.action")
	public String alarmRecord(String PatientId) {
		if (PatientId == null || PatientId.length() == 0) {
			return "{\"success\":false,\"msg\":病人ID不能为空！}";
		}

		JSONObject jsonObject = new JSONObject();
		// 时间格式处理
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
		// 验证该用户是否设置报警
		List<Alarm> alarmByPatientId = alarmService.getAlarmByPatientId(PatientId);
		if (alarmByPatientId == null) {
			return "{\"success\":false,\"msg\":此病人没有注册报警功能！}";
		}
		// 查询该病人报警信息
		List<AlarmRecord> alarmRecord = alarmService.getAlarmRecordByPatientId(PatientId);
		if (alarmRecord == null) {
			return "{\"success\":false,\"msg\":此病人没有报警信息！}";
		}
		
		JSONArray json = JSONArray.fromObject(alarmRecord, jsonConfig);
		jsonObject.put("RealList", json);
		return jsonObject.toString();
	}

//	/**
//	 * 初始化报警参数
//	 * @param PatienTtype
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping("/initAlarmData.action")
//	public String initAlarmData(String PatienTtype) {
//		JSONObject jsonObject = new JSONObject();
//		// 时间格式处理
//		JsonConfig jsonConfig = new JsonConfig();
//		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
//		List<Alarm> initAlarmData = alarmService.getAlarmByPatientId(PatienTtype);
//		JSONArray json = JSONArray.fromObject(initAlarmData, jsonConfig);
//		jsonObject.put("InitAlarmList", json);
//		return jsonObject.toString();
//	}

//	/**
//	 * 报警设置查询
//	 * 
//	 * @param PatientId
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping("/alarmSetup.action")
//	public String getAlarmByID(String mac) {
//		JSONObject jsonObject = new JSONObject();
//		// 时间格式处理
//		JsonConfig jsonConfig = new JsonConfig();
//		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
//		List<Alarm> Alarm = (List<Alarm>) DeviceSettings.AlarmDevice.get(mac);
//		JSONArray json = JSONArray.fromObject(Alarm, jsonConfig);
//		jsonObject.put("Alarm", json);
//		return jsonObject.toString();
//	}

}
