package com.bodystm.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bodystm.bean.HistoricalData;
import com.bodystm.service.HistoricalDataService;
import com.bodystm.util.JsonDateValueProcessor;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping("/HistoricalData")
public class HistoricalDataController {

	@Autowired
	private HistoricalDataService historicalDataService;

	/**
	 * 历史数据回顾
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/queryData.action")
	public String querydata(String Patient_Id, String Starttime, String Endtime,String Resolution) {
		JSONObject jsonObject = new JSONObject();
		try {
			if(Patient_Id==null || Patient_Id.length()==0) {
				return "{\"success\":false,\"msg\":病人ID不能为空！}";
			}
			// 时间格式处理
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
			// 获取历史数据
			List<HistoricalData> List = historicalDataService.getHistoricalDataByPatientId(Patient_Id, Starttime,
					Endtime,Resolution);
			JSONArray json = JSONArray.fromObject(List, jsonConfig);	
			jsonObject.put("Hist", json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
}
