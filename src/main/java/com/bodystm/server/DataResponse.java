package com.bodystm.server;

import org.apache.commons.lang.ArrayUtils;

/**
 * 娑堟伅鐨勮繑鍥炲�
 * 
 * @author ggeagle
 *
 */
public class DataResponse {
	/**
	 * 娑堟伅ID
	 */
	private byte msgId;
	/**
	 * 璁惧鍙�
	 */
	private byte[] equipNo;
	/**
	 * 搴忓垪鍙�
	 */
	private byte serialNo;
	/**
	 * 鏍￠獙鍜�
	 */
	private byte checkSum;

	public byte getCheckSum() {
		this.checkSum = (byte) (this.getMsgId() ^ 0X00);
		for (byte b : this.getEquipNo()) {
			this.checkSum = (byte) (this.checkSum ^ b);
		}
		this.checkSum = (byte) (this.checkSum ^ this.getSerialNo());
		return checkSum;
	}

	public void setCheckSum(byte checkSum) {
		this.checkSum = checkSum;
	}

	public byte getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(byte serialNo) {
		this.serialNo = serialNo;
	}

	public byte[] getEquipNo() {
		if (this.equipNo == null) {
			this.equipNo = ArrayUtils.EMPTY_BYTE_ARRAY;
		}
		return equipNo;
	}

	public void setEquipNo(byte[] equipNo) {
		this.equipNo = equipNo;
	}

	public byte getMsgId() {
		return msgId;
	}

	public void setMsgId(byte msgId) {
		this.msgId = msgId;
	}

	public int getLength() {
		int result = 3;
		if (this.getEquipNo() != null) {
			result += this.getEquipNo().length;
		}
		return result;
	}
}