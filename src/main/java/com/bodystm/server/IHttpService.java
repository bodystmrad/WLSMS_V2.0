package com.bodystm.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

public interface IHttpService {
	void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest request);
}
