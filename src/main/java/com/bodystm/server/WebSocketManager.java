package com.bodystm.server;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.bodystm.config.PublicSetting;

public class WebSocketManager implements Runnable{
	private static Logger logger=Logger.getLogger(WebSocketManager.class);
	public volatile static WebSocketServer wsServer=null;
	/*static{
		wsServer=new WebSocketServer(6666);
		if (wsServer!=null) {			
			wsServer.start();
			logger.info("WebSocketServer 启动成功！端口号为6666");
		}
	}*/
	@Override
	public void run() {
		//初始化系统参数：
		org.springframework.core.io.Resource resource = new ClassPathResource("/applicationContext.properties");//
        Properties props = null;
		try {
			props = PropertiesLoaderUtils.loadProperties(resource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String ip=props.getProperty("websocket_host");
		int port=Integer.parseInt(props.getProperty("websocket_port"));
		PublicSetting.websocketUrl="ws://"+ip+":"+port;
		wsServer=new WebSocketServer(ip,port);//4000
		if (wsServer!=null) {	
			logger.info("启动WebSocketServer！端口号为"+port);
			wsServer.start();
			
		}
	}
}
