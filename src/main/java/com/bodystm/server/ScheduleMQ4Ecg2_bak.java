package com.bodystm.server;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import redis.clients.jedis.Jedis;
/**
 * 消费者使用ScheduleMQ接收数据
 * ScheduleMQ mq = new ScheduleMQ();  //也就是consumer
       mq.start();
 * @author ehl
 *
 */
public class ScheduleMQ4Ecg2_bak extends Thread {
	Logger logger=Logger.getLogger(ScheduleMQ4Ecg2_bak.class);
	private byte[] mac;
	private String strMac;
	public ScheduleMQ4Ecg2_bak(byte[] mac){
		this.mac=mac;
		this.strMac=ByteUtil.convertByteToString(mac);
	}
	@Override
	public void run(){
		//==========================
		//建立websocket服务，等待客户端连接
		
		//==========================
		Jedis jedis=null;
		try {
			while(true) {
			    jedis = RedisManager.getJedis();  
			    if (jedis==null) {
			    	logger.error("redis服务连接失败！");
					break;
				}
			    //阻塞式brpop，List中无数据时阻塞  
			    //参数0表示一直阻塞下去，直到List出现数据  
			    List<byte[]> list = jedis.brpop(0, mac);  
			    Channel channel =null;
			    for (byte[] bs : list) {
			    	String flag=ByteUtil.convertByteToString(bs[7]);
				}
			    WebSocketManager.wsServer.sendMessage(strMac, strMac);
			    if (WebSocketManager.wsServer!=null) {
			    	channel = WebSocketManager.wsServer.channelsMap.get(strMac);
				}
		        if (channel!=null&&channel.isOpen()) {
		        	for(byte[] s : list) {  
				        System.out.println(s);  
				        //此处编写消息处理逻辑，比如解析消息后通过websocket将心电血氧波形数据发送到客户端
				        channel.writeAndFlush(new TextWebSocketFrame("dsfsfsftesttest"));
				        
				    } 
		        	//channel.flush();
				}
			     
  
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			//jedis.close();  
			RedisManager.close(jedis);
		}
	}
}
