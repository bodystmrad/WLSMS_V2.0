package com.bodystm.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Repository;
import com.bodystm.bean.Alarm;
import com.bodystm.service.AlarmService;

/**
 * 报警设置初始化默认参数
 * 
 * @author fy
 *
 */
@Repository
public class InitAlarmData implements ApplicationListener {

	@Autowired
	private AlarmService alarmService;

	public AlarmService getAlarmService() {
		return alarmService;
	}

	public void setAlarmService(AlarmService alarmService) {
		this.alarmService = alarmService;
	}

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		List<Alarm> list = new ArrayList<Alarm>();
		List<Alarm> AlarmData = alarmService.findList("from Alarm");
		if (AlarmData.size() <= 0) {

			// 报警初始化参数-成人
			Alarm alarmHR2 = new Alarm();
			alarmHR2.setAlarm_type("HR");
			alarmHR2.setBolt_lock("1");
			alarmHR2.setLower_limit("50");
			alarmHR2.setUpper_limit("120");
			alarmHR2.setPatient_Type("2");
			alarmHR2.setWhether("0");
			alarmHR2.setPriority("1");

			Alarm alarmRR2 = new Alarm();
			alarmRR2.setAlarm_type("RR");
			alarmRR2.setBolt_lock("1");
			alarmRR2.setLower_limit("8");
			alarmRR2.setUpper_limit("30");
			alarmRR2.setPatient_Type("2");
			alarmRR2.setWhether("0");
			alarmRR2.setPriority("1");

			Alarm alarmSBP2 = new Alarm();
			alarmSBP2.setAlarm_type("SBP");
			alarmSBP2.setBolt_lock("1");
			alarmSBP2.setLower_limit("90");
			alarmSBP2.setUpper_limit("160");
			alarmSBP2.setPatient_Type("2");
			alarmSBP2.setWhether("0");
			alarmSBP2.setPriority("1");

			Alarm alarmDBP2 = new Alarm();
			alarmDBP2.setAlarm_type("DBP");
			alarmDBP2.setBolt_lock("1");
			alarmDBP2.setLower_limit("50");
			alarmDBP2.setUpper_limit("90");
			alarmDBP2.setPatient_Type("2");
			alarmDBP2.setWhether("0");
			alarmDBP2.setPriority("1");

			Alarm alarmMBP2 = new Alarm();
			alarmMBP2.setAlarm_type("MBP");
			alarmMBP2.setBolt_lock("1");
			alarmMBP2.setLower_limit("60");
			alarmMBP2.setUpper_limit("110");
			alarmMBP2.setPatient_Type("2");
			alarmMBP2.setWhether("0");
			alarmMBP2.setPriority("1");

			Alarm alarmSpo2 = new Alarm();
			alarmSpo2.setAlarm_type("Spo2");
			alarmSpo2.setBolt_lock("1");
			alarmSpo2.setLower_limit("90");
			alarmSpo2.setUpper_limit("100");
			alarmSpo2.setPatient_Type("2");
			alarmSpo2.setWhether("0");
			alarmSpo2.setPriority("1");

			Alarm alarmPR2 = new Alarm();
			alarmPR2.setAlarm_type("PR");
			alarmPR2.setBolt_lock("1");
			alarmPR2.setLower_limit("50");
			alarmPR2.setUpper_limit("120");
			alarmPR2.setPatient_Type("2");
			alarmPR2.setWhether("0");
			alarmPR2.setPriority("1");

			Alarm alarmTemp2 = new Alarm();
			alarmTemp2.setAlarm_type("Temp");
			alarmTemp2.setBolt_lock("1");
			alarmTemp2.setLower_limit("36.0");
			alarmTemp2.setUpper_limit("39.0");
			alarmTemp2.setPatient_Type("2");
			alarmTemp2.setWhether("0");
			alarmTemp2.setPriority("1");

			list.add(alarmHR2);
			list.add(alarmRR2);
			list.add(alarmSBP2);
			list.add(alarmDBP2);
			list.add(alarmMBP2);
			list.add(alarmSpo2);
			list.add(alarmPR2);
			list.add(alarmTemp2);

			// 报警初始化参数-幼儿 1
			Alarm alarmHR1 = new Alarm();
			alarmHR1.setAlarm_type("HR");
			alarmHR1.setBolt_lock("1");
			alarmHR1.setLower_limit("75");
			alarmHR1.setUpper_limit("160");
			alarmHR1.setPatient_Type("1");
			alarmHR1.setWhether("0");
			alarmHR1.setPriority("1");

			Alarm alarmRR1 = new Alarm();
			alarmRR1.setAlarm_type("RR");
			alarmRR1.setBolt_lock("1");
			alarmRR1.setLower_limit("8");
			alarmRR1.setUpper_limit("30");
			alarmRR1.setPatient_Type("1");
			alarmRR1.setWhether("0");
			alarmRR1.setPriority("1");

			Alarm alarmSBP1 = new Alarm();
			alarmSBP1.setAlarm_type("SBP");
			alarmSBP1.setBolt_lock("1");
			alarmSBP1.setLower_limit("70");
			alarmSBP1.setUpper_limit("120");
			alarmSBP1.setPatient_Type("1");
			alarmSBP1.setWhether("0");
			alarmSBP1.setPriority("1");

			Alarm alarmDBP1 = new Alarm();
			alarmDBP1.setAlarm_type("DBP");
			alarmDBP1.setBolt_lock("1");
			alarmDBP1.setLower_limit("40");
			alarmDBP1.setUpper_limit("70");
			alarmDBP1.setPatient_Type("1");
			alarmDBP1.setWhether("0");
			alarmDBP1.setPriority("1");

			Alarm alarmMBP1 = new Alarm();
			alarmMBP1.setAlarm_type("MBP");
			alarmMBP1.setBolt_lock("1");
			alarmMBP1.setLower_limit("50");
			alarmMBP1.setUpper_limit("90");
			alarmMBP1.setPatient_Type("1");
			alarmMBP1.setWhether("0");
			alarmMBP1.setPriority("1");

			Alarm alarmSpo21 = new Alarm();
			alarmSpo21.setAlarm_type("Spo2");
			alarmSpo21.setBolt_lock("1");
			alarmSpo21.setLower_limit("90");
			alarmSpo21.setUpper_limit("100");
			alarmSpo21.setPatient_Type("1");
			alarmSpo21.setWhether("0");
			alarmSpo21.setPriority("1");

			Alarm alarmPR1 = new Alarm();
			alarmPR1.setAlarm_type("PR");
			alarmPR1.setBolt_lock("1");
			alarmPR1.setLower_limit("75");
			alarmPR1.setUpper_limit("160");
			alarmPR1.setPatient_Type("1");
			alarmPR1.setWhether("0");
			alarmPR1.setPriority("1");

			Alarm alarmTemp1 = new Alarm();
			alarmTemp1.setAlarm_type("Temp");
			alarmTemp1.setBolt_lock("1");
			alarmTemp1.setLower_limit("36.0");
			alarmTemp1.setUpper_limit("39.0");
			alarmTemp1.setPatient_Type("1");
			alarmTemp1.setWhether("0");
			alarmTemp1.setPriority("1");

			list.add(alarmHR1);
			list.add(alarmRR1);
			list.add(alarmSBP1);
			list.add(alarmDBP1);
			list.add(alarmMBP1);
			list.add(alarmSpo21);
			list.add(alarmPR1);
			list.add(alarmTemp1);

			// 报警初始化参数-新生儿 0
			Alarm alarmHR0 = new Alarm();
			alarmHR0.setAlarm_type("HR");
			alarmHR0.setBolt_lock("1");
			alarmHR0.setLower_limit("100");
			alarmHR0.setUpper_limit("200");
			alarmHR0.setPatient_Type("0");
			alarmHR0.setWhether("0");
			alarmHR0.setPriority("1");

			Alarm alarmRR0 = new Alarm();
			alarmRR0.setAlarm_type("RR");
			alarmRR0.setBolt_lock("1");
			alarmRR0.setLower_limit("30");
			alarmRR0.setUpper_limit("100");
			alarmRR0.setPatient_Type("0");
			alarmRR0.setWhether("0");
			alarmRR0.setPriority("1");

			Alarm alarmSBP0 = new Alarm();
			alarmSBP0.setAlarm_type("SBP");
			alarmSBP0.setBolt_lock("1");
			alarmSBP0.setLower_limit("40");
			alarmSBP0.setUpper_limit("90");
			alarmSBP0.setPatient_Type("0");
			alarmSBP0.setWhether("0");
			alarmSBP0.setPriority("1");

			Alarm alarmDBP0 = new Alarm();
			alarmDBP0.setAlarm_type("DBP");
			alarmDBP0.setBolt_lock("1");
			alarmDBP0.setLower_limit("20");
			alarmDBP0.setUpper_limit("60");
			alarmDBP0.setPatient_Type("0");
			alarmDBP0.setWhether("0");
			alarmDBP0.setPriority("1");

			Alarm alarmMBP0 = new Alarm();
			alarmMBP0.setAlarm_type("MBP");
			alarmMBP0.setBolt_lock("1");
			alarmMBP0.setLower_limit("25");
			alarmMBP0.setUpper_limit("70");
			alarmMBP0.setPatient_Type("0");
			alarmMBP0.setWhether("0");
			alarmMBP0.setPriority("1");

			Alarm alarmSpo20 = new Alarm();
			alarmSpo20.setAlarm_type("Spo2");
			alarmSpo20.setBolt_lock("1");
			alarmSpo20.setLower_limit("80");
			alarmSpo20.setUpper_limit("95");
			alarmSpo20.setPatient_Type("0");
			alarmSpo20.setWhether("0");
			alarmSpo20.setPriority("1");

			Alarm alarmPR0 = new Alarm();
			alarmPR0.setAlarm_type("PR");
			alarmPR0.setBolt_lock("1");
			alarmPR0.setLower_limit("100");
			alarmPR0.setUpper_limit("200");
			alarmPR0.setPatient_Type("0");
			alarmPR0.setWhether("0");
			alarmPR0.setPriority("1");

			Alarm alarmTemp0 = new Alarm();
			alarmTemp0.setAlarm_type("Temp");
			alarmTemp0.setBolt_lock("1");
			alarmTemp0.setLower_limit("36.0");
			alarmTemp0.setUpper_limit("39.0");
			alarmTemp0.setPatient_Type("0");
			alarmTemp0.setWhether("0");
			alarmTemp0.setPriority("1");

			list.add(alarmHR0);
			list.add(alarmRR0);
			list.add(alarmSBP0);
			list.add(alarmDBP0);
			list.add(alarmMBP0);
			list.add(alarmSpo20);
			list.add(alarmPR0);
			list.add(alarmTemp0);

			// 物理报警信息只保存一次，病人类型为：3
			Alarm alarmNIBPCON = new Alarm();
			alarmNIBPCON.setAlarm_type("NIBP主机未连接");
			alarmNIBPCON.setBolt_lock("1");
			alarmNIBPCON.setPatient_Type("3");
			alarmNIBPCON.setWhether("0");
			alarmNIBPCON.setPriority("2");

			Alarm alarmNIBPBATTERY = new Alarm();
			alarmNIBPBATTERY.setAlarm_type("NIBP传感器电量低");
			alarmNIBPBATTERY.setBolt_lock("1");
			alarmNIBPBATTERY.setPatient_Type("3");
			alarmNIBPBATTERY.setWhether("0");
			alarmNIBPBATTERY.setPriority("2");

			Alarm alarmEGC = new Alarm();
			alarmEGC.setAlarm_type("ECG导联脱落");
			alarmEGC.setBolt_lock("1");
			alarmEGC.setPatient_Type("3");
			alarmEGC.setWhether("0");
			alarmEGC.setPriority("2");

			Alarm alarmEGCYY = new Alarm();
			alarmEGCYY.setAlarm_type("ECGYY导联脱落");
			alarmEGCYY.setBolt_lock("1");
			alarmEGCYY.setPatient_Type("3");
			alarmEGCYY.setWhether("0");
			alarmEGCYY.setPriority("2");

			Alarm alarmEGCCON = new Alarm();
			alarmEGCCON.setAlarm_type("ECG主机未连接");
			alarmEGCCON.setBolt_lock("1");
			alarmEGCCON.setPatient_Type("3");
			alarmEGCCON.setWhether("0");
			alarmEGCCON.setPriority("2");

			Alarm alarmEGCBATTERY = new Alarm();
			alarmEGCBATTERY.setAlarm_type("ECG传感器电量低");
			alarmEGCBATTERY.setBolt_lock("1");
			alarmEGCBATTERY.setPatient_Type("3");
			alarmEGCBATTERY.setWhether("0");
			alarmEGCBATTERY.setPriority("2");

			Alarm alarmSpO2CON = new Alarm();
			alarmSpO2CON.setAlarm_type("SpO2 主机未连接");
			alarmSpO2CON.setBolt_lock("1");
			alarmSpO2CON.setPatient_Type("3");
			alarmSpO2CON.setWhether("0");
			alarmSpO2CON.setPriority("2");

			Alarm alarmSpO2BATTERY = new Alarm();
			alarmSpO2BATTERY.setAlarm_type("SpO2传感器电量过低");
			alarmSpO2BATTERY.setBolt_lock("1");
			alarmSpO2BATTERY.setPatient_Type("3");
			alarmSpO2BATTERY.setWhether("0");
			alarmSpO2BATTERY.setPriority("2");

			Alarm alarmTempCON = new Alarm();
			alarmTempCON.setAlarm_type("Temp主机未连接");
			alarmTempCON.setBolt_lock("1");
			alarmTempCON.setPatient_Type("3");
			alarmTempCON.setWhether("0");
			alarmTempCON.setPriority("2");

			Alarm alarmTempBATTERY = new Alarm();
			alarmTempBATTERY.setAlarm_type("Temp传感器低电量");
			alarmTempBATTERY.setBolt_lock("1");
			alarmTempBATTERY.setPatient_Type("3");
			alarmTempBATTERY.setWhether("0");
			alarmTempBATTERY.setPriority("2");

			// 物理
			list.add(alarmNIBPCON);
			list.add(alarmNIBPBATTERY);
			list.add(alarmEGC);
			list.add(alarmEGCYY);
			list.add(alarmEGCCON);
			list.add(alarmEGCBATTERY);
			list.add(alarmSpO2CON);
			list.add(alarmSpO2BATTERY);
			list.add(alarmTempCON);
			list.add(alarmTempBATTERY);
			alarmService.saveEntityList(list);
		}
	}
}
