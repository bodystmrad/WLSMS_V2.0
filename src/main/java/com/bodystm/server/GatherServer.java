package com.bodystm.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bodystm.service.AlarmRecordService;
import com.bodystm.service.BedService;
import com.bodystm.service.PatientBedService;
import com.bodystm.service.PatientService;
import com.bodystm.service.TemperatureService;

/**
 * 閲囬泦鍣ㄧ殑鏈嶅姟绔�
 * 
 * @author ggeagle
 *
 */
public class GatherServer implements Runnable {
	/**
	 * 鏃ュ織璁板綍鍣�
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private TemperatureService temperatureService;
	private PatientService patientService;
	private PatientBedService patientBedUserService;
	private ServletConfig servletConfig;
	private BedService bedService;
	private AlarmRecordService alarmRecordService;
	

	
	/**
	 * 鏈嶅姟鍣ㄧ绋嬪簭
	 */
	private ServerSocket serverSocket;


	public TemperatureService getTemperatureService() {
		return temperatureService;
	}

	public void setTemperatureService(TemperatureService temperatureService) {
		this.temperatureService = temperatureService;
	}
	
	public BedService getBedService() {
		return bedService;
	}

	public void setBedService(BedService bedService) {
		this.bedService = bedService;
	}

	public AlarmRecordService getAlarmRecordService() {
		return alarmRecordService;
	}

	public void setAlarmRecordService(AlarmRecordService alarmRecordService) {
		this.alarmRecordService = alarmRecordService;
	}

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	public PatientBedService getPatientBedUserService() {
		return patientBedUserService;
	}

	public void setPatientBedUserService(PatientBedService patientBedUserService) {
		this.patientBedUserService = patientBedUserService;
	}

	public ServletConfig getServletConfig() {
		return servletConfig;
	}

	public void setServletConfig(ServletConfig servletConfig) {
		this.servletConfig = servletConfig;
	}

	private ExecutorService executorService;// 绾跨▼姹�
	private final int POOL_SIZE = 5000;// 鍗曚釜CPU绾跨▼姹犲ぇ灏�

	public GatherServer(int port) throws IOException {
		this.serverSocket = new ServerSocket(port);
		// Runtime鐨刟vailableProcessor()鏂规硶杩斿洖褰撳墠绯荤粺鐨凜PU鏁扮洰.
		executorService = Executors.newFixedThreadPool(Runtime.getRuntime()
				.availableProcessors() * POOL_SIZE);
		logger.info("服务器启动");
	}

	@Override
	public void run() {
		while (true) {
			Socket socket = null;
			try {
				// 鎺ユ敹瀹㈡埛杩炴帴,鍙瀹㈡埛杩涜浜嗚繛鎺�灏变細瑙﹀彂accept();浠庤�寤虹珛杩炴帴
				socket = serverSocket.accept();
				executorService.execute(new DataGather(socket,this.servletConfig,
						this.temperatureService,this.patientService,this.patientBedUserService,this.bedService,this.alarmRecordService));
			} catch (Exception e) {
				logger.error("接收客户端连接时，发生异常！", e);
			}
		}
	}
}
