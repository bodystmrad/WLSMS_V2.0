package com.bodystm.server;

import org.apache.commons.lang.StringUtils;

public class ByteUtil {
	/**
	 * 灏嗗瓧鑺備俊鎭浆鎹㈡垚涓烘暟缁�
	 * 
	 * @param array
	 * @return
	 */
	public static String convertByteToString(byte... array) {
		String result = StringUtils.EMPTY;
		if (array != null) {
			StringBuilder builder = new StringBuilder();
			String value = null;
			for (byte b : array) {
				value = Integer.toHexString(b);
				if (value.length() > 2) {
					builder.append(value.substring(value.length() - 2));
				} else if (value.length() < 2) {
					builder.append(0).append(value);
				} else {
					builder.append(value);
				}
			}
			result = builder.toString().toUpperCase();
		}
		return result;
	}

	/**
	 * 灏嗗瓧鑺備俊鎭浆鎹㈡垚涓烘暟缁�
	 * 
	 * @param array
	 * @return
	 */
	public static byte[] convertStringToByte(String value) {
		byte[] result = null;
		if (StringUtils.isNotBlank(value)) {
			result = new byte[value.length() / 2];
			int number;
			for (int i = 0; i < value.length(); i += 2) {
				number = Integer.decode("0X" + value.subSequence(i, i + 2));
				result[i % 2] = (byte) number;
			}
		}
		return result;
	}
}