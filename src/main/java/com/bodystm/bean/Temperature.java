package com.bodystm.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.mimosa.common.entity.BaseEntity;

/**
 *	床位信息
 * 
 * @author rxz
 *
 */
@Table(name = "T_Temperature")
@Entity
public class Temperature extends BaseEntity {
	//private String equipNo;
	private String concentratorNo;
	/**
	 * 娴嬮噺鏃堕棿
	 */
	private Date measureTime;
	/**
	 * 娴嬮噺娓╁害1
	 */
	private float measureNum;
	private float measureNum2;
	/**
	 * 温度类型，1表示温度1,2表示温度2
	 */
	/*private float tType=1;*/
	/**
	 * 探头体温（未经算法处理过的）
	 */
	//private float measureNumThree;
	/**
	 * 电池电量
	 */
	private int powerNum;
	
	
	public String getConcentratorNo() {
		return concentratorNo;
	}
	public void setConcentratorNo(String concentratorNo) {
		this.concentratorNo = concentratorNo;
	}
	public Date getMeasureTime() {
		return measureTime;
	}
	public void setMeasureTime(Date measureTime) {
		this.measureTime = measureTime;
	}
	public float getMeasureNum() {
		return measureNum;
	}
	public void setMeasureNum(float measureNum) {
		this.measureNum = measureNum;
	}
	/*public float gettType() {
		return tType;
	}
	public void settType(float tType) {
		this.tType = tType;
	}*/
	public int getPowerNum() {
		return powerNum;
	}
	public void setPowerNum(int powerNum) {
		this.powerNum = powerNum;
	}
	public float getMeasureNum2() {
		return measureNum2;
	}
	public void setMeasureNum2(float measureNum2) {
		this.measureNum2 = measureNum2;
	}	
}