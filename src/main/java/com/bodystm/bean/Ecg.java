package com.bodystm.bean;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.bodystm.bean.datadeal.EcgAnalysis;
import com.mimosa.common.entity.BaseEntity;
@Table(name = "T_Ecg")
@Entity
public class Ecg extends BaseEntity{
	private String concentratorNo;
	private int resp;//呼吸率，单位RPM
	private int hr;//心率 BPM
	@Transient
	private EcgAnalysis ecgAnalysis=null;
	public String getConcentratorNo() {
		return concentratorNo;
	}
	public void setConcentratorNo(String concentratorNo) {
		this.concentratorNo = concentratorNo;
	}
	public int getResp() {
		return resp;
	}
	public void setResp(int resp) {
		this.resp = resp;
	}
	public int getHr() {
		return hr;
	}
	public void setHr(int hr) {
		this.hr = hr;
	}
	public EcgAnalysis getEcgAnalysis() {
		return ecgAnalysis;
	}
	public void setEcgAnalysis(EcgAnalysis ecgAnalysis) {
		this.ecgAnalysis = ecgAnalysis;
	}
}
