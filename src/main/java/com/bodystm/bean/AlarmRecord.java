package com.bodystm.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.mimosa.common.entity.BaseEntity;

@Table(name = "T_AlarmRecord")
@Entity
public class AlarmRecord extends BaseEntity{

	//病人id
	private String Patient_Id;
	
	//事件类型
	private String Alarm_Type;
	
	//报警详情
	private String Alarm_Details;
	
	//报警参数
	private String Alarm_Parameter;

	public String getAlarm_Parameter() {
		return Alarm_Parameter;
	}

	public void setAlarm_Parameter(String alarm_Parameter) {
		Alarm_Parameter = alarm_Parameter;
	}

	public String getPatient_Id() {
		return Patient_Id;
	}

	public void setPatient_Id(String patient_Id) {
		Patient_Id = patient_Id;
	}

	public String getAlarm_Type() {
		return Alarm_Type;
	}

	public void setAlarm_Type(String alarm_Type) {
		Alarm_Type = alarm_Type;
	}

	public String getAlarm_Details() {
		return Alarm_Details;
	}

	public void setAlarm_Details(String alarm_Details) {
		Alarm_Details = alarm_Details;
	}
	
}
