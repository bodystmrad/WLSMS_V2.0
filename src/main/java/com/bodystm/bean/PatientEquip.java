package com.bodystm.bean;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mimosa.common.entity.BaseEntity;

/**
 * 病人与
 * @author rxz
 *
 */
@Table(name = "T_Patient_Equip")
@Entity
public class PatientEquip extends BaseEntity{
	/**
	 * 病人
	 */
//	@ManyToOne(cascade = { javax.persistence.CascadeType.ALL })
//	@JoinColumn(name = "tPatient_ID")
//	private Patient patient;
	private String patientID;
	/**
	 * 设备号
	 */
	private String equipNo;
	/*public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}*/
	public String getEquipNo() {
		return equipNo;
	}
	public void setEquipNo(String equipNo) {
		this.equipNo = equipNo;
	}
	public String getPatientID() {
		return patientID;
	}
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	
}
