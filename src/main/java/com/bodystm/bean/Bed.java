package com.bodystm.bean;

import java.text.NumberFormat;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.common.primitives.UnsignedBytes;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.system.user.TUser;

/**
 * 鐥呬汉淇℃伅
 * 
 * @author ggeagle
 *
 */
@Table(name = "T_Bed")
@Entity
public class Bed extends BaseEntity {
	/**
	 * 床位名称
	 */
	private String name;
	/**
	 * 状态，0表示为空，1表示有病人
	 */
	private String status;
	/**
	 * 床位绑定的集中器的mac地址
	 */
	private String concentratorNo;
	/**
	 * 床位是否已经被删除
	 * 0代表已经删除，为历史床位
	 * 1代表还未删除，为当前床位
	 */
	private int usable=1;
	
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private TUser user;
	/**
	 * 床位对应病人
	 */
	//@Transient
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private Patient patient;
	//private String patientId;
	//2018年1月5日00:45:46  当前血氧、温度等数据是通过float类型字段来记录还是通过manytoone来绑定呢还是通过temperatureDevice来记录？
	/**
	 * 温度
	 */
	@Transient
//	@ManyToOne
	private Temperature temperature;
//	@ManyToOne
	@Transient
	private Oximetry oximetry;
//	@ManyToOne
	@Transient
	private BloodPressure bloodPressure;
//	@ManyToOne
	@Transient
	private Ecg ecg;
	/**
	 * 设备状态（温度）
	 * 0表示断开连接，1表示连接正常
	 */
	@Transient
	private long temUpdateMs=-1;
	/**
	 * 设备状态（血氧）
	 * 0表示断开连接，1表示连接正常
	 */
	@Transient
	private long oxiUpdateMs=-1;
	/**
	 * 设备状态（血压）
	 * 0表示断开连接，1表示连接正常
	 */
	@Transient
	private long bloUpdateMs=-1;
	/**
	 * 设备状态（心电）
	 * 0表示断开连接，1表示连接正常
	 */
	@Transient
	private long ecgUpdateMs=-1;
	@Transient
	private long respUpdateMs=-1;
	//报警相关
//	private 
	public TUser getUser() {
		return user;
	}

	//
	public void setUser(TUser user) {
		this.user = user;
	}

	

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getTiwenValue() {
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);
		return nf.format(this.temperature);
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getConcentratorNo() {
		return concentratorNo;
	}

	public void setConcentratorNo(String concentratorNo) {
		this.concentratorNo = concentratorNo;
	}

	public int getUsable() {
		return usable;
	}

	public void setUsable(int usable) {
		this.usable = usable;
	}

	public Temperature getTemperature() {
		return temperature;
	}

	public void setTemperature(Temperature temperature) {
		this.temperature = temperature;
	}

	public Oximetry getOximetry() {
		return oximetry;
	}

	public void setOximetry(Oximetry oximetry) {
		this.oximetry = oximetry;
	}

	public BloodPressure getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(BloodPressure bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public Ecg getEcg() {
		return ecg;
	}

	public void setEcg(Ecg ecg) {
		this.ecg = ecg;
	}

	public long getTemUpdateMs() {
		return temUpdateMs;
	}

	public void setTemUpdateMs(long temUpdateMs) {
		this.temUpdateMs = temUpdateMs;
	}

	public long getOxiUpdateMs() {
		return oxiUpdateMs;
	}

	public void setOxiUpdateMs(long oxiUpdateMs) {
		this.oxiUpdateMs = oxiUpdateMs;
	}

	public long getBloUpdateMs() {
		return bloUpdateMs;
	}

	public void setBloUpdateMs(long bloUpdateMs) {
		this.bloUpdateMs = bloUpdateMs;
	}

	public long getEcgUpdateMs() {
		return ecgUpdateMs;
	}

	public void setEcgUpdateMs(long ecgUpdateMs) {
		this.ecgUpdateMs = ecgUpdateMs;
	}

	public long getRespUpdateMs() {
		return respUpdateMs;
	}

	public void setRespUpdateMs(long respUpdateMs) {
		this.respUpdateMs = respUpdateMs;
	}

}