package com.bodystm.bean.datadeal;

public enum BodystmProduct {
	BodystmSpO2,//0
	BodystmECG,//1
	BodystmNIBP_PWV,//2
	BodystmResp,//3
	BodystmTemp1,//4
	BodystmTemp2,//5
	BodystmNIBP_VAL,//6
	BodystmIBP,
	BodystmCapnograph,
	BodystmEEG,
	BOdystmDEVMax;
	
}
