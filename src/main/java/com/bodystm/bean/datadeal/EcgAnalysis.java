package com.bodystm.bean.datadeal;

public class EcgAnalysis {
	public static int SAMPLE_RATE=250;//采样率
//	private int
	public static final int ECG_POINTS_PER_PACK=12;//每包数据中包含12个时间点的数据，每个时间点三个通道的数据
	public static enum EcgChannel{
		标1通道,
		标2通道,
		胸导通道;
		public static EcgChannel valueOf(int ordinal) {
	        if (ordinal < 0 || ordinal >= values().length) {
	            throw new IndexOutOfBoundsException("Invalid ordinal");
	        }
	        return values()[ordinal];
	    }
	}
	public EcgChannel channelType=EcgChannel.标1通道;
	public static volatile int npTargetSampleRate=96;
	public String getStrEcgChannel(){
		if (channelType.ordinal()==0) {
			return "Ⅰ";//ⅢⅣ
		}else if (channelType.ordinal()==1) {
			return "Ⅱ";
		}else {
			return "Ⅴ";
		}
	}
	//不同芯片的适配相关
	public static enum EcgChipType{
		chip_1293,
		chip_1294
	}
	public EcgChipType chipType=EcgChipType.chip_1294;
	private int simplifyArg=-1;
	/**
	 * 获取心电数据经过低通中值滤波后需要除以的参数
	 * @return
	 */
	public int getSimplifyArg(){
		if (simplifyArg==-1) {
			if (chipType.ordinal()==0) {
				simplifyArg= 8000;
			}else if (chipType.ordinal()==1) {
				simplifyArg= 21000;
			}
		}
		return simplifyArg;
	}
}

