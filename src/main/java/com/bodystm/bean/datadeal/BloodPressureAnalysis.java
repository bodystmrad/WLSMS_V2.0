package com.bodystm.bean.datadeal;

import java.util.ArrayList;
import java.util.List;

public class BloodPressureAnalysis {
	public static final int BP_POINTS_PER_PACK=27;
	public static int sampleRate=500;//500hz采样，5秒数据长度；
//	public static final int NIBP_PWV_CalcBufferLen=2500;
	public static volatile int npTargetSampleRate=96;
	public double maxNum=Double.MIN_VALUE;
	public double minNum=Double.MAX_VALUE;
	//周期内更新极值，防止算法初期脏数据的影响造成极值过大
		//private long maxUpdateTime=0;
		List<Double> dataIn3s=new ArrayList<Double>();
		 List<Long> ms4DataIn3s=new ArrayList<Long>();
	public void extremeStastics(double newValue){
		Long curMs=System.currentTimeMillis();	
		for(int i=0;i<ms4DataIn3s.size();i++){
			if((curMs-ms4DataIn3s.get(i))/1000>3){
				ms4DataIn3s.remove(i);
				dataIn3s.remove(i);
				i--;
			}else{
				break;
			}
		}
		ms4DataIn3s.add(curMs);
		dataIn3s.add(newValue);
		
		double max=0;
		double min=0;
		for(int i=0;i<dataIn3s.size();i++){
			if(i==0){
				max=dataIn3s.get(i);
				min=dataIn3s.get(i);
			}else{
				max=dataIn3s.get(i)>max?dataIn3s.get(i):max;
				min=dataIn3s.get(i)<min?dataIn3s.get(i):min;
			}
		}
		if (max!=0||min!=0) {
			maxNum=max;
			minNum=min;
		}
		/*if (newValue>maxNum) {
			maxNum=newValue;
		}else if (newValue<minNum) {
			minNum=newValue;
		}*/
	}
}
