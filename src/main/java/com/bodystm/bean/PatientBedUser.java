package com.bodystm.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.system.user.TUser;

/**
 * 病人床位关系表
 * 
 * @author rxz
 *
 */
@Table(name = "T_Patient_Bed_User")
@Entity
public class PatientBedUser extends BaseEntity {
	/*
	 * 病人
	 */
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	@JoinColumn(name = "tPatient_ID")
	private Patient patient;
	/*
	 * 病人床位
	 */
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private Bed bed;
	// 科室
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private TUser tUser;
	/**
	 * 床位对应的集中器的mac地址，以方便查看病人历史数据
	 */
	private String mac;
	/**
	 * 病人的床位状态：0表示历史床位，1表示当前床位
	 */
	private Integer status;
	/**
	 * 病人的科室状态：0表示历史科室，1表示当前科室
	 */
	private Integer statusOfDepartment;
	
	/*
	 * 病床当前病人所携带的温度贴片的工作状态：0表示中断（出现故障），1表示该设备上传数据正常
	 */
	private Integer statusOfDevice;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Bed getBed() {
		return bed;
	}

	public void setBed(Bed bed) {
		this.bed = bed;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public TUser gettUser() {
		return tUser;
	}

	public void settUser(TUser tUser) {
		this.tUser = tUser;
	}

	public Integer getStatusOfDepartment() {
		return statusOfDepartment;
	}

	public void setStatusOfDepartment(Integer statusOfDepartment) {
		this.statusOfDepartment = statusOfDepartment;
	}

	public Integer getStatusOfDevice() {
		return statusOfDevice;
	}

	public void setStatusOfDevice(Integer statusOfDevice) {
		this.statusOfDevice = statusOfDevice;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

}