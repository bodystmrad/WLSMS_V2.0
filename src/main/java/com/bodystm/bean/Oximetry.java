package com.bodystm.bean;

import java.beans.Transient;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.mimosa.common.entity.BaseEntity;

/**
 * 血压数据类
 * 
 * @author rxz
 *
 */
@Table(name = "T_Oximetry")
@Entity
public class Oximetry extends BaseEntity {

	/**
	 * 设备号
	 */
	//private String equipNo;

	/**
	 * 测量时间
	 */
	private Date measureTime;
	/**
	 * 血氧饱和度Spo2（百分数）
	 */
	private int Spo2;
	/**
	 *灌注指数（PI）
	 */
	private int PI;
	/**
	 * 电量
	 */
	private int powerNum;
	/**
	 * 脉搏、脉率（BPM）
	 */
	private int pulserate;
	
	/**
	 * 信号强度
	 */
	private int rssi;
	private String concentratorMac;
	//@Transient
	//private long msOfLastUpdate=0;
	
	public Date getMeasureTime() {
		return measureTime;
	}

	public void setMeasureTime(Date measureTime) {
		this.measureTime = measureTime;
	}

	public int getSpo2() {
		return Spo2;
	}

	public void setSpo2(int spo2) {
		Spo2 = spo2;
	}

	public int getPI() {
		return PI;
	}

	public void setPI(int pI) {
		PI = pI;
	}

	public int getPowerNum() {
		return powerNum;
	}

	public void setPowerNum(int powerNum) {
		this.powerNum = powerNum;
	}

	public int getPulserate() {
		return pulserate;
	}

	public void setPulserate(int pulserate) {
		this.pulserate = pulserate;
	}

	public int getRssi() {
		return rssi;
	}

	public void setRssi(int rssi) {
		this.rssi = rssi;
	}

	public String getConcentratorMac() {
		return concentratorMac;
	}

	public void setConcentratorMac(String concentratorMac) {
		this.concentratorMac = concentratorMac;
	}

}