package com.bodystm.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.system.user.TUser;

/**
 * 病人床位关系表
 * 
 * @author rxz
 *
 */
@Table(name = "T_Concentrator")
@Entity
public class Concentrator extends BaseEntity {
	private String bedName;

	private String concentratorNo;
	
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private TUser user;
	/**
	 * 是否正常运行,0表示异常，1表示正常
	 */
	private int status;
/*	*//**
	 * 是否有信号（有数据上传）
	 *//*
	private boolean signaled;

	
	 * 是否弹出警示，集中器已经失去连接超过一段时间
	 
	private boolean warningOrNot=true;*/

	public String getConcentratorNo() {
		return concentratorNo;
	}

	public void setConcentratorNo(String concentratorNo) {
		this.concentratorNo = concentratorNo;
	}

	public String getBedName() {
		return bedName;
	}

	public void setBedName(String bedName) {
		this.bedName = bedName;
	}
	public TUser getUser() {
		return user;
	}

	public void setUser(TUser user) {
		this.user = user;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}