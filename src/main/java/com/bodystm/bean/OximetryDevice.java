package com.bodystm.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bodystm.algorithm.MedFilter;

public  class OximetryDevice {
	/**
	 * 设备的mac地址
	 */
	private String strMac;
	/**
	 * 该设备最后一次保存数据的时间
	 */
	private Date lastSavTime;
	/**
	 * 血氧饱和度Spo2（百分数）
	 */
	private int Spo2;
	/**
	 * 灌注指数（PI）
	 */
	private int PI;
	/**
	 * 脉搏
	 */
	private int pulserate;
	/**
	 * 警报类型（0代表正常不报警，1代表低温报警）
	 */
	int warningType=0;
	/**
	 * 是否报警，当血氧恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	boolean warningOrNot=true;
	/**
	 * 当失去连接，持续一段时间不上数时是否报警，当温度恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	boolean warningOrNot4NoSign=true;
	/**
	 * 该设备第一次保存数据的时间，用于判断是否超过50小时，超过50小时的数据则不再存储和显示，直接忽略
	 */
	private Date firstSavTime;
	/**
	 * 没电时是否提醒用户
	 */
	Boolean warningOrNot4NoPower;
	/**
	 * 血氧探头脱落提醒相关
	 */
	private boolean ifProbeFallOff=false;//是否探头脱落
	private boolean ifWarn4ProbeFallOff=true;//是否对探头脱落弹出提醒
	/**
	 * 实时血氧波形图相关
	 */
	List<Date> date4PointsIn2Min=new ArrayList<Date>();
	List<Integer> points=new ArrayList<Integer>();
	/**
	 * 该设备上次更新当前显示数据的时间
	 */
	private Date lastUpdateTime;
	//低通滤波参数数组
//	private float[] argfrb={0.0286f,0.1429f,0.3286f,0.3286f,0.1429f,0.0286f};
	private float[] argfrb={0.01431394f,0.03029204f,0.07227989f,0.12451932f,0.16697794f,0.18323374f,0.16697794f,0.12451932f,0.07227989f,0.03029204f,0.01431394f};
//	private int[] datatemp=new int[6];
	private int[] datatemp=new int[11];
	private MedFilter med=new MedFilter();
	public String getStrMac() {
		return strMac;
	}
	public void setStrMac(String strMac) {
		this.strMac = strMac;
	}
	public Date getLastSavTime() {
		return lastSavTime;
	}
	public void setLastSavTime(Date lastSavTime) {
		this.lastSavTime = lastSavTime;
	}
	public int getSpo2() {
		return Spo2;
	}
	public void setSpo2(int spo2) {
		Spo2 = spo2;
	}
	public int getPI() {
		return PI;
	}
	public void setPI(int pI) {
		PI = pI;
	}
	public int getPulserate() {
		return pulserate;
	}
	public void setPulserate(int pulserate) {
		this.pulserate = pulserate;
	}
	public boolean isWarningOrNot() {
		return warningOrNot;
	}
	public void setWarningOrNot(boolean warningOrNot) {
		this.warningOrNot = warningOrNot;
	}
	public Date getFirstSavTime() {
		return firstSavTime;
	}
	public void setFirstSavTime(Date firstSavTime) {
		this.firstSavTime = firstSavTime;
	}
	public Boolean getWarningOrNot4NoPower() {
		return warningOrNot4NoPower;
	}
	public void setWarningOrNot4NoPower(Boolean warningOrNot4NoPower) {
		this.warningOrNot4NoPower = warningOrNot4NoPower;
	}
	public boolean isWarningOrNot4NoSign() {
		return warningOrNot4NoSign;
	}
	public void setWarningOrNot4NoSign(boolean warningOrNot4NoSign) {
		this.warningOrNot4NoSign = warningOrNot4NoSign;
	}
	public int getWarningType() {
		return warningType;
	}
	public void setWarningType(int warningType) {
		this.warningType = warningType;
	}
	public List<Date> getDate4PointsIn2Min() {
		return date4PointsIn2Min;
	}
	public void setDate4PointsIn2Min(List<Date> date4PointsIn2Min) {
		this.date4PointsIn2Min = date4PointsIn2Min;
	}
	public List<Integer> getPoints() {
		return points;
	}
	public void setPoints(List<Integer> points) {
		this.points = points;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public boolean isIfProbeFallOff() {
		return ifProbeFallOff;
	}
	public void setIfProbeFallOff(boolean ifProbeFallOff) {
		this.ifProbeFallOff = ifProbeFallOff;
	}
	public boolean isIfWarn4ProbeFallOff() {
		return ifWarn4ProbeFallOff;
	}
	public void setIfWarn4ProbeFallOff(boolean ifWarn4ProbeFallOff) {
		this.ifWarn4ProbeFallOff = ifWarn4ProbeFallOff;
	}
	public int[] getDatatemp() {
		return datatemp;
	}
	public void setDatatemp(int[] datatemp) {
		this.datatemp = datatemp;
	}
	public MedFilter getMed() {
		return med;
	}
	public void setMed(MedFilter med) {
		this.med = med;
	}
	public float[] getArgfrb() {
		return argfrb;
	}

}
