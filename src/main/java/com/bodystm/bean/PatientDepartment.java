package com.bodystm.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.system.user.TUser;

/**
 * 闁汇儱鎳嶅Ч澶嬬┍閳╁啩绱�
 * 
 * @author ggeagle
 *
 */
//@Table(name = "T_Patient_Department")
//@Entity
public class PatientDepartment extends BaseEntity {
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public TUser gettUser() {
		return tUser;
	}
	public void settUser(TUser tUser) {
		this.tUser = tUser;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	/*
	 *病人
	 */
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	@JoinColumn(name = "tPatient_ID")
	private Patient patient;
	//科室
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private TUser tUser;
	/**
	 * 病人的床位状态：0表示历史床位，1表示当前床位
	 */
	private Integer status;

}