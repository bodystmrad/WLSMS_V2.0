package com.bodystm.bean;

import java.util.Date;

public  class BloodPressureDevice {
	/**
	 * 设备的mac地址
	 */
	private String strMac;
	/**
	 * 该设备最后一次保存数据的时间
	 */
	private Date lastSavTime;
	/**
	 * 设备当前高压，即最后一次上传的高压
	 */
	private int highPressure=0;
	/**
	 * 设备当前低压，即最后一次上传的低压
	 */
	private int lowPressure=0;
	/**
	 * 脉搏
	 */
	private int pulserate;
	/**
	 * 警报类型（0代表正常不报警，1代表低温报警）
	 */
	int warningType=0;
	/**
	 * 是否报警，当血氧恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	boolean warningOrNot=true;
	/**
	 * 当失去连接，持续一段时间不上数时是否报警，当温度恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	boolean warningOrNot4NoSign=true;
	/**
	 * 该设备第一次保存数据的时间，用于判断是否超过50小时，超过50小时的数据则不再存储和显示，直接忽略
	 */
	private Date firstSavTime;
	/**
	 * 没电时是否提醒用户
	 */
	Boolean warningOrNot4NoPower;
	/**
	 * 设备当前压力值
	 */
	private int curPressure=0;
	public String getStrMac() {
		return strMac;
	}
	public void setStrMac(String strMac) {
		this.strMac = strMac;
	}
	public Date getLastSavTime() {
		return lastSavTime;
	}
	public void setLastSavTime(Date lastSavTime) {
		this.lastSavTime = lastSavTime;
	}
	public int getHighPressure() {
		return highPressure;
	}
	public void setHighPressure(int highPressure) {
		this.highPressure = highPressure;
	}
	public int getLowPressure() {
		return lowPressure;
	}
	public void setLowPressure(int lowPressure) {
		this.lowPressure = lowPressure;
	}
	public int getPulserate() {
		return pulserate;
	}
	public void setPulserate(int pulserate) {
		this.pulserate = pulserate;
	}
	public int getWarningType() {
		return warningType;
	}
	public void setWarningType(int warningType) {
		this.warningType = warningType;
	}
	public boolean isWarningOrNot() {
		return warningOrNot;
	}
	public void setWarningOrNot(boolean warningOrNot) {
		this.warningOrNot = warningOrNot;
	}
	public boolean isWarningOrNot4NoSign() {
		return warningOrNot4NoSign;
	}
	public void setWarningOrNot4NoSign(boolean warningOrNot4NoSign) {
		this.warningOrNot4NoSign = warningOrNot4NoSign;
	}
	public Date getFirstSavTime() {
		return firstSavTime;
	}
	public void setFirstSavTime(Date firstSavTime) {
		this.firstSavTime = firstSavTime;
	}
	public Boolean getWarningOrNot4NoPower() {
		return warningOrNot4NoPower;
	}
	public void setWarningOrNot4NoPower(Boolean warningOrNot4NoPower) {
		this.warningOrNot4NoPower = warningOrNot4NoPower;
	}
	public int getCurPressure() {
		return curPressure;
	}
	public void setCurPressure(int curPressure) {
		this.curPressure = curPressure;
	}
}
