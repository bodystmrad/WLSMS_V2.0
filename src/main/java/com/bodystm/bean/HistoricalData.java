package com.bodystm.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.mimosa.common.entity.BaseEntity;
@Table(name = "T_HistoricalData")
@Entity
public class HistoricalData extends BaseEntity{
	
	//病人id
	private String Patient_id;
	//床位id
	private String Bed_id;
	//用户id
	private String User_id;
	//心率
	private int Hr;
	//呼吸
	private int Rr;
	//血氧SpO2
	private int SpO2;
	//血氧脉率PR
	private int PR;
	//灌度指数PI
	private float PI;
	//收缩压sbp 
	private int Sbp;
	//舒张压dbp
	private int Dbp;
	//血压脉率BP4PR
	private int Abp;
	//温度1
	private float Temperature1;
	//温度2
	private float Temperature2;

	
	public String getPatient_id() {
		return Patient_id;
	}
	public void setPatient_id(String patient_id) {
		Patient_id = patient_id;
	}

	public String getBed_id() {
		return Bed_id;
	}
	public void setBed_id(String bed_id) {
		Bed_id = bed_id;
	}

	public String getUser_id() {
		return User_id;
	}
	public void setUser_id(String user_id) {
		User_id = user_id;
	}
	public int getHr() {
		return Hr;
	}
	public void setHr(int hr) {
		Hr = hr;
	}
	public int getRr() {
		return Rr;
	}
	public void setRr(int rr) {
		Rr = rr;
	}
	public int getSpO2() {
		return SpO2;
	}
	public void setSpO2(int spO2) {
		SpO2 = spO2;
	}
	public int getPR() {
		return PR;
	}
	public void setPR(int pR) {
		PR = pR;
	}
	public float getPI() {
		return PI;
	}
	public void setPI(float pI) {
		PI = pI;
	}
	public int getSbp() {
		return Sbp;
	}
	public void setSbp(int sbp) {
		Sbp = sbp;
	}
	public int getDbp() {
		return Dbp;
	}
	public void setDbp(int dbp) {
		Dbp = dbp;
	}
	public float getTemperature1() {
		return Temperature1;
	}
	public void setTemperature1(float temperature1) {
		Temperature1 = temperature1;
	}
	public float getTemperature2() {
		return Temperature2;
	}
	public void setTemperature2(float temperature2) {
		Temperature2 = temperature2;
	}
	public int getAbp() {
		return Abp;
	}
	public void setAbp(int abp) {
		Abp = abp;
	}
	
	
}
