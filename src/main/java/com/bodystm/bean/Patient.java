package com.bodystm.bean;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;

import com.mimosa.common.entity.BaseEntity;
import com.mimosa.common.system.user.TUser;

/**
 * 鐥呬汉淇℃伅
 * 
 * @author ggeagle
 *
 */
@Table(name = "T_Patient")
@Entity
public class Patient  extends BaseEntity{
	/**
	 * 住院号，跟人先对接使用
	 */
	private String admissionNumber;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 年龄
	 */
	private int age;
	/**
	 * 性别姓别
	 */
	private String sex;
	/**
	 * 身份证号码
	 */
	private String idnumber;
	
	/**
	 * 病人类型  0:成人 1：幼儿 2：新生儿
	 */
	private String patientType;
	
	/**
	 * 是否起搏 0:是 1：否
	 */
	private String pacing;

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getPatientType() {
		return patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public String getPacing() {
		return pacing;
	}

	public void setPacing(String pacing) {
		this.pacing = pacing;
	}

	/**
	 * 设备号
	 * 目前改为表示集中器mac地址 2018年1月4日23:02:44
	 */
	private String equipNo;
	/**
	 * 住院时间
	 */
	private Date startTime;

	private Date listenStartTime;
	private Date listenEndTime;

	@Column(nullable = true)
	private float rangeStart=35.5f;
	@Column(nullable = true)
	private float rangeEnd=38.5f;
	private float superHighDegree=39.5f;
//	@ManyToOne(cascade = { javax.persistence.CascadeType.ALL })
//	private Bed bed;
	private String bedId;
	//// // 科室
	@ManyToOne(cascade = { javax.persistence.CascadeType.REFRESH })
	private TUser user;

	

	@Transient
	private String bedIds;
	// 人员
	@SuppressWarnings("unchecked")
	@OneToMany(mappedBy = "patient")
	// @Cascade(value = { org.hibernate.annotations.CascadeType.ALL }), orphanRemoval = true
	private Set<PatientBedUser> bedUserList = Collections.EMPTY_SET;


	/**
	 * 出院时间
	 */
	private Date endTime;
	/**
	 * 此病人携带的设备最后接收存储数据的时间
	 */
	private Date lastSavTime;
	/**
	 * 记录用户临时修正温度数据时要加上的数（接收温度数据时加上这个数再存储）
	 */
	private float num4Correct=0;
	//报警相关
	/**
	 * 警报类型（0代表正常不报警，1代表低温报警，2代表高温报警，3代表超高温报警）
	 */
	private int warningType=0;
	/**
	 * 是否报警，当温度恢复正常时或者用户点击停止后将其设置回true的状态
	 */
	private boolean warningOrNot=true;
	//各项指标报警相关 2018年3月14日19:42:20
	private int spo2LowAlarmValue=90;
	private int spo2HighAlarmValue=100;
	private int hrLowAlarmValue=50;
	private int hrHighAlarmValue=120;
	

	public Set<PatientBedUser> getBedList() {
		return bedUserList;
	}

	public void setBedList(Set<PatientBedUser> bedList) {
		this.bedUserList = bedList;
	}

	public String getBedIds() {
		if (StringUtils.isBlank(bedIds)) {
			StringBuilder bedIds = new StringBuilder();
			for (PatientBedUser t : this.getBedList()) {
				if (bedIds.length() > 0) {
					bedIds.append(",");
				}
				bedIds.append(t.getBed().getId());
			}
			return bedIds.toString();
		} else {
			return bedIds;
		}
	}
	public TUser getUser() {
		return user;
	}

	public void setUser(TUser user) {
		this.user = user;
	}
	public void setBedIds(String bedIds) {
		this.bedIds = bedIds;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEquipNo() {
		return equipNo;
	}

	public void setEquipNo(String equipNo) {
		this.equipNo = equipNo;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getListenStartTime() {
		return listenStartTime;
	}

	public void setListenStartTime(Date listenStartTime) {
		this.listenStartTime = listenStartTime;
	}

	public Date getListenEndTime() {
		return listenEndTime;
	}

	public void setListenEndTime(Date listenEndTime) {
		this.listenEndTime = listenEndTime;
	}

	public float getRangeStart() {
		return rangeStart;
	}

	public void setRangeStart(float rangeStart) {
		this.rangeStart = rangeStart;
	}

	public float getRangeEnd() {
		return rangeEnd;
	}

	public void setRangeEnd(float rangeEnd) {
		this.rangeEnd = rangeEnd;
	}

	public float getSuperHighDegree() {
		return superHighDegree;
	}

	public void setSuperHighDegree(float superHighDegree) {
		this.superHighDegree = superHighDegree;
	}

	public Set<PatientBedUser> getBedUserList() {
		return bedUserList;
	}

	public void setBedUserList(Set<PatientBedUser> bedUserList) {
		this.bedUserList = bedUserList;
	}

	public int getWarningType() {
		return warningType;
	}

	public void setWarningType(int warningType) {
		this.warningType = warningType;
	}

	public boolean isWarningOrNot() {
		return warningOrNot;
	}

	public void setWarningOrNot(boolean warningOrNot) {
		this.warningOrNot = warningOrNot;
	}

	/*public Bed getBed() {
		return bed;
	}

	public void setBed(Bed bed) {
		this.bed = bed;
	}*/

	public Date getLastSavTime() {
		return lastSavTime;
	}

	public void setLastSavTime(Date lastSavTime) {
		this.lastSavTime = lastSavTime;
	}

	public float getNum4Correct() {
		return num4Correct;
	}

	public void setNum4Correct(float num4Correct) {
		this.num4Correct = num4Correct;
	}

	public String getBedId() {
		return bedId;
	}

	public void setBedId(String bedId) {
		this.bedId = bedId;
	}

	public int getSpo2LowAlarmValue() {
		return spo2LowAlarmValue;
	}

	public void setSpo2LowAlarmValue(int spo2LowAlarmValue) {
		this.spo2LowAlarmValue = spo2LowAlarmValue;
	}

	public int getSpo2HighAlarmValue() {
		return spo2HighAlarmValue;
	}

	public void setSpo2HighAlarmValue(int spo2HighAlarmValue) {
		this.spo2HighAlarmValue = spo2HighAlarmValue;
	}

	public int getHrLowAlarmValue() {
		return hrLowAlarmValue;
	}

	public void setHrLowAlarmValue(int hrLowAlarmValue) {
		this.hrLowAlarmValue = hrLowAlarmValue;
	}

	public int getHrHighAlarmValue() {
		return hrHighAlarmValue;
	}

	public void setHrHighAlarmValue(int hrHighAlarmValue) {
		this.hrHighAlarmValue = hrHighAlarmValue;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}
}