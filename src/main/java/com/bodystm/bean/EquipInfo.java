package com.bodystm.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.mimosa.common.entity.BaseEntity;

/**
 * 设备信息 modle fy
 * 
 * @author Administrator
 *
 */
@Table(name = "T_Equip")
@Entity
public class EquipInfo extends BaseEntity {

	private String Equip_Type;

	private String Mac;

	private String Equip_Desc;

	private String Equip_State;
	
	private String Patient_Id;
	
	private String Bed_Id;
	
	private String User_Id;

	public String getEquip_Type() {
		return Equip_Type;
	}

	public void setEquip_Type(String equip_Type) {
		Equip_Type = equip_Type;
	}

	public String getMac() {
		return Mac;
	}

	public void setMac(String mac) {
		Mac = mac;
	}

	public String getEquip_Desc() {
		return Equip_Desc;
	}

	public void setEquip_Desc(String equip_Desc) {
		Equip_Desc = equip_Desc;
	}

	public String getEquip_State() {
		return Equip_State;
	}

	public void setEquip_State(String equip_State) {
		Equip_State = equip_State;
	}

	public String getPatient_Id() {
		return Patient_Id;
	}

	public void setPatient_Id(String patient_Id) {
		Patient_Id = patient_Id;
	}

	public String getBed_Id() {
		return Bed_Id;
	}

	public void setBed_Id(String bed_Id) {
		Bed_Id = bed_Id;
	}

	public String getUser_Id() {
		return User_Id;
	}

	public void setUser_Id(String user_Id) {
		User_Id = user_Id;
	}
}
