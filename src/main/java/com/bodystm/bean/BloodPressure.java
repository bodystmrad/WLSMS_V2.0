package com.bodystm.bean;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.mimosa.common.entity.BaseEntity;

/**
 * 血压数据类
 * 
 * @author rxz
 *
 */
@Table(name = "T_BloodPressure")
@Entity
public class BloodPressure extends BaseEntity {

	/**
	 * 设备号
	 */
	//private String equipNo;
	private String concentratorNo;
	/**
	 * 测量时间
	 */
	private Date measureTime;
	/**
	 * 高压
	 */
	private int highPressure;
	/**
	 *低压
	 */
	private int lowPressure;
	/**
	 * 电量
	 */
	private int powerNum;
	/**
	 * 脉搏
	 */
	private int pulserate;
	
	/**
	 * 信号强度
	 */
	private int rssi;
	/**
	 * 计数
	 */
	private int count;

	public Date getMeasureTime() {
		return measureTime;
	}
	public void setMeasureTime(Date measureTime) {
		this.measureTime = measureTime;
	}
	public int getHighPressure() {
		return highPressure;
	}
	public void setHighPressure(int highPressure) {
		this.highPressure = highPressure;
	}
	public int getLowPressure() {
		return lowPressure;
	}
	public void setLowPressure(int lowPressure) {
		this.lowPressure = lowPressure;
	}
	public int getPowerNum() {
		return powerNum;
	}
	public void setPowerNum(int powerNum) {
		this.powerNum = powerNum;
	}
	public int getPulserate() {
		return pulserate;
	}
	public void setPulserate(int pulserate) {
		this.pulserate = pulserate;
	}
	public int getRssi() {
		return rssi;
	}
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	public String getConcentratorNo() {
		return concentratorNo;
	}
	public void setConcentratorNo(String concentratorNo) {
		this.concentratorNo = concentratorNo;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	
}