package com.bodystm.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.mimosa.common.entity.BaseEntity;

/**
 *  报警功能 modle fy
 * @author Administrator
 *
 */
@Table(name = "T_Alarm")
@Entity
public class Alarm extends BaseEntity{

	//参数类型
	private String Alarm_type;
	//参数上限
	private String Upper_limit;
	//参数下限
	private String Lower_limit;
	//优先级
	private String Priority;
	//使能
	private String Whether;
	//栓锁
	private String Bolt_lock;
	//病人类型
	private String Patient_Type;
	
	public String getPatient_Type() {
		return Patient_Type;
	}
	public void setPatient_Type(String patient_Type) {
		Patient_Type = patient_Type;
	}
	public String getAlarm_type() {
		return Alarm_type;
	}
	public void setAlarm_type(String alarm_type) {
		Alarm_type = alarm_type;
	}
	public String getUpper_limit() {
		return Upper_limit;
	}
	public void setUpper_limit(String upper_limit) {
		Upper_limit = upper_limit;
	}
	public String getLower_limit() {
		return Lower_limit;
	}
	public void setLower_limit(String lower_limit) {
		Lower_limit = lower_limit;
	}
	public String getPriority() {
		return Priority;
	}
	public void setPriority(String priority) {
		Priority = priority;
	}
	public String getWhether() {
		return Whether;
	}
	public void setWhether(String whether) {
		Whether = whether;
	}
	public String getBolt_lock() {
		return Bolt_lock;
	}
	public void setBolt_lock(String bolt_lock) {
		Bolt_lock = bolt_lock;
	}
}
